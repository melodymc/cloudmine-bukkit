package ru.bird.pets.entity.armorstand.cart;

import net.minecraft.server.v1_16_R2.DataWatcher;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;
import ru.bird.main.API;
import ru.bird.main.logger.BaseLogger;
import ru.bird.pets.Pets;
import ru.bird.main.packets.WrapperPlayServerEntityTeleport;
import ru.bird.main.particles.ParticleEffect;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.MathUtils;

import java.util.UUID;

public class PetCart extends AbstractArmoredPet {

    private final String helmetURL;
    private ParticleEffect particleEffect;

    public PetCart(UUID id, int uniqueID, ParticleEffect particleEffect, String helmetURL) {
        super(id, uniqueID);
        this.helmetURL = helmetURL;
        this.particleEffect = particleEffect;
    }

    @Override
    protected DataWatcher build() {
        ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
        armor.setSmall(true);
        armor.setBasePlate(false);
        armor.setLeftLegPose(new EulerAngle(0.3, 0, 0));
        armor.setRightLegPose(new EulerAngle(0.3, 0, 0));
        armor.setLeftArmPose(new EulerAngle(-1.57, 0, 0));
        armor.setRightArmPose(new EulerAngle(-1.57, 0, 0));
        armor.setArms(true);
        armor.setVisible(false);
        DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
        armor.remove();
        return data;
    }

    @Override
    protected float getOffsetY() {
        return offset;
    }

    @Override
    public ItemStack getMainHand() {
        return null;
    }

    @Override
    public ItemStack getOffHand() {
        return null;
    }

    @Override
    public ItemStack getBoots() {
        return null;
    }

    @Override
    public ItemStack getLeggings() {
        return null;
    }

    @Override
    public ItemStack getChestplate() {
        return null;
    }

    @Override
    public ItemStack getHelmet() {
        return GameUtils.getHead(helmetURL);
    }

    @Override
    public double getAngle() {
        return angle;
    }

    private double	angle	= 0;
    private float	offset	= 0F;

    @Override
    public void onLivingUpdate() {
        offset = -0.4F + 0.2F * MathUtils.sin();
        angle += 10;
        angle = angle % 360;
        Location locTo = getOwner().getLocation();
        double localAngle = locTo.getYaw() + getAngle();
        double newX = locTo.getX() - 1D * Math.sin(Math.toRadians(localAngle));
        double newY = locTo.getBlockY() + getOffsetY();
        double newZ = locTo.getZ() + 1D * Math.cos(Math.toRadians(localAngle));
        this.petLocation = new Location(getOwner().getWorld(), newX, newY, newZ);

        WrapperPlayServerEntityTeleport position = new WrapperPlayServerEntityTeleport();
        position.setEntityID(getEntityID());
        position.setX(newX);
        position.setY(newY);
        position.setZ(newZ);
        position.setYaw(locTo.getYaw());
        position.setPitch(locTo.getPitch());
        position.broadcastPacket();

        float decadeAngle = (float) getAngle() - 110F;
        this.setHeadPose(0, decadeAngle, 0);
        this.sendWatcherUpdate();
        if(particleEffect != null) {
            particleEffect.sendToPlayers(Bukkit.getOnlinePlayers(), this.getPetLocation().clone().add(0, 1F, 0), 0.1F, 0, 0.1F, 1 / 100F, 0);
        }
    }
}