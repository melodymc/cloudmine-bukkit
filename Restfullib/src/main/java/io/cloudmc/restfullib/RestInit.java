package io.cloudmc.restfullib;

import io.cloudmc.restfullib.core.Permission;
import io.cloudmc.restfullib.network.Worker;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;

import javax.ws.rs.client.*;
import java.util.ArrayList;
public class RestInit {

    private static Client client;

    private static int version = 1;
    private static String serverAddress = "https://cloudmc.io/v" + version;

    public static void init(String v)
    {
        client = ClientBuilder.newClient().property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true);

        try
        {
            /* Query permissions */
            Worker clientWorker = new Worker(client, serverAddress, "", new ArrayList<>());

            String[] rawAccountPermissions = clientWorker.option("allows", String[].class);
            ArrayList<Permission> allowEntities = new ArrayList<>();
            for (String permissionName: rawAccountPermissions)
                allowEntities.add(Permission.valueOf(permissionName));


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
