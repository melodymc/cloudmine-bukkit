/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.ru.bird.jobs.command.CommandExecutor
 *  org.bukkit.ru.bird.jobs.command.PluginCommand
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.plugin.java.JavaPlugin
 *  ru.bird.main.game.GameSettings
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.buildbattle;

import com.wellfail.buildbattle.GameFactory;
import com.wellfail.buildbattle.Setup;
import java.util.ArrayList;
import org.bukkit.Location;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.game.GameSettings;
import ru.bird.main.utils.GameUtils;

public class Main
extends JavaPlugin {
    private static Main main;

    public static Main getInstance() {
        return main;
    }

    public void onLoad() {
        main = this;
    }

    public void onEnable() {
        this.saveDefaultConfig();
        GameSettings.isPlace = true;
        GameSettings.isBreak = true;
        GameSettings.inventory = true;
        GameSettings.physical = true;
        GameSettings.isSpectate = true;
        GameSettings.gameTime = 360;
        GameSettings.isPlace = true;
        GameSettings.isBreak = true;
        GameSettings.wboardname = "build battle";
        GameSettings.mapLocation = GameUtils.stringToLocation(this.getConfig().getString("Map"));
        GameSettings.game = this.getConfig().getString("Game");
        new GameFactory();
        if (this.getConfig().getBoolean("Setup")) {
            this.getCommand("bsetup").setExecutor((CommandExecutor)new Setup());
        }
    }
}

