package ru.bird.pets.entity.armorstand.planets;

import net.minecraft.server.v1_16_R2.DataWatcher;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;
import ru.bird.main.API;
import ru.bird.pets.Pets;
import ru.bird.main.packets.WrapperPlayServerEntityTeleport;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;

import java.util.UUID;

public class PetJungle extends AbstractArmoredPet {

	public PetJungle(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setArms(false);
		armor.setVisible(false);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getMainHand() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTg4YmNlNDk3Y2ZhNWY2MTE4MzlmNmRhMjFjOTVkMzRlM2U3MjNjMmNjNGMzYzMxOWI1NjI3NzNkMTIxNiJ9fX0=");
	}

	@Override
	public ItemStack getOffHand() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmFhZDZmZmFmNmZiOWE4ZWVhOGYzZGJlYTZkZGYzNDcyYTBhNTQ2YjVlMTk0YmQ1NWI0MzNiZDlkMTU4OTMwIn19fQ==");
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		return null;
	}

	@Override
	public ItemStack getChestplate() {
		return null;
	}

	@Override
	public ItemStack getHelmet() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNmRlZDg0MzkzYmVjMjUzY2NhNjZjMDQzMTY3MjZhODU4ZTU3NDcxMmI2ZmY5ZjI3MzY2OGRhOThlZmNjMjhmIn19fQ==");
	}

	@Override
	public double getAngle() {
		return angle;
	}

	private double angle = 0;

	@Override
	public void onLivingUpdate() {
		angle += 10;
		angle = angle % 360;
		Location locTo = getOwner().getLocation();
		double localAngle = locTo.getYaw() + getAngle();
		double newX = locTo.getX() - 1D * Math.sin(Math.toRadians(localAngle));
		double newY = locTo.getBlockY() + getOffsetY();
		double newZ = locTo.getZ() + 1D * Math.cos(Math.toRadians(localAngle));
		this.petLocation = new Location(getOwner().getWorld(), newX, newY, newZ);

		WrapperPlayServerEntityTeleport position = new WrapperPlayServerEntityTeleport();
		position.setEntityID(getEntityID());
		position.setX(newX);
		position.setY(newY);
		position.setZ(newZ);
		position.setYaw(locTo.getYaw());
		position.setPitch(locTo.getPitch());
		position.broadcastPacket();

		this.setHeadPose(0, (float) angle + 120F, 0);
		this.setLeftArmPose(0, (float) angle, 230);
		this.setRightArmPose(0, (float) angle, 130);
		this.sendWatcherUpdate();
	}

}
