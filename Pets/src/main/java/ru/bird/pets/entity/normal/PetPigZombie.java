package ru.bird.pets.entity.normal;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.PigZombie;

import net.minecraft.server.v1_16_R2.DataWatcher;
import ru.bird.pets.entity.AbstractStandardPet;

public class PetPigZombie extends AbstractStandardPet {

	public PetPigZombie(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		PigZombie entity = (PigZombie) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
//		entity.setBaby(true);
		DataWatcher data = ((CraftEntity) entity).getHandle().getDataWatcher();
		entity.remove();
		return data;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.ZOGLIN;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

}
