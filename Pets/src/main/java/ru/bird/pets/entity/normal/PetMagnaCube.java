package ru.bird.pets.entity.normal;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.MagmaCube;

import net.minecraft.server.v1_16_R2.DataWatcher;
import ru.bird.pets.entity.AbstractStandardPet;

public class PetMagnaCube extends AbstractStandardPet {

	public PetMagnaCube(UUID id, int uniqueID) {
		super(id, uniqueID);
	}


	@Override
	protected DataWatcher build() {
		MagmaCube entity = (MagmaCube) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		entity.setSize(1);
		DataWatcher data = ((CraftEntity) entity).getHandle().getDataWatcher();
		entity.remove();
		return data;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.MAGMA_CUBE;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}


}
