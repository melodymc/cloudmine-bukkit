package ru.bird.main.server;

import com.google.gson.Gson;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import ru.bird.main.API;
import ru.bird.main.game.GameState;
import ru.bird.main.logger.BaseLogger;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

public class ManagerRabbitMQ {

//    private final String CONFIG_PATH = "/rabbitMQ.yml";
//    private YamlConfiguration config;
//    private Connection connection;
//
//    public void build() {
//        config();
//        connect();
//    }
//
//    public void config() {
//        File f = new File(API.getInstance().getDataFolder().getAbsolutePath() + CONFIG_PATH);
//        config = YamlConfiguration.loadConfiguration(f);
//        config.addDefault("connection.host", "localhost");
//        config.addDefault("connection.port", 5672);
//        config.addDefault("connection.username", "rabbitmq");
//        config.addDefault("connection.password", "U6oM1EvRYX3K");
//        try {
//            config.options().copyDefaults(true);
//            config.save(f);
//        } catch (IOException e) {
//            BaseLogger.error("ERROR: can not save config to " + CONFIG_PATH + ". Please check it.");
//        }
//    }
//
//    public void connect() {
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost((String) getFromConfig("host"));
//        factory.setPort((int) getFromConfig("port"));
//        factory.setPassword((String) getFromConfig("password"));
//        factory.setUsername((String) getFromConfig("username"));
//        try {
//            connection = factory.newConnection();
//        } catch (IOException | TimeoutException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    private Object getFromConfig(String name) {
//        return config.get("connection." + name);
//    }
//
//    public String print(String message, String queueName) throws IOException, InterruptedException, TimeoutException {
//        Channel channel = connection.createChannel();
//        final String corrId = UUID.randomUUID().toString();
//
//        String replyQueueName = channel.queueDeclare().getQueue();
//        AMQP.BasicProperties props = new AMQP.BasicProperties
//                .Builder()
//                .correlationId(corrId)
//                .replyTo(replyQueueName)
//                .build();
//
//        channel.basicPublish("", queueName, props, message.getBytes("UTF-8"));
//
//        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);
//
//        String ctag = channel.basicConsume(replyQueueName, true, new DefaultConsumer(channel) {
//            @Override
//            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
//                if (properties.getCorrelationId().equals(corrId)) {
//                    response.offer(new String(body, "UTF-8"));
//                }
//            }
//        });
//
//        String result = response.take();
//        channel.basicCancel(ctag);
////        channel.close();
//        return result;
//    }
//
//    public void write(String name) {
//        try{
//            Channel channel = connection.createChannel();
//            channel.queueDeclare(name, false, false, false, null);
//            BaseLogger.info("Start writing in channel: " + name);
//            Consumer consumer = new DefaultConsumer(channel) {
//                @Override
//                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
//                        throws IOException {
//                    AMQP.BasicProperties replyProps = new AMQP.BasicProperties
//                            .Builder()
//                            .correlationId(properties.getCorrelationId())
//                            .build();
//                    String in = new String(body, "UTF-8");
//                    BaseLogger.info("[Servers] Received '" + in + "'");
//                    String out = in + " lalalal";
//
//                    channel.basicPublish( "", properties.getReplyTo(), replyProps, out.getBytes());
//                    channel.basicAck(envelope.getDeliveryTag(), false);
//                }
//            };
//            channel.basicConsume(name, false, consumer);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public String sendServerInfo(GameState state) {
//        try {
//            String systemname = Bukkit.getWorldContainer().getCanonicalFile().getName();
//            Gson server = new Gson();
//            String message = server.toJson(Arrays.asList(systemname, state.getNum()));
//            return print(message, "servers");
//        } catch (IOException|TimeoutException|InterruptedException e) {
//            BaseLogger.error("[Servers] An error occurred while sending to RabbitMQ");
//            e.printStackTrace();
//            return null;
//        }
//    }

}