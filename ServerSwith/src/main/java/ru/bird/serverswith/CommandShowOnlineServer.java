package ru.bird.serverswith;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.bird.chat.AbstractCommand;
import ru.bird.main.API;

public class CommandShowOnlineServer extends AbstractCommand {
    public CommandShowOnlineServer() {
        super("sos");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            return false;
        }
        if (!(sender instanceof Player)) {
            return false;
        }
        String server = args[0];
        SocketManager socketManager = API.getInstance().getSocketManager();
        sender.sendMessage("Online on " + server + " equal " + socketManager.getOnlineServer(server));
        return true;
    }
}
