//
// Decompiled by Procyon v0.5.30
//

package ru.bird.skywars.game;

import java.io.*;

import org.bukkit.*;
import org.bukkit.block.Block;
import ru.bird.main.API;
import ru.bird.main.event.listeners.DamageListener;
import ru.bird.main.event.listeners.PhysicalListener;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.Gamer;
import ru.bird.main.hologram.Hologram;
import ru.bird.main.hologram.HologramsManager;
import ru.bird.inventories.Item;
import ru.bird.inventories.ItemRunnable;
import ru.bird.inventories.Kit;
import ru.bird.inventories.ShopManager;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.utils.FIterator;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.Title;
import ru.bird.skywars.Main;
import org.bukkit.event.HandlerList;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import ru.bird.skywars.sidebar.GameSidebar;

public class GameFactory extends Game
{
    public static Map<Player, GPlayer> players;
    public static FileConfiguration configMain;
    public static FileConfiguration itemDropsConfig;
    private FIterator<Location> spawns;
    private int delayColb;
    private ConfigurationSection shop;
    private static FileConfiguration kitsConfig;
    private File kitsFile;
    private Player lastAlive;
    private File mainFile;
    private File itemDropFile;
    public static ConfigurationSection allDrops;
    private HologramsManager hologramsManager;

    public void saveKits() {
        if (kitsFile == null || GameFactory.kitsConfig == null) {
            return;
        }
        try {
            GameFactory.kitsConfig.save(this.kitsFile);
        }
        catch (IOException ex) {
            System.out.println("Ошибка при чтении файла kits.yml конфигурации.");
        }
        GameFactory.kitsConfig.options().copyDefaults(true);
    }

    public void saveMain() {
        if (mainFile == null || GameFactory.configMain == null) {
            return;
        }
        try {
            GameFactory.configMain.save(this.mainFile);
        }
        catch (IOException ex) {
            System.out.println("Ошибка при чтении файла main.yml конфигурации.");
        }
        GameFactory.configMain.options().copyDefaults(true);
    }

    public void saveItemDrops() {
        if (itemDropFile == null || GameFactory.itemDropsConfig == null) {
            return;
        }
        try {
            GameFactory.itemDropsConfig.save(this.itemDropFile);
        }
        catch (IOException ex) {
            System.out.println("Ошибка при чтении файла items.yml конфигурации.");
        }
        GameFactory.itemDropsConfig.options().copyDefaults(true);
    }

    private void init() {
        hologramsManager = API.getInstance().getHologramsManager();
        loadKits();
        loadMain();
        loadDropItem();
    }

    private void loadKits() {
        kitsFile = new File(Main.getInstance().getDataFolder(), "kits.yml");
        kitsConfig = YamlConfiguration.loadConfiguration(kitsFile);
        final InputStream defConfigStream1 = Main.getInstance().getResource("kits.yml");
        if (defConfigStream1 != null) {
            final YamlConfiguration defConfig1;
            try {
                defConfig1 = YamlConfiguration.loadConfiguration(new BufferedReader(new InputStreamReader(defConfigStream1, "UTF-8")));
                GameFactory.kitsConfig.setDefaults(defConfig1);
            } catch (UnsupportedEncodingException e) {
                BaseLogger.error("[SkyWars] Не загружен kits.yml");
                e.printStackTrace();
            }
        }
        try {
            kitsConfig.options().copyDefaults(true);
            kitsConfig.save(kitsFile);
        } catch (IOException e) {
            BaseLogger.error("[SkyWars] Не загружен kits.yml");
        }
        saveKits();
    }

    private void loadMain() {
        mainFile = new File(Main.getInstance().getDataFolder(), "main.yml");
        configMain = YamlConfiguration.loadConfiguration(mainFile);
        final InputStream defConfigStream1 = Main.getInstance().getResource("main.yml");
        if (defConfigStream1 != null) {
            final YamlConfiguration defConfig;
            try {
                defConfig = YamlConfiguration.loadConfiguration(new BufferedReader(new InputStreamReader(defConfigStream1, "UTF-8")));
                GameFactory.configMain.setDefaults(defConfig);
            } catch (UnsupportedEncodingException e) {
                BaseLogger.error("[SkyWars] Не загружен main.yml");
                e.printStackTrace();
            }
        }
        saveMain();
    }

    private void loadDropItem() {
        itemDropFile = new File(Main.getInstance().getDataFolder(), "items.yml");
        itemDropsConfig = YamlConfiguration.loadConfiguration(itemDropFile);
        final InputStream defConfigStream1 = Main.getInstance().getResource("items.yml");
        if (defConfigStream1 != null) {
            final YamlConfiguration defConfig;
            try {
                defConfig = YamlConfiguration.loadConfiguration(new BufferedReader(new InputStreamReader(defConfigStream1, "UTF-8")));
                GameFactory.itemDropsConfig.setDefaults(defConfig);
            } catch (UnsupportedEncodingException e) {
                BaseLogger.error("[SkyWars] Не загружен main.yml");
                e.printStackTrace();
            }
        }
        saveItemDrops();
    }

    public GameFactory() {
        super(new GameListener());
        players = new HashMap<Player, GPlayer>();
        init();
        loadConfig();
        loadItems();
        loadDropItems();
    }

    public static int getPlayers() {
        return GameFactory.players.size();
    }

    public static GPlayer getGplayer(final Player player) {
        return GameFactory.players.get(player);
    }

    private void loadDropItems(){
        allDrops = GameFactory.itemDropsConfig.getConfigurationSection("items");
    }

    private void loadConfig() {
        try {
            final FileConfiguration config = Main.getInstance().getConfig();
            this.delayColb = config.getInt("DStart");
            GameSettings.spectrLocation = GameUtils.stringToLocation(config.getString("Spectator"));
            this.shop = GameFactory.kitsConfig.getConfigurationSection("Shop");

            GameSettings.respawnLocation = GameUtils.stringToLocation(configMain.getString("Lobby"));
            final Set<Location> set = (Set<Location>)config.getStringList("Spawns").stream().map(GameUtils::stringToLocation).collect(Collectors.toCollection(LinkedHashSet::new));
            GameSettings.mapLocation = GameUtils.stringToLocation(config.getString("Map"));
            this.spawns = (FIterator<Location>)new FIterator((Collection)set);
            this.loadShop(GameFactory.kitsConfig);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
//        Board.spectator = new SpectatorBoard();
//        GameSettings.spectatorSidebar = new SpectatorSidebar();
    }

    private void loadItems() {
        final ItemStack shop = new ItemStack(Material.CHEST);
        final ItemMeta meta = shop.getItemMeta();
        meta.setDisplayName("§a[‣] §fМагазин наборов  §7(ПКМ)");
        shop.setItemMeta(meta);
        this.shopItem = new Item(shop, new ItemRunnable() {
            public void onUse(final PlayerInteractEvent e) {
                final Gamer gamer = Gamer.getGamer(e.getPlayer());
                if (gamer.getPurchase() != null && !gamer.isCancelbuy()) {
                    e.getPlayer().sendMessage(GameSettings.prefix + "Нажмите еще раз, чтобы аннулировать покупку набора " + gamer.getPurchase().getName());
                    e.setCancelled(true);
                    e.getPlayer().closeInventory();
                    gamer.setCancelbuy(true);
                    return;
                }
                if (gamer.getPurchase() != null) {
                    e.getPlayer().sendMessage(GameSettings.prefix + "Вам были возвращены деньги за покупку набора " + gamer.getPurchase().getName());
                    API.getInstance().getVaultManager().getEconomy().depositPlayer(e.getPlayer().getName(), (double)gamer.getPurchase().getPrice());
                    gamer.setCancelbuy(false);
                    gamer.setPurchase(null);
                    e.setCancelled(true);
                    e.getPlayer().closeInventory();
                    return;
                }
                final Inventory inv = Bukkit.createInventory(null, 45, "§rМагазин наборов");
                for (int i = 0; i < 45; ++i) {
                    final Kit kit = ShopManager.instance.getKit(i);
                    if (kit != null) {
                        inv.setItem(i, kit.getItem());
                    }
                }
                e.getPlayer().openInventory(inv);
            }

            public void onClick(final InventoryClickEvent paramInventoryClickEvent) {
            }
        }, new Action[] { Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK });
    }

    private void loadShop(final FileConfiguration config) {
        for (final String s : this.shop.getKeys(false)) {
            ShopManager.instance.addKit(new Kit(config.getString("Shop." + s + ".kit"), config.getStringList("Shop." + s + ".lore"), config.getInt("Shop." + s + ".perm")));
        }
    }

    public void onRespawn(final Player player) {
    }

    protected void onStartGame() {
        setupHolograms();
        final DamageListener dmg = new DamageListener();
        final PhysicalListener phys = new PhysicalListener();
        new BukkitRunnable() {
            public void run() {
                HandlerList.unregisterAll(dmg);
                HandlerList.unregisterAll(phys);
            }
        }.runTaskLater(Main.getInstance(), (long)(this.delayColb + 60));
        GameSettings.isBreak = false;
        GameSettings.isPlace = false;
        for (final Player p : Bukkit.getOnlinePlayers()) {

            final Location loc = this.spawns.getNext();
            if (!loc.getChunk().isLoaded()) {
                loc.getChunk().load();
            }
            p.teleport(loc);
            GPlayer gplayer = new GPlayer(p);
            gplayer.setGlassCount(3);
            gplayer.setFirstChestOpen(true);
            GameFactory.players.put(p, gplayer);
            p.sendMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
            p.sendMessage("");
            p.sendMessage("                                   §c§lSkyWars");
            p.sendMessage("");
            p.sendMessage("                      §eПостарайся уничтожить всех, кроме себя.");
            p.sendMessage("                      §eВсе необходимое ты найдешь в сундуке...");
            p.sendMessage("");
            p.sendMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");

            GameUtils.removeBlocks(p.getLocation());
            GameUtils.buildCell(p);
            new BukkitRunnable() {
                public void run() {
                    GameUtils.removeBlocks(p.getLocation());
                    GameSettings.isBreak = true;
                    GameSettings.isPlace = true;
                    String title = "§bSkyWars ";
                    String subtitle = "§fПриятной игры, " + p.getDisplayName() + "!";
                    new Title(title, subtitle, 1, 2, 1).send(p);
                }
            }.runTaskLater(Main.getInstance(), (long)delayColb);
            if (Gamer.getGamer(p).getPurchase() != null) {
                for (final ItemStack item : Gamer.getGamer(p).getPurchase().getItems()) {
                    if (item == null) {
                        continue;
                    }
                    p.getInventory().addItem(item);
                }
            }

            new GameSidebar(p);
        }
    }

    private void setupHolograms() {
        World w = GameSettings.mapLocation.getWorld();
        for (Chunk c : w.getLoadedChunks()) {
            int cx = c.getX() << 4;
            int cz = c.getZ() << 4;
            for (int x = cx; x < cx + 16; x++) {
                for (int z = cz; z < cz + 16; z++) {
                    for (int y = 0; y < 128; y++) {
                        Block block = w.getBlockAt(x, y, z);
                        if (block.getType() == Material.STAINED_GLASS && block.getData() == 10) {
//                            glass.add();
                            Hologram hologram = new Hologram(block.getLocation().add(0.5, 0.7, 0.5), Main.getInstance(), Arrays.asList(ChatColor.GREEN + "Сломай стёклышко что бы", ChatColor.GREEN + "получить случайный бонусный эффект"));

//                            int i;
//                            List<String> anim = Arrays.asList("");
//                            hologram.setDoUpdate(new BukkitRunnable() {
//                                @Override
//                                public void run() {
//
//                                }
//                            });
                            hologramsManager.registetHologram(hologram);
//                            new BukkitRunnable() {
//                                @Override
//                                public void run() {
//                                    hologramsManager.unregisterHologram(hologram);
//                                }
//                            }.runTaskLater(Main.getInstance(), delayColb + 20 * 10);
                        }
                    }
                }
            }
        }
    }

    protected void onEndGame() {
        final Collection<Gamer> gamers = Gamer.getGamers();
        this.lastAlive = GameUtils.getLastAlive();
        if (this.lastAlive == null) {
            return;
        }
        Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("                               §c§lSky Wars");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("                         §6Победил игрок - " + this.lastAlive.getDisplayName());
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        Gamer.getGamer(this.lastAlive).addMoney(50);
        String title = "&e" + this.lastAlive.getDisplayName();
        String subtitle = "&bПобедил в этой игре &c&l[!]";
        new Title(title, subtitle,1, 2, 1).broadcast();
        for (final Gamer gamer : gamers) {
            if (GameFactory.players != null && gamer != null && gamer.getPlayer() != null) {
                if (GameFactory.players.get(gamer.getPlayer()) == null) {
                    continue;
                }
                Player player = gamer.getPlayer();
                boolean win = false;
                if (this.lastAlive != null && this.lastAlive.getName().equals(gamer.getPlayer().getName())) {
                    win = true;
                }
                try {
                    PreparedStatement st = API.getInstance().getSQLConnection().getConnection().prepareStatement("CALL save_stat_sw(?, ?, ?, ?);");
                    st.setString(1, player.getUniqueId().toString());
                    st.setInt(2, gamer.getDeaths());
                    st.setBoolean(3, win);
                    st.setBoolean(4, gamer.getFb());
                    BaseLogger.debug(st.toString());
                    st.executeUpdate();
                } catch (SQLException e) {
                    BaseLogger.error(e.getMessage());
                }
            }
        }
    }

    public static boolean isFirstOnenChest(Player player) {
        return getGplayer(player).isFirstChestOpen();
    }
}