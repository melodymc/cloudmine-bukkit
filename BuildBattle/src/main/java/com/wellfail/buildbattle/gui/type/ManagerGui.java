/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.buildbattle.gui.type;

import com.wellfail.buildbattle.BuildRegion;
import com.wellfail.buildbattle.GameFactory;
import com.wellfail.buildbattle.gui.Gui;
import com.wellfail.buildbattle.gui.GuiAction;
import com.wellfail.buildbattle.gui.GuiType;
import com.wellfail.buildbattle.gui.type.HeadsManagerGui;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.ItemUtils;

public class ManagerGui
extends Gui {
    private Gui gui;

    public ManagerGui(Gui gui) {
        super(gui, 0, 4, "Управление");
        this.gui = gui;
    }

    public void init() {
        this.fillInventory();
        this.fillActions();
        this.addAction(31, this.gui.getGui(GuiType.MANAGER));
    }

    private void fillInventory() {
        this.inventory.setItem(11, ItemUtils.createItem(Material.YELLOW_FLOWER, "§aПогода", new String[]{"§7Изменить погоду", "§7вышего участка"}));
        this.inventory.setItem(12, ItemUtils.createItem(Material.WATCH, "§aВремя", new String[]{"§7Изменить время", "§7вашего участка"}));
        this.inventory.setItem(14, ItemUtils.createItem(Material.EMPTY_MAP, "§aБиом", new String[]{"§7Изменить биом", "§7вашего участка"}));
        this.inventory.setItem(15, ItemUtils.createItem(Material.STAINED_CLAY, "§aПол", new String[]{"§7Положите сюда блок", "§7чтобы изменить пол"}));
        this.inventory.setItem(21, ItemUtils.createItem(Material.LEGACY_SKULL, "§aГоловы", new String[]{"§7Используйте головы для", "§7декорации"}));
        this.inventory.setItem(23, ItemUtils.createItem(Material.BLAZE_POWDER,"§aЭффекты (скоро)", new String[]{"§7Установить эффекты", "§7на ваш участок"}));
    }

    private void fillActions() {
        this.addAction(11, this.gui.getGui(GuiType.WEATHER));
        this.addAction(12, this.gui.getGui(GuiType.TIME));
        this.addAction(14, this.gui.getGui(GuiType.BIOME));
        this.addAction(15, e -> {
            if (e.getCursor() == null) {
                return;
            }
            if (e.getCursor() == null || e.getCursor().getType() == Material.AIR) {
                e.getWhoClicked().sendMessage(GameFactory.getPrefix() + "Положите блок, чтобы изменить пол.");
                return;
            }
            GameFactory.getInstance().getRegion((Player)e.getWhoClicked()).fillFloor(e.getCursor());
            ManagerGui.this.inventory.getItem(15).setType(e.getCursor().getType());
            ManagerGui.this.inventory.getItem(15).setDurability(e.getCursor().getDurability());
        });
        this.addAction(21, HeadsManagerGui.getGui());
//        this.addAction(23, this.gui.getGui(GuiType.PARTICLES));
        this.addAction(40, e -> {
            if (e.getCursor() == null) {
                return;
            }
            GameFactory.getInstance().getRegion((Player)e.getWhoClicked()).music();
        });
    }

}

