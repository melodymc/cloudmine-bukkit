/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.buildbattle.gui.type;

import com.wellfail.buildbattle.GameFactory;
import com.wellfail.buildbattle.gui.Gui;
import com.wellfail.buildbattle.gui.GuiType;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import ru.bird.main.utils.ItemUtils;

public class WeatherGui
extends Gui {
    private Gui gui;

    public WeatherGui(Gui gui) {
        super(gui, 1, 4, "Погода");
        this.gui = gui;
    }

    public void init() {
        this.fillItems();
        this.addAction(31, this.gui.getGui(GuiType.MANAGER));
    }

    private void fillItems() {
        this.inventory.setItem(3, ItemUtils.createItem( Material.WATER_BUCKET, "§aДождь"));
        this.inventory.setItem(4, ItemUtils.createItem( Material.SNOW_BALL, "§aСнег"));
        this.inventory.setItem(5, ItemUtils.createItem( Material.DOUBLE_PLANT, "§aСолнечно"));
        this.inventory.setItem(31, ItemUtils.createItem( Material.ARROW, "§aНазад"));
    }

    @Override
    protected void onClick(InventoryClickEvent e) {
        int slot = e.getSlot();
        if (slot < 3 || slot > 5) {
            return;
        }
        int type = slot - 2;
        GameFactory.getInstance().getRegion((Player)e.getWhoClicked()).setWeather(type);
    }
}

