/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.player.PlayerLoginEvent
 *  org.bukkit.event.player.PlayerLoginEvent$Result
 */
package ru.bird.main.event.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerLoginEvent;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.GameState;

public class LoginListener
extends BasicListener {
    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        if (GameState.current != GameState.WAITING) {
            e.allow();
        } else if (Bukkit.getOnlinePlayers().size() == GameSettings.slots) {
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Нет мест");
        }
    }
}

