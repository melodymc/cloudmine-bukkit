package ru.bird.pets.entity.normal;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.EntityType;

import net.minecraft.server.v1_16_R2.DataWatcher;
import ru.bird.pets.entity.AbstractStandardPet;

public class PetChicken extends AbstractStandardPet {

	public PetChicken(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		Chicken entity = (Chicken) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		entity.setBaby();
		DataWatcher data = ((CraftEntity) entity).getHandle().getDataWatcher();
		entity.remove();
		return data;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.CHICKEN;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

}

