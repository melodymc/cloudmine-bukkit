package ru.bird.pets.entity.armorstand.humanoid;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import ru.bird.main.API;
import ru.bird.pets.Pets;
import ru.bird.main.packets.WrapperPlayServerEntityTeleport;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;

public class PetSpaceman extends AbstractArmoredPet {

	public PetSpaceman(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setArms(true);
		armor.setVisible(false);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}

	@Override
	protected float getOffsetY() {
		return offset;
	}

	@Override
	public ItemStack getMainHand() {
		return null;
	}

	@Override
	public ItemStack getOffHand() {
		return null;
	}

	@Override
	public ItemStack getBoots() {
		return new ItemStack(Material.IRON_BOOTS);
	}

	@Override
	public ItemStack getLeggings() {
		return new ItemStack(Material.IRON_LEGGINGS);
	}

	@Override
	public ItemStack getChestplate() {
		return new ItemStack(Material.IRON_CHESTPLATE);
	}

	@Override
	public ItemStack getHelmet() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmI4Nzg2NTJkMDRmYjNkODMzNTZmMzVmN2U4ZDk2NWZjNGRjODRjNmY5MmM5ZTc5MmIyNmQxMzUzOTk3NzM4In19fQ==");
	}

	@Override
	public double getAngle() {
		return angle;
	}

	private double	angle	= 0;
	private float	offset	= 1.2F;

	@Override
	public void onLivingUpdate() {
		offset = 1.2F + 0.2F * MathUtils.sin();
		angle += 1;
		angle = angle % 360;
		Location locTo = getOwner().getLocation();
		double localAngle = locTo.getYaw() + getAngle();
		double newX = locTo.getX() - 1D * Math.sin(Math.toRadians(localAngle));
		double newY = locTo.getBlockY() + getOffsetY();
		double newZ = locTo.getZ() + 1D * Math.cos(Math.toRadians(localAngle));
		this.petLocation = new Location(getOwner().getWorld(), newX, newY, newZ);
		
		WrapperPlayServerEntityTeleport position = new WrapperPlayServerEntityTeleport();
		position.setEntityID(getEntityID());
		position.setX(newX);
		position.setY(newY);
		position.setZ(newZ);
		position.setYaw(locTo.getYaw());
		position.setPitch(locTo.getPitch());
		position.broadcastPacket();
		
		this.setHeadPose(5 * MathUtils.cos(), -10 + 5 * MathUtils.sin(), 5 * MathUtils.sin());
		this.setRightLegPose(-5 * MathUtils.sin(), 10 + 5 * MathUtils.sin(), 10 + -5 * MathUtils.cos());
		this.setLeftLegPose(5 * MathUtils.sin(), -10 + 5 * MathUtils.cos(), -10 + 5 * MathUtils.cos());
		this.sendWatcherUpdate();
	}
}