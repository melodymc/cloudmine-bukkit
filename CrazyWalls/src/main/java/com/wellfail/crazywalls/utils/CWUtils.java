/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.enchantments.Enchantment
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  ru.wellfail.wapi.Main
 *  ru.wellfail.wapi.game.GameSettings
 *  ru.wellfail.wapi.game.Gamer
 *  ru.wellfail.wapi.shop.Kit
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.crazywalls.utils;

import com.wellfail.crazywalls.Main;
import com.wellfail.crazywalls.config.Config;
import com.wellfail.crazywalls.wall.Wall;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.Gamer;
import ru.bird.main.utils.GameUtils;

import java.util.ArrayList;
import java.util.List;

public class CWUtils {
    public static List<Wall> getWalls(FileConfiguration config, String wall) {
        ArrayList<Wall> wals = new ArrayList<>();
        config.getStringList(wall).forEach(line -> {
            String[] locs = line.split("/");
            wals.add(new Wall(GameUtils.stringToLocation((String)locs[0]), GameUtils.stringToLocation((String)locs[1]), Config.wallHeight));
        }
        );
        return wals;
    }

    public static void sendStartMessages(Player p) {
        p.sendMessage("\u00a7a\u00a7l\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac");
        p.sendMessage("");
        p.sendMessage("                                   \u00a7c\u00a7lCrazyWalls");
        p.sendMessage("");
        p.sendMessage("                      \u00a7e\u041f\u043e\u0441\u0442\u0430\u0440\u0430\u0439\u0441\u044f \u0443\u043d\u0438\u0447\u0442\u043e\u0436\u0438\u0442\u044c \u0432\u0441\u0435\u0445, \u043a\u0440\u043e\u043c\u0435 \u0441\u0435\u0431\u044f.");
        p.sendMessage("                      \u00a7e\u0412\u0441\u0435 \u043d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e\u0435 \u0442\u044b \u043d\u0430\u0439\u0434\u0435\u0448\u044c \u0432 \u0441\u0443\u043d\u0434\u0443\u043a\u0435...");
        p.sendMessage("");
        p.sendMessage("\u00a7a\u00a7l\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac");
    }

    public static void giveStartItems(Player player) {
        player.getInventory().setItem(0, new ItemStack(Material.STONE_SWORD));
        ItemStack pick = new ItemStack(Material.IRON_PICKAXE);
        ItemMeta meta = pick.getItemMeta();
        meta.addEnchant(Enchantment.DIG_SPEED, 2, true);
        pick.setItemMeta(meta);
        player.getInventory().setItem(1, pick);
        player.getInventory().setItem(2, new ItemStack(Material.IRON_AXE));
        if (Gamer.getGamer(player).getPurchase() != null) {
            for (ItemStack item : Gamer.getGamer(player).getPurchase().getItems()) {
                if (item == null) continue;
                player.getInventory().addItem(item);
            }
        }
    }

    public static void removeCell(final Player p) {
        new BukkitRunnable(){
            public void run() {
                GameUtils.removeBlocks((Location)p.getLocation());
                GameSettings.isBreak = true;
                GameSettings.isPlace = true;
            }
        }.runTaskLater(Main.getInstance(), (long)Config.delayColb);
    }

    public static int getAmountItem(ItemStack item, Player player) {
        int amount = 0;
        for (ItemStack it : player.getInventory().getContents()) {
            if (it == null || item.getType() != it.getType()) continue;
            amount += it.getAmount();
        }
        return amount;
    }

}

