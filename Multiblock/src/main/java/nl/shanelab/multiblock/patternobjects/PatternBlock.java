package nl.shanelab.multiblock.patternobjects;

import javax.annotation.Nonnull;

import ru.bird.storageUtils.utils.DeserializeUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.util.Vector;

import nl.shanelab.multiblock.IMaterial;
import nl.shanelab.multiblock.MaterialWrapper;
import nl.shanelab.multiblock.PatternObject;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Block pattern for the multiblock structure
 * 
 * @author ShaneCraft
 *
 */
public class PatternBlock extends PatternObject {

	private IMaterial IMaterial;

	public PatternBlock() {}

	public PatternBlock(@Nonnull Material blockMaterial, int x, int y, int z) {
		this(blockMaterial, new Vector(x, y, z));
	}
	
	public PatternBlock(@Nonnull Material blockMaterial, Vector relativeVec) {
		super(relativeVec);
		
		if (!blockMaterial.isBlock()) {
			throw new IllegalArgumentException(String.format("The given blockMaterial %s is not a valid block material.", blockMaterial.toString()));
		}
		
		this.IMaterial = new MaterialWrapper(blockMaterial);
	}
	
	public PatternBlock(@Nonnull IMaterial material, int x, int y, int z) {
		this(material, new Vector(x, y, z));
	}
	
	public PatternBlock(@Nonnull IMaterial material, Vector relativeVec) {
		super(relativeVec);
		
		if (!material.getType().isBlock()) {
			throw new IllegalArgumentException(String.format("The given blockMaterial %s is not a valid block material.", material.getType().toString()));
		}
		
		this.IMaterial = material;
	}

	@Override
	protected PatternObject createRotatedClone(Vector vector) {
		return new PatternBlock(IMaterial, vector);
	}

	@Override
	public boolean isValid(Location location) {
		return IMaterial.isValidBlock(location.getBlock());
	}
	
	public Material getMaterialBukkit() {
		return IMaterial.getType();
	}

	public IMaterial getIMaterial() {
		return IMaterial;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> list = super.serialize();
		list.put("IMaterial", getIMaterial().serialize());
		return list;
	}

	@Override
	public <T> T deserialize(Map<String, Object> args) throws IllegalArgumentException, NullPointerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		IMaterial material = DeserializeUtils.staticDeserialize((Map<String, Object>) args.get("IMaterial"));
		return (T) new PatternBlock(material, (int) args.get("relativeX"), (int) args.get("relativeY"), (int)  args.get("relativeZ"));
	}
}
