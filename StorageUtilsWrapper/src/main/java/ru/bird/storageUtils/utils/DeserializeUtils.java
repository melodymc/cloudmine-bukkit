package ru.bird.storageUtils.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class DeserializeUtils {

    public static <T> T staticDeserialize(Map<String, Object> args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class<?> classVar = Class.forName((String) args.get("=="));
        args.remove("==");
        Object fakeObject = classVar.newInstance();
        return (T) classVar
                .getMethod("deserialize", Map.class)
                .invoke(fakeObject, args);
    }

}
