package ru.bird.jobs.jobgame;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class Worker
{
    public final UUID workerUuid;

    private int points;
    private JobGame jobInstance;

    public Worker(UUID workerUuid, int initialPoints)
    {
        this.workerUuid = workerUuid;
        this.points = initialPoints;
    }

    public Worker(UUID workerUuid)
    {
        this(workerUuid, 0);
    }

    public Player getPlayer()
    {
        return Bukkit.getServer().getPlayer(workerUuid);
    }

    public Player getOnlinePlayer()
    {
        Player player = getPlayer();

        return (player != null && player.isOnline()) ? player : null;
    }

    public int getPoints()
    {
        return points;
    }

    public void setPoints(int points)
    {
        this.points = points;
    }

    public JobGame getJobInstance()
    {
        return jobInstance;
    }

    public void newJobInstance(JobGame jobInstance)
    {
        this.jobInstance = jobInstance;
    }

    public boolean isWorking()
    {
        return jobInstance != null;
    }
}
