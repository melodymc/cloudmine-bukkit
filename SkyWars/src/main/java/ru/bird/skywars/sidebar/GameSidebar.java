package ru.bird.skywars.sidebar;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.bird.main.board.api.PersonalBoard;
import ru.bird.main.game.GameSettings;
import ru.bird.main.utils.GameUtils;
import ru.bird.skywars.game.GPlayer;
import ru.bird.skywars.game.GameFactory;
import ru.pir.main.utils.BoardStringAnimator;

public class GameSidebar extends PersonalBoard
{
    private final byte HEADER_UPDATE_RATE = 3;
    private final byte BOARD_UPDATE_RATE = 20;

    public GameSidebar(Player player) {
        super("§e§l" + GameSettings.wboardname.toUpperCase(), player);

        GameFactory.getGplayer(player).setBoard(this);
        addUpdaterHeader(HEADER_UPDATE_RATE, BoardStringAnimator.concatFrameBuffers(
                BoardStringAnimator.generateStatic(GameSettings.wboardname.toUpperCase(), "&e", 5),
                BoardStringAnimator.generateMetalShine(GameSettings.wboardname.toUpperCase(), "&e", "&6", "&f"),
                BoardStringAnimator.generateBlinking(GameSettings.wboardname.toUpperCase(), "&f", "&e", 5)
        ));

        GPlayer gPlayer = GameFactory.getGplayer(player);
        int spectators = Bukkit.getOnlinePlayers().size() - GameUtils.getAlivePlayers().size();

        write(0, "§3");
        write(1, "Игроков: §a " +  GameFactory.players.size());
        write(2, "Наблюдателей: §a " + spectators);
        write(3, "§2");
        write(4, "§fУбийств: §a⚔ " + gPlayer.getKills());
        write(5, "§1");

        addUpdater(BOARD_UPDATE_RATE, board -> {
            int spec = Bukkit.getOnlinePlayers().size() - GameUtils.getAlivePlayers().size();

            board.modifyLine(1, "Наблюдателей: §a " + spec);
            board.modifyLine(2, "Игроков: §a" + GameUtils.getAlivePlayers().size());
        });
        create();
    }
}