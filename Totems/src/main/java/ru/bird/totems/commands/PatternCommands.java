package ru.bird.totems.commands;

import com.ticxo.modelengine.api.model.ActiveModel;
import com.ticxo.modelengine.api.model.ModeledEntity;
import com.ticxo.modelengine.api.model.component.ModelOption;
import de.leonhard.storage.Json;
import nl.shanelab.multiblock.MaterialWrapper;
import nl.shanelab.multiblock.MultiBlockPattern;
import nl.shanelab.multiblock.MultiBlockPatternFacing;
import nl.shanelab.multiblock.patternobjects.PatternBlock;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Cow;
import org.bukkit.event.block.Action;
import ru.bird.storageUtils.utils.StorageMeow;
import ru.bird.totems.EventMeowRunnable;
import ru.bird.totems.Utils.Cuboid;
import ru.bird.totems.Utils.LocationUtils;
import ru.bird.totems.Utils.PluginManagerUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import ru.bird.chat.AbstractCommand;
import ru.bird.totems.Main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class PatternCommands extends AbstractCommand {
    public PatternCommands() {
        super("pattern");
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        try {
            if(args.length == 1) {
                ArrayList<String> helpTab = new ArrayList<>();
                if(sender.hasPermission("pattern.ru.bird.jobs.command.help.admin")) {
                    helpTab.add("create");
                } else {
                    helpTab.add("other_user_command");
                }
                return helpTab;
            }
            String subcommand = args[0];
            args = Arrays.copyOfRange(args, 1, args.length);
            switch (subcommand){
                case "create":
                    if(args.length == 1) {
                        return Arrays.stream(MultiBlockPatternFacing.values()).map(MultiBlockPatternFacing::toString).collect(Collectors.toList());
                    }
                    if(args.length == 2) {
                        return new ArrayList<>();
                    }
                default:
                    sender.sendMessage(ChatColor.RED + "Этот аргумент не поддерживает TAB");
                    return new ArrayList<>();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            if(sender.hasPermission("pattern.ru.bird.jobs.command.admin.messages.taberror")) {
                sender.sendMessage("Ошибка автозаполнения.");
            }
            return new ArrayList<>();
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player)){
            sender.sendMessage("only for player");
            return false;
        }
        if(!sender.hasPermission("pattern.ru.bird.jobs.command")) {
            sender.sendMessage("Нет доступа к этой команде.");
            return false;
        }
        if(args.length == 0) {
            help(label, sender);
            return false;
        }
        Player player = (Player) sender;
        String subcommand = args[0];
        if(!sender.hasPermission("pattern.ru.bird.jobs.command." + subcommand)) {
            sender.sendMessage("Нет доступа к этой саб-команде.");
            return false;
        }
        args = Arrays.copyOfRange(args, 1, args.length);
        switch (subcommand){
            case "create":
                if(args.length == 0) {
                    player.sendMessage("Укажи сторону света либо " + MultiBlockPatternFacing.CARDINAL + " для любой");
                    return false;
                }
                try{
                    MultiBlockPatternFacing patternFacing = MultiBlockPatternFacing.valueOf(args[0]);
                    if(args.length == 1) {
                        player.sendMessage("Название паттерна");
                        return false;
                    }
                    String patternName = args[1];
                    return create(patternFacing, patternName, player);
                } catch (IllegalArgumentException e) {
//                    e.printStackTrace();
                    player.sendMessage("Неверная сторона света");
                    return false;
                }
            case "tomb":
                ModeledEntity entity = Main.getThisClass().modelEngineApi.createModeledEntity(
                        player, new ModelOption(
                                "rat",
                                false,
                                true,
                                true,
                                true,
                                true
                        ),
                        true
                );
                return true;
            default:
                player.sendMessage(ChatColor.GRAY + "Нет такой саб-команды.");
                return false;
        }
    }

    private boolean create(MultiBlockPatternFacing patternFacing, String patternName, Player player) {
        Main plugin = Main.getThisClass();
        AtomicReference<Material> coreMaterial = new AtomicReference<>();
        AtomicReference<Location> coreLocation = new AtomicReference<>();
        AtomicReference<Location> first = new AtomicReference<>();
        player.sendMessage("Выберите точку отсчёта");
        try{
            PluginManagerUtils.oneTimeEvent(PlayerInteractEvent.class, plugin, EventPriority.LOWEST, new EventMeowRunnable<PlayerInteractEvent>() {
                @Override
                public void meowRun(PlayerInteractEvent event) {
                    Block block = event.getClickedBlock();
                    if(block != null) {
                        Material material = block.getType();
                        coreMaterial.set(material);
                        coreLocation.set(block.getLocation());
                        player.sendMessage("Выберите первую точку");
                        PluginManagerUtils.oneTimeEvent(PlayerInteractEvent.class, plugin, EventPriority.LOWEST, new EventMeowRunnable<PlayerInteractEvent>() {
                            @Override
                            public void meowRun(PlayerInteractEvent eventTwo) {
                                Block blockTwo = eventTwo.getClickedBlock();
                                if(blockTwo != null) {
                                    Location locationFirst = blockTwo.getLocation();
                                    first.set(locationFirst);
                                    player.sendMessage("Выберите вторую точку");
                                    PluginManagerUtils.oneTimeEvent(PlayerInteractEvent.class, plugin, EventPriority.LOWEST, new EventMeowRunnable<PlayerInteractEvent>() {
                                        @Override
                                        public void meowRun(PlayerInteractEvent eventThree) {
                                            Block blockThree = eventThree.getClickedBlock();
                                            if(blockThree != null) {
                                                Location locationTwo = blockThree.getLocation();
                                                Cuboid cuboid = new Cuboid(first.get(), locationTwo);
                                                List<Block> blocks = cuboid.getBlocks();
                                                System.out.println("blocks " + blocks.toString());
                                                Location coreLocationMeow = coreLocation.get();
                                                PatternBlock[] objects = blocks.stream()
                                                        .filter(block1 -> (block1.getType() != Material.AIR) && !(LocationUtils.equals(coreLocationMeow, block1.getLocation())))
                                                        .map(block1 -> {
                                                            PatternBlock patternBlock = new PatternBlock(block1.getType(), block1.getLocation().subtract(coreLocationMeow).toVector());
                                                            patternBlock.rotate(patternFacing);
                                                            return patternBlock;
                                                        }
                                                ).toArray(PatternBlock[]::new);
                                                MultiBlockPattern multiBlockPattern = new MultiBlockPattern(new MaterialWrapper(coreMaterial.get()), patternFacing, objects);
                                                Json configFile = StorageMeow.loadJsonConfig(patternName, plugin.getPatternsDir());
                                                configFile.addDefaultsFromMap(multiBlockPattern.serialize());
                                                player.sendMessage("material " + coreMaterial.get().toString());
                                                player.sendMessage("location 1 " + first.get().toString());
                                                player.sendMessage("location 2 " + locationTwo.toString());
                                                player.sendMessage("done");
                                            }
                                        }

                                        @Override
                                        public boolean validate(PlayerInteractEvent eventThreeValidate) {
                                            Block blockThreeForValidator = eventThreeValidate.getClickedBlock();
                                            Action actionThree = eventThreeValidate.getAction();
                                            if(actionThree == Action.LEFT_CLICK_BLOCK) {
                                                if(blockThreeForValidator != null) {
                                                    Location locationValidatorTwo = blockThreeForValidator.getLocation();
                                                    boolean isNotFirstLocation = locationValidatorTwo.distance(first.get()) > 0;
                                                    if(!isNotFirstLocation) {
                                                        player.sendMessage("Укажи не ту же точку");
                                                    }
                                                    Cuboid cuboid = new Cuboid(first.get(), locationValidatorTwo);
                                                    List<Block> blocks = cuboid.getBlocks();
                                                    boolean isCoreBlockInCuboid = false;
                                                    for (Block b : blocks) {
                                                        if(b.getLocation().distance(coreLocation.get()) == 0) {
                                                            isCoreBlockInCuboid = true;
                                                            break;
                                                        }
                                                    }
                                                    if(!isCoreBlockInCuboid) {
                                                        player.sendMessage("Главный блок должен быть внутри кубоида");
                                                    }
                                                    return isNotFirstLocation && isCoreBlockInCuboid;
                                                }
                                            } else if(actionThree == Action.RIGHT_CLICK_BLOCK || actionThree == Action.RIGHT_CLICK_AIR) {
                                                eventThreeValidate.setCancelled(false);
                                            }
                                            return false;
                                        }
                                    });
                                }
                            }

                            @Override
                            public boolean validate(PlayerInteractEvent eventTwoValidate) {
                                Action eventTwoAction = eventTwoValidate.getAction();
                                if(eventTwoAction == Action.LEFT_CLICK_BLOCK) {
                                    return true;
                                } else if(eventTwoAction == Action.RIGHT_CLICK_BLOCK || eventTwoAction == Action.RIGHT_CLICK_AIR) {
                                    eventTwoValidate.setCancelled(false);
                                }
                                return false;
                            }
                        });
                    }
                }

                @Override
                public boolean validate(PlayerInteractEvent eventFirstValidate) {
                    Action eventFirstAction = eventFirstValidate.getAction();
                    if(eventFirstAction == Action.LEFT_CLICK_BLOCK) {
                        return true;
                    } else if(eventFirstAction == Action.RIGHT_CLICK_BLOCK || eventFirstAction == Action.RIGHT_CLICK_AIR) {
                        eventFirstValidate.setCancelled(false);
                    }
                    return false;
                }
            });
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }

    private void help(String label, CommandSender sender) {
        PluginDescriptionFile desc = Main.getThisClass().getDescription();
        sender.sendMessage(ChatColor.GOLD + "Доступные команды " + ChatColor.GREEN + desc.getName() + ChatColor.WHITE + " v" + desc.getVersion());
        if(sender.hasPermission("pattern.ru.bird.jobs.command.create")) {
            sender.sendMessage(new String[]{
                    ChatColor.BOLD + "/" + label + ChatColor.GRAY + " create - " + ChatColor.GREEN + " <имя> создать паттерн и сохранить в файл"
            });
        }
    }
}
