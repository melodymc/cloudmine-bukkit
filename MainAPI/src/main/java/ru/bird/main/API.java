package ru.bird.main;

import ru.bird.main.board.api.Board;

import java.io.File;
import java.sql.SQLException;

public class API extends APIInitialiser {

	public static API instance;

	@Override
	public void onLoad() {
		instance = this;
	}

	@Override
	public void onEnable() {
		this.initApiManager(instance);
	}

	@Override
	public void onDisable() {
		Board.REGISTERED_BOARDS.forEach(Board::remove);
	}

	public static API getInstance() {
		return instance;
	}
}