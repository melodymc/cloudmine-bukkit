package nl.shanelab.multiblock.utils;

import nl.shanelab.multiblock.MultiBlockPatternFacing;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class MultiBlockPatternUtils {

    public static List<Location> byCoreBlockLocationGetRealLocations(Location coreBlockLocation, List<Vector> subtractedVectors, MultiBlockPatternFacing facing) {
        List<Location> locations = new ArrayList<>();
        for(Vector v : subtractedVectors) {
            if(facing == MultiBlockPatternFacing.NORTH) {
                locations.add(coreBlockLocation.clone().add(v));
            } else {
                locations.remove(coreBlockLocation.clone().add(v));
            }
        }
        return locations;
    }

}
