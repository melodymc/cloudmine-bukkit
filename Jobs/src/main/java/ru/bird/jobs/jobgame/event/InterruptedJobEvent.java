package ru.bird.jobs.jobgame.event;

import ru.bird.jobs.jobgame.JobGame;

public class InterruptedJobEvent extends JobEventUnified
{
    public InterruptedJobEvent(JobGame instance)
    {
        super(instance);
    }
}
