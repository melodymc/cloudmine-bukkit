//
// Decompiled by Procyon v0.5.30
//

package com.wellfail.buildbattle.listeners;

import com.wellfail.buildbattle.Main;
import org.bukkit.event.EventPriority;
import org.bukkit.Sound;
import ru.bird.main.event.listeners.BasicListener;
import ru.bird.main.game.Game;
import com.wellfail.buildbattle.GameFactory;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.entity.Player;
import ru.bird.main.game.GameState;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.main.game.Gamer;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.ItemUtils;

public class VoteListener extends BasicListener
{
    private ItemStack[] voteItems;

    public VoteListener() {
        Bukkit.getOnlinePlayers().stream().filter(player -> !Gamer.getGamer(player).isSpectator()).forEach(player -> {
            player.getInventory().clear();
            this.giveVoteItems(player);
            return;
        });
        new BukkitRunnable() {
            public void run() {
                if (GameState.current != GameState.GAME) {
                    this.cancel();
                    return;
                }
                Bukkit.getOnlinePlayers().stream().filter(player -> !Gamer.getGamer(player).isSpectator()).forEach(VoteListener.this::giveVoteItems);
            }
        }.runTaskTimer(Main.getInstance(), 200L, 200L);
    }

    @EventHandler
    public void onBlockExpose(final BlockExplodeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onEntityExpose(final EntityExplodeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPhysic(final BlockPhysicsEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent e) {
        e.setCancelled(true);
    }

    private void giveVoteItems(final Player player) {
        if (this.voteItems == null) {
            final List<String> list = null;
            (this.voteItems = new ItemStack[6])[0] = ItemUtils.createItem(Material.STAINED_CLAY, 1, 14, "§4Ужастно", list);
            this.voteItems[1] = ItemUtils.createItem(Material.STAINED_CLAY, 1, 2, "§cОтвратительно", list);
            this.voteItems[2] = ItemUtils.createItem(Material.STAINED_CLAY, 1, 5, "§aСойдет", list);
            this.voteItems[3] = ItemUtils.createItem(Material.STAINED_CLAY, 1, 13, "§2Хорошо", list);
            this.voteItems[4] = ItemUtils.createItem(Material.STAINED_CLAY, 1, 11, "§9Класс", list);
            this.voteItems[5] = ItemUtils.createItem(Material.STAINED_CLAY, 1, 4, "§6Отлично", list);
        }
        int i = 0;
        for (final ItemStack item : this.voteItems) {
            player.getInventory().setItem(i, item);
            ++i;
        }
    }

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent e) {
        e.getWhoClicked().closeInventory();
        e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInteract(final PlayerInteractEvent e) {
        final ItemStack item = e.getPlayer().getItemInHand();
        if (item == null) {
            return;
        }
        if (item.getType() != Material.STAINED_CLAY) {
            return;
        }
        e.setCancelled(true);
        if (GameFactory.getInstance().getCurrent() == null) {
            return;
        }
        if (GameFactory.getInstance().getCurrent().getPlayer() == e.getPlayer()) {
            e.getPlayer().sendMessage(Game.getPrefix() + "Вы не можете голосовать на своем участке");
            return;
        }
        final int id = item.getDurability();
        int points = 0;
        switch (id) {
            case 14: {
                points = 0;
                break;
            }
            case 2: {
                points = 1;
                break;
            }
            case 5: {
                points = 2;
                break;
            }
            case 13: {
                points = 3;
                break;
            }
            case 11: {
                points = 4;
                break;
            }
            case 4: {
                points = 5;
                break;
            }
            default: {
                return;
            }
        }
        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_CAT_HISS, 2.0f, 0.4f * points);
        GameFactory.getInstance().getCurrent().setPoints(e.getPlayer(), points);
        e.getPlayer().sendMessage(Game.getPrefix() + "Ваша оценка - " + item.getItemMeta().getDisplayName());
    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockPlace(final BlockPlaceEvent e) {
        e.setCancelled(true);
    }
}
