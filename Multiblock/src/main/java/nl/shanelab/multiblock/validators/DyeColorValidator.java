package nl.shanelab.multiblock.validators;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.Block;

import nl.shanelab.multiblock.IMaterialValidator;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class DyeColorValidator implements IMaterialValidator {
	
	private DyeColor dyeColor;
	
	private Material material;

	public DyeColorValidator() {}
	
	public DyeColorValidator(DyeColor dyeColor) {
		this(dyeColor, Material.WHITE_WOOL);
	}
	
	public DyeColorValidator(DyeColor dyeColor, Material alternate) {
		this.dyeColor = dyeColor;
		this.material = alternate;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean validateBlock(Block block) {
		if (block == null || block.getType() != material) {
			return false;
		}
		
		return DyeColor.getByDyeData(block.getData()) == dyeColor; // ((Wool)block.getState().getData()).getColor() == dyeColor;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> result = IMaterialValidator.super.serialize();
		result.put("dyeColor", dyeColor.toString());
		result.put("material", material.toString());
		return result;
	}

	@Override
	public <T> T deserialize(Map<String, Object> args) throws IllegalArgumentException, NullPointerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return (T) new DyeColorValidator(DyeColor.valueOf((String) args.get("dyeColor")), Material.valueOf((String) args.get("material")));
	}
}
