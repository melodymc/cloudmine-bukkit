package ru.bird.pets.entity.armorstand.emotions;

import net.minecraft.server.v1_16_R2.DataWatcher;
import net.minecraft.server.v1_16_R2.EnumItemSlot;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import ru.bird.main.particles.ParticleEffect;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.MathUtils;

import java.util.UUID;

public class PetEmotion extends AbstractArmoredPet {

	public PetEmotion(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setArms(true);
		armor.setVisible(false);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getHelmet() {
		return normal();
	}

	private int stage = 0;

	@Override
	public void onLivingUpdate() {
		stage++;
		switch (stage) {
			case 0:
			case 20:
				sendEquipmentPacket(EnumItemSlot.HEAD, normal());
				break;
		}
	}

	private ItemStack normal() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODExNWRjYzE3YjJlMTVjZDQxODMxODg1ZDdkYmI4ZmYyZTljYWM0ZmVjNzA4MDM1OGZlNTVmOTNlZWExOWIifX19");
	}
}