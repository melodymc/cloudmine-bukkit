package ru.bird.jobs.jobgame.enchanter;

import com.mojang.datafixers.util.Pair;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.bird.inventories.menu.Menu;
import ru.bird.inventories.menu.interfaces.GuiCallback;
import ru.bird.jobs.JobsPlugin;
import ru.bird.jobs.jobgame.JobGame;
import ru.bird.jobs.jobgame.event.InterruptedJobEvent;
import ru.bird.jobs.jobgame.event.StartJobEvent;
import ru.bird.jobs.jobgame.generator.GeneratorGroup;
import ru.bird.jobs.jobgame.generator.GeneratorItem;
import ru.bird.jobs.jobgame.generator.LevelGenerator;
import ru.bird.jobs.jobgame.generator.patterns.SimpleSimulator;
import ru.bird.jobs.utils.Directions;
import ru.bird.jobs.utils.InvUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.BiConsumer;

public class JobGameEnchanter extends JobGame
{
    /* GAME VARIABLES */
    private int clickedSlot = -1;
    private boolean[][] checkedSlots;

    /* MAINTENANCE DATA */
    public final Menu gui = new Menu(JobsPlugin.get(), "Зачаровыватель", 6);
    private LevelGenerator generator;

    /* CONSTANT DATA */
    //

    public JobGameEnchanter(UUID playerUuid)
    {
        super(playerUuid);

        GeneratorGroup group = new GeneratorGroup();
        group.defineItem( new GeneratorItem(Material.GRAY_STAINED_GLASS_PANE, 0.25d) );
        group.defineItem( new GeneratorItem(Material.YELLOW_STAINED_GLASS_PANE, 0.25d) );
        group.defineItem( new GeneratorItem(Material.LIGHT_BLUE_STAINED_GLASS_PANE, 0.25d) );
        group.defineItem( new GeneratorItem(Material.LIME_STAINED_GLASS_PANE, 0.25d) );

        this.generator = new LevelGenerator(group);
        generator.setSimulator( new SimpleSimulator() );

        gui.setGuiOpen(onGuiBuildEventHandler);
        gui.setGuiClick(onGuiClickEventHandler);
        gui.setGuiClose(onGuiDestructEventHandler);

        worker.newJobInstance(this);
    }

    /* GUI event handlers */
    private final GuiCallback onGuiBuildEventHandler = (player, container) -> {
        Bukkit.getServer().getPluginManager().callEvent( new StartJobEvent(this) );
        generator.generateLevel(container);
    };

    private final GuiCallback onGuiDestructEventHandler = (player, container) -> {
        Bukkit.getServer().getPluginManager().callEvent( new InterruptedJobEvent(this) );
        worker.newJobInstance(JobGame.EMPTY);
    };

    private final BiConsumer<Player, InventoryClickEvent> onGuiClickEventHandler = (player, event) -> {
        if (event.getCurrentItem() == null) return;
        if (event.getClickedInventory() != gui.getBukkitInventory()) return;

        Inventory inv = event.getInventory();

        if (clickedSlot == -1)
        {
            clickedSlot = event.getSlot();
        }
        else
        {
            if (InvUtils.getAllNearestSlots(clickedSlot, 9, 6).contains(event.getSlot()))
            {
                swap(inv, clickedSlot, event.getSlot());

                List<ItemStack> matchedStacks = findAll(inv, event.getSlot());
                matchedStacks.addAll(findAll(inv, clickedSlot));

                for (ItemStack s: matchedStacks)
                {
                    s.setAmount(0);
                }
            }

            clickedSlot = -1;
        }
    };

    private List<ItemStack> findAll(Inventory inv, int slot)
    {
        checkedSlots = new boolean[6][9];

        ItemStack stack = inv.getItem(slot);
        if (stack != null)
        {
            return findAll(inv, stack.getType(), slot);
        }

        return new ArrayList<>();
    }

    private List<ItemStack> findAll(Inventory inv, Material type, int slot)
    {
        ArrayList<ItemStack> stacks = new ArrayList<>();

        Pair<Integer, Integer> xy = InvUtils.index2xy(slot, 9);
        if (xy.getFirst() != -1 && xy.getSecond() != -1 && xy.getFirst() <= 9 && xy.getSecond() <= 6)
        {
            if (!checkedSlots[xy.getSecond()][xy.getFirst()])
            {
                ItemStack stack = inv.getItem(slot);
                if (stack != null)
                {
                    if (stack.getType() == type)
                    {
                        stacks.add(stack);
                        checkedSlots[xy.getSecond()][xy.getFirst()] = true;

                        stacks.addAll(findAll(inv, type, InvUtils.getNearestSlot(slot, 9, 6, Directions.UP)));
                        stacks.addAll(findAll(inv, type, InvUtils.getNearestSlot(slot, 9, 6, Directions.RIGHT)));
                        stacks.addAll(findAll(inv, type, InvUtils.getNearestSlot(slot, 9, 6, Directions.DOWN)));
                        stacks.addAll(findAll(inv, type, InvUtils.getNearestSlot(slot, 9, 6, Directions.LEFT)));
                    }
                }
            }
        }

        return stacks;
    }

    private void swap(Inventory inv, int src, int dst)
    {
        ItemStack tmp = inv.getItem(src);

        inv.setItem(src, inv.getItem(dst));
        inv.setItem(dst, tmp);
    }

//    private void fall(Inventory inv, int slot)
//    {
//        int currentSlot = slot;
//        int nextSlot = InvUtils.getNearestSlot(currentSlot, 9, 6, Directions.UP);
//        while (nextSlot != -1)
//        {
//            inv.setItem(currentSlot, inv.getItem(nextSlot));
//
//            currentSlot = nextSlot;
//            nextSlot = InvUtils.getNearestSlot(currentSlot, 9, 6, Directions.UP);
//        }
//
//        generator.generateItem(inv, currentSlot);
//    }
//
//    private void getThree(Inventory inv, int slot, Directions dir, List<Integer> items, int max)
//    {
//        int nextSlot = InvUtils.getNearestSlot(slot, 9, 6, dir);
//        if (items.size() < max && nextSlot != -1)
//        {
//            ItemStack current = inv.getItem(slot);
//            ItemStack next = inv.getItem(nextSlot);
//
//            if (current != null && next != null)
//            {
//                if (current.getType() == next.getType())
//                {
//                    items.add(nextSlot);
//                    getThree(inv, nextSlot, dir, items, max);
//                }
//            }
//        }
//    }
}
