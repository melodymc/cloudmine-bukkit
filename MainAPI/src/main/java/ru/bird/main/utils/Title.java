package ru.bird.main.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Title {
    private String title = "";
    private ChatColor titleColor = ChatColor.WHITE;
    private String subtitle = "";
    private ChatColor subtitleColor = ChatColor.WHITE;
    private int fadeInTime = -1;
    private int stayTime = -1;
    private int fadeOutTime = -1;
    private boolean ticks = false;

    public Title(String title) {
        this.title = title;
    }

    public Title(String title, String subtitle) {
        this.title = title;
        this.subtitle = subtitle;
    }

    public Title(Title title) {
        this.title = title.title;
        this.subtitle = title.subtitle;
        this.titleColor = title.titleColor;
        this.subtitleColor = title.subtitleColor;
        this.fadeInTime = title.fadeInTime;
        this.fadeOutTime = title.fadeOutTime;
        this.stayTime = title.stayTime;
        this.ticks = title.ticks;
    }

    public Title(String title, String subtitle, int fadeInTime, int stayTime, int fadeOutTime) {
        this.title = title;
        this.subtitle = subtitle;
        this.fadeInTime = fadeInTime;
        this.stayTime = stayTime;
        this.fadeOutTime = fadeOutTime;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getSubtitle() {
        return this.subtitle;
    }

    public void setTitleColor(ChatColor color) {
        this.titleColor = color;
    }

    public void setSubtitleColor(ChatColor color) {
        this.subtitleColor = color;
    }

    public void setFadeInTime(int time) {
        this.fadeInTime = time;
    }

    public void setFadeOutTime(int time) {
        this.fadeOutTime = time;
    }

    public void setStayTime(int time) {
        this.stayTime = time;
    }

    public void setTimingsToTicks() {
        this.ticks = true;
    }

    public void setTimingsToSeconds() {
        this.ticks = false;
    }

    public void send(Player player)
    {
        try
        {
            int fadeInTime_tmp = this.fadeInTime * (this.ticks ? 1 : 20);
            int stayTime_tmp = this.stayTime * (this.ticks ? 1 : 20);
            int fadeOutTime_tmp = this.fadeOutTime * (this.ticks ? 1 : 20);
            player.sendTitle(
                    (String) ChatColor.translateAlternateColorCodes('&', this.title),
                    (String) ChatColor.translateAlternateColorCodes('&', this.subtitle),
                    fadeInTime_tmp, stayTime_tmp, fadeOutTime_tmp
            );
//            player.sendMessage(this. title + " " + this.subtitle);
        }

        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void broadcast() {
        Bukkit.getOnlinePlayers().forEach(this::send);
    }
}