package ru.bird.main.board.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.google.common.collect.Maps;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import ru.bird.main.API;
import ru.bird.main.board.ProtocolBoard;
import ru.bird.main.packets.WrapperPlayServerScoreboardDisplayObjective;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class PacketListener
    extends PacketAdapter
    implements Listener {

    private Map<Player, String> scoreboards = Maps.newHashMap();

    public PacketListener() {
        super(API.getInstance(), PacketType.Play.Server.SCOREBOARD_DISPLAY_OBJECTIVE);
    }

    @Override
    public void onPacketSending(PacketEvent event) {
        WrapperPlayServerScoreboardDisplayObjective packet
                = new WrapperPlayServerScoreboardDisplayObjective(event.getPacket());
        if(packet.getPosition() != 1) return;
        if(scoreboards.containsKey(event.getPlayer())) scoreboards.remove(event.getPlayer());
        scoreboards.put(event.getPlayer(), packet.getScoreName());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        if(scoreboards.containsKey(event.getPlayer())) scoreboards.remove(event.getPlayer());
    }

    public String getCurrentScoreboard(Player player) {
        return scoreboards.get(player);
    }

    public Set<Player> getPlayersWithObjective(String objective) {
        return scoreboards.entrySet().stream()
                .filter(entry -> entry.getValue().equals(objective))
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }
}
