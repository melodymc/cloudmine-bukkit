package ru.bird.main.npc;

import net.minecraft.server.v1_16_R2.NBTTagCompound;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftVillager;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;

public class NPCCreator {
    private Location location;

    public NPCCreator(Location location) {
        this.location = location;
    }

    private void setNoAI(Villager villager) {
        net.minecraft.server.v1_16_R2.EntityVillager nmsVillager = ((CraftVillager) villager).getHandle();
        NBTTagCompound tag = new NBTTagCompound();
        nmsVillager.saveData(tag);
        tag.setInt("NoAI", 1);
    }

    private Villager spawn(final Location location, boolean freez) {
        if (!location.getChunk().isLoaded()) {
            location.getChunk().load(true);
        }

        final Villager villager = (Villager) location.getWorld().spawnEntity(location, EntityType.VILLAGER);
        if(freez) {
            setNoAI(villager);
        }
        return villager;
    }

    public Villager createNPC(boolean freez)
    {
        return spawn(this.location, freez);
    }
}

