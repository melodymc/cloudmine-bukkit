/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.Listener
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.inventory.InventoryDragEvent
 *  org.bukkit.event.inventory.InventoryType
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryHolder
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  ru.wellfail.wapi.Main
 */
package com.wellfail.buildbattle.gui;

import com.wellfail.buildbattle.BuildRegion;
import com.wellfail.buildbattle.Main;
import com.wellfail.buildbattle.gui.type.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;

public class Gui
implements Listener {
    protected Inventory inventory;
    private Map<Integer, GuiAction> slots;
    public Map<Integer, Gui> guis = new HashMap<Integer, Gui>();

    public Gui getGui(GuiType type) {
        return this.guis.get(type.getID());
    }

    protected Gui(Gui gui, int type, int rows, String name) {
        this.slots = new HashMap<Integer, GuiAction>();
        this.inventory = Bukkit.createInventory((InventoryHolder)null, (int)(rows * 9), (String)name);
        gui.guis.put(type, this);
        Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin) Main.getInstance());
    }

    public Gui(BuildRegion region) {
        ManagerGui mgui = new ManagerGui(this);
        ParticlesGui pgui = new ParticlesGui(this);
        BiomeGui bgui = new BiomeGui(this);
        TimeGui tgui = new TimeGui(this);
        WeatherGui wgui = new WeatherGui(this);
        new HeadsManagerGui(this);
//        new ParticlesManagerGui(this, region);
        mgui.init();
        pgui.init();
        bgui.init();
        tgui.init();
        wgui.init();
    }

    public Gui(int rows, String name) {
        this.slots = new HashMap<Integer, GuiAction>();
        this.inventory = Bukkit.createInventory((InventoryHolder)null, (int)(rows * 9), (String)name);
        Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)Main.getInstance());
    }

    public void open(final Player player) {
        player.closeInventory();
        new BukkitRunnable(){

            public void run() {
                player.openInventory(Gui.this.inventory);
            }
        }.runTaskLater((Plugin)Main.getInstance(), 1);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (!this.inventory.equals((Object)e.getInventory())) {
            return;
        }
        if (e.isCancelled()) {
            return;
        }
        if (e.getRawSlot() < 0 || e.getRawSlot() >= this.inventory.getSize()) {
            return;
        }
        e.setCancelled(true);
        if (e.getCurrentItem() == null) {
            return;
        }
        GuiAction action = this.slots.get(e.getSlot());
        if (action == null) {
            this.onClick(e);
        } else {
            action.onClick(e);
        }
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent e) {
        e.setCancelled(true);
    }

    public void addAction(int slot, GuiAction action) {
        this.slots.put(slot, action);
    }

    public void addAction(int slot, Gui gui) {
        this.slots.put(slot, e -> {
            gui.open((Player)e.getWhoClicked());
        }
        );
    }

    protected void onClick(InventoryClickEvent e) {
    }

    private boolean isInventory(Inventory inventory) {
        if (inventory.getType() != this.inventory.getType()) {
            return false;
        }
        if (inventory.getSize() != this.inventory.getSize()) {
            return false;
        }
        if (!inventory.getName().equals(this.inventory.getName())) {
            return false;
        }
        return true;
    }

}

