package nl.shanelab.multiblock;

import ru.bird.storageUtils.interfaces.Deserializable;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.Map;

public class MaterialWrapper implements IMaterial, Deserializable {

	private Material material;

	public MaterialWrapper() {}

	public MaterialWrapper(Material material) {
		this.material = material;
	}

	@Override
	public Material getType() {
		return material;
	}

	@Override
	public boolean isValidBlock(Block block) {
		return block != null && block.getType() == material;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> result = new LinkedHashMap();
		result.put("==", getClass().getName());
		result.put("material", getType().toString());
		return result;
	}

	@Override
	public <T> T deserialize(Map<String, Object> args) throws IllegalArgumentException, NullPointerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		Material material = Material.valueOf((String) args.get("material"));
		return (T) new MaterialWrapper(material);
	}
}