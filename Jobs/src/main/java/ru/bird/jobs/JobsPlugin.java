package ru.bird.jobs;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.jobs.command.JobServerCommand;
import ru.bird.jobs.jobgame.enchanter.EnchanterListener;
import ru.bird.jobs.jobgame.miner.MinerListener;
import ru.bird.jobs.menu.MenuManager;

public class JobsPlugin extends JavaPlugin {
    private final String CONFIG_PATH = "/jobs/config.yml";

    public FileConfiguration config;
    private static JobsPlugin instance;
    private static long canUpdatePlotTime;
    private static long canUpdateJobTime;
    public File playersDir;

    public static JobsPlugin get() {
        return instance;
    }

    @Override
    public void onEnable() {
        // Job game listeners
        Bukkit.getPluginManager().registerEvents(new MinerListener(), this);
        Bukkit.getPluginManager().registerEvents(new EnchanterListener(), this);

//        Runnable jobTask = new Runnable() {
//            @Override
//            public void run() {
//                instance.getServer().getOnlinePlayers().forEach(player -> {
//                    Economy econ = VaultModule.getEcon();
//                    double original = 250;
//                    double multiplay = GroupsModule.getDoubleValueByPermission(player, "googland.multiplay", true, 1);
//                    double zp = original * multiplay;
//                    econ.depositPlayer(player, zp);
//                    player.sendMessage(ChatColor.GRAY + "Начислено " + ChatColor.GOLD + original + ChatColor.GRAY +" + " + ChatColor.GOLD + (zp - original) + ChatColor.GRAY + " (от множетеля (" + ChatColor.GOLD + multiplay + "X" + ChatColor.GRAY + ") ) = " + VaultModule.getBeapi().format(zp) + ChatColor.GRAY +" в виде почасовой зарплаты");
//                });
//            }
//        };
        new MenuManager();
        startChecker();
//        this.getServer().getPluginManager().registerEvents(new JobsListener(), this);
        loadCommands();
//        PayDay.addTimer("jobs", jobTask);
    }

    private void startChecker() {
        Bukkit.getServer().getScheduler().runTaskLater(this, () -> {
            for(JobsUser u : StorageJob.jobsusers.values()){
                checkCanPlotUpdate(u);
            }
        }, 20 * 60 * 10);
    }

    public static void checkCanPlotUpdate(JobsUser u) {
        if(!u.isCanUpdatePlot()) {
            if (u.getSumm() >= canUpdatePlotTime) {
                u.setCanUpdatePlot(true);
                u.setTime(0);
                Player player = Bukkit.getPlayer(u.getUUID());
                if (player != null) {
                    player.playSound(player.getLocation(), Sound.BLOCK_IRON_TRAPDOOR_OPEN, 1f, 1f);
                    player.sendMessage(ChatColor.GOLD + "[*] " + ChatColor.GRAY + " Доступно обновление плота. /js menu для подробностей.");
                }
            }
        }
    }

    public static void checkCanJobChange(JobsUser u) {
        if(!u.isCanChangeJob()) {
            if (u.getSumm() >= canUpdateJobTime) {
                u.setCanChangeJob(true);
                u.setTime(0);
                Player player = Bukkit.getPlayer(u.getUUID());
                if (player != null) {
                    player.playSound(player.getLocation(), Sound.BLOCK_IRON_TRAPDOOR_OPEN, 1f, 1f);
                    player.sendMessage(ChatColor.GOLD + "[*] " + ChatColor.GRAY + " Доступно обновление плота. /js menu для подробностей.");
                }
            }
        }
    }

    private void loadCommands() {
        new JobServerCommand().register();
    }

    @Override
    public void onLoad() {
        playersDir = new File(this.getDataFolder().getPath() + "/players");
        instance = this;
        this.getLogger().info("Loading Jobs");
//        config(plugin);
    }

    @Override
    public void onDisable() {
        StorageJob.insertAll();
        this.getLogger().info("Disable Jobs");
    }

    public static long getCanUpdatePlotTime() {
        return canUpdatePlotTime;
    }

    public void config() {
        File f = new File(this.getDataFolder().getAbsolutePath() + CONFIG_PATH);
        config = YamlConfiguration.loadConfiguration(f);
        config.addDefault("canUpdatePlotTime", 7200);
        config.addDefault("canUpdateJobTime", 86400);
        try {
            config.options().copyDefaults(true);
            config.save(f);
        } catch (IOException e) {
            System.out.println("ERROR: can not save config to " + CONFIG_PATH + ". Please onLoad it.");
        }
        canUpdatePlotTime = config.getLong("canUpdatePlotTime") * 1000;
        canUpdateJobTime = config.getLong("canUpdateJobTime") * 1000;
    }

    public static String getTimeToChangeJob(long time) {
        long diff = (canUpdateJobTime - time);
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        StringBuilder sb = new StringBuilder();
        sb.append(diffDays != 0 ? diffDays + " дней, ": "");
        sb.append(diffHours != 0 ? diffHours + " часов, ": "");
        sb.append(diffMinutes != 0 ? diffMinutes + " минут, ": "");
        sb.append(diffSeconds != 0 ? diffSeconds + " секунд": "");

        return sb.toString();
    }

    public static String getTimeToUpdatePlot(long time) {
        long diff = (canUpdatePlotTime - time);
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        StringBuilder sb = new StringBuilder();
        sb.append(diffDays != 0 ? diffDays + " дней, ": "");
        sb.append(diffHours != 0 ? diffHours + " часов, ": "");
        sb.append(diffMinutes != 0 ? diffMinutes + " минут, ": "");
        sb.append(diffSeconds != 0 ? diffSeconds + " секунд": "");

        return sb.toString();
    }

    public static String getTimeToChangeJobStr(Player player) {
        JobsUser user = StorageJob.get(player.getUniqueId());
        return getTimeToChangeJobStr(user);
    }
    public static String getTimeToChangeJobStr(JobsUser user) {

        return getTimeToChangeJob(user.getJobTimeSumm());
    }

}