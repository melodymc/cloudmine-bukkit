/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.World
 *  org.bukkit.ru.bird.jobs.command.Command
 *  org.bukkit.ru.bird.jobs.command.CommandExecutor
 *  org.bukkit.ru.bird.jobs.command.CommandSender
 *  org.bukkit.ru.bird.jobs.command.PluginCommand
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.entity.Player
 */
package ru.bird.main.game;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.bird.chat.AbstractCommand;
import ru.bird.main.API;
import ru.bird.main.utils.GameUtils;

import java.util.*;
import java.util.stream.Collectors;

public class GameSetup extends AbstractCommand {
    private API plugin;
    private Map<Integer, Location> locations;
    private Map<String, List<String>> locations_group;
    private Map<String, ItemStack[]> items;

    public GameSetup() {
        super("setup");
        this.locations = new HashMap<>();
        this.locations_group = new HashMap<>();
        this.items = new HashMap<>();
        this.plugin = API.getInstance();
    }

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (args.length > 0) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("only for players");
                return false;
            }
            Player player = (Player) sender;
            if (!player.hasPermission("setup")) {
                return false;
            }
            String one;
            String two;
            Location loc;
            int i;
            double d;
            String two_m;
            String subcommand = args[0];
            args = Arrays.copyOfRange(args, 1, args.length);
            switch (subcommand) {
                case "setlobby":
                    return setLobby(player, args);

                case "setspectator":
                    this.plugin.getConfig().set("Spectator", GameUtils.locationToString(player.getLocation(), true));
                    this.plugin.saveConfig();
                    sender.sendMessage("OK");
                    return true;

                case "setvillager":
                    return setVillager(player, args);

                case "tp":
                    return tp(player, args);
                case "killentitys":
                    return killEntitys(player, args);
                case "setInv":
                    return setInv(player, args);
                case "loc_remove":
                    return locRemove(player, args);
                case "loc":
                    return loc(player, args);
                case "loc_group":
                    return locGroup(player, args);
                default:
                    return help(player);
            }
        } else {
            return help(sender);
        }
    }

    private boolean tp(Player player, String[] args) {
        if(args.length == 0) {
            player.sendMessage("Введите название мира");
            return false;
        }
        String world = args[0];
        if (Bukkit.getWorld(world) != null) {
            player.teleport(new Location(Bukkit.getWorld(world), 0.0, 80.0, 0.0));
        } else {
            player.sendMessage("/setup tp [world:string]. Мира - '" + world + "' не существует. ");
            return false;
        }
        player.sendMessage("OK");
        return true;
    }

    private boolean locGroup(Player sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("Введите группу (string)");
            return false;
        }
        String zero = args[0];
        if (args.length == 1) {
            sender.sendMessage("Введите + по y");
            return false;
        }
        String one = args[1];
        double plus;
        try {
            plus = Double.parseDouble(one);
        } catch (Exception e) {
            sender.sendMessage("Должно быть числом");
            return false;
        }
        Location loc = sender.getLocation().add(0, plus, 0);
        if (args.length == 2) {
            sender.sendMessage("Введите номер локации");
            return false;
        }
        String two = args[2];
        int i;
        try {
            i = Integer.parseInt(two);
        } catch (Exception e) {
            sender.sendMessage("Должно быть числом");
            return false;
        }
        List<String> old = locations_group.get(zero);
        String loc_str = GameUtils.locationToString(loc, false);
        if (old != null) {
            sender.sendMessage("Group " + zero + " exist");
            try{

                old.set(i, loc_str);
                sender.sendMessage("Loc " + i + " replace");
                locations_group.replace(zero, locations_group.get(zero), old);
            } catch (IndexOutOfBoundsException e) {
                sender.sendMessage("Loc " + i + " put");
                old.add(loc_str);
                locations_group.put(zero, old);
            }
        } else {
//            List<String> old = locations_group.get(zero);
            sender.sendMessage("Group " + zero + " not exist");
            List<String> list = new ArrayList<>();
            list.add(loc_str);
            locations_group.put(zero, list);
            sender.sendMessage("Loc " + i + " put");
        }

        save_group();
        return true;
    }

    private boolean equalsLoc(Location one, Location two) {
        return one.getBlockX() == two.getBlockX() && one.getBlockY() == two.getBlockY() && one.getBlockZ() == two.getBlockZ();
    }

    private boolean loc(Player player, String[] args) {
        if (args[0] == null) {
            player.sendMessage("Введите номер локации");
            return false;
        }
        String one = args[0];
        int i;
        try {
            i = Integer.parseInt(one);
        } catch (Exception e) {
            player.sendMessage("Должно быть числом");
            return false;
        }
        if (args[1] == null) {
            player.sendMessage("Введите + по y");
            return false;
        }
        String two = args[1];
        double two_d;
        try {
            two_d = Double.parseDouble(two);
        } catch (Exception e) {
            player.sendMessage("Должно быть числом");
            return false;
        }
        Location loc = player.getLocation().add(0, two_d, 0);
        if (!locations.containsKey(i)) {
            locations.put(i, loc);
            player.sendMessage("OK. Loc " + i + " put");
        } else {
            locations.replace(i, locations.get(i), loc);
            player.sendMessage("OK. Loc " + i + " replace");
        }
        save();
        return true;
    }

    private boolean locRemove(Player player, String[] args) {
        if (args[0] == null) {
            return false;
        }
        String one = args[0];
        int i;
        try {
            i = Integer.parseInt(one);
        } catch (Exception e) {
            return false;
        }
        locations.remove(i);
        save();
        player.sendMessage("OK. Loc " + i + " remove");
        return true;
    }

    private boolean setInv(Player player, String[] args) {
        if (args[0] == null) {
            return false;
        }
        String one = args[0];
        ItemStack[] currentItems = player.getInventory().getContents();
        if (this.items.containsKey(one)) {
            this.items.remove(one, this.items.get(one));
            this.items.put(one, currentItems);
            player.sendMessage("Замена");
        } else {
            player.sendMessage("Добавление");
            this.items.put(one, currentItems);
        }
        player.sendMessage("OK");
        return true;
    }

    private boolean killEntitys(Player player, String[] args) {
        List<Entity> entitys = player.getWorld().getEntities();
        entitys.forEach(entity -> {
            if (!(entity instanceof Player)) {
                if (args[0] != null && Objects.equals(args[0], "true")) {
                    player.sendMessage("Удаление " + entity.getName());
                }
                entity.remove();
            }
        });
        return true;
    }

    private boolean setLobby(Player player, String[] args) {
        this.plugin.getConfig().set("Lobby", GameUtils.locationToString(player.getLocation(), true));
        this.plugin.saveConfig();
        player.sendMessage("OK");
        return true;
    }

    private boolean setVillager(Player player, String[] args) {
        if(args.length == 0){
            player.sendMessage("Введите группу");
            return false;
        }
        String group = args[0];
        if(args.length == 1){
            player.sendMessage("Введите тип");
            return false;
        }
        String type_str = args[1];
        int type;
        try{
            type = Integer.parseInt(type_str);
        } catch (Exception e) {
            player.sendMessage("Должно быть числом");
            return false;
        }
        this.plugin.getConfig().set("Villagers." + group + ".type", type);
        this.plugin.getConfig().set("Villagers." + group + ".location", GameUtils.locationToString(player.getLocation(), true));
        this.plugin.saveConfig();
        player.sendMessage("Житель "+ group +" создан");
        player.sendMessage("OK");
        return true;
    }

    private boolean help(CommandSender sender) {
        sender.sendMessage("/setup loc - add location");
        sender.sendMessage("/setup setlobby - set lobby");
        sender.sendMessage("/setup setspectator - set setspectator spawn");
        sender.sendMessage("/setup tp [world:string] - tp to world");
        sender.sendMessage("/setup setvillager [id:int] [type:int] - set villager loc, type");
        sender.sendMessage("/setup setInv [id:int] add itemStack");
        sender.sendMessage("/setup killentitys [(optional)view:boolen] kill all entitys in player world");
        return true;
    }

    private void save() {
        FileConfiguration config = this.plugin.getConfig();
        List list = this.locations.values().stream().map(loc -> GameUtils.locationToString(loc, true)).collect(Collectors.toList());
        config.set("Spawns", list);
        Map<String, Object> map = new HashMap<>();
        items.forEach((name, items)->{
            Map<String, Object> itemMap = new HashMap<>();
            for(ItemStack item : items) {
                if(item != null) {
                    Map<String, Object> currentItem = item.serialize();
                    itemMap.put(UUID.randomUUID().toString(), currentItem);
                }
            }
            map.put(name, itemMap);
        });
        config.set("Items", map);
        this.plugin.saveConfig();
    }

    private void save_group() {
        FileConfiguration config = this.plugin.getConfig();
        config.set("Spawns_group", locations_group);
        Map<String, Object> map = new HashMap<>();
        items.forEach((name, items)->{
            Map<String, Object> itemMap = new HashMap<>();
            for(ItemStack item : items) {
                if(item != null) {
                    Map<String, Object> currentItem = item.serialize();
                    itemMap.put(UUID.randomUUID().toString(), currentItem);
                }
            }
            map.put(name, itemMap);
        });
        config.set("Items", map);
        this.plugin.saveConfig();
    }
}