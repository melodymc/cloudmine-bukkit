package ru.bird.chat.formatted;

import static ru.bird.chat.formatted.TextualComponent.rawText;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

//import org.bukkit.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.Statistic.Type;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;

public class FormattedMessage implements JsonRepresentedObject, Cloneable, Iterable<MessagePart>, ConfigurationSerializable {

	static {
		ConfigurationSerialization.registerClass(FormattedMessage.class);
	}

	private List<MessagePart>	messageParts;
	private String				jsonString;
	private boolean				dirty;

	private static Constructor<?> nmsPacketPlayOutChatConstructor;

	@Override
	public FormattedMessage clone() throws CloneNotSupportedException {
		FormattedMessage instance = (FormattedMessage) super.clone();
		instance.messageParts = new ArrayList<MessagePart>(messageParts.size());
		for (int i = 0; i < messageParts.size(); i++) {
			instance.messageParts.add(i, messageParts.get(i).clone());
		}
		instance.dirty = false;
		instance.jsonString = null;
		return instance;
	}

	public FormattedMessage(final String firstPartText) {
		this(rawText(firstPartText));
	}

	public FormattedMessage(final TextualComponent firstPartText) {
		messageParts = new ArrayList<MessagePart>();
		messageParts.add(new MessagePart(firstPartText));
		jsonString = null;
		dirty = false;

		if (nmsPacketPlayOutChatConstructor == null) {
			try {
				nmsPacketPlayOutChatConstructor = Reflection.getNMSClass("PacketPlayOutChat").getDeclaredConstructor(Reflection.getNMSClass("IChatBaseComponent"));
				nmsPacketPlayOutChatConstructor.setAccessible(true);
			} catch (NoSuchMethodException e) {
				Bukkit.getLogger().log(Level.SEVERE, "Could not find Minecraft method or constructor.", e);
			} catch (SecurityException e) {
				Bukkit.getLogger().log(Level.WARNING, "Could not access constructor.", e);
			}
		}
	}

	public FormattedMessage() {
		this((TextualComponent) null);
	}

	public FormattedMessage text(String text) {
		MessagePart latest = latest();
		latest.text = rawText(text);
		dirty = true;
		return this;
	}

	public FormattedMessage text(TextualComponent text) {
		MessagePart latest = latest();
		latest.text = text;
		dirty = true;
		return this;
	}

	public FormattedMessage color(final ChatColor color) {
		if (!color.isColor()) {
			throw new IllegalArgumentException(color.name() + " is not a color");
		}
		latest().color = color;
		dirty = true;
		return this;
	}

	public FormattedMessage style(ChatColor... styles) {
		for (final ChatColor style : styles) {
			if (!style.isFormat()) {
				throw new IllegalArgumentException(style.name() + " is not a style");
			}
		}
		latest().styles.addAll(Arrays.asList(styles));
		dirty = true;
		return this;
	}

	public FormattedMessage file(final String path) {
		onClick("open_file", path);
		return this;
	}

	public FormattedMessage link(final String url) {
		onClick("open_url", url);
		return this;
	}

	public FormattedMessage suggest(final String command) {
		onClick("suggest_command", command);
		return this;
	}

	public FormattedMessage insert(final String command) {
		latest().insertionData = command;
		dirty = true;
		return this;
	}

	public FormattedMessage command(final String command) {
		onClick("run_command", command);
		return this;
	}

//	public FormattedMessage achievementTooltip(final String name) {
//		onHover("show_achievement", new JsonString("achievement." + name));
//		return this;
//	}
//
//	public FormattedMessage achievementTooltip(final Achievement which) {
//		try {
//			Object achievement = Reflection.getMethod(Reflection.getOBCClass("CraftStatistic"), "getNMSAchievement", Achievement.class).invoke(null, which);
//			return achievementTooltip((String) Reflection.getField(Reflection.getNMSClass("Achievement"), "name").get(achievement));
//		} catch (IllegalAccessException e) {
//			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
//			return this;
//		} catch (IllegalArgumentException e) {
//			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
//			return this;
//		} catch (InvocationTargetException e) {
//			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
//			return this;
//		}
//	}

//	public FormattedMessage statisticTooltip(final Statistic which) {
//		Type type = which.getType();
//		if (type != Type.UNTYPED) {
//			throw new IllegalArgumentException("That statistic requires an additional " + type + " parameter!");
//		}
//		try {
//			Object statistic = Reflection.getMethod(Reflection.getOBCClass("CraftStatistic"), "getNMSStatistic", Statistic.class).invoke(null, which);
//			return achievementTooltip((String) Reflection.getField(Reflection.getNMSClass("Statistic"), "name").get(statistic));
//		} catch (IllegalAccessException e) {
//			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
//			return this;
//		} catch (IllegalArgumentException e) {
//			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
//			return this;
//		} catch (InvocationTargetException e) {
//			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
//			return this;
//		}
//	}
//
//	public FormattedMessage statisticTooltip(final Statistic which, Material item) {
//		Type type = which.getType();
//		if (type == Type.UNTYPED) {
//			throw new IllegalArgumentException("That statistic needs no additional parameter!");
//		}
//		if ((type == Type.BLOCK && item.isBlock()) || type == Type.ENTITY) {
//			throw new IllegalArgumentException("Wrong parameter type for that statistic - needs " + type + "!");
//		}
//		try {
//			Object statistic = Reflection.getMethod(Reflection.getOBCClass("CraftStatistic"), "getMaterialStatistic", Statistic.class, Material.class).invoke(null, which, item);
//			return achievementTooltip((String) Reflection.getField(Reflection.getNMSClass("Statistic"), "name").get(statistic));
//		} catch (IllegalAccessException e) {
//			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
//			return this;
//		} catch (IllegalArgumentException e) {
//			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
//			return this;
//		} catch (InvocationTargetException e) {
//			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
//			return this;
//		}
//	}
//
//	public FormattedMessage statisticTooltip(final Statistic which, EntityType entity) {
//		Type type = which.getType();
//		if (type == Type.UNTYPED) {
//			throw new IllegalArgumentException("That statistic needs no additional parameter!");
//		}
//		if (type != Type.ENTITY) {
//			throw new IllegalArgumentException("Wrong parameter type for that statistic - needs " + type + "!");
//		}
//		try {
//			Object statistic = Reflection.getMethod(Reflection.getOBCClass("CraftStatistic"), "getEntityStatistic", Statistic.class, EntityType.class).invoke(null, which, entity);
//			return achievementTooltip((String) Reflection.getField(Reflection.getNMSClass("Statistic"), "name").get(statistic));
//		} catch (IllegalAccessException e) {
//			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
//			return this;
//		} catch (IllegalArgumentException e) {
//			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
//			return this;
//		} catch (InvocationTargetException e) {
//			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
//			return this;
//		}
//	}

	public FormattedMessage itemTooltip(final String itemJSON) {
		onHover("show_item", new JsonString(itemJSON));
		return this;
	}

	public FormattedMessage itemTooltip(final ItemStack itemStack) {
		try {
			Object nmsItem = Reflection.getMethod(Reflection.getOBCClass("inventory.CraftItemStack"), "asNMSCopy", ItemStack.class).invoke(null, itemStack);
			return itemTooltip(Reflection.getMethod(Reflection.getNMSClass("ItemStack"), "save", Reflection.getNMSClass("NBTTagCompound")).invoke(nmsItem, Reflection.getNMSClass("NBTTagCompound").newInstance()).toString());
		} catch (Exception e) {
			e.printStackTrace();
			return this;
		}
	}

	public FormattedMessage tooltip(final String text) {
		onHover("show_text", new JsonString(text));
		return this;
	}

	public FormattedMessage tooltip(final Iterable<String> lines) {
		tooltip(ArrayWrapper.toArray(lines, String.class));
		return this;
	}

	public FormattedMessage tooltip(final String... lines) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < lines.length; i++) {
			builder.append(lines[i]);
			if (i != lines.length - 1) {
				builder.append('\n');
			}
		}
		tooltip(builder.toString());
		return this;
	}

	public FormattedMessage formattedTooltip(FormattedMessage text) {
		for (MessagePart component : text.messageParts) {
			if (component.clickActionData != null && component.clickActionName != null) {
				throw new IllegalArgumentException("The tooltip text cannot have click data.");
			} else if (component.hoverActionData != null && component.hoverActionName != null) {
				throw new IllegalArgumentException("The tooltip text cannot have a tooltip.");
			}
		}
		onHover("show_text", text);
		return this;
	}

	public FormattedMessage formattedTooltip(FormattedMessage... lines) {
		if (lines.length < 1) {
			onHover(null, null);
			return this;
		}

		FormattedMessage result = new FormattedMessage();
		result.messageParts.clear();
		for (int i = 0; i < lines.length; i++) {
			try {
				for (MessagePart component : lines[i]) {
					if (component.clickActionData != null && component.clickActionName != null) {
						throw new IllegalArgumentException("The tooltip text cannot have click data.");
					} else if (component.hoverActionData != null && component.hoverActionName != null) {
						throw new IllegalArgumentException("The tooltip text cannot have a tooltip.");
					}
					if (component.hasText()) {
						result.messageParts.add(component.clone());
					}
				}
				if (i != lines.length - 1) {
					result.messageParts.add(new MessagePart(rawText("\n")));
				}
			} catch (CloneNotSupportedException e) {
				Bukkit.getLogger().log(Level.WARNING, "Failed to clone object", e);
				return this;
			}
		}
		return formattedTooltip(result.messageParts.isEmpty() ? null : result);
	}

	public FormattedMessage formattedTooltip(final Iterable<FormattedMessage> lines) {
		return formattedTooltip(ArrayWrapper.toArray(lines, FormattedMessage.class));
	}

	public FormattedMessage translationReplacements(final String... replacements) {
		for (String str : replacements) {
			latest().translationReplacements.add(new JsonString(str));
		}
		dirty = true;

		return this;
	}

	public FormattedMessage translationReplacements(final FormattedMessage... replacements) {
		for (FormattedMessage str : replacements) {
			latest().translationReplacements.add(str);
		}

		dirty = true;

		return this;
	}

	public FormattedMessage translationReplacements(final Iterable<FormattedMessage> replacements) {
		return translationReplacements(ArrayWrapper.toArray(replacements, FormattedMessage.class));
	}

	public FormattedMessage then(final String text) {
		return then(rawText(text));
	}

	public FormattedMessage then(final TextualComponent text) {
		if (!latest().hasText()) {
			throw new IllegalStateException("previous message part has no text");
		}
		messageParts.add(new MessagePart(text));
		dirty = true;
		return this;
	}

	public FormattedMessage then() {
		if (!latest().hasText()) {
			throw new IllegalStateException("previous message part has no text");
		}
		messageParts.add(new MessagePart());
		dirty = true;
		return this;
	}

	@Override
	public void writeJson(JsonWriter writer) throws IOException {
		if (messageParts.size() == 1) {
			latest().writeJson(writer);
		} else {
			writer.beginObject().name("text").value("").name("extra").beginArray();
			for (final MessagePart part : this) {
				part.writeJson(writer);
			}
			writer.endArray().endObject();
		}
	}

	public String toJSONString() {
		if (!dirty && jsonString != null) {
			return jsonString;
		}
		StringWriter string = new StringWriter();
		JsonWriter json = new JsonWriter(string);
		try {
			writeJson(json);
			json.close();
		} catch (IOException e) {
			throw new RuntimeException("invalid message");
		}
		jsonString = string.toString();
		dirty = false;
		return jsonString;
	}

	public void send(Player player) {
		send(player, toJSONString());
	}

	private void send(CommandSender sender, String jsonString) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(toOldMessageFormat());
			return;
		}
		Player player = (Player) sender;
		try {
			Object handle = Reflection.getHandle(player);
			Object connection = Reflection.getField(handle.getClass(), "playerConnection").get(handle);
			Reflection.getMethod(connection.getClass(), "sendPacket", Reflection.getNMSClass("Packet")).invoke(connection, createChatPacket(jsonString));
		} catch (IllegalArgumentException e) {
			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
		} catch (IllegalAccessException e) {
			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
		} catch (InstantiationException e) {
			Bukkit.getLogger().log(Level.WARNING, "Underlying class is abstract.", e);
		} catch (InvocationTargetException e) {
			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
		} catch (NoSuchMethodException e) {
			Bukkit.getLogger().log(Level.WARNING, "Could not find method.", e);
		} catch (ClassNotFoundException e) {
			Bukkit.getLogger().log(Level.WARNING, "Could not find class.", e);
		}
	}

	// The ChatSerializer's instance of Gson
	private static Object	nmsChatSerializerGsonInstance;
	private static Method	fromJsonMethod;

	private Object createChatPacket(String json) throws IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
		if (nmsChatSerializerGsonInstance == null) {
			// Find the field and its value, completely bypassing obfuscation
			Class<?> chatSerializerClazz;

			String version = Reflection.getVersion();
			double majorVersion = Double.parseDouble(version.replace('_', '.').substring(1, 4));
			int lesserVersion = Integer.parseInt(version.substring(6, 7));

			if (majorVersion < 1.8 || (majorVersion == 1.8 && lesserVersion == 1)) {
				chatSerializerClazz = Reflection.getNMSClass("ChatSerializer");
			} else {
				chatSerializerClazz = Reflection.getNMSClass("IChatBaseComponent$ChatSerializer");
			}

			if (chatSerializerClazz == null) {
				throw new ClassNotFoundException("Can't find the ChatSerializer class");
			}

			for (Field declaredField : chatSerializerClazz.getDeclaredFields()) {
				if (Modifier.isFinal(declaredField.getModifiers()) && Modifier.isStatic(declaredField.getModifiers()) && declaredField.getType().getName().endsWith("Gson")) {

					declaredField.setAccessible(true);
					nmsChatSerializerGsonInstance = declaredField.get(null);
					fromJsonMethod = nmsChatSerializerGsonInstance.getClass().getMethod("fromJson", String.class, Class.class);
					break;
				}
			}
		}

		Object serializedChatComponent = fromJsonMethod.invoke(nmsChatSerializerGsonInstance, json, Reflection.getNMSClass("IChatBaseComponent"));

		return nmsPacketPlayOutChatConstructor.newInstance(serializedChatComponent);
	}

	public void send(CommandSender sender) {
		send(sender, toJSONString());
	}

	public void send(final Iterable<? extends CommandSender> senders) {
		String string = toJSONString();
		for (final CommandSender sender : senders) {
			send(sender, string);
		}
	}

	public String toOldMessageFormat() {
		StringBuilder result = new StringBuilder();
		for (MessagePart part : this) {
			result.append(part.color == null ? "" : part.color);
			for (ChatColor formatSpecifier : part.styles) {
				result.append(formatSpecifier);
			}
			result.append(part.text);
		}
		return result.toString();
	}

	private MessagePart latest() {
		return messageParts.get(messageParts.size() - 1);
	}

	private void onClick(final String name, final String data) {
		final MessagePart latest = latest();
		latest.clickActionName = name;
		latest.clickActionData = data;
		dirty = true;
	}

	private void onHover(final String name, final JsonRepresentedObject data) {
		final MessagePart latest = latest();
		latest.hoverActionName = name;
		latest.hoverActionData = data;
		dirty = true;
	}

	public Map<String, Object> serialize() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("messageParts", messageParts);
		return map;
	}

	@SuppressWarnings("unchecked")
	public static FormattedMessage deserialize(Map<String, Object> serialized) {
		FormattedMessage msg = new FormattedMessage();
		msg.messageParts = (List<MessagePart>) serialized.get("messageParts");
		msg.jsonString = serialized.containsKey("JSON") ? serialized.get("JSON").toString() : null;
		msg.dirty = !serialized.containsKey("JSON");
		return msg;
	}

	public Iterator<MessagePart> iterator() {
		return messageParts.iterator();
	}

	private static JsonParser _stringParser = new JsonParser();

	public static FormattedMessage deserialize(String json) {
		JsonObject serialized = _stringParser.parse(json).getAsJsonObject();
		JsonArray extra = serialized.getAsJsonArray("extra");
		FormattedMessage returnVal = new FormattedMessage();
		returnVal.messageParts.clear();
		for (JsonElement mPrt : extra) {
			MessagePart component = new MessagePart();
			JsonObject messagePart = mPrt.getAsJsonObject();
			for (Map.Entry<String, JsonElement> entry : messagePart.entrySet()) {
				if (TextualComponent.isTextKey(entry.getKey())) {
					Map<String, Object> serializedMapForm = new HashMap<String, Object>();
					serializedMapForm.put("key", entry.getKey());
					if (entry.getValue().isJsonPrimitive()) {
						serializedMapForm.put("value", entry.getValue().getAsString());
					} else {
						for (Map.Entry<String, JsonElement> compositeNestedElement : entry.getValue().getAsJsonObject().entrySet()) {
							serializedMapForm.put("value." + compositeNestedElement.getKey(), compositeNestedElement.getValue().getAsString());
						}
					}
					component.text = TextualComponent.deserialize(serializedMapForm);
				} else if (MessagePart.stylesToNames.inverse().containsKey(entry.getKey())) {
					if (entry.getValue().getAsBoolean()) {
						component.styles.add(MessagePart.stylesToNames.inverse().get(entry.getKey()));
					}
				} else if (entry.getKey().equals("color")) {
					component.color = ChatColor.valueOf(entry.getValue().getAsString().toUpperCase());
				} else if (entry.getKey().equals("clickEvent")) {
					JsonObject object = entry.getValue().getAsJsonObject();
					component.clickActionName = object.get("action").getAsString();
					component.clickActionData = object.get("value").getAsString();
				} else if (entry.getKey().equals("hoverEvent")) {
					JsonObject object = entry.getValue().getAsJsonObject();
					component.hoverActionName = object.get("action").getAsString();
					if (object.get("value").isJsonPrimitive()) {
						component.hoverActionData = new JsonString(object.get("value").getAsString());
					} else {
						component.hoverActionData = deserialize(object.get("value").toString());
					}
				} else if (entry.getKey().equals("insertion")) {
					component.insertionData = entry.getValue().getAsString();
				} else if (entry.getKey().equals("with")) {
					for (JsonElement object : entry.getValue().getAsJsonArray()) {
						if (object.isJsonPrimitive()) {
							component.translationReplacements.add(new JsonString(object.getAsString()));
						} else {
							component.translationReplacements.add(deserialize(object.toString()));
						}
					}
				}
			}
			returnVal.messageParts.add(component);
		}
		return returnVal;
	}
}
