package ru.bird.storageUtils.interfaces;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public interface Deserializable {

    default <T> T deserialize(Map<String, Object> args) throws IllegalArgumentException, NullPointerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        throw new IllegalArgumentException("rewrite this class");
    }

}