package ru.bird.inventories.menu;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.inventories.PageManager;
import ru.bird.inventories.menu.interfaces.GuiAction;
import ru.bird.inventories.menu.interfaces.GuiCallback;
import ru.bird.utils.ArraysUtils;

import java.util.*;

public class PageMenus {
    private JavaPlugin plugin;
    private String name;
    private LinkedList<Menu> menus;

    public PageMenus(JavaPlugin plugin, String name, List<ItemStack> items, GuiAction action, GuiCallback open){
        this.plugin = plugin;
        this.name = name;
        menus = new LinkedList<>();
        final int[] i = {0};
        List<ItemStack[]> original = ArraysUtils.splitArray(items.toArray(new ItemStack[0]), 45);
        original.forEach(list -> {
            int page = (i[0] + 1);
            Menu menu = new Menu(this.plugin,name + " страница " + page, 6);
            Inventory inventory = menu.getBukkitInventory();
            for(int g = 0; g < list.length; g++) {
                ItemStack item = list[g];
                inventory.setItem(g, item);
                if(action != null) {
                    menu.addAction(g, action);
                }
            }
            if(open != null) {
                menu.setGuiOpen(open);
            }
//            inventory.setContents(list);
            menus.add(i[0], menu);

            //назад
            if(i[0] != 0) {
                int curPage = (page - 1);
                ItemStack backItem = new ItemStack(Material.WHITE_WOOL);
                backItem.setAmount(curPage);
                ItemMeta meta = backItem.getItemMeta();
                meta.setLore(Collections.singletonList(ChatColor.GREEN + "На страницу " + curPage));
                meta.setDisplayName("Назад");
                backItem.setItemMeta(meta);
                inventory.setItem(45, backItem);
                menu.addAction(45, event -> {
                    Player player = (Player) event.getWhoClicked();
                    Integer current = PageManager.getPage(this.plugin, player);
                    int back = current - 1;
                    open(back, player);
                });
            }
            //вперёд
            if(i[0] != original.size() - 1) {
                int curPage = (page + 1);
                ItemStack nextItem = new ItemStack(Material.RED_WOOL);
                nextItem.setAmount(curPage);
                ItemMeta meta = nextItem.getItemMeta();
                meta.setLore(Collections.singletonList(ChatColor.GREEN + "На страницу " + curPage));
                meta.setDisplayName("Вперёд");
                nextItem.setItemMeta(meta);
                inventory.setItem(53, nextItem);
                menu.addAction(53, event -> {
                    Player player = (Player) event.getWhoClicked();
                    Integer current = PageManager.getPage(this.plugin, player);
                    int next = current + 1;
                    open(next, player);
                });
            }
            i[0]++;
        });
    }

    public boolean openStart(Player player){
        return open(0, player);
    }

    public boolean open(int i, Player player) {
        Menu menu = menus.get(i);
        if(menu != null) {
            PageManager.setPage(this.plugin, player, i);
            menu.showTo(player);
            return true;
        } else {
            return false;
        }
    }

    public String getName() {
        return name;
    }

    public LinkedList<Menu> getMenus() {
        return menus;
    }
}