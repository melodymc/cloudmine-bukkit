/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 */
package ru.bird.bedwars.config;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Villager;

import java.util.*;

public class Config {
    public static List<Integer> blocks;
    public static Location lobby;
    public static int team;
    public static int slots;
    public static List<Location> brick;
    public static List<Location> iron;
    public static List<Location> gold;
    public static Location[] spawns;
    public static List<Location>[] bed;
    public static ConfigurationSection list;
}