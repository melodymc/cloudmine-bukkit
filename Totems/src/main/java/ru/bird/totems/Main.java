package ru.bird.totems;

import com.ticxo.modelengine.api.ModelEngineAPI;
import com.ticxo.modelengine.api.manager.ModelManager;
import de.leonhard.storage.Json;
import nl.shanelab.multiblock.MultiBlockFactory;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.storageUtils.utils.StorageMeow;
import ru.bird.totems.commands.PatternCommands;
import ru.bird.totems.structures.ChickenSoupMultiBlock;
import ru.bird.totems.structures.RawChickenTomb;
import ru.bird.totems.structures.TestTombFromFile;

import java.io.*;

public class Main
extends JavaPlugin {
    private static Main staticThis;
    private File patternsDir;
    private File tombsDir;
    public ModelManager modelEngineApi;

    public static Main getThisClass() {
        return staticThis;
    }

    private Json json;

    public void onLoad() {
        patternsDir = new File(this.getDataFolder().getPath() + "/patterns");
        tombsDir = new File(this.getDataFolder().getPath() + "/tombs");
        staticThis = this;
        mainConfig();
    }

    private void mainConfig() {
        json = loadJsonConfig("main");
    }

    public Json getJson() {
        return json;
    }

    public Json loadJsonConfig(String name) {
        return StorageMeow.loadJsonConfig(name, this.getDataFolder());
    }

    public Json loadPatternConfig(String name) {
        return StorageMeow.loadJsonConfig(name, patternsDir);
    }
    public Json loadTombConfig(String name) {
        return StorageMeow.loadJsonConfig(name, tombsDir);
    }

    public void onEnable() {
        modelEngineApi = ModelEngineAPI.getModelManager();
        MultiBlockFactory.INSTANCE.register(this, RawChickenTomb.class);
        MultiBlockFactory.INSTANCE.register(this, ChickenSoupMultiBlock.class);
        MultiBlockFactory.INSTANCE.register(this, TestTombFromFile.class);
        PatternCommands commands = new PatternCommands();
        commands.register();
    }

    public File getPatternsDir() {
        if(!patternsDir.exists()) {
            patternsDir.mkdir();
        }
        return patternsDir;
    }
}