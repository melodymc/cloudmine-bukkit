/*
 * Decompiled with CFR 0_118.
 *
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.entity.Player
 *  org.bukkit.event.HandlerList
 *  org.bukkit.event.Listener
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  ru.wellfail.holograms.Holograms
 *  ru.wellfail.wapi.Main
 *  ru.wellfail.wapi.board.Board
 *  ru.wellfail.wapi.game.Game
 *  ru.wellfail.wapi.game.GameSettings
 *  ru.wellfail.wapi.game.Gamer
 *  ru.wellfail.wapi.listeners.DamageListener
 *  ru.wellfail.wapi.logger.WLogger
 *  ru.wellfail.wapi.utils.FIterator
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.crazywalls.game;

import com.wellfail.crazywalls.CrazyWalls;
import com.wellfail.crazywalls.sidebar.GameBoard;
import com.wellfail.crazywalls.config.Config;
import com.wellfail.crazywalls.game.GameListener;
import com.wellfail.crazywalls.game.GamePlayer;
import com.wellfail.crazywalls.kits.KitLoader;
import com.wellfail.crazywalls.shop.Shop;
import com.wellfail.crazywalls.shop.ShopLoader;
import com.wellfail.crazywalls.timer.Timer;
import com.wellfail.crazywalls.utils.Barrier;
import com.wellfail.crazywalls.utils.CWUtils;
import com.wellfail.crazywalls.wall.Wall;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import ru.wellfail.holograms.Holograms;
import ru.wellfail.wapi.Main;
import ru.wellfail.wapi.board.Board;
import ru.wellfail.wapi.game.Game;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.game.Gamer;
import ru.wellfail.wapi.listeners.DamageListener;
import ru.wellfail.wapi.logger.WLogger;
import ru.wellfail.wapi.utils.FIterator;
import ru.wellfail.wapi.utils.Utils;

public class GameFactory
        extends Game {
    public static Map<Player, GamePlayer> players = new HashMap<Player, GamePlayer>();
    public static Timer timer;
    public static Map<String, Shop> shops;

    public GameFactory() {
        super("CW", (Listener)new GameListener());
        this.loadConfiguration();
        GameFactory.createcolumns((String)"ALTER TABLE stats ADD cwfb INT DEFAULT 0, ADD cwdeaths INT DEFAULT 0,ADD cwwins INT DEFAULT 0, ADD cwgames INT DEFAULT 0, ADD cwkills INT DEFAULT 0;");
        new ShopLoader();
        new KitLoader();
    }

    private void loadConfiguration() {
        FileConfiguration config = CrazyWalls.getInstance().getConfig();
        try {
            Config.wallHeight = config.getInt("WallHeight");
            Config.greenWalls = CWUtils.getWalls(config, "GreenWalls");
            Config.redWalls = CWUtils.getWalls(config, "RedWalls");
            Config.locations = Utils.stringListToLocations((List)config.getStringList("Locations"));
            Config.delayColb = config.getInt("DelayColb");
            Config.traped = config.getString("Traped");
            Config.chest = config.getString("Chest");
            Config.replace = new ItemStack(config.getInt("Replace")).getType();
            Config.shops = Utils.stringListToLocations((List)config.getStringList("Shop"));
            Config.center = Utils.stringToLocation((String)config.getString("Center"));
            Config.radius = config.getInt("Radius");
            Config.barrier = new Barrier(Utils.stringToLocation((String)config.getString("BarrierMin")), Utils.stringToLocation((String)config.getString("BarrierMax")));
            GameSettings.respawnLocation = Utils.stringToLocation((String)config.getString("Lobby"));
            GameSettings.spectrLocation = Config.center;
        }
        catch (Exception ex) {
            WLogger.error((String)(GameFactory.getPrefix() + "\u041e\u0448\u0438\u0431\u043a\u0430 \u043f\u0440\u0438 \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0435 \u043a\u043e\u043d\u0444\u0438\u0433\u0443\u0440\u0430\u0446\u0438\u0438!" + ex.getMessage()));
        }
    }

    protected void onStartGame() {
        timer = new Timer(840);
        GameSettings.isBreak = false;
        GameSettings.isPlace = false;
        FIterator it = new FIterator(Config.locations);
        Bukkit.getOnlinePlayers().stream().forEach(player -> {
                    players.put(player, new GamePlayer(player));
                    CWUtils.sendStartMessages(player);
                    Location loc = (Location)it.getNext();
                    CWUtils.removeCell(player);
                    player.teleport(loc);
                    Utils.buildCell((Player)player);
                    player.teleport(loc);
                    CWUtils.giveStartItems(player);
                    CWUtils.removeCell(player);
                    new GameBoard(player);
                    shops.put(player.getName(), new Shop());
                }
        );
        ShopLoader.hologramses.stream().forEach(holograms -> {
                    holograms.showAll();
                }
        );
        final DamageListener dmg = new DamageListener();
        new BukkitRunnable(){

            public void run() {
                HandlerList.unregisterAll((Listener)dmg);
            }
        }.runTaskLater((Plugin)Main.getInstance(), (long)(Config.delayColb + 200));
        Board.spectator = new SpectatorBoard();
    }

    protected void onEndGame() {
        Collection<Gamer> gamers = Gamer.getGamers();
        Player lastAlive = Utils.getLastAlive();
        if (lastAlive == null) {
            return;
        }
        Bukkit.broadcastMessage((String)"\u00a7a\u00a7l\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac");
        Bukkit.broadcastMessage((String)"");
        Bukkit.broadcastMessage((String)"                               \u00a7c\u00a7lCrazy Walls");
        Bukkit.broadcastMessage((String)"");
        Bukkit.broadcastMessage((String)("                         \u00a76\u041f\u043e\u0431\u0435\u0434\u0438\u043b \u0438\u0433\u0440\u043e\u043a - " + lastAlive.getDisplayName()));
        Bukkit.broadcastMessage((String)"");
        Bukkit.broadcastMessage((String)"\u00a7a\u00a7l\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac");
        Gamer.getGamer((Player)lastAlive).addMoney(50);
        for (Gamer gamer : gamers) {
            if (players == null || gamer == null || gamer.getPlayer() == null || players.get((Object)gamer.getPlayer()) == null) continue;
            boolean win = false;
            if (lastAlive.getName().equals(gamer.getPlayer().getName())) {
                win = true;
            }
            String sql = "INSERT INTO `stats` (`name`, `cwfb`, `cwgames`, `cwkills`, `cwwins`, `cwdeaths`) VALUES ('" + gamer.getPlayer().getName() + "', '" + players.get((Object)gamer.getPlayer()).getFb() + "', '1'," + "'" + players.get((Object)gamer.getPlayer()).getKills() + "'," + "'" + (win ? 1 : 0) + "'," + "'" + (!win ? 1 : 0) + "')" + "ON DUPLICATE KEY UPDATE ";
            if (players.get(gamer.getPlayer()).getKills() != 0) {
                sql = sql + "`cwkills`=`cwkills`+'" + gamer.getKills() + "',";
            }
            if (!win) {
                sql = sql + "`cwdeaths`=`cwdeaths`+'1',";
            }
            if (win) {
                sql = sql + "`cwwins`=`cwwins`+'1',";
            }
            sql = sql + "`cwfb`=`cwfb`+'" + players.get(gamer.getPlayer()).getFb() + "',";
            sql = sql + "`cwgames`=`cwgames`+'1'";
            try {
                WLogger.debug((String)sql);
                GameFactory.saveStat((String)sql);
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void onRespawn(Player player) {
    }

    static {
        shops = new HashMap<String, Shop>();
    }

}