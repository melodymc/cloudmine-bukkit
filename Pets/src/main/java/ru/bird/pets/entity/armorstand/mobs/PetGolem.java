package ru.bird.pets.entity.armorstand.mobs;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import ru.bird.main.particles.ParticleEffect;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;

public class PetGolem extends AbstractArmoredPet {

	public PetGolem(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setArms(true);
		armor.setVisible(true);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}



	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getMainHand() {
		return new ItemStack(Material.PACKED_ICE);
	}

	@Override
	public ItemStack getOffHand() {
		return new ItemStack(Material.PACKED_ICE);
	}

	@Override
	public ItemStack getBoots() {
		ItemStack leggings = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta lim = (LeatherArmorMeta) leggings.getItemMeta();
		lim.setColor(Color.fromBGR(197, 183, 27));
		leggings.setItemMeta(lim);
		return leggings;
	}

	@Override
	public ItemStack getLeggings() {
		ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		LeatherArmorMeta lim = (LeatherArmorMeta) leggings.getItemMeta();
		lim.setColor(Color.fromBGR(197, 183, 27));
		leggings.setItemMeta(lim);
		return leggings;
	}

	@Override
	public ItemStack getChestplate() {
		ItemStack chestPlate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		LeatherArmorMeta cim = (LeatherArmorMeta) chestPlate.getItemMeta();
		cim.setColor(Color.fromBGR(197, 183, 27));
		chestPlate.setItemMeta(cim);
		return chestPlate;
	}

	@Override
	public ItemStack getHelmet() {
		ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta meta = (SkullMeta) head.getItemMeta();
		meta.setOwner("Golem");
		head.setItemMeta(meta);
		return head;
	}

	@Override
	public void onLivingUpdate() {
		this.setHeadPose(5 * MathUtils.cos(), 0, 5 * MathUtils.sin());
		this.setLeftArmPose(20 * MathUtils.cos(), 0, -10 + 10 * MathUtils.sin());
		this.setRightArmPose(20 * MathUtils.cos(), 0, 10 + 10 * MathUtils.cos());
		this.setLeftLegPose(15 + 5 * MathUtils.cos(), 0, 0);
		this.setRightLegPose(15 + 10 * MathUtils.sin(), 0, 0);
		this.sendWatcherUpdate();
		Location location = this.getPetLocation().clone().add(MathUtils.random(-0.5F, 0.5F), 0.2F + MathUtils.random(0.85F), MathUtils.random(-0.5F, 0.5F));
		ParticleEffect.REDSTONE.sendColor(Bukkit.getOnlinePlayers(), location, Color.AQUA);
		
	}

}
