package ru.bird.bedwars;

import ru.bird.bedwars.game.GameFactory;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.game.GameSettings;
import ru.bird.main.utils.GameUtils;

import java.util.ArrayList;

public class Main
extends JavaPlugin {
    private static Main instance;

    public static Main getInstance() {
        return instance;
    }

    public void onLoad() {
        instance = this;
    }

    public void onEnable() {
        this.saveDefaultConfig();
        GameSettings.itemdrop = true;
        GameSettings.prefix = "§b§lBedWars §8⋙§f ";
        GameSettings.wboardname = "§e§lBEDWARS";
        GameSettings.damage = true;
        GameSettings.isSpectate = true;
        GameSettings.damage = true;
        GameSettings.pickup = true;
        GameSettings.inventory = true;
        GameSettings.isBreak = true;
        GameSettings.isPlace = true;
        GameSettings.drop = false;
        GameSettings.fallDamage = true;
        GameSettings.physical = true;
        GameSettings.itemSpawn = true;
        GameSettings.chest = true;
        GameSettings.isFoodChange = true;
        GameSettings.teamChanger = true;
        GameSettings.respawn = false;
        GameSettings.crops = true;
        GameSettings.removearrowonhit = true;
//        ArrayList<String> list = new ArrayList<String>();
//        list.add("§e§lBEDWARS");
//        list.add("§e§lBEDWARS");
//        list.add("§e§lBEDWARS");
//        list.add("§e§lBEDWARS");
//        list.add("§e§lBEDWARS");
//        list.add("§6§lB§e§lEDWARS");
//        list.add("§f§lB§6§lE§e§lDWARS");
//        list.add("§f§lBE§6§lD§e§lWARS");
//        list.add("§f§lBED§6§lW§e§lARS");
//        list.add("§f§lBEDW§6§lA§e§lRS");
//        list.add("§f§lBEDWA§6§lR§e§lS");
//        list.add("§e§lBEDWAR§6§lS");
//        list.add("§f§lBEDWARS");
//        list.add("§e§lBEDWARS");
//        list.add("§f§lBEDWARS");
//        list.add("§e§lBEDWARS");
//        list.add("§f§lBEDWARS");
//        list.add("§e§lBEDWARS");
//        list.add("§e§lBEDWARS");
//        list.add("§e§lBEDWARS");
//        list.add("§e§lBEDWARS");
//        GameSettings.animation = list;
        GameSettings.gameTime = -1;
        if (this.getConfig().getBoolean("Setup")) {
            GameSettings.setup = true;
        }
        GameSettings.slots = this.getConfig().getInt("PlayersInTeam") * 4;
        GameSettings.toStart = GameSettings.slots - 4;
        GameSettings.mapLocation = GameUtils.stringToLocation(this.getConfig().getString("Map"));
        new GameFactory();
    }
}