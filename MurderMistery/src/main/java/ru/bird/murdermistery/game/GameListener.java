// 
// Decompiled by Procyon v0.5.30
// 

package ru.bird.murdermistery.game;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.Event;
import org.bukkit.material.Lever;
import org.golde.bukkit.corpsereborn.CorpseAPI.CorpseAPI;
import org.golde.bukkit.corpsereborn.nms.Corpses;
import ru.wellfail.wapi.events.DeathEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.*;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.util.EulerAngle;
import ru.bird.murdermistery.Main;
import ru.wellfail.wapi.actionbar.ActionBarApi;
import ru.wellfail.wapi.events.DeathEvent;
import ru.wellfail.wapi.events.KillEvent;
import ru.wellfail.wapi.game.Game;
import org.bukkit.event.EventPriority;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.Plugin;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.game.Gamer;
import ru.wellfail.wapi.listeners.FListener;
import ru.wellfail.wapi.logger.WLogger;
import ru.wellfail.wapi.utils.Title;
import ru.wellfail.wapi.utils.Utils;
import org.bukkit.scheduler.BukkitRunnable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.bukkit.event.Listener;

public class GameListener implements Listener
{

    public GameListener() {
    }

    private void checkEnd(Player player) {
        WLogger.info("lol 1");
        GPlayer gPlayer = GameFactory.getGplayer(player);
        WLogger.info("lol 2");
        Role role = gPlayer.getRole();
        if(Utils.getAlivePlayers().size() <= 1 || role == GameFactory.killer) {
            WLogger.info("lol 3" + role.getDisplayname());
            if(role == GameFactory.villager || role == GameFactory.commisar) {
                GameFactory.win_type = false;
            }
            if (role == GameFactory.killer) {
                GameFactory.win_type = true;
            }
            Game.getInstance().endGame();
        }
    }

    @EventHandler
    public void onKill(KillEvent e) {
        Player player = e.getPlayer();
        Player killer = e.getKiller();
        GPlayer gPlayer = GameFactory.getGplayer(player);
        GPlayer gPlayer_killer = GameFactory.getGplayer(killer);
        Role gPlayer_killerRole = gPlayer_killer.getRole();
        Role gPlayerRole = gPlayer.getRole();
        new BukkitRunnable() {
            public void run() {
                if (gPlayerRole == GameFactory.killer) {
                    if(gPlayer_killerRole == GameFactory.commisar) {
                        gPlayer_killer.addScore(1300);
                        gPlayer.setScore(0);
                    } else if (gPlayer_killerRole == GameFactory.villager) {
                        gPlayer_killer.addScore(700);
                        gPlayer.setScore(0);
                    }
                } else if (gPlayerRole == GameFactory.commisar) {
                    gPlayer_killer.addScore(1000);
                    gPlayer.setScore(0);
                    gPlayer.setRole(GameFactory.villager);
                    Title title = new Title("§6Детектив был убит!", "§eНайди лук и у тебя будет шанс остановить Убийцу!", 1, 3, 2);
                    Bukkit.getOnlinePlayers().forEach(title::send);
                    spawnBow(player.getLocation());
                } else if (gPlayerRole == GameFactory.villager) {
                    GameFactory.villagers.remove(player);
                    if(gPlayer_killerRole == GameFactory.commisar) {
                        gPlayer_killer.addScore(-200);
                    } else if (gPlayer_killerRole == GameFactory.killer) {
                        gPlayer_killer.addScore(500);
                    }
                    gPlayer.setScore(0);
                }
                checkEnd(player);
                if(Bukkit.getPluginManager().isPluginEnabled("CorpseReborn")) {
                    CorpseAPI.spawnCorpse(player, player.getLocation());
                }
            }
        }.runTaskLater(Main.getInstance(), 4);
    }

    private void spawnBow(Location loc) {
        if(!loc.getChunk().isLoaded()) {
            loc.getChunk().load(true);
        }
        ArmorStand armor = (ArmorStand)loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
        armor.setVisible(false);
        armor.getEquipment().setItemInMainHand(new ItemStack(Material.BOW));
        armor.setArms(true);
        armor.setGravity(false);
        EulerAngle eulerAngle = new EulerAngle(-189.0,-99.0,99.0);
        armor.setRightArmPose(eulerAngle);
        GameFactory.bowlist.add(armor);
        GameFactory.bow = loc;
        WLogger.info("bow loc - " + GameFactory.bow + " bow - " + eulerAngle.toString());
    }

    @EventHandler
    public void onItemDmg(final PlayerItemDamageEvent e) {
        final ItemStack item = e.getItem();
        if (item.getType().equals(Material.STONE_SWORD) || item.getType().equals(Material.BOW)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onSpawn(final CreatureSpawnEvent e) {
        if(!(e.getEntity() instanceof ArmorStand)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBurn(final BlockBurnEvent e) {
        e.setCancelled(true);
    }
    
    @EventHandler
    public void onBlockSpread(final BlockSpreadEvent e) {
        if (e.getSource().getTypeId() == 51) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(final BlockBreakEvent e) {
        e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlace(final BlockPlaceEvent e) {
        e.setCancelled(true);
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onExplode(final EntityExplodeEvent e) {
        e.blockList().clear();
    }

    @EventHandler
    public void onQuit(final PlayerQuitEvent e) {
        Player player = e.getPlayer();
        GPlayer gPlayer = GameFactory.getGplayer(player);
        Role role = gPlayer.getRole();
        Game.getInstance().onRespawn(player);
        if(role == GameFactory.commisar) {
            Title title = new Title("§6Детектив покинул нас!", "§eВышел из игры и лук был выброшен на месте его выхода!", 1, 3, 2);
            Bukkit.getOnlinePlayers().forEach(title::send);
        } else if(role == GameFactory.killer) {
            Title title = new Title("§6Убийца вышел!", "§eУбийца покинул игру, конец.", 1, 3, 2);
            Bukkit.getOnlinePlayers().forEach(title::send);
        }
        checkEnd(player);
    }

    @EventHandler
    public void onPickup(final PlayerPickupItemEvent e) {
        Player player = e.getPlayer();
        GPlayer gPlayer = GameFactory.getGplayer(player);
        Role role = gPlayer.getRole();
        Inventory inventory = e.getPlayer().getInventory();

        if(e.getItem().getItemStack().getData().getItemType() == Material.GOLD_INGOT) {
            e.setCancelled(true);
            e.getItem().remove();
            ItemStack itemStack = new ItemStack(Material.GOLD_INGOT);
            int count_in8_slot_new;
            if(inventory.getItem(8) == null) {
                count_in8_slot_new = 1;
            } else {
                count_in8_slot_new = inventory.getItem(8).getAmount()+1;
            }
            itemStack.setAmount(count_in8_slot_new);
            inventory.setItem(8, itemStack);
            ActionBarApi.sendActionBar(player, "§6+ 15 очков (золото)", 3);
            gPlayer.addScore(15);
        }
        if(inventory.contains(Material.GOLD_INGOT, 10) && role != GameFactory.killer) {
            //TODO дать лук и 1 стрелу, после выстрела забрать
            ActionBarApi.sendActionBar(player, "§6+ Поднял 10 золота", 3);
            for (int x = 0; x <= 10; x++) {
                inventory.remove(Material.GOLD_INGOT);
            }
            if(role == GameFactory.villager) {
                inventory.addItem(new ItemStack(Material.BOW));
            }
            inventory.setItem(2, new ItemStack(Material.ARROW));
        }
    }

    @EventHandler
    public void onDamageItemFrame(EntityDamageByEntityEvent e){
        if(e.getEntity() instanceof ItemFrame){
            e.setCancelled(true);
        }
        if(e.getDamager() == e.getEntity()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void clickOnEntity(PlayerInteractAtEntityEvent e){
        if(e.getRightClicked() instanceof ArmorStand || e.getRightClicked() instanceof ItemFrame || e.getRightClicked() instanceof Lever){
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void DamageByEntity(final EntityDamageByEntityEvent e) {
        if(e.getEntity() instanceof Player) {
            if (e.getDamager() instanceof Arrow) {
                Arrow arrow = (Arrow) e.getDamager();
                if (arrow.getShooter() instanceof Player) {
                    KillEvent event = new KillEvent((Player) e.getEntity(), (Player) arrow.getShooter());
                    Bukkit.getPluginManager().callEvent(event);
                }
            } else if (e.getDamager() instanceof Player) {
                Player player = (Player) e.getDamager();
                ItemStack inhand = player.getItemInHand();
                if (inhand.getData().getItemType() == Material.STONE_SWORD) {
                    KillEvent event = new KillEvent((Player) e.getEntity(), GameFactory.killer_pl);
                    Bukkit.getPluginManager().callEvent(event);
                } else {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent e) {
        Player p = e.getPlayer();
        Gamer gamer = Gamer.getGamer(p);
        if (!gamer.isSpectator()) {
            GPlayer gPlayer = GameFactory.getGplayer(p);
            Role role = gPlayer.getRole();
            if (role == GameFactory.villager) {
                for (Entity en : p.getNearbyEntities(1.0, 2.0, 1.0)) {
                    if (!(en instanceof ArmorStand) || !GameFactory.bowlist.contains(en)) continue;
                    ArmorStand am = (ArmorStand) en;
                    GameFactory.bowlist.remove(am);
                    GameFactory.getGplayer(p).setRole(GameFactory.commisar);
                    Title title = new Title("Лук подобран игроком");
                    Bukkit.getOnlinePlayers().forEach(title::send);
                    am.remove();
                    for (ItemStack itm : GameFactory.commisar.getStartInv()) {
                        p.getInventory().addItem(itm);
                    }
                }
            }
            if(GameFactory.compas) {
                if(role == GameFactory.villager || role == GameFactory.commisar) {
                    if (!GameFactory.killer_pl.getInventory().contains(GameFactory.compasitem)) {
                        GameFactory.killer_pl.getInventory().setItem(4, GameFactory.compasitem);
                    }
                    Main.getInstance().getServer().getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
                        public void run() {
                            GameFactory.killer_pl.setCompassTarget(GameFactory.compaspl.getLocation());
                        }
                    }, 40);// 60 L == 3 sec, 20 ticks == 1 sec
                }
            }
        }
    }
}