package ru.bird.pets.entity.armorstand.cart;

import ru.bird.main.particles.ParticleEffect;

import java.util.UUID;

public class PetEmeraldCart extends PetCart {

    public PetEmeraldCart(UUID id, int uniqueID) {
        super(id, uniqueID, null, "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmZjM2ZjZDE3MDVkYzAzYWRmMzFlNmRjOGU5Yzk1NDYwYzdjYTRkNzQ3YmU5YTY5YzRiY2RjNzVjNDMzNThlIn19fQ==");
    }
}