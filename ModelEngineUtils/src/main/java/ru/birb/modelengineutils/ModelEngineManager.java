package ru.birb.modelengineutils;

import java.util.*;

import com.ticxo.modelengine.api.model.ModeledEntity;
import com.ticxo.modelengine.api.model.component.StateProperty;
import com.ticxo.modelengine.api.model.component.ModelState;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.Location;
import com.ticxo.modelengine.api.model.component.ModelOption;
import org.bukkit.entity.LivingEntity;
import com.ticxo.modelengine.api.ModelEngineAPI;
import org.bukkit.entity.EntityType;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

public class ModelEngineManager
{
    private JavaPlugin plugin;
    private Entity entity;
    private boolean useModelEngine;
    private ArmorStand armorStand;
    private dc buildPet;
    private String modelId;
    private String state;
    private EulerAngle head;
    private EulerAngle body;
    private Vector preVec;
    private ItemStack idleItemStack;
    private ItemStack walkItemStack;
    private ItemStack attackItemStack;
    private String animationItemNameIdle;
    private String animationItemNameWalk;
    private String animationItemNameAttack;
    private int animationCustomModelDataIdle;
    private int animationCustomModelDataWalk;
    private int animationCustomModelDataAttack;
    private int animationFrameAttack;
    public Map<String, Integer> animationFrameMap;
    private double modelLocationH;
    private List animationModelList;
    public static List<ModelEngineManager> modelEntityList;
    private static List<ModelEngineManager> commonModel;

    public JavaPlugin getPlugin() {
        return this.plugin;
    }

    public void setItemStack(final Material material, final Material material2, final Material material3) {
        this.idleItemStack = new ItemStack(material);
        this.walkItemStack = new ItemStack(material2);
        this.attackItemStack = new ItemStack(material3);
    }

    public ModelEngineManager(JavaPlugin plugin, final Entity entity, final String modelId) {
        new ModelEngineManager(plugin, entity, modelId, Optional.empty(), Optional.empty(), Optional.empty());
    }

    public ModelEngineManager(JavaPlugin plugin, final Entity entity, final String modelId, final Optional<Material> material, final Optional<Material> material2, final Optional<Material> material3) {
        this.plugin = plugin;
        this.head = new EulerAngle(0.0, 0.0, 0.0);
        this.body = new EulerAngle(0.0, 0.0, 0.0);
        this.idleItemStack = new ItemStack(Material.DIAMOND_HOE);
        this.walkItemStack = new ItemStack(Material.DIAMOND_HOE);
        this.attackItemStack = new ItemStack(Material.DIAMOND_HOE);
        this.animationFrameMap = new LinkedHashMap();
        entity.setMetadata("imipet:model", new FixedMetadataValue(this.getPlugin(), true));
        this.modelId = modelId;
        this.entity = entity;
        this.setInVisible();
        this.setState("idle", false);
        if(material.isPresent() && material2.isPresent() && material3.isPresent()) {
            this.setItemStack(material.get(), material2.get(), material3.get());
        }
    }

    public void spawnModel(final boolean b) {
        if (!b) {
            final Location add = this.entity.getLocation().add(0.0, this.modelLocationH, 0.0);
            if (this.buildPet != null) {
                this.armorStand = da.a((ArmorStand)this.entity.getWorld().spawnEntity(add, EntityType.ARMOR_STAND), this.buildPet);
            }
            else {
                this.armorStand = da.a((ArmorStand)this.entity.getWorld().spawnEntity(add, EntityType.ARMOR_STAND), this.modelId);
            }
        }
        else {
            ModelEngineAPI.getModelManager().createModeledEntity((LivingEntity)this.entity, ModelOption.create(this.modelId), false);
        }
        ModelEngineManager.modelEntityList.add(this);
    }

    public void tpModel() {
        if (!this.isUseModelEngine()) {
            if (this.armorStand == null || this.entity == null) {
                return;
            }
            this.updateRotation();
            this.preVec = this.entity.getLocation().toVector();
            this.armorStand.teleport(this.entity.getLocation().add(0.0, this.modelLocationH, 0.0));
        }
    }

    public boolean isUseModelEngine() {
        return this.useModelEngine;
    }

    public void setUseModelEngine(final boolean useModelEngine) {
        this.useModelEngine = useModelEngine;
    }

    private void setInVisible() {
        if (this.entity instanceof LivingEntity) {
            ((LivingEntity)this.entity).addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 3, false, false), true);
        }
        if (this.entity instanceof ArmorStand) {
            ((ArmorStand)this.entity).setVisible(false);
        }
    }

    public static void fastCommonSpawnModel(final Entity entity, final String s) {
        if (db.m.get(s) == null) {
            return;
        }
        final db db = db.m.get(s);
        final ModelEngineManager modelEntityManager = new ModelEngineManager(entity, s);
        modelEntityManager.setAnimationItemNameIdle(db.f());
        modelEntityManager.setAnimationItemNameWalk(db.g());
        modelEntityManager.setAnimationItemNameAttack(db.h());
        modelEntityManager.setAnimationCustomModelDataIdle(db.c());
        modelEntityManager.setAnimationCustomModelDataWalk(db.d());
        modelEntityManager.setAnimationCustomModelDataAttack(db.e());
        modelEntityManager.setModelLocationH(db.a());
        modelEntityManager.setUseModelEngine(db.isUseModelEngine());
        modelEntityManager.spawnModel(db.isUseModelEngine());
        modelEntityManager.setAnimationModelList(db.getAnimationModelList());
        ModelEngineManager.commonModel.add(modelEntityManager);
    }

    public static void fastSpawnModel(final ModelEngineManager modelEntityManager, final String s, final boolean b) {
        if (db.m.get(s) == null) {
            return;
        }
        final db db = db.m.get(s);
        modelEntityManager.setAnimationItemNameIdle(db.f());
        modelEntityManager.setAnimationItemNameWalk(db.g());
        modelEntityManager.setAnimationItemNameAttack(db.h());
        modelEntityManager.setAnimationCustomModelDataIdle(db.c());
        modelEntityManager.setAnimationCustomModelDataWalk(db.d());
        modelEntityManager.setAnimationCustomModelDataAttack(db.e());
        modelEntityManager.setModelLocationH(db.a());
        modelEntityManager.spawnModel(b);
        modelEntityManager.setAnimationModelList(db.getAnimationModelList());
        ModelEngineManager.commonModel.add(modelEntityManager);
    }

    public void setAnimationItemNameIdle(final String animationItemNameIdle) {
        this.animationItemNameIdle = animationItemNameIdle;
    }

    public void setAnimationItemNameWalk(final String animationItemNameWalk) {
        this.animationItemNameWalk = animationItemNameWalk;
    }

    public void setAnimationItemNameAttack(final String animationItemNameAttack) {
        this.animationItemNameAttack = animationItemNameAttack;
    }

    public void setAnimationCustomModelDataAttack(final int animationCustomModelDataAttack) {
        this.animationCustomModelDataAttack = animationCustomModelDataAttack;
    }

    public void setAnimationCustomModelDataWalk(final int animationCustomModelDataWalk) {
        this.animationCustomModelDataWalk = animationCustomModelDataWalk;
    }

    public void setAnimationCustomModelDataIdle(final int animationCustomModelDataIdle) {
        this.animationCustomModelDataIdle = animationCustomModelDataIdle;
    }

    public void setModelLocationH(final double modelLocationH) {
        this.modelLocationH = modelLocationH;
    }

    public void addModel() {
        final db db = db.m.get(this.modelId);
        final ModeledEntity modeledEntity = ModelEngineAPI.getModelManager().getModeledEntity(this.entity.getUniqueId());
        final String s2 = state;
        switch (s2) {
            case "idle": {
                final Iterator iterator = modeledEntity.getModels(this.modelId).iterator();
                while (iterator.hasNext()) {
                    iterator.next().setState(ModelState.IDLE);
                }
            }
            case "walk": {
                final Iterator iterator2 = modeledEntity.getModels(this.modelId).iterator();
                while (iterator2.hasNext()) {
                    iterator2.next().setState(ModelState.WALK);
                }
            }
            case "attack": {
                if (this.animationFrameAttack < db.V()) {
                    final Iterator iterator3 = modeledEntity.getModels(this.modelId).iterator();
                    while (iterator3.hasNext()) {
                        iterator3.next().addState(state, StateProperty.create(this.animationFrameAttack, 0, 0));
                    }
                    ++this.animationFrameAttack;
                    break;
                }
                this.animationFrameAttack = 0;
                break;
            }
        }
    }

    public ed getAnimationModel(final String s) {
        if (this.animationModelList != null) {
            for (final ed ed : this.animationModelList) {
                if (ed.D().equals(s.toLowerCase())) {
                    return ed;
                }
            }
        }
        return null;
    }

    public List getAnimationModelList() {
        return this.animationModelList;
    }

    public void setAnimationModelList(final List animationModelList) {
        this.animationModelList = animationModelList;
    }

    private ItemStack itemModel(final String displayName, final int i, final ItemStack itemStack) {
        final ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(displayName);
        if (!ImiPet.getServerInfo().n()) {
            itemMeta.setCustomModelData(Integer.valueOf(i));
        }
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public void removeModel() {
        if (this.armorStand != null) {
            this.armorStand.remove();
        }
    }

    public static void removeAllModel() {
        if (!ModelEngineManager.modelEntityList.isEmpty()) {
            for (final ModelEngineManager modelEntityManager : ModelEngineManager.modelEntityList) {
                final dc buildPet = modelEntityManager.buildPet;
                if (buildPet != null) {
                    if (buildPet.a() != null) {
                        buildPet.a().delete();
                    }
                    if (buildPet.a() != null) {
                        buildPet.a().delete();
                    }
                    buildPet.k();
                    if (modelEntityManager.isUseModelEngine()) {
                        continue;
                    }
                    modelEntityManager.armorStand.remove();
                }
            }
            ModelEngineManager.modelEntityList.clear();
        }
        if (!ModelEngineManager.commonModel.isEmpty()) {
            for (final ModelEngineManager modelEntityManager2 : ModelEngineManager.commonModel) {
                modelEntityManager2.removeModel();
                modelEntityManager2.entity.remove();
            }
            ModelEngineManager.commonModel.clear();
        }
    }

    public void setState(final String state, final boolean b) {
        if (this.state == null) {
            this.state = state;
            return;
        }
        if (b) {
            this.state = state;
            return;
        }
        if (!this.state.startsWith("skill-")) {
            this.state = state;
        }
    }

    public String getState() {
        return this.state;
    }

    public Entity getEntity() {
        return this.entity;
    }

    private void updateRotation() {
        this.head = new EulerAngle(Math.toRadians(this.entity.getLocation().getPitch() % 360.0f), Math.toRadians(this.entity.getLocation().getYaw() % 360.0f), 0.0);
        if (this.getState().equals("attack")) {
            return;
        }
        if (this.preVec != null && !this.preVec.equals((Object)this.entity.getLocation().toVector())) {
            this.body = new EulerAngle(0.0, Math.toRadians((this.entity.getLocation().getYaw() + this.clamp(this.angleDiff(Math.toRadians(this.entity.getLocation().getYaw()), this.getAngle(this.entity.getLocation().toVector().subtract(this.preVec))), -50.0, 50.0) + 360.0) % 360.0), 0.0);
            this.preVec.setY(0);
            this.setState("walk", false);
        }
        else {
            this.body = new EulerAngle(0.0, Math.toRadians((this.entity.getLocation().getYaw() + this.clamp(this.angleDiff(Math.toRadians(this.entity.getLocation().getYaw()), this.body.getY()), -50.0, 50.0) + 360.0) % 360.0), 0.0);
            this.setState("idle", false);
        }
    }

    private double getAngle(final Vector direction) {
        final Location location = this.entity.getLocation();
        location.setDirection(direction);
        return Math.toRadians(location.getYaw());
    }

    private double clamp(final double b, final double a, final double a2) {
        return Math.max(a, Math.min(a2, b));
    }

    private double angleDiff(double angrad, double angrad2) {
        angrad = ((Math.toDegrees(angrad) > 180.0) ? (Math.toDegrees(angrad) - 360.0) : Math.toDegrees(angrad));
        angrad2 = ((Math.toDegrees(angrad2) > 180.0) ? (Math.toDegrees(angrad2) - 360.0) : Math.toDegrees(angrad2));
        double n = angrad2 - angrad;
        if (n > 180.0) {
            n -= 360.0;
        }
        else if (n < -180.0) {
            n += 360.0;
        }
        return n;
    }

    public String getModelId() {
        return this.modelId;
    }

    public int getAnimationFrameMap(final String key) {
        return this.animationFrameMap.get(key);
    }

    public void setAnimationFrameMap(final String key, final int i) {
        this.animationFrameMap.put(key, i);
    }

    public boolean existAnimationFrameMap(final String key) {
        return this.animationFrameMap.containsKey(key);
    }

    public void removeAnimationFrameMap(final String key) {
        this.animationFrameMap.remove(key);
    }

    static {
        ModelEngineManager.modelEntityList = new ArrayList<>();
        ModelEngineManager.commonModel = new ArrayList<>();
    }
}
