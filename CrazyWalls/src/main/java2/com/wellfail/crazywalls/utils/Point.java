// 
// Decompiled by Procyon v0.5.30
// 

package com.wellfail.crazywalls.utils;

import org.bukkit.World;

public class Point
{
    private int x;
    private int z;
    private World world;
    
    public Point(final int x, final int z, final World world) {
        this.x = x;
        this.z = z;
        this.world = world;
    }
    
    public World getWorld() {
        return this.world;
    }
    
    public int getX() {
        return this.x;
    }
    
    public int getZ() {
        return this.z;
    }
}
