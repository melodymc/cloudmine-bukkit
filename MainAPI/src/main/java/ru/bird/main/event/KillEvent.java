package ru.bird.main.event;

import org.bukkit.entity.Player;

public class KillEvent extends BasicEvent {
    private Player player;
    private Player killer;

    public Player getPlayer() {
        return this.player;
    }

    public Player getKiller() {
        return this.killer;
    }

    public KillEvent(Player player, Player killer) {
        this.player = player;
        this.killer = killer;
    }
}
