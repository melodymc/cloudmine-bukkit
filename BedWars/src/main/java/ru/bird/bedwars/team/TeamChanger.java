package ru.bird.bedwars.team;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.meta.ItemMeta;
import ru.bird.bedwars.config.Config;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.GameState;
import ru.bird.main.game.Gamer;
import ru.bird.main.groups.Groups;
import ru.bird.inventories.Item;
import ru.bird.inventories.ItemRunnable;
import ru.bird.inventories.menu.Menu;
import ru.bird.main.utils.ItemUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeamChanger
extends Menu {
    private Map<Item, Team> items = new HashMap<Item, Team>();

    public TeamChanger() {
        super("Выбор команд", 3);
        this.fillGui();
    }

    private void fillGui() {
        Team.teams.forEach(team -> {
            Item item = new Item(ItemUtils.createItem(Material.WOOL, 1, team.getSub(), this.getName(team), this.getLore(team)), new ItemRunnable(){

                public void onUse(PlayerInteractEvent playerInteractEvent) {
                }

                public void onClick(InventoryClickEvent e) {
                    e.setCancelled(true);
                    TeamChanger.this.chooseTeam((Player)e.getWhoClicked(), team);
                }
            }, new Action[]{Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK}, false, true);
            getBukkitInventory().setItem(team.getSlot(), item.getItem());
            this.items.put(item, team);
        }
        );
    }

    private void chooseTeam(Player player, Team team) {
        if (team.getSize() >= Config.team) {
            player.sendMessage(GameSettings.prefix + "В данной команде нет мест");
            return;
        }
        if (Team.select.containsKey(player) && Team.select.get(player).getColor() == team.getColor()) {
            player.sendMessage(GameSettings.prefix + "Вы уже выбрали эту команду");
            return;
        }
        player.sendMessage(GameSettings.prefix + "Вы выбрали команду " + (Object)team.getColor() + team.getName());
        if (Team.select.containsKey(player)) {
            Team.select.get(player).removePlayer(player);
        }
        team.addPlayer(player);
        player.getInventory().getItem(0).setDurability((short)team.getSub());
        this.updateItems();
    }

    private void updateItems() {
        for (Map.Entry<Item, Team> set : this.items.entrySet()) {
            Item item = set.getKey();
            ItemMeta meta = item.getItem().getItemMeta();
            meta.setLore(this.getLore(set.getValue()));
            meta.setDisplayName(set.getValue().getColor() + set.getValue().getName() + " §f- [§a" + set.getValue().getSize() + "/" + Config.team + "§f]");
            item.getItem().setItemMeta(meta);
            getBukkitInventory().setItem(set.getValue().getSlot(), item.getItem());
        }
    }

    private String getName(Team team) {
        String name = (Object)team.getColor() + team.getName() + " §f- [§a" + team.getSize() + "/" + Config.team + "§f]";
        return name;
    }

    private List<String> getLore(Team team) {
        ArrayList<String> lore = new ArrayList<String>();
        lore.add("");
        team.getPlayers().forEach(player -> {
            lore.add(player.getDisplayName());
        }
        );
        return lore;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        if (!Gamer.getGamer((Player)e.getPlayer()).isSpectator() && GameState.current != GameState.END) {
            Team.teams.forEach(team -> {
                team.removePlayer(e.getPlayer());
            }
            );
        }
        if (GameState.current == GameState.WAITING || GameState.current == GameState.STARTING) {
            this.updateItems();
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if (GameState.current == GameState.GAME) {
            return;
        }
        if (e.isCancelled()) {
            return;
        }
        String format = "%player%suffix: %message";
        if (format.contains("%prefix") || format.contains("%suffix")) {
            Player player = e.getPlayer();
            String prefix = Groups.getPrefix(player);
            String suffix = Groups.getSuffix(player);
            format = format.replace("%prefix", prefix).replace("%suffix", suffix);
        }
        format = format.replace("%message", "%2$s").replace("%player", "%1$s").replace("&", "§");
        e.setFormat(format);
    }

}

