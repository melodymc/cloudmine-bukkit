package ru.bird.chat.formatted;

import java.io.IOException;

import com.google.gson.stream.JsonWriter;


interface JsonRepresentedObject {

	public void writeJson(JsonWriter writer) throws IOException;
	
}
