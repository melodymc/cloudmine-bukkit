/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.entity.Player
 *  org.bukkit.event.HandlerList
 *  org.bukkit.event.Listener
 *  org.bukkit.event.block.Action
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  ru.wellfail.wapi.board.Board
 *  ru.wellfail.wapi.game.Game
 *  ru.wellfail.wapi.game.GameManager
 *  ru.wellfail.wapi.game.GameSettings
 *  ru.wellfail.wapi.game.Gamer
 *  ru.wellfail.wapi.gui.Item
 *  ru.wellfail.wapi.gui.ItemRunnable
 *  ru.wellfail.wapi.logger.BaseLogger
 *  ru.wellfail.wapi.utils.Utils
 */
package ru.bird.bedwars.game;

import org.bukkit.configuration.file.YamlConfiguration;
import ru.bird.bedwars.Main;
import ru.bird.bedwars.config.Config;
import ru.bird.bedwars.shop.Shop;
import ru.bird.bedwars.sidebar.SpectatorBoard;
import ru.bird.bedwars.spawner.Spawner;
import ru.bird.bedwars.team.Team;
import ru.bird.bedwars.team.TeamChanger;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.*;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.bird.main.API;
import ru.bird.main.actionbar.ActionBarApi;
import ru.bird.main.board.api.Board;
import ru.bird.config.ConfigFile;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameManager;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.Gamer;
import ru.bird.inventories.ItemRunnable;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.npc.NPCCreator;
import ru.bird.main.utils.GameUtils;
import ru.bird.inventories.Item;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public class GameFactory
extends Game {
    public static Map<Player, GamePlayer> players = new HashMap<Player, GamePlayer>();
    TeamChanger teamChangerr;
    public static Team win;
    private Shop shop;
    private File shopYmlFile;
    public static ConfigFile shopConfig;


    public GameFactory() {
        super(new GameListener());
        this.shopConfig();
        this.loadConfig();
        this.loadTeams();
        this.loadGame();
        this.loadVillagers();
        //GameFactory.createcolumns("ALTER TABLE stats ADD swfb INT DEFAULT 0, ADD swdeaths INT DEFAULT 0,ADD swwins INT DEFAULT 0, ADD swgames INT DEFAULT 0, ADD swkills INT DEFAULT 0;");
    }

    private void loadVillagers() {
        try {
            FileConfiguration config = Main.getInstance().getConfig();
            ConfigurationSection tmp = config.getConfigurationSection("Villagers");
            if(tmp == null) {
                BaseLogger.info("Не грузим жителей? Мб не настроено, проверь");
                return;
            }
            Set<String> all = tmp.getKeys(false);
            for (String element: all) {
//                BaseLogger.info("[Villagers] El "+ element);
                Integer type = config.getInt("Villagers."+ element + ".type");
//                BaseLogger.info("[Villagers] El type "+ type);
                Location loc = GameUtils.stringToLocation(config.getString("Villagers." + element + ".location"));
                NPCCreator npc = new NPCCreator(loc);
                Villager v = npc.createNPC(true);
                switch (type) {
                    case 1:
                        v.setCustomName("Магазин командных улучшений");
                        break;
                    default:
                        v.setCustomName("Магазин");
                        break;
                }
            }
        }
        catch (Exception ex) {
            BaseLogger.error("[BW] Ошибка при загрузке конфигурации.");
            ex.printStackTrace();
        }
    }

    private void shopConfig() {
        File file = new File(Main.getInstance().getDataFolder(), "kits.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
        final InputStream defConfigStream1 = Main.getInstance().getResource("kits.yml");
        if (defConfigStream1 != null) {
            final YamlConfiguration defConfig1;
            try {
                defConfig1 = YamlConfiguration.loadConfiguration(new BufferedReader(new InputStreamReader(defConfigStream1, "UTF-8")));
                config.setDefaults(defConfig1);
                BaseLogger.error("[BedWars] kits.yml был успешно загружен.");
            } catch (UnsupportedEncodingException e) {
                BaseLogger.error("[BedWars] Не загружен kits.yml");
                e.printStackTrace();
            }
        }
        try {
            config.options().copyDefaults(true);
            config.save(file);
        } catch (IOException e) {
            BaseLogger.error("[BedWars] Не загружен kits.yml");
        }
        shopConfig = new ConfigFile(config, file);

        shopSave(shopConfig);
    }

    public void shopSave(ConfigFile configFile) {
        if (configFile.getFile() == null || configFile.getMemory() == null) {
            return;
        }
        try {
            configFile.save();
        }
        catch (IOException ex) {
            System.out.println("Ошибка при чтении файла kits.yml конфигурации.");
        }
        configFile.getMemory().options().copyDefaults(true);
    }

    private void loadConfig() {
        try {
            FileConfiguration config = Main.getInstance().getConfig();
            Config.blocks = config.getIntegerList("Blocks");
            Config.lobby = GameUtils.stringToLocation(config.getString("Spawns.Lobby"));
            Config.team = config.getInt("PlayersInTeam");
            Config.slots = Config.team * 4;
            Config.brick = GameUtils.stringListToLocations(config.getStringList("Items.Brick"));
            Config.iron = GameUtils.stringListToLocations(config.getStringList("Items.Iron"));
            Config.gold = GameUtils.stringListToLocations(config.getStringList("Items.Gold"));
            Config.spawns = new Location[4];
            Config.spawns[0] = GameUtils.stringToLocation(config.getString("Spawns.Red"));
            Config.spawns[1] = GameUtils.stringToLocation(config.getString("Spawns.Blue"));
            Config.spawns[2] = GameUtils.stringToLocation(config.getString("Spawns.Green"));
            Config.spawns[3] = GameUtils.stringToLocation(config.getString("Spawns.Yellow"));
            Config.bed = new ArrayList[4];
            Config.bed[0] = GameUtils.stringListToLocations(config.getStringList("Bed.Red"));
            Config.bed[1] = GameUtils.stringListToLocations(config.getStringList("Bed.Blue"));
            Config.bed[2] = GameUtils.stringListToLocations(config.getStringList("Bed.Green"));
            Config.bed[3] = GameUtils.stringListToLocations(config.getStringList("Bed.Yellow"));
            GameSettings.respawnLocation = GameUtils.stringToLocation(config.getString("Spawns.Lobby"));
            GameSettings.spectrLocation = GameUtils.stringToLocation(config.getString("Spawns.Spectator"));
            Config.list = shopConfig.getMemory().getConfigurationSection("Menu");
        }
        catch (Exception ex) {
            BaseLogger.error("[BW] Ошибка при загрузке конфигурации.");
            ex.printStackTrace();
        }
    }

    public static Color getColor(ChatColor color) {
        return color == ChatColor.RED?Color.RED:(color == ChatColor.GREEN?Color.GREEN:(color == ChatColor.BLUE?Color.BLUE:(color == ChatColor.YELLOW?Color.YELLOW:null)));
    }

    private void loadTeams() {
        Team.teams.add(new Team("Красных", "Красные", Config.spawns[0], Config.bed[0], ChatColor.RED, 14, 10));
        Team.teams.add(new Team("Синих", "Синие", Config.spawns[1], Config.bed[1], ChatColor.BLUE, 11, 12));
        Team.teams.add(new Team("Зеленых", "Зелёные", Config.spawns[2], Config.bed[2], ChatColor.GREEN, 5, 14));
        Team.teams.add(new Team("Желтых", "Жёлтые", Config.spawns[3], Config.bed[3], ChatColor.YELLOW, 4, 16));
    }

    public static GamePlayer getGamePlayer(Player p) {
        return players.get(p);
    }

    private void loadGame() {
        shop = new Shop();
        this.teamChangerr = new TeamChanger();
        ItemStack team = new ItemStack(Material.WHITE_WOOL);
        ItemMeta meta = team.getItemMeta();
        meta.setDisplayName("§a[‣] §fВыбор команды  §7(ПКМ)");
        team.setItemMeta(meta);
        this.teamChanger = new Item(team, new ItemRunnable(){
            public void onUse(PlayerInteractEvent e) {
                GameFactory.this.teamChangerr.open(e.getPlayer());
            }

            public void onClick(InventoryClickEvent paramInventoryClickEvent) {
            }
        }, new Action[]{Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK}, true);
        //        BaseLogger.info("sidebar " + sidebar.getLines().toString());
        GameSettings.spectatorSidebar = new SpectatorBoard();
    }

    public void onRespawn(Player player) {}

    private void teamSet() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (Team.select.containsKey(player)) continue;
            Team team2 = getMinTeam();
            if (team2 == null) {
                player.kickPlayer("not seted ru.bird.jobs.command?");
                GameManager.redirectToLobby(player);
                continue;
            }
            if (team2.getSize() == Config.team) continue;
            team2.addPlayer(player);
        }
        Team.teams.forEach(team -> {
            team.teleportToSpawn();
            team.setTags();
        }
        );
    }

    private Team getMinTeam() {
        Team team = null;
        for (Team t : Team.teams) {
            if (t.getSize() == Config.team) continue;
            if (team == null) {
                team = t;
                continue;
            }
            if (t.getSize() >= team.getSize()) continue;
            team = t;
        }
        return team;
    }

    protected void onStartGame() {
        this.teamSet();
        Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("                                     §c§lBedWars");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("                §eТвоя главная цель - сломать кровати, расположенные на");
        Bukkit.broadcastMessage("                         §eостровах твоих врагов. Желаем удачи!");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        new Spawner(Config.brick, Config.iron, Config.gold);
        HandlerList.unregisterAll(this.teamChangerr);
        for(Player player : players.keySet()) {
            startUpdateActionBar(player);
        }
//        GameSettings.spectatorSidebar = new SpectatorSidebar();
    }

    private void startUpdateActionBar(Player player) {
        Main.getInstance().getServer().getScheduler().runTaskTimer(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                GamePlayer gamer = GameFactory.getGamePlayer(player);
                int num = gamer.getBalance();
                ActionBarApi.sendActionBar(player, "§8▸ §fОчки: §b☄" + num, 40, false);

            }
        }, 1, 40);
    }

    protected void onEndGame() {
        Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("                               §c§lBed Wars");
        Bukkit.broadcastMessage("");
        if (win != null) {
            Bukkit.broadcastMessage(("                         §6Победила команда - " + win.getColor() + win.getName()));
        } else {
            Bukkit.broadcastMessage("                         §6Победила команда - Ошибка");
        }
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        for (Gamer g : Gamer.getGamers()) {
            Player player = g.getPlayer();
            GamePlayer gplayer = players.get(player);
            Gamer gamer = Gamer.getGamer(player);
            if(g.isSpectator()) {
                gplayer.getGameSidebar().hide();
            }
            // порядок процедуры
            //`uuid`,
            //`bwbed`,
            //`bwkills`,
            //`bwwins`,
            //`bwfb`,
            //`bwdeaths`
            try {
                PreparedStatement st = API.getInstance().getSQLConnection().getConnection().prepareStatement("CALL save_stat_bw(?, ?, ?, ?, ?, ?);");
                st.setString(1, player.getUniqueId().toString());
                st.setInt(2, gplayer.beds);
                st.setInt(3, gamer.getKills());
                st.setBoolean(4, gplayer.win);
                st.setBoolean(5, gplayer.fb);
                st.setInt(6, gamer.getDeaths());
                BaseLogger.debug(st.toString());
                st.executeUpdate();
            } catch (SQLException e) {
                BaseLogger.error(e.getMessage());
            }
//            String sql = "CALL `save_stat_bw`('" + player.getUniqueId() + "', " +
//                    "'" + gplayer.beds + "', " +
//                    "'" + gamer.getKills() + "', " +
//                    "'" + (gplayer.win ? 1 : 0) + "', " +
//                    "'" + (gplayer.fb ? 1 : 0) + "', " +
//                    "'" + gamer.getDeaths() + "', " +
//                    "'1')";
//            GameFactory.saveStat(sql);
        }
    }
}