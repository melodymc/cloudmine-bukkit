package ru.bird.jobs.jobgame.miner;

import ru.bird.jobs.jobgame.generator.GenerableItem;
import ru.bird.jobs.jobgame.generator.GeneratorItem;
import ru.bird.jobs.jobgame.generator.ItemExtractor;

public final class ItemData implements GenerableItem<GeneratorItem>
{
    public final static ItemData IMPOSSIBLE = new ItemData(GeneratorItem.IMPOSSIBLE, 0);

    public final GeneratorItem aggregatedItem;

    public final int reward;
    public final boolean isTrash;
    public final int bonus;
    public final double bonusChance;

    public ItemData(GeneratorItem aggregatedItem, int reward, boolean isTrash, int bonus, double bonusChance)
    {
        this.aggregatedItem = aggregatedItem;

        this.reward = reward;
        this.isTrash = isTrash;
        this.bonus = bonus;
        this.bonusChance = bonusChance;
    }

    public ItemData(GeneratorItem aggregatedItem, int reward)
    {
        this(aggregatedItem, reward, false, 0, 0.0d);
    }

    @Override
    public GeneratorItem getAggregatedItem()
    {
        return aggregatedItem;
    }

    @Override
    public <B> void invokeExtractor(ItemExtractor<B> extractor)
    {
        extractor.invoke(this);
    }
}
