package ru.bird.main.board.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import ru.bird.main.board.api.Board;

public class PlayerListener
    implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent event) {
        for(Board board : Board.REGISTERED_BOARDS) {
            board.getObjective().send(event.getPlayer());
            board.lineWrappers().forEach(line -> line.update(event.getPlayer()));
        }
    }
}