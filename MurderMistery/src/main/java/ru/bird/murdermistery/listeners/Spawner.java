package ru.bird.murdermistery.listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import ru.bird.murdermistery.Main;
import ru.wellfail.wapi.logger.WLogger;

import java.util.List;

public class Spawner
{
    private List<Location> locations;
    List<BukkitTask> tasks;
    Main main = Main.getInstance();

    public Spawner(final List<Location> locations) {
        this.locations = locations;
        this.start();
    }

    public void cancel() {
        this.tasks.forEach(BukkitTask::cancel);
    }

    private void start() {
        this.tasks.add(new BukkitRunnable() {
            public void run() {
                WLogger.info("spawn gold");
                Spawner.this.locations.forEach(loc -> loc.getWorld().dropItem(loc, new ItemStack(Material.GOLD_INGOT)));
            }
        }.runTaskTimer(main, 0L, 900L));
    }
}