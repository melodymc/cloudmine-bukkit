// 
// Decompiled by Procyon v0.5.30
// 

package ru.bird.murdermistery.board;

import org.bukkit.ChatColor;
import ru.bird.main.board.api.Board;
import ru.bird.main.board.api.BoardUpdater;
import ru.bird.main.board.api.PersonalBoard;
import ru.bird.murdermistery.game.GameFactory;
import ru.bird.murdermistery.listeners.StartListener;

import java.text.SimpleDateFormat;
import java.util.*;

import org.bukkit.entity.Player;

public class GameBoard
{
    private List<String> list;
    private Player player;
    private int i;

    public GameBoard(final Player player) {
        this.player = player;
        this.list = new ArrayList<>();
        list.add("§e§lMURDER MYSTERY");
        list.add("§e§lMURDER MYSTERY");
        list.add("§e§lMURDER MYSTERY");
        list.add("§e§lMURDER MYSTERY");
        list.add("§6§lM§e§lURDER MYSTERY");
        list.add("§f§lM§6§lU§e§lRDER MYSTERY");
        list.add("§f§lMU§6§lR§e§lDER MYSTERY");
        list.add("§f§lMUR§6§lD§e§lER MYSTERY");
        list.add("§f§lMURD§6§lE§e§lR MYSTERY");
        list.add("§f§lMURDE§6§lR§e§l MYSTERY");
        list.add("§f§lMURDER§6§l §e§lMYSTERY");
        list.add("§f§lMURDER §6§lM§e§lYSTERY");
        list.add("§f§lMURDER M§6§lY§e§lSTERY");
        list.add("§f§lMURDER MY§6§lS§e§lTERY");
        list.add("§f§lMURDER MYS§6§lT§e§lERY");
        list.add("§f§lMURDER MYST§6§lE§e§lRY");
        list.add("§f§lMURDER MYSTE§6§lR§e§lY");
        list.add("§f§lMURDER MYSTEr§6§lY§e§l");
        list.add("§f§lMURDER MYSTE§6§lR§e§lY");
        list.add("§f§lMURDER MYST§6§lE§e§lRY");
        list.add("§f§lMURDER MYS§6§lT§e§lERY");
        list.add("§f§lMURDER MY§6§lS§e§lTERY");
        list.add("§f§lMURDER M§6§lY§e§lSTERY");
        list.add("§f§lMURDER §6§lM§e§lYSTERY");
        list.add("§e§lMURDE§6§lR§e§l MYSTERY");
        list.add("§f§lMURDE§6§lR§e§l MYSTERY");
        list.add("§f§lMURD§6§lE§e§lR MYSTERY");
        list.add("§f§lMUR§6§lD§e§lER MYSTERY");
        list.add("§f§lMU§6§lR§e§lDER MYSTERY");
        list.add("§f§lM§6§lU§e§lRDER MYSTERY");
        list.add("§6§lM§e§lURDER MYSTERY");
        list.add("§f§lMURDER MYSTERY");
        list.add("§f§lMURDER MYSTERY");
        list.add("§f§lMURDER MYSTERY");
        list.add("§f§lMURDER MYSTERY");
        list.add("§f§lMURDER MYSTERY");
        Date dateNow = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd/MM/yyyy");
        Board.getPersonalBuilder("§e§lMURDER MISTERY", player)
        .write(11, ChatColor.GRAY + "")
        .write(10, formatForDateNow.format(dateNow))
        .write(9, "Роль: §a" + GameFactory.getGplayer(this.player).getRole().getDisplayname())
        .write(8, ChatColor.BLACK + "")
        .write(7, "Жителей осталось: §a" + GameFactory.villagers.size())
        .write(6, "Таймер: §a" + getTimeToEnd(StartListener.getTime()))
        .write(5, ChatColor.AQUA + "")
        .write(4, GameFactory.bow != null ? "§aЛук потерян": "§cЛук не потерян")
        .write(3, ChatColor.GREEN + "")
        .write(2, "Очки: §a" + GameFactory.getGplayer(this.player).getScore())
        .write(1, ChatColor.DARK_GREEN + "")
        .updater(20, board -> {
            if(board instanceof PersonalBoard) {
                PersonalBoard pBoard = (PersonalBoard) board;
                if(i >= list.size()) {
                    i = 0;
                }
                board.setDisplay(list.get(i));
                pBoard.modifyLine(2, "Очки" + GameFactory.getGplayer(pBoard.getOwner()).getScore());
                pBoard.modifyLine(5, GameFactory.bow != null ? "§aЛук потерян": "§cЛук не потерян");
                pBoard.modifyLine(6, "Таймер: §a" + getTimeToEnd(StartListener.getTime()));
                pBoard.modifyLine(7, "Жителей осталось: §a" + GameFactory.villagers.size());
                pBoard.modifyLine(9, "Роль: §a" + GameFactory.getGplayer(this.player).getRole().getDisplayname());
            }
            i++;
        });
    }
    
//    public Set<BoardLine> getLines() {
//        final Set<BoardLine> set = new HashSet<BoardLine>();
//        set.add(this.date = new BoardLine(this, "", 12));
//        Date dateNow = new Date();
//        SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd/MM/yyyy");
//        this.dynamicLine(this.date.getNumber(), "§7", formatForDateNow.format(dateNow) + "");
//
//        set.add(new BoardLine(this, "§2", 11));
//
//        set.add(this.role = new BoardLine(this, "Роль: §a", 10));
//        String role_str = GameFactory.getGplayer(this.player).getRole().getDisplayname();
//        this.dynamicLine(this.role.getNumber(), "Роль: §a", role_str + "");
//
//        set.add(new BoardLine(this, "§2", 9));
//
//        set.add(this.players = new BoardLine(this, "Жителей осталось: §a", 8));
//        this.dynamicLine(this.players.getNumber(), "Жителей осталось: §a", GameFactory.villagers.size() + "");
//
//        set.add(this.timer = new BoardLine(this, "Таймер: §a", 7));
//        final String timer = GameBoard.this.getTimeToEnd(StartListener.getTime());
//        this.dynamicLine(this.timer.getNumber(), "Таймер: §a", timer + "");
//        set.add(new BoardLine(this, "§2", 6));
//        set.add(this.bow = new BoardLine(this, "Лук: ", 5));
//        final boolean bow = GameFactory.bow != null;
//        this.dynamicLine(this.bow.getNumber(), "", bow ? "§aЛук потерян": "§cЛук не потерян");
//        set.add(new BoardLine(this, "§2", 4));
//        set.add(this.score = new BoardLine(this, "Очки: §a", 3));
//        this.dynamicLine(this.score.getNumber(), "Очки: §a", GameFactory.getGplayer(this.player).getScore() + "");
//        set.add(new BoardLine(this, "§2", 2));
//        this.update(new Runnable() {
//            @Override
//            public void run() {
//                String role_str = GameFactory.getGplayer(GameBoard.this.player).getRole().getDisplayname();
//                GameBoard.this.dynamicLine(GameBoard.this.role.getNumber(), "Роль: §a", role_str + "");
//            }
//        }, 100L);
//        this.update(new Runnable() {
//            @Override
//            public void run() {
//                GameBoard.this.dynamicLine(GameBoard.this.players.getNumber(), "Жителей осталось: §a", "" + GameFactory.villagers.size());
//                GameBoard.this.dynamicLine(GameBoard.this.score.getNumber(), "Очки: §a", "" + GameFactory.getGplayer(player).getScore());
//                GameBoard.this.dynamicLine(GameBoard.this.timer.getNumber(), "Таймер: §a", GameBoard.this.getTimeToEnd(StartListener.getTime()) + "");
//                final boolean bow = GameFactory.bow != null;
//                GameBoard.this.dynamicLine(GameBoard.this.bow.getNumber(), "", bow ? "§aЛук потерян": "§cЛук не потерян");
//            }
//        }, 20L);
//        return set;
//    }

    private String getTimeToEnd(final int t) {
        final int minutes = t / 60;
        final int seconds = t - minutes * 60;
        String m = String.valueOf(minutes);
        String s = String.valueOf(seconds);
        if (m.length() == 1) {
            m = "0" + m;
        }
        if (s.length() == 1) {
            s = "0" + s;
        }
        return m + ":" + s;
    }
}