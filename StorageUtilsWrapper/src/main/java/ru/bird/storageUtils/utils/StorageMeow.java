package ru.bird.storageUtils.utils;

import de.leonhard.storage.Json;
import de.leonhard.storage.internal.settings.ReloadSettings;

import java.io.File;

public class StorageMeow {
    public static Json loadJsonConfig(String name, File folder) {
        return loadJsonConfig(name, folder, ReloadSettings.MANUALLY);
    }

    public static Json loadJsonConfig(String name, File folder, ReloadSettings reloadSettings) {
        return new Json(name + ".json", folder.getPath(), null, reloadSettings);
    }
}
