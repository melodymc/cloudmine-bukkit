package nl.shanelab.multiblock.validators;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.Step;

import nl.shanelab.multiblock.IMaterialValidator;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class SlabValidator implements IMaterialValidator {
	
	private Material material;
	
	private Material stepType;
	
	private boolean up;
	
	private boolean sided;

	public SlabValidator() {}
	
	public SlabValidator(Material material) {
		this(material, false);
		
		this.sided = false;
	}
	
	public SlabValidator(Material material, Material alternative) {
		this(material, false, alternative);
		
		this.sided = false;
	}
	
	public SlabValidator(Material material, boolean up) {
		this(material, up, Material.LEGACY_STEP);
	}

	public SlabValidator(Material material, boolean up, Material alternative) {
		this.material = material;
		this.stepType = alternative;
		this.up = up;
		this.sided = true;
	}

	private SlabValidator(Material material, Material alternative, boolean up, boolean sided) {
		this.material = material;
		this.stepType = alternative;
		this.up = up;
		this.sided = sided;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean validateBlock(Block block) {
		if (block == null || block.getType() != stepType){
			return false;
		}
		
		if (stepType == Material.LEGACY_STEP) {
			Step step = (Step) block.getState().getData();
			
			return step.getMaterial() == material && (!sided || (sided && step.isInverted() == up));
		} else {
			// 0 = down
			// 8 = top
			byte data = block.getData();
			return block.getState().getType() == material && (!sided || (sided && ((up && data == 8) || (!up && data == 0))));
		}
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> result = IMaterialValidator.super.serialize();
		result.put("material", material.toString());
		result.put("stepType", stepType.toString());
		result.put("up", up);
		result.put("sided", sided);
		return result;
	}

	@Override
	public <T> T deserialize(Map<String, Object> args) throws IllegalArgumentException, NullPointerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return (T) new SlabValidator(Material.valueOf((String) args.get("material")), Material.valueOf((String) args.get("stepType")), (boolean) args.get("up"), (boolean) args.get("material"));
	}
}