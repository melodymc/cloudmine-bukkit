package pir.teraingenerator;

import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.Random;

public final class WorldUtils
{
    public static final int CHUNK_SIZE = 16;

    public static long getSeed(String rawSeed)
    {
        long seed = (new Random()).nextLong();
        if (rawSeed.isEmpty() == false)
        {
            try
            {
                long parsed = Long.parseLong(rawSeed);

                if (parsed != 0L)
                    seed = parsed;
            }
            catch (NumberFormatException exception)
            {
                seed = rawSeed.hashCode();
            }
        }

        return seed;
    }

    public static void copyChunk(World supplier, int sx, int sz, World consumer, int cx, int cz)
    {
        Chunk supplierChunk = supplier.getChunkAt(sx, sz);
        Chunk consumerChunk = consumer.getChunkAt(cx, cz);

        if ( !supplierChunk.isLoaded() )
        {
            supplierChunk.load(true);
        }

        int minHeight = Math.min(supplier.getMaxHeight(), consumer.getMaxHeight());
        for (int x = 0; x < CHUNK_SIZE; x++)
        {
            for (int z = 0; z < CHUNK_SIZE; z++)
            {
                for (int y = 0; y < minHeight; y++)
                {
                    Block supplierBlock = supplierChunk.getBlock(x, y, z);
                    Block consumerBlock = consumerChunk.getBlock(x, y, z);

                    consumerBlock.setBlockData( supplierBlock.getBlockData() );
                }
            }
        }
    }
}
