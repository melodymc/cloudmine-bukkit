package ru.bird.pets.entity.armorstand.humanoid;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;

public class PetSteampunkGirl extends AbstractArmoredPet {

	public PetSteampunkGirl(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setArms(true);
		armor.setVisible(true);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}



	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getMainHand() {
		return new ItemStack(Material.IRON_INGOT);
	}

	@Override
	public ItemStack getOffHand() {
		return null;
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		return new ItemStack(Material.LEATHER_LEGGINGS, 1);
	}

	@Override
	public ItemStack getChestplate() {
		return new ItemStack(Material.LEATHER_CHESTPLATE, 1);
	}

	@Override
	public ItemStack getHelmet() {
		ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta meta = (SkullMeta) head.getItemMeta();
		meta.setOwner("SteampunkGirl");
		head.setItemMeta(meta);
		return head;
	}

	@Override
	public void onLivingUpdate() {
		this.setLeftLegPose(20 + 20 * MathUtils.cos(), 0, 0);
		this.setRightLegPose(20 + 10 * MathUtils.sin(), 0, 0);
		this.setLeftArmPose(20 * MathUtils.sin(), 0, 350);
		this.setRightArmPose(20 * MathUtils.sin(), 0, 10);
		this.sendWatcherUpdate();
	}

}
