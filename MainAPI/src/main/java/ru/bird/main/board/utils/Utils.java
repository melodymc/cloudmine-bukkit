package ru.bird.main.board.utils;

public class Utils {

    public static String parse(String input) {
        return parse(input, 16);
    }

    public static String parse(String input, int maxLength) {
        if(input.length() > maxLength) return input.substring(0, maxLength);
        return input;
    }
}
