/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.buildbattle.gui.type;

import com.wellfail.buildbattle.gui.Gui;
import com.wellfail.buildbattle.gui.GuiType;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.ItemUtils;

public class ParticlesGui
extends Gui {
    private Gui gui;

    public ParticlesGui(Gui gui) {
        super(gui, 3, 4, "\u042d\u0444\u0444\u0435\u043a\u0442\u044b");
        this.gui = gui;
    }

    public void init() {
        this.fillItems();
        this.addAction(31, this.gui.getGui(GuiType.MANAGER));
        this.addAction(32, this.gui.getGui(GuiType.PARTICLESLIST));
    }

    private void fillItems() {
        this.inventory.setItem(1, ItemUtils.createItem(Material.SLIME_BALL, (String)"\u00a7a\u0421\u043b\u0438\u0437\u044c"));
        this.inventory.setItem(2, ItemUtils.createItem(Material.LAVA_BUCKET, (String)"\u00a7a\u041b\u0430\u0432\u0430"));
        this.inventory.setItem(3, ItemUtils.createItem(Material.WATER_BUCKET, (String)"\u00a7a\u0412\u043e\u0434\u0430"));
        this.inventory.setItem(4, ItemUtils.createItem(Material.JUKEBOX, (String)"\u00a7a\u041d\u043e\u0442\u044b"));
        this.inventory.setItem(5, ItemUtils.createItem(Material.GOLDEN_APPLE, (String)"\u00a7a\u0421\u0435\u0440\u0434\u0435\u0447\u043a\u0438"));
        this.inventory.setItem(6, ItemUtils.createItem(Material.EMERALD, (String)"\u00a7a\u0421\u0447\u0430\u0441\u0442\u043b\u0438\u0432\u044b\u0439 \u0436\u0438\u0442\u0435\u043b\u044c"));
        this.inventory.setItem(7, ItemUtils.createItem(Material.BLAZE_POWDER, (String)"\u00a7a\u041e\u0433\u043e\u043d\u044c"));
        this.inventory.setItem(10, ItemUtils.createItem(Material.ENCHANTMENT_TABLE, (String)"\u00a7a\u0417\u0430\u043a\u043b\u0438\u043d\u0430\u043d\u0438\u0435"));
        this.inventory.setItem(11, ItemUtils.createItem(Material.WATER_BUCKET, (String)"\u00a7a\u041e\u0431\u043b\u0430\u043a\u0430"));
        this.inventory.setItem(12, ItemUtils.createItem(Material.MOB_SPAWNER, (String)"\u00a7a\u0418\u0441\u043a\u0440\u044b"));
        this.inventory.setItem(13, ItemUtils.createItem(Material.TNT, (String)"\u00a7a\u0412\u0437\u0440\u044b\u0432"));
        this.inventory.setItem(14, ItemUtils.createItem(Material.DIAMOND_SWORD, (String)"\u00a7a\u041a\u0440\u0438\u0442\u044b"));
        this.inventory.setItem(15, ItemUtils.createItem(Material.GLASS_BOTTLE, (String)"\u00a7a\u041f\u0443\u0437\u044b\u0440\u0438"));
        this.inventory.setItem(16, ItemUtils.createItem(Material.BOAT, (String)"\u00a7a\u041c\u0430\u0433\u0438\u044f"));
        this.inventory.setItem(19, ItemUtils.createItem(Material.ARROW, (String)"\u00a7a\u0412\u0441\u043f\u044b\u0448\u043a\u0438"));
        this.inventory.setItem(20, ItemUtils.createItem(Material.NETHER_BRICK, (String)"\u00a7a\u0417\u043b\u043e\u0439 \u0436\u0438\u0442\u0435\u043b\u044c"));
        this.inventory.setItem(21, ItemUtils.createItem(Material.FIREWORK, (String)"\u00a7a\u0424\u0435\u0439\u0435\u0440\u0432\u0435\u0440\u043a"));
        this.inventory.setItem(22, ItemUtils.createItem(Material.REDSTONE, (String)"\u00a7a\u0420\u0435\u0434\u0441\u0442\u043e\u0443\u043d"));
        this.inventory.setItem(23, ItemUtils.createItem(Material.SNOW_BALL, (String)"\u00a7a\u0421\u043d\u0435\u0433"));
        this.inventory.setItem(31, ItemUtils.createItem(Material.ARROW, (String)"\u00a7a\u041d\u0430\u0437\u0430\u0434"));
        this.inventory.setItem(32, ItemUtils.createItem(Material.CHEST, (String)"\u00a7a\u0423\u0441\u0442\u0430\u043d\u043e\u0432\u043b\u0435\u043d\u043d\u044b\u0435 \u044d\u0444\u0444\u0435\u043a\u0442\u044b"));
    }

    @Override
    protected void onClick(InventoryClickEvent e) {
        ItemStack item = this.inventory.getItem(e.getSlot());
        if (item == null) {
            return;
        }
        e.getWhoClicked().getInventory().setItem(7, new ItemStack(item));
    }
}

