package ru.bird.jobs.jobgame.generator;

import org.bukkit.inventory.Inventory;
import ru.bird.jobs.JobsPlugin;
import ru.bird.jobs.jobgame.generator.patterns.Simulator;
import ru.bird.jobs.utils.ItemStackUtils;

import java.util.concurrent.ThreadLocalRandom;

public class LevelGenerator
{
    private final GeneratorGroup group;
    private Simulator simulator;

    public LevelGenerator(GeneratorGroup group)
    {
        if (!group.isFullGroup())
        {
            JobsPlugin.get().getLogger().warning("These items is not full events group! Summary probability equal not one.");
        }

        this.group = group;
    }

    public void setSimulator(Simulator simulator)
    {
        this.simulator = simulator;
    }

    public GeneratorItem simulateItem(Inventory levelData, int slotId, double randomValue)
    {
        return simulator.simulate(levelData, group, slotId, randomValue);
    }

    public void generateItem(Inventory levelData, int slotId)
    {
        double randomValue = ThreadLocalRandom.current().nextDouble();
        GeneratorItem simulatedItem = simulateItem(levelData, slotId, randomValue);
        int randomItemCount = ThreadLocalRandom.current().nextInt(simulatedItem.minCount, simulatedItem.maxCount + 1);
        levelData.setItem(slotId, ItemStackUtils.createItem(simulatedItem.material, randomItemCount));
    }

    public void generateLevel(Inventory levelData)
    {
        levelData.clear();

        for (int slotId = 0; slotId < levelData.getSize(); ++slotId)
        {
            generateItem(levelData, slotId);
        }
    }
}
