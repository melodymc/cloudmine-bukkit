/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  com.mojang.authlib.GameProfile
 *  org.bukkit.Material
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.ItemMeta
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.buildbattle.gui.type.heads;

import com.wellfail.buildbattle.gui.Gui;
import com.wellfail.buildbattle.gui.type.HeadsManagerGui;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.ItemUtils;

import java.util.HashMap;
import java.util.Map;

public abstract class HeadsGui
extends Gui {
    private static Map<Integer, Gui> guis = new HashMap<Integer, Gui>();

    public static Gui getGui(int type) {
        return guis.get(type);
    }

    public HeadsGui(int id, String name) {
        super(6, name);
        guis.put(id, this);
    }

    protected void fill(String[][] array) {
        for (String[] value : array) {
            this.addHead(value[0], value[1]);
        }
        this.inventory.setItem(53, ItemUtils.createItem((Material)Material.ARROW, (String)"\u00a7a\u041d\u0430\u0437\u0430\u0434"));
    }

    protected void addHead(String name, String value) {
        ItemStack head = GameUtils.getHead(value);
        ItemMeta meta = head.getItemMeta();
        meta.setDisplayName(name);
        head.setItemMeta(meta);
        this.inventory.addItem(head);
    }

    @Override
    protected void onClick(InventoryClickEvent e) {
        if (e.getSlot() == 53) {
            HeadsManagerGui.getGui().open((Player)e.getWhoClicked());
            return;
        }
        ItemStack item = e.getInventory().getItem(e.getSlot());
        if (item == null) {
            return;
        }
        e.getWhoClicked().getInventory().setItem(7, item);
    }
}

