package ru.pir.main.utils;

import ru.bird.main.utils.GameUtils;

import java.util.ArrayList;

public final class BoardStringAnimator
{
    private static final String BOLD_FLAG = "&l";

    @SafeVarargs
    public static ArrayList<String> concatFrameBuffers(final ArrayList<String>... frameBuffers)
    {
        return new ArrayList<String>() {{ for (int i = 0; i < frameBuffers.length; ++i) addAll(frameBuffers[i]); }};
    }

    public static ArrayList<String> generateStatic(final String stringInstance, final String color, final int frames)
    {
        return new ArrayList<String>()
        {{
            String cookedString = GameUtils.colorSet(color + BOLD_FLAG + stringInstance);

            for (int i = 0; i < frames; ++i) add(cookedString);
        }};
    }

    public static ArrayList<String> generateBlinking(final String stringInstance, final String firstColor, final String secondColor, final int flashes)
    {
        return new ArrayList<String>()
        {{
            String cookedString_variant1 = GameUtils.colorSet(firstColor + BOLD_FLAG + stringInstance);
            String cookedString_variant2 = GameUtils.colorSet(secondColor + BOLD_FLAG + stringInstance);

            for (int i = 0; i < flashes; ++i)
                add( (i % 2 == 0) ? cookedString_variant1 : cookedString_variant2);
        }};
    }

    public static ArrayList<String> generateMetalShine(final String stringInstance, final String mainColor, final String shineColor, final String glareColor)
    {
        return new ArrayList<String>()
        {{
            for (int i = 0; i < stringInstance.length(); ++i)
            {
                String shineLetter = shineColor + BOLD_FLAG + stringInstance.charAt(i);
                String preSubstring = glareColor + BOLD_FLAG + stringInstance.substring(0, i);
                String postSubstring = mainColor + BOLD_FLAG + stringInstance.substring(i+1);

                String cookedString = GameUtils.colorSet(preSubstring + shineLetter + postSubstring);

                add(cookedString);
            }
        }};
    }
}
