package ru.bird.jobs.jobgame.generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GeneratorGroup
{
    private final List<GeneratorItem> group;

    public GeneratorGroup()
    {
        this.group = new ArrayList<>();
    }

    public boolean isFullGroup()
    {
        return calculateFullGroup() == 1.0d;
    }

    public double calculateFullGroup()
    {
        return group.stream()
                .mapToDouble(item -> item.spawnChance)
                .sum();
    }

    public List<GeneratorItem> getGroup()
    {
        return Collections.unmodifiableList(group);
    }

    public <T extends GenerableItem> void defineItem(T item)
    {
        GeneratorItemExtractor extractor = new GeneratorItemExtractor();
        item.invokeExtractor(extractor);

        group.add( extractor.extract() );
    }
}
