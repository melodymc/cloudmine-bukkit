package ru.bird.inventories.menu.interfaces;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public interface GuiAction {
    public void onClick(InventoryClickEvent var1);
}