/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.buildbattle.gui.type;

import com.wellfail.buildbattle.gui.Gui;
import com.wellfail.buildbattle.gui.GuiType;
import com.wellfail.buildbattle.gui.type.heads.Blocks1;
import com.wellfail.buildbattle.gui.type.heads.Blocks2;
import com.wellfail.buildbattle.gui.type.heads.Colors;
import com.wellfail.buildbattle.gui.type.heads.Foods;
import com.wellfail.buildbattle.gui.type.heads.Games;
import com.wellfail.buildbattle.gui.type.heads.HeadsGui;
import com.wellfail.buildbattle.gui.type.heads.Mobs;
import com.wellfail.buildbattle.gui.type.heads.Other;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.ItemUtils;

public class HeadsManagerGui
extends Gui {
    private static HeadsManagerGui instance;
    private Gui gui;

    public static HeadsManagerGui getGui() {
        return instance;
    }

    public HeadsManagerGui(Gui gui) {
        super(4, "Головы");
        this.gui = gui;
        instance = this;
        this.init();
        this.addAction(31, this.gui.getGui(GuiType.MANAGER));
    }

    public void init() {
        this.inventory.setItem(1, ItemUtils.createItem((Material)Material.COOKIE, (String)"§aЕда"));
        this.inventory.setItem(2, ItemUtils.createItem((Material)Material.STAINED_CLAY, (String)"§aЦвета"));
        this.inventory.setItem(3, ItemUtils.createItem((Material)Material.SLIME_BALL, (String)"§aИгры"));
        this.inventory.setItem(4, ItemUtils.createItem((Material)Material.MONSTER_EGG, (String)"§aМобы"));
        this.inventory.setItem(5, ItemUtils.createItem((Material)Material.STONE, (String)"§aБлоки"));
        this.inventory.setItem(6, ItemUtils.createItem((Material)Material.GRASS, (String)"§aБлоки"));
        this.inventory.setItem(7, ItemUtils.createItem((Material)Material.WATER_BUCKET, (String)"§aПрочее"));
        this.inventory.setItem(31, ItemUtils.createItem((Material)Material.ARROW, (String)"§aНазад"));
        new Blocks1();
        new Blocks2();
        new Colors();
        new Foods();
        new Games();
        new Mobs();
        new Other();
    }

    @Override
    protected void onClick(InventoryClickEvent e) {
        if (e.getSlot() > 0 && e.getSlot() < 8) {
            HeadsGui.getGui(e.getSlot()).open((Player)e.getWhoClicked());
        }
    }
}

