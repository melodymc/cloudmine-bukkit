/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.inventory.ItemStack
 */
package com.wellfail.crazywalls.shop;

import org.bukkit.inventory.ItemStack;

class Cost {
    private ItemStack costItem;
    private int amount;
    private String name;

    public Cost(ItemStack item, int amount, String name) {
        this.amount = amount;
        this.costItem = item;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public ItemStack getCostItem() {
        return this.costItem;
    }

    public int getAmount() {
        return this.amount;
    }
}

