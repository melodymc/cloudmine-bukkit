package ru.bird.pets.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.main.event.listeners.BasicListener;
import ru.bird.pets.Pets;
import ru.bird.pets.PetsDB;
import ru.bird.pets.entity.AbstractPet;
import ru.bird.pets.entity.EnumPet;
import ru.bird.pets.player.PetAccount;

import java.util.UUID;

public class PetsPluginListener extends BasicListener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        PetsDB.load(event.getPlayer().getUniqueId());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();
        new BukkitRunnable() {
            @Override
            public void run() {
                PetAccount u = PetsDB.get(player);
                Pets.getPetsManager().hiddenPet(player);
                UUID uuid = player.getUniqueId();
                if(u.isNeedInsert()) {
                    PetsDB.insert(player);
                }
                PetsDB.removePetAccount(uuid);
            }
        }.runTask(Pets.getPlugin());
    }

//    @EventHandler(priority = EventPriority.LOWEST)
//    public void onMove(PlayerMoveEvent event){
//        AbstractPet aPet = Pets.getPetsManager().getPlayerPet(event.getPlayer());
//        if(aPet != null) {
//            aPet.move(event.getPlayer());
//        }
//    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onEntitySpawn(EntitySpawnEvent e) {
        e.setCancelled(false);
    }

    @EventHandler
    public void onTp(PlayerTeleportEvent event) {
        PetAccount acc = PetsDB.get(event.getPlayer());
        if(acc != null) {
            EnumPet pet = acc.getActivePet();
            if(pet != null) {
                Pets.getPetsManager().spawnPetFor(event.getPlayer(), pet);
            }
        }
    }
}