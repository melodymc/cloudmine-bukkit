package ru.bird.main.groups;

import net.milkbowl.vault.permission.Permission;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.bird.main.API;

import java.util.ArrayList;
import java.util.List;

public class Groups {
    private static Groups instance;
    public static Permission api;


    public Groups() {
        instance = this;
        api = API.getInstance().getVaultManager().getPermission();
    }

    public static Groups getInstance() {
        return instance;
    }

    private static double getDoubleValueByPermission(Player player, String name) {
        return 0;
    }

    private static int getIntValueByPermission(Player player, String name) {
        return 0;
    }

    private static double getDoubleByGroup(String group, String name) {
        return 0;
    }

    public static List<String> getPermissionPlayer(Player player){
        List<String> list = new ArrayList<>();
        list.add("test");
        return list;
    }

    private static int getIntByGroup(String group, String name) {
        return 0;
    }

    public static ItemStack getItemValueByPermission(Player player) {
        return new ItemStack(Material.GLASS);
    }

    static double getMax(List<Double> array) {
        double max = Double.MIN_VALUE;
        for(int i = 0; i < array.size(); i++) {
            if(array.get(i) > max) {
                max = array.get(i);
            }
        }
        return max;
    }

    public static double getMultiply(Player player) {
        return getDoubleValueByPermission(player, "multiply");
    }

    public static int getLevel(Player player) {
        return getIntValueByPermission(player, "level");
    }

    public static double getMultiply(String group) {
        return getDoubleByGroup(group, "multiply");
    }

    public static int getLevel(String group) {
        return getIntByGroup(group, "level");
    }

    public static boolean hasPermission(Player player, String permission) {
        return false;
    }

    public static String getPrefix(Player player){
        return "";
//        String prefix = getPermissionPlayer(player).getPrefix();
//        if(prefix == null) {
//            prefix = "";
//        }
//        return prefix.replace("&", "§");
    }

    public static String getSuffix(Player player){
        return "";
//        String suffix = getPermissionPlayer(player).getSuffix();
//        if(suffix == null) {
//            suffix = "";
//        }
//        return suffix.replace("&", "§");
    }

    public static String getFullName(Player player){
        String prefix = getPrefix(player);
        if(prefix.length() > 3) {
            prefix = prefix + " ";
        }
        return prefix + player.getName() + getSuffix(player);
    }
}