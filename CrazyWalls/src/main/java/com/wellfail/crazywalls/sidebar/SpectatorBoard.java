package com.wellfail.crazywalls.sidebar;

public class SpectatorBoard// extends Board
{
//    private int d;
//    private List<String> list;
//    private BoardLine players;
//    private BoardLine spectators;
//    private BoardLine kills;
//    private BoardLine greenwall;
//    private BoardLine redwall;
//    private BoardLine border;
//    private boolean endred = false;
//    private boolean endgreen = false;
//    private boolean end = false;
//
//    public SpectatorBoard() {
//        Set<BoardLine> lines = this.getLines();
//        this.create(lines, "\u00a7e\u00a7lCRAZY WALLS");
//        this.list = new ArrayList<String>();
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a76\u00a7lC\u00a7e\u00a7lRAZY WALLS");
//        this.list.add("\u00a7f\u00a7lC\u00a76\u00a7lR\u00a7e\u00a7lAZY WALLS");
//        this.list.add("\u00a7f\u00a7lCR\u00a76\u00a7lA\u00a7e\u00a7lZY WALLS");
//        this.list.add("\u00a7f\u00a7lCRA\u00a76\u00a7lZ\u00a7e\u00a7lY WALLS");
//        this.list.add("\u00a7f\u00a7lCRAZ\u00a76\u00a7lY\u00a7e\u00a7l WALLS");
//        this.list.add("\u00a7f\u00a7lCRAZY \u00a76\u00a7lW\u00a7e\u00a7lALLS");
//        this.list.add("\u00a7f\u00a7lCRAZY W\u00a76\u00a7lA\u00a7e\u00a7lLLS");
//        this.list.add("\u00a7f\u00a7lCRAZY WA\u00a76\u00a7lL\u00a7e\u00a7lLS");
//        this.list.add("\u00a7f\u00a7lCRAZY WAL\u00a76\u00a7lL\u00a7e\u00a7lS");
//        this.list.add("\u00a7f\u00a7lCRAZY WALL\u00a76\u00a7lS");
//        this.list.add("\u00a7f\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7f\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7f\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7f\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.list.add("\u00a7e\u00a7lCRAZY WALLS");
//        this.update(() -> {
//            if (this.d == this.list.size()) {
//                this.d = 0;
//            }
//            this.setDisplay(this.list.get(this.d));
//            ++this.d;
//        }
//        , 3);
//        this.startUpdate();
//    }
//
//    public Set<BoardLine> getLines() {
//        HashSet<BoardLine> set = new HashSet<BoardLine>();
//        set.add(new BoardLine((Board)this, "\u00a71", 10));
//        this.greenwall = new BoardLine((Board)this, "\u00a7a\u0417\u0435\u043b\u0435\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: \u00a7a" + Utils.getTimeToEnd((int)GameFactory.timer.getGreen()), 9);
//        set.add(this.greenwall);
//        this.dynamicLine(this.greenwall.getNumber(), "\u00a7a\u0417\u0435\u043b\u0435\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: \u00a7a", Utils.getTimeToEnd((int)GameFactory.timer.getGreen()));
//        this.redwall = new BoardLine((Board)this, "\u00a7c\u041a\u0440\u0430\u0441\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: \u00a7c" + Utils.getTimeToEnd((int)GameFactory.timer.getRed()), 8);
//        set.add(this.redwall);
//        this.dynamicLine(this.redwall.getNumber(), "\u00a7c\u041a\u0440\u0430\u0441\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: \u00a7c", Utils.getTimeToEnd((int)GameFactory.timer.getRed()));
//        set.add(new BoardLine((Board)this, "\u00a72", 7));
//        this.border = new BoardLine((Board)this, "\u0414\u0435\u0437\u043c\u0430\u0442\u0447: \u00a7a" + Utils.getTimeToEnd((int)GameFactory.timer.getBorder()), 6);
//        this.dynamicLine(this.border.getNumber(), "\u0414\u0435\u0437\u043c\u0430\u0442\u0447: \u00a7a", Utils.getTimeToEnd((int)GameFactory.timer.getBorder()));
//        set.add(this.border);
//        set.add(new BoardLine((Board)this, "\u00a73", 5));
//        this.players = new BoardLine((Board)this, "\u0418\u0433\u0440\u043e\u043a\u043e\u0432: \u00a7a", 4);
//        set.add(this.players);
//        this.dynamicLine(this.players.getNumber(), "\u0418\u0433\u0440\u043e\u043a\u043e\u0432: \u00a7a", "" + GameFactory.players.size() + "");
//        this.spectators = new BoardLine((Board)this, "\u041d\u0430\u0431\u043b\u044e\u0434\u0430\u0442\u0435\u043b\u0435\u0439: \u00a7a", 3);
//        set.add(this.spectators);
//        this.dynamicLine(this.spectators.getNumber(), "\u041d\u0430\u0431\u043b\u044e\u0434\u0430\u0442\u0435\u043b\u0435\u0439: \u00a7a", "" + (Bukkit.getOnlinePlayers().size() - GameFactory.players.size()) + "");
//        set.add(new BoardLine((Board)this, "\u00a74", 2));
//        set.add(new BoardLine((Board)this, "\u041a\u0430\u0440\u0442\u0430: \u00a7a" + GameSettings.mapLocation.getWorld().getName(), 2));
//        set.add(new BoardLine((Board)this, "\u0421\u0435\u0440\u0432\u0435\u0440: \u00a7a" + Main.getUsername(), 1));
//        this.update(() -> {
//            if (GameFactory.timer.getBorder() == 0) {
//                if (!this.end) {
//                    this.setLine(this.border.getNumber(), "\u041a\u043e\u043d\u0435\u0446:\u00a7a " + Utils.getTimeToEnd());
//                    this.end = true;
//                }
//                this.dynamicLine(this.border.getNumber(), "\u041a\u043e\u043d\u0435\u0446:\u00a7a ", Utils.getTimeToEnd());
//            } else {
//                this.dynamicLine(this.border.getNumber(), "\u0414\u0435\u0437\u043c\u0430\u0442\u0447: \u00a7a", Utils.getTimeToEnd((int)GameFactory.timer.getBorder()));
//            }
//            if (GameFactory.timer.getGreen() == 0) {
//                if (!this.endgreen) {
//                    this.setLine(this.greenwall.getNumber(), "\u00a77\u00a7m\u0417\u0435\u043b\u0435\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: \u00a77\u00a7m00:00\u00a76");
//                    this.endgreen = true;
//                } else {
//                    this.dynamicLine(this.greenwall.getNumber(), "\u00a77\u00a7m\u0417\u0435\u043b\u0435\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: \u00a77\u00a7m00:00\u00a76", "");
//                }
//            } else {
//                this.dynamicLine(this.greenwall.getNumber(), "\u00a7a\u0417\u0435\u043b\u0435\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: \u00a7a", Utils.getTimeToEnd((int)GameFactory.timer.getGreen()));
//            }
//            if (GameFactory.timer.getRed() == 0) {
//                if (!this.endred) {
//                    this.setLine(this.redwall.getNumber(), "\u00a77\u00a7m\u041a\u0440\u0430\u0441\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: \u00a77\u00a7m00:00\u00a77");
//                    this.endred = true;
//                } else {
//                    this.dynamicLine(this.redwall.getNumber(), "\u00a77\u00a7m\u041a\u0440\u0430\u0441\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: \u00a77\u00a7m00:00\u00a77", "");
//                }
//            } else {
//                this.dynamicLine(this.redwall.getNumber(), "\u00a7c\u041a\u0440\u0430\u0441\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: \u00a7c", Utils.getTimeToEnd((int)GameFactory.timer.getRed()));
//            }
//        }
//        , 20);
//        this.update(() -> {
//            this.dynamicLine(this.spectators.getNumber(), "\u041d\u0430\u0431\u043b\u044e\u0434\u0430\u0442\u0435\u043b\u0435\u0439: \u00a7a", "" + (Bukkit.getOnlinePlayers().size() - Utils.getAlivePlayers().size()) + "");
//            this.dynamicLine(this.players.getNumber(), "\u0418\u0433\u0440\u043e\u043a\u043e\u0432: \u00a7a", "" + Utils.getAlivePlayers().size() + "");
//        }
//        , 100);
//        return set;
//    }
}

