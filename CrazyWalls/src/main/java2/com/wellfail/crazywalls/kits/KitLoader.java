// 
// Decompiled by Procyon v0.5.30
// 

package com.wellfail.crazywalls.kits;

import java.io.IOException;
import java.io.InputStream;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.YamlConfiguration;
import com.wellfail.crazywalls.CrazyWalls;
import java.util.Iterator;
import org.bukkit.inventory.meta.ItemMeta;
import ru.wellfail.wapi.gui.Item;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import ru.wellfail.wapi.shop.ShopManager;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.Bukkit;
import ru.wellfail.wapi.shop.Kit;
import ru.wellfail.wapi.Main;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.game.Gamer;
import org.bukkit.event.player.PlayerInteractEvent;
import ru.wellfail.wapi.gui.ItemRunnable;
import com.wellfail.crazywalls.game.GameFactory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import java.io.File;
import org.bukkit.configuration.file.FileConfiguration;

public class KitLoader
{
    private FileConfiguration config;
    private File locationsFile;
    
    public KitLoader() {
        this.locationsFile = null;
        this.init();
        this.loadShop(this.config);
        this.load();
    }
    
    public void load() {
        final ItemStack shop = new ItemStack(Material.CHEST);
        final ItemMeta meta = shop.getItemMeta();
        meta.setDisplayName("§a\u041c\u0430\u0433\u0430\u0437\u0438\u043d \u043d\u0430\u0431\u043e\u0440\u043e\u0432 §7(\u041f\u041a\u041c)");
        shop.setItemMeta(meta);
        GameFactory.getInstance().shopItem = new Item(shop, (ItemRunnable)new ItemRunnable() {
            public void onUse(final PlayerInteractEvent e) {
                final Gamer gamer = Gamer.getGamer(e.getPlayer());
                if (gamer.getPurchase() != null && !gamer.isCancelbuy()) {
                    e.getPlayer().sendMessage(GameSettings.prefix + "\u041d\u0430\u0436\u043c\u0438\u0442\u0435 \u0435\u0449\u0435 \u0440\u0430\u0437, \u0447\u0442\u043e\u0431\u044b \u0430\u043d\u043d\u0443\u043b\u0438\u0440\u043e\u0432\u0430\u0442\u044c \u043f\u043e\u043a\u0443\u043f\u043a\u0443 \u043d\u0430\u0431\u043e\u0440\u0430 " + gamer.getPurchase().getName());
                    e.setCancelled(true);
                    e.getPlayer().closeInventory();
                    gamer.setCancelbuy(true);
                    return;
                }
                if (gamer.getPurchase() != null) {
                    e.getPlayer().sendMessage(GameSettings.prefix + "\u0412\u0430\u043c \u0431\u044b\u043b\u0438 \u0432\u043e\u0437\u0432\u0440\u0430\u0449\u0435\u043d\u044b \u0434\u0435\u043d\u044c\u0433\u0438 \u0437\u0430 \u043f\u043e\u043a\u0443\u043f\u043a\u0443 \u043d\u0430\u0431\u043e\u0440\u0430 " + gamer.getPurchase().getName());
                    Main.economy.depositPlayer(e.getPlayer().getName(), (double)gamer.getPurchase().getPrice());
                    gamer.setCancelbuy(false);
                    gamer.setPurchase((Kit)null);
                    e.setCancelled(true);
                    e.getPlayer().closeInventory();
                    return;
                }
                final Inventory inv = Bukkit.createInventory((InventoryHolder)null, 45, "§r\u041c\u0430\u0433\u0430\u0437\u0438\u043d \u043d\u0430\u0431\u043e\u0440\u043e\u0432");
                for (int i = 0; i < 45; ++i) {
                    final Kit kit = ShopManager.instance.getKit(i);
                    if (kit != null) {
                        inv.setItem(i, kit.getItem());
                    }
                }
                e.getPlayer().openInventory(inv);
            }
            
            public void onClick(final InventoryClickEvent paramInventoryClickEvent) {
            }
        }, new Action[] { Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK });
    }
    
    private void loadShop(final FileConfiguration config) {
        for (final String s : config.getConfigurationSection("Shop").getKeys(false)) {
            ShopManager.instance.addKit(new Kit(config.getString("Shop." + s + ".kit"), config.getStringList("Shop." + s + ".lore"), config.getInt("Shop." + s + ".perm")));
        }
    }
    
    private void init() {
        this.locationsFile = new File(CrazyWalls.getInstance().getDataFolder(), "kits.yml");
        this.config = (FileConfiguration)YamlConfiguration.loadConfiguration(this.locationsFile);
        final InputStream defConfigStream1 = CrazyWalls.getInstance().getResource("kits.yml");
        if (defConfigStream1 != null) {
            final YamlConfiguration defConfig1 = YamlConfiguration.loadConfiguration(defConfigStream1);
            this.config.setDefaults((Configuration)defConfig1);
        }
        this.config.options().copyDefaults(true);
        this.saveLocationsConfig();
    }
    
    public void saveLocationsConfig() {
        if (this.locationsFile == null || this.config == null) {
            return;
        }
        try {
            this.config.save(this.locationsFile);
        }
        catch (IOException ex) {
            System.out.println("\u041e\u0448\u0438\u0431\u043a\u0430 \u043f\u0440\u0438 \u0447\u0442\u0435\u043d\u0438\u0438 \u0444\u0430\u0439\u043b\u0430 \u043a\u043e\u043d\u0444\u0438\u0433\u0443\u0440\u0430\u0446\u0438\u0438.");
        }
    }
}
