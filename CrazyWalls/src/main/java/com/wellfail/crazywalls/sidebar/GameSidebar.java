package com.wellfail.crazywalls.sidebar;

import com.wellfail.crazywalls.game.GPlayer;
import com.wellfail.crazywalls.game.GameFactory;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.bird.main.board.api.PersonalBoard;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.utils.GameUtils;
import ru.pir.main.utils.BoardStringAnimator;

import java.util.concurrent.ThreadLocalRandom;

public class GameSidebar extends PersonalBoard
{
    public GameSidebar(Player player)
    {
        super(GameSettings.wboardname.toUpperCase(), player);
        String gameTitle = GameSettings.wboardname.toUpperCase();
        addUpdaterHeader(3, BoardStringAnimator.concatFrameBuffers(
                BoardStringAnimator.generateStatic(gameTitle, "&e", 5),
                BoardStringAnimator.generateMetalShine(gameTitle, "&e", "&6", "&f"),
                BoardStringAnimator.generateBlinking(gameTitle, "&f", "&e", 5)
        ));

        GPlayer gPlayer = GameFactory.getGplayer(player);

        write(0);
        write(1, "Убийств: §a " + gPlayer.getKills());
        write(2);
        write(3, "Наблюдателей: §a " + Integer.toString(Game.spectators.size()));
        write(4);
        write(5, "Игроков: §a " + Integer.toString(Bukkit.getOnlinePlayers().size() - Game.spectators.size()));
        write(6);
        write(7, "Дезматч: §a " + GameUtils.getTimeToEnd(GameFactory.timer.getBorder()));
        write(8);
        write(9, "§cКрасная стена: §c " + GameUtils.getTimeToEnd(GameFactory.timer.getRed()));
        write(10);
        write(11, "§aЗеленая стена: §a " + GameUtils.getTimeToEnd(GameFactory.timer.getGreen()));
        write(12);

        addUpdater(20, board -> {
            if (GameFactory.timer.getBorder() == 0)
            {
                board.modifyLine(7, "Конец: §a " + GameUtils.getTimeToEnd());
            }
            else
            {
                board.modifyLine(7, "Дезматч: §a " + GameUtils.getTimeToEnd(GameFactory.timer.getBorder()));
            }

            if (GameFactory.timer.getGreen() == 0)
            {
                board.modifyLine(11, "§7§mЗеленая стена: §7§m00:00§6");
            }
            else
            {
                board.modifyLine(11, "§aЗеленая стена: §a " + GameUtils.getTimeToEnd(GameFactory.timer.getGreen()));
            }

            if (GameFactory.timer.getRed() == 0)
            {
                board.modifyLine(9, "§7§mКрасная стена: §7§m00:00§7");
            }
            else
            {
                board.modifyLine(9, "§cКрасная стена: §c " + GameUtils.getTimeToEnd(GameFactory.timer.getRed()));
            }
        });

        addUpdater(100, board -> {
            board.modifyLine(1, "Убийств: §a " + gPlayer.getKills());
            board.modifyLine(3, "Наблюдателей: §a " + Integer.toString(Game.spectators.size()));
            board.modifyLine(5, "Игроков: §a " + Integer.toString(Bukkit.getOnlinePlayers().size() - Game.spectators.size()));
        });

        create();
    }
}

