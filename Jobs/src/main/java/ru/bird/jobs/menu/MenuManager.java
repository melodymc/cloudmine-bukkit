package ru.bird.jobs.menu;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.bird.inventories.menu.Menu;
import ru.bird.inventories.menu.interfaces.GuiCallback;
import ru.bird.jobs.JobEnum;
import ru.bird.jobs.JobsPlugin;
import ru.bird.jobs.JobsUser;
import ru.bird.jobs.StorageJob;
import ru.bird.jobs.jobgame.enchanter.JobGameEnchanter;
import ru.bird.jobs.jobgame.miner.JobGameMiner;
import ru.bird.jobs.utils.ItemStackUtils;

import java.util.*;

public class MenuManager
{
    /* Miner job game */
    public static final Map<UUID, JobGameMiner> minerPool = new HashMap<>();
    public static final Map<UUID, JobGameEnchanter> enchanterPool = new HashMap<>();

    /* ??? */
    private static Menu startMenu;
    private Map<JobEnum, Integer> startMenuJobsMap;

    public MenuManager()
    {

    }

    public int getRandomNumberUsingNextInt(int min, int max)
    {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    private void fillStartMenu()
    {
        startMenuJobsMap = new HashMap<>();
        startMenu = new Menu(JobsPlugin.get(), "Выбор профессии", 1);
        startMenu.setGuiOpen(new GuiCallback()
        {
            @Override
            public void execute(Player player, Inventory inventory)
            {
                JobsUser user = StorageJob.get(player);
                if (user.getJobToString() != null)
                {
                    if (!user.isCanChangeJob())
                    {
                        player.sendMessage("Смена профессии доступна через " + JobsPlugin.getTimeToChangeJobStr(user));
                    }
                    player.closeInventory();
                    int slot = startMenuJobsMap.get(user.job);
                }
            }
        });

        List<String> lore = new ArrayList<>();
        lore.add("Профессия нацелена на добычу, переработку");
        lore.add("и продажу полезных ископаемых игрокам либо серверу");
        addJob("Шахтёр", JobEnum.MINER, 0, Material.DIAMOND_PICKAXE, lore);
        lore.clear();
        lore.add("Строй фермы и собирай урожай с них на продажу");
        addJob("Фермер", JobEnum.FARMER, 1, Material.GOLDEN_HOE, lore);
    }

    private void addJob(String displayname, JobEnum job, int slot, Material material, List<String> lore)
    {
        startMenuJobsMap.put(job, slot);
        ItemStack item = ItemStackUtils.createItem(material, 1, 0, displayname, lore);
        Inventory inv = startMenu.getBukkitInventory();
        inv.setItem(slot, item);
        startMenu.addAction(slot, e ->
        {
            Player player = (Player) e.getWhoClicked();
            StorageJob.get(player.getUniqueId()).setJob(job);
            JobsUser user = StorageJob.get(player);
            if (user != null)
            {
                ;
            }
            else
            {
                System.out.println("[Jobs] Не загружен объект игрока работ");
            }

        });
    }

    public static Menu getStartMenu()
    {
        return startMenu;
    }
}
