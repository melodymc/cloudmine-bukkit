package ru.bird.lobby.lobby;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import ru.bird.main.actionbar.ActionBarApi;
import ru.bird.main.event.listeners.BasicListener;

import java.util.HashMap;
import java.util.Map;

public class LobbyListener extends BasicListener {

    private Map<Player, BukkitTask> tasks;

    public LobbyListener(){
        tasks = new HashMap<>();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
//        new Title("&fПривет&e☀", "Мы рады тебе &d❤&f, приятной игры").send(player);
        tasks.put(event.getPlayer(), new BukkitRunnable() {
            @Override
            public void run() {
                ActionBarApi.sendActionBar(event.getPlayer(), Main.getPlugin().getConfig().getString("title.msg").replace("&", ChatColor.COLOR_CHAR + ""), 20 * 10 + 1, false);
            }
        }.runTaskTimer(Main.getPlugin(), 0, 20 * 10));
    }

    @EventHandler
    public void onJoin(PlayerQuitEvent event){
        tasks.get(event.getPlayer()).cancel();
    }
}