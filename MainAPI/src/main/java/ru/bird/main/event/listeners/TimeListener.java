/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.World
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitScheduler
 */
package ru.bird.main.event.listeners;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import ru.bird.main.API;
import ru.bird.main.game.GameSettings;

public class TimeListener
implements Runnable {
    public TimeListener() {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(API.getInstance(), this, 20, 200);
    }

    @Override
    public void run() {
        Bukkit.getWorlds().forEach(world1 -> {
            world1.setTime((long)GameSettings.worldTime);
        }
        );
    }
}

