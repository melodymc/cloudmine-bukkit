package ru.bird.main.inventories;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Player;
import ru.bird.main.API;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.Gamer;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class ShopManager {
    private Map<Integer, Kit> shop;
    public static ShopManager instance;

    public ShopManager() {
        instance = this;
        this.shop = new ConcurrentHashMap<Integer, Kit>();
    }

    public void addKit(Kit kit) {
        this.shop.put(kit.getId(), kit);
    }

    public Kit getKit(int id) {
        return this.shop.get(id);
    }

    public void buy(Player player, int id) {
        Gamer gamer = Gamer.getGamer(player);
        Kit kit = this.shop.get(id);
        if (kit == null) {
            return;
        }
        if (player == null) {
            return;
        }
        if (gamer.getPurchase() == null) {
            Economy econ = API.getInstance().getVaultManager().getEconomy();
            if (econ.getBalance(player.getName()) < (double)this.shop.get(id).getPrice()) {
                player.sendMessage(GameSettings.prefix + "Недостаточно монеток для покупки набора");
                return;
            }
            gamer.setPurchase(this.shop.get(id));
            econ.withdrawPlayer(player.getName(), (double)this.shop.get(id).getPrice());
            player.sendMessage(GameSettings.prefix + "Вы приобрели набор " + kit.getName() + " за §e⛂" + kit.getPrice() + "§f.");
        }
    }
}