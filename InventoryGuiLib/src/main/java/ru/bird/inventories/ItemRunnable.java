package ru.bird.inventories;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public interface ItemRunnable {
    public void onUse(PlayerInteractEvent var1);

    public void onClick(InventoryClickEvent var1);
}