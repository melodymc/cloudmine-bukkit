/*
 * Decompiled with CFR 0_122.
 *
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.GameMode
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.entity.Player
 *  org.bukkit.event.HandlerList
 *  org.bukkit.event.Listener
 *  org.bukkit.event.block.Action
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  ru.wellfail.wapi.board.Board
 *  ru.bird.main.game.Game
 *  ru.bird.main.game.GameSettings
 *  ru.bird.main.game.Gamer
 *  ru.wellfail.wapi.gui.Item
 *  ru.wellfail.wapi.gui.ItemRunnable
 *  ru.wellfail.wapi.logger.WLogger
 *  ru.wellfail.wapi.utils.Title
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.buildbattle;

import com.wellfail.buildbattle.board.GameBoard;
import com.wellfail.buildbattle.board.VoteBoard;
import com.wellfail.buildbattle.gui.GuiType;
import com.wellfail.buildbattle.listeners.GameListener;
import com.wellfail.buildbattle.listeners.PhysicListener;
import com.wellfail.buildbattle.listeners.VoteListener;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import ru.bird.main.API;
import ru.bird.main.board.api.Board;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.Gamer;
import ru.bird.inventories.Item;
import ru.bird.inventories.ItemRunnable;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.ItemUtils;
import ru.bird.main.utils.Title;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public class GameFactory
extends Game {
    private static String theme;
    private Map<Player, BuildRegion> players = new HashMap<Player, BuildRegion>();
    private List<BuildRegion> regions;
    private Item manager;
    private BukkitTask task;
    private Board board;
    private Listener vote;
    private BuildRegion current;
    //private final String create = "ALTER TABLE stats ADD bbfirst INT DEFAULT 0, ADD bbsecond INT DEFAULT 0,ADD bbthird INT DEFAULT 0, ADD bbgames INT DEFAULT 0;";

    public static GameFactory getInstance() {
        return (GameFactory)Game.getInstance();
    }

    public static String getTheme() {
        return theme;
    }

    public BuildRegion getRegion(Player player) {
        return this.players.get(player);
    }

    public List<BuildRegion> getRegions() {
        return this.regions;
    }

    public GameFactory() {
        super( new GameListener());
        GameFactory.setPrefix((String)"§eBuildBattle §f| ");
        this.regions = new ArrayList<BuildRegion>();
        this.loadConfig();
        //GameFactory.createcolumns((String)"ALTER TABLE stats ADD bbfirst INT DEFAULT 0, ADD bbsecond INT DEFAULT 0,ADD bbthird INT DEFAULT 0, ADD bbgames INT DEFAULT 0;");
        GameSettings.slots = this.regions.size();
        GameSettings.toStart = this.regions.size() > 5 ? this.regions.size() - 4 : 2;
        this.manager = new Item(ItemUtils.createItem((Material)Material.NETHER_STAR, (String)"§aУправление §7(ПКМ)"), new ItemRunnable(){

            public void onUse(PlayerInteractEvent e) {
                GameFactory.this.getRegion(e.getPlayer()).getGui(GuiType.MANAGER).open(e.getPlayer());
            }

            public void onClick(InventoryClickEvent inventoryClickEvent) {
            }
        }, new Action[]{Action.RIGHT_CLICK_BLOCK, Action.RIGHT_CLICK_AIR});
    }

    private void loadConfig() {
        FileConfiguration config = Main.getInstance().getConfig();
        GameSettings.respawnLocation = GameUtils.stringToLocation(config.getString("Lobby"));
        List themes = config.getStringList("Themes");
        theme = this.getRandomTheme(themes);
        for (String s : config.getConfigurationSection("Regions").getKeys(false)) {
            Location one = GameUtils.stringToLocation((String)config.getString("Regions." + s + ".1"));
            Location two = GameUtils.stringToLocation((String)config.getString("Regions." + s + ".2"));
            this.regions.add(new BuildRegion(one, two));
        }
        GameSettings.spectrLocation = this.regions.get(0).getSpawn();
    }

    protected void onStartGame() {
        this.board = new GameBoard();
        Iterator<BuildRegion> iterator = this.regions.iterator();
        for (Player player : Bukkit.getOnlinePlayers()) {
            BuildRegion region = iterator.next();
            this.players.put(player, region);
            region.setPlayer(player);
            player.getInventory().setItem(8, this.manager.getItem());
        }
        Title title = new Title("§eПостройте §b" + theme, "§eСтройке как можно лучше, и вас оценят!", 0, 5, 3);
        title.broadcast();
        this.task = new BukkitRunnable(){

            public void run() {
                Bukkit.getOnlinePlayers().stream().filter(player -> !Gamer.getGamer((Player)player).isSpectator()).forEach(player -> {
                    player.getInventory().setItem(8, GameFactory.this.manager.getItem());
                }
                );
            }
        }.runTaskTimer(Main.getInstance(), 100, 100);
        Bukkit.broadcastMessage((String)"§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        Bukkit.broadcastMessage((String)"");
        Bukkit.broadcastMessage((String)"                                 §c§lBuild Battle");
        Bukkit.broadcastMessage((String)"");
        Bukkit.broadcastMessage((String)("                  §eУ вас есть 6 минут, чтобы построить §a" + theme));
        Bukkit.broadcastMessage((String)"                     §eСтройке как можно лучше, и вас оценят!");
        Bukkit.broadcastMessage((String)"");
        Bukkit.broadcastMessage((String)"§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        board.send();
        new PhysicListener();
    }

    private String getRandomTheme(List<String> themes) {
        Random random = new Random();
        int i = random.nextInt(themes.size());
        return themes.get(i);
    }

    public BuildRegion getCurrent() {
        return this.current;
    }

    public void endGame() {
        if (this.task != null) {
            this.task.cancel();
        }
        this.vote = new VoteListener();
        this.board.remove();
        LinkedHashSet regions = new LinkedHashSet();
        this.players.values().forEach(region -> {
            if (region.getPlayer() != null && region.getPlayer().isOnline()) {
                regions.add(region);
            }
        }
        );
        Bukkit.getOnlinePlayers().forEach(player -> {
            player.setGameMode(GameMode.ADVENTURE);
            player.setAllowFlight(true);
        }
        );
        final Iterator iterator = regions.iterator();
        new BukkitRunnable(){
            public void run() {
                if (!iterator.hasNext()) {
                    GameFactory.this.end();
                    this.cancel();
                } else {
                    BuildRegion next = (BuildRegion)iterator.next();
                    if (GameFactory.this.current == null) {
                        GameFactory.this.current = next;
                        VoteBoard voteBoard = new VoteBoard();
                        Bukkit.getOnlinePlayers().stream().forEach(player -> {
                            if (Gamer.getGamer(player).isSpectator()) {
                                Gamer.getGamer(player).setGhost(true);
                            }
                            voteBoard.send(player);
                        }
                        );
                    } else {
                        GameFactory.this.current = next;
                    }
                    next.teleport();
                }
            }
        }.runTaskTimer(Main.getInstance(), 20, 300);
    }

    private void end() {
        HandlerList.unregisterAll(vote);
        super.endGame();
    }

    protected void onEndGame() {
        BuildRegion[] winners = new BuildRegion[3];
        ArrayList<BuildRegion> regions = new ArrayList<BuildRegion>();
        regions.addAll(this.players.values());
        HashSet<BuildRegion> removed = new HashSet<BuildRegion>();
        for (int i = 0; i < 3; ++i) {
            for (BuildRegion region2 : regions) {
                if (removed.contains(region2)) continue;
                if (winners[i] == null) {
                    winners[i] = region2;
                    continue;
                }
                if (region2.getScore() <= winners[i].getScore()) continue;
                winners[i] = region2;
            }
            removed.add(winners[i]);
        }
        winners[0].teleport();
        this.current = winners[0];
        Player[] names = new Player[3];
        for (int i = 0; i < 3; ++i) {
            BuildRegion region2;
            region2 = winners[i];
            if (region2 == null) continue;
            names[i] = region2.getPlayer();
        }

        this.regions.stream().forEach(region -> {
            if (region.getPlayer() != null && region.getPlayer().isOnline()) {
                Gamer.getGamer(region.getPlayer().getName()).addMoney(region.getScore());
            }
        }
        );
        if(names[0] != null && names[1] != null && names[2] != null) {
            Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("                                 §c§lBuild Battle");
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("                            §e1 место §7- " + names[0].getDisplayName());
            Bukkit.broadcastMessage("                          §62 место §7- " + names[1].getDisplayName());
            Bukkit.broadcastMessage("                            §c3 место §7- " + names[2].getDisplayName());
            Bukkit.broadcastMessage("");
            Bukkit.getOnlinePlayers().stream().filter(player -> !Gamer.getGamer(player).isSpectator()).forEach(player -> {
                player.getInventory().clear();
                player.sendMessage("                           §eВы набрали §c" + this.getRegion(player).getScore() + "§e очков");
            }
            );
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
            for (BuildRegion region2 : regions) {
                Gamer gamer;
                Player player = region2.getPlayer();
                if (player == null || (gamer = Gamer.getGamer(player.getName())) == null) continue;
                boolean first = false;
                boolean second = false;
                boolean third = false;
                if (names[0].getName().equals(gamer.getName())) {
                    first = true;
                }
                if (names[1].getName().equals(gamer.getName())) {
                    second = true;
                }
                if (names[2].getName().equals(gamer.getName())) {
                    third = true;
                }
                try {
                    PreparedStatement st = API.getInstance().getSQLConnection().getConnection().prepareStatement("CALL save_stat_bb(?, ?, ?, ?);");
                    st.setString(1, player.getUniqueId().toString());
                    st.setBoolean(2, first);
                    st.setBoolean(3, second);
                    st.setBoolean(4, third);
                    BaseLogger.debug(st.toString());
                    st.executeUpdate();
                } catch (SQLException e) {
                    BaseLogger.error(e.getMessage());
                }
    //            String sql = "CALL `save_stat_bb`(" +
    //                    "'" + player.getUniqueId() + "', " +
    //                    "'" + (first ? 1 : 0) + "', " +
    //                    "'" + (second ? 1 : 0) + "', " +
    //                    "'" + (third ? 1 : 0) + "', " +
    //                    "'1')";
    //            BaseLogger.debug(sql);
    //            GameFactory.saveStat(sql);
                }
        } else {
            Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("                                 §c§lBuild Battle");
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("                     §eНе достаточно игроков для окончания игры");
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        }
    }

    public void onRespawn(Player player) {
    }

}

