/*
 * Decompiled with CFR 0_122.
 */
package com.wellfail.buildbattle.gui;

public enum GuiType {
    MANAGER(0),
    WEATHER(1),
    TIME(2),
    PARTICLES(3),
    BIOME(4),
    PARTICLESLIST(5),
    HEADS(6);
    
    private int type;

    private GuiType(int type) {
        this.type = type;
    }

    public int getID() {
        return this.type;
    }
}

