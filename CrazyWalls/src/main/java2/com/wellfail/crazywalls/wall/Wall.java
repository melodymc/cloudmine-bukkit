// 
// Decompiled by Procyon v0.5.30
// 

package com.wellfail.crazywalls.wall;

import org.bukkit.plugin.Plugin;
import com.wellfail.crazywalls.CrazyWalls;
import org.bukkit.Material;
import com.wellfail.crazywalls.config.Config;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.Location;
import com.wellfail.crazywalls.utils.Point;

public class Wall implements IWall
{
    private Point start;
    private Point end;
    private int height;
    
    @Override
    public void setStart(final Location start) {
        this.start = new Point(start.getBlockX(), start.getBlockZ(), start.getWorld());
    }
    
    @Override
    public void setEnd(final Location end) {
        this.end = new Point(end.getBlockX(), end.getBlockZ(), end.getWorld());
    }
    
    @Override
    public Point getStart() {
        return this.start;
    }
    
    @Override
    public Point getEnd() {
        return this.end;
    }
    
    @Override
    public int getHeight() {
        return this.height;
    }
    
    @Override
    public void setHeight(final int height) {
        this.height = height;
    }
    
    @Override
    public void destroy() {
        new BukkitRunnable() {
            public void run() {
                for (int x = Wall.this.start.getX(); x <= Wall.this.end.getX(); ++x) {
                    for (int y = 9; y <= Config.wallHeight + 20; ++y) {
                        for (int z = Wall.this.start.getZ(); z <= Wall.this.end.getZ(); ++z) {
                            if (y < Config.wallHeight) {
                                Wall.this.start.getWorld().getBlockAt(x, y, z).setType(Config.replace);
                            }
                            else {
                                Wall.this.start.getWorld().getBlockAt(x, y, z).setType(Material.AIR);
                            }
                        }
                    }
                }
            }
        }.runTask((Plugin)CrazyWalls.getInstance());
    }
    
    public Wall(final Location start, final Location end, final int height) {
        this.setHeight(height);
        this.setStart(start);
        this.setEnd(end);
    }
}
