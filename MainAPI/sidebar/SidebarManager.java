package ru.bird.main.sidebar;

import me.clip.placeholderapi.PlaceholderAPIPlugin;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import ru.bird.main.API;

import java.util.ArrayList;
import java.util.List;

public class SidebarManager {

    private static PlaceholderAPIPlugin placeholderAPI;
    private static List<Sidebar> sidebars = new ArrayList<Sidebar>();

    public void build(){
        API api = API.getInstance();
        if (api.getServer().getPluginManager().isPluginEnabled("PlaceholderAPI"))
        {
            try { placeholderAPI = (PlaceholderAPIPlugin) api.getServer().getPluginManager().getPlugin("PlaceholderAPI"); }
            catch (ClassCastException | NullPointerException e) {}

            if (placeholderAPI != null)
            {
                api.getLogger().info("Hooked PlaceholderAPI v" + placeholderAPI.getDescription().getVersion());
                new SidebarPlaceholders().hook();
            }
        }
        else {
            placeholderAPI = null;
        }
    }


    protected static void registerSidebar(Sidebar sidebar)
    {
        sidebars.add(sidebar);
    }


    @SuppressWarnings("unchecked")
    public static <T extends List<?>> T cast(Object obj) {
        return (T) obj;
    }

    public static void unregisterSidebar(Sidebar sidebar)
    {
        sidebars.remove(sidebar);
    }

    /**
     * Gets the PlaceholderAPIPlugin instance.
     * @return (PlaceholderAPIPlugin) - the instance of null if the plugin isn't hooked.
     * @since 2.4
     */
    public static PlaceholderAPIPlugin getPlaceholderAPI()
    {
        return placeholderAPI;
    }

    /**
     * Gets the Sidebar Object associated with the specified player.
     * Note that this will only return a Sidebar Object if it has been shown to the player and then not been hidden again.
     * Note also that this will only return a Sidebar Object if the specified player's scoreboard sidebar had been created with this API.
     * @param forPlayer (Player) - the player
     * @return
     */
    public static Sidebar getSidebar(Player forPlayer)
    {
        if (forPlayer == null) {
            throw new NullPointerException("forPlayer cannot be null!");
        }
        Objective obj = forPlayer.getScoreboard().getObjective(DisplaySlot.SIDEBAR);

        if (obj == null)
            return null;

        if (!sidebars.isEmpty()) {
            for (Sidebar sidebar : sidebars) {
                if (sidebar.getTitle().equals(obj.getDisplayName())) {
                    return sidebar;
                }
            }
        }
        return null;
    }
}