// 
// Decompiled by Procyon v0.5.30
// 

package com.wellfail.crazywalls.wall;

import com.wellfail.crazywalls.utils.Point;
import org.bukkit.Location;

public interface IWall
{
    void setHeight(final int p0);
    
    int getHeight();
    
    void setStart(final Location p0);
    
    void setEnd(final Location p0);
    
    Point getStart();
    
    Point getEnd();
    
    void destroy();
}
