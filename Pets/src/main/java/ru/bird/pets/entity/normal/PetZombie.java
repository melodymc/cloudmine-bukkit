package ru.bird.pets.entity.normal;

import java.util.UUID;

import net.minecraft.server.v1_16_R2.Entity;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;

import net.minecraft.server.v1_16_R2.DataWatcher;
import ru.bird.pets.entity.AbstractStandardPet;

public class PetZombie extends AbstractStandardPet {

	public PetZombie(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		Zombie entity = (Zombie) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		entity.setBaby(true);
        Entity craftEntity = ((CraftEntity) entity).getHandle();
        DataWatcher data = craftEntity.getDataWatcher();
		data.a();
		entity.remove();
		return data;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.ZOMBIE;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

}