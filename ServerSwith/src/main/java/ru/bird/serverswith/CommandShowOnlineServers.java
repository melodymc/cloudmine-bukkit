package ru.bird.serverswith;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.bird.chat.AbstractCommand;

public class CommandShowOnlineServers extends AbstractCommand {
    public CommandShowOnlineServers() {
        super("sosg");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            return false;
        }
        if (!(sender instanceof Player)) {
            return false;
        }
        String group = args[0];
//        SocketManager socketManager = API.getInstance().getSocketManager();

        sender.sendMessage("Online on groups servers: " + group + " equal " + MessageManager.getGroupOnline(group));
        return true;
    }
}