/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.entity.Player
 */
package ru.bird.main.game;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.bird.main.utils.GameUtils;

public class GameManager {

    public static void redirectToLobby() {
        Bukkit.getOnlinePlayers().forEach(GameManager::redirectToLobby);
    }
//
    public static void redirectToLobby(Player player) {
        GameUtils.redirectToGroup(player, "Lobby");
    }

}