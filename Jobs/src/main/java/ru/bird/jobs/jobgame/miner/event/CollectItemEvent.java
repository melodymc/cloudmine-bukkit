package ru.bird.jobs.jobgame.miner.event;

import org.bukkit.inventory.Inventory;
import ru.bird.jobs.jobgame.event.JobEventUnified;
import ru.bird.jobs.jobgame.miner.ItemData;
import ru.bird.jobs.jobgame.miner.JobGameMiner;

public class CollectItemEvent extends JobEventUnified
{
    private int gotPoints;
    private int slot;
    private Inventory levelData;
    private ItemData itemData;
    private boolean isBonus;

    public CollectItemEvent(JobGameMiner instance, int gotPoints, int slot, Inventory levelData, ItemData itemData, boolean isBonus)
    {
        super(instance);

        this.gotPoints = gotPoints;
        this.slot = slot;
        this.levelData = levelData;
        this.itemData = itemData;
        this.isBonus = isBonus;
    }

    public int getDiffByPoints()
    {
        return gotPoints;
    }

    public int getSlot()
    {
        return slot;
    }

    public Inventory getLevelData()
    {
        return levelData;
    }

    public ItemData getItemData()
    {
        return itemData;
    }

    public boolean isBonus()
    {
        return isBonus;
    }
}
