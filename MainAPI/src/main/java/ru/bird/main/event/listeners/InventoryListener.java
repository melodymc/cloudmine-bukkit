/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.PlayerInventory
 */
package ru.bird.main.event.listeners;

import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.PlayerInventory;
import ru.bird.main.event.listeners.FListener;

public class InventoryListener
extends BasicListener {
    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        e.setCancelled(true);
        if (e.getClickedInventory() == null || e.getWhoClicked() == null || e.getWhoClicked().getInventory() == null) {
            return;
        }
        if (e.getClickedInventory().equals((Object)e.getWhoClicked().getInventory())) {
            e.getWhoClicked().closeInventory();
        }
    }
}

