/*
 * Decompiled with CFR 0_118.
 *
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.World
 *  org.bukkit.block.Block
 *  org.bukkit.block.Chest
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Item
 *  org.bukkit.entity.Player
 *  org.bukkit.entity.Villager
 *  org.bukkit.event.Event
 *  org.bukkit.event.Event$Result
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.block.BlockBreakEvent
 *  org.bukkit.event.block.BlockPlaceEvent
 *  org.bukkit.event.entity.EntityDamageEvent
 *  org.bukkit.event.entity.EntityExplodeEvent
 *  org.bukkit.event.inventory.InventoryAction
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.player.PlayerInteractEntityEvent
 *  org.bukkit.event.player.PlayerMoveEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  ru.wellfail.wapi.events.GenChestEvent
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.crazywalls.game;

import com.wellfail.crazywalls.config.Config;
import com.wellfail.crazywalls.game.GameFactory;
import com.wellfail.crazywalls.shop.Shop;
import com.wellfail.crazywalls.utils.Barrier;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.wellfail.wapi.events.GenChestEvent;
import ru.wellfail.wapi.utils.Utils;

public class GameListener
        implements Listener {
    @EventHandler
    public void onNpcClick(PlayerInteractEntityEvent e) {
        Player player = e.getPlayer();
        Entity entity = e.getRightClicked();
        if (!(entity instanceof Villager)) {
            return;
        }
        e.setCancelled(true);
        GameFactory.shops.get(e.getPlayer().getName()).open(e.getPlayer());
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Villager) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onChestGen(GenChestEvent e) {
        if (e.getChest().getType() == Material.TRAPPED_CHEST) {
            Utils.stringToItems((Inventory)e.getChest().getInventory(), (String)Config.traped, (boolean)true, (boolean)false);
        } else {
            Utils.stringToItems((Inventory)e.getChest().getInventory(), (String)Config.chest, (boolean)true, (boolean)false);
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (e.getTo().getX() == e.getFrom().getX() && e.getTo().getY() == e.getFrom().getY() && e.getTo().getZ() == e.getFrom().getZ()) {
            return;
        }
        if (Config.barrier.isInside(e.getTo())) {
            return;
        }
        e.getPlayer().teleport(e.getFrom());
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        Material type = e.getBlock().getType();
        if (!Config.barrier.isInside(e.getBlock().getLocation())) {
            e.setCancelled(true);
            return;
        }
        if (e.getBlock().getLocation().getY() > 38.0) {
            e.setCancelled(true);
        }
        if (type == Material.STAINED_CLAY && (e.getBlock().getData() == 14 || e.getBlock().getData() == 5)) {
            e.setCancelled(true);
            return;
        }
        if (type == Material.STAINED_GLASS && (e.getBlock().getData() == 14 || e.getBlock().getData() == 5)) {
            e.setCancelled(true);
            return;
        }
        if (type == Material.IRON_ORE) {
            e.setCancelled(true);
            e.getBlock().setType(Material.AIR);
            e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.IRON_INGOT));
        }
        if (type == Material.GOLD_ORE) {
            e.setCancelled(true);
            e.getBlock().setType(Material.AIR);
            e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.GOLD_INGOT));
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!Config.barrier.isInside(event.getBlock().getLocation())) {
            event.setCancelled(true);
            return;
        }
        if (event.getBlock().getLocation().getY() > 38.0) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onExplode(EntityExplodeEvent e) {
        Iterator it = e.blockList().iterator();
        while (it.hasNext()) {
            Block block = (Block)it.next();
            if (Config.barrier.isInside(block.getLocation())) {
                if (block.getType() != Material.STAINED_CLAY && block.getData() != 14 && block.getData() != 5 && block.getType() != Material.STAINED_GLASS && block.getData() != 14 && block.getData() != 5) continue;
                it.remove();
                continue;
            }
            it.remove();
        }
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void moveInv(InventoryClickEvent e) {
        if (e.getInventory().getName().equals("\u041c\u0430\u0433\u0430\u0437\u0438\u043d")) {
            if (e.getAction() == InventoryAction.HOTBAR_MOVE_AND_READD || e.getAction() == InventoryAction.HOTBAR_SWAP || e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY) {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                return;
            }
            GameFactory.shops.get(e.getWhoClicked().getName()).clickEvent(e);
        }
    }
}

