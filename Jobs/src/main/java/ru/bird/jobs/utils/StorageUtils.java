package ru.bird.jobs.utils;

import de.leonhard.storage.Json;
import de.leonhard.storage.internal.settings.ReloadSettings;
import ru.bird.jobs.JobsPlugin;
import ru.bird.storageUtils.utils.StorageMeow;

public class StorageUtils {

    public static Json jobsUserJson(String stringUUID) {
        return StorageMeow.loadJsonConfig(stringUUID, JobsPlugin.get().playersDir, ReloadSettings.AUTOMATICALLY);
    }
}
