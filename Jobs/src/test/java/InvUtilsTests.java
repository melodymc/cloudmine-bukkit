import org.testng.Assert;
import org.testng.annotations.Test;
import ru.bird.jobs.utils.Directions;
import ru.bird.jobs.utils.InvUtils;

public class InvUtilsTests
{
    @Test
    public void test_edgeSlot()
    {
        Assert.assertEquals(InvUtils.edgeSlot(13, Directions.UP, 9, 3), 4);
        Assert.assertEquals(InvUtils.edgeSlot(13, Directions.DOWN, 9, 3), 22);
        Assert.assertEquals(InvUtils.edgeSlot(13, Directions.RIGHT, 9, 3), 17);
        Assert.assertEquals(InvUtils.edgeSlot(13, Directions.LEFT, 9, 3), 9);
    }
}
