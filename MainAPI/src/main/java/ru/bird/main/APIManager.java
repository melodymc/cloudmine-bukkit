package ru.bird.main;

import org.apache.logging.log4j.LogManager;
import ru.bird.main.event.listeners.GamerListener;
import ru.bird.main.groups.Groups;
import ru.bird.main.logger.BaseLogger;

public class APIManager {
    public void build(API instance) {
        instance.initProtocolManager();
//        instance.initRabbitMQ();
        instance.initGame();
//        instance.initSocket();
//        instance.initVault();
        instance.initHolograms();
//        instance.initSidebar();
//        instance.initServerManagerClient();
    }
}