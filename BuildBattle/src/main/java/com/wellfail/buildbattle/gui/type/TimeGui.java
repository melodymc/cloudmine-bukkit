/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.buildbattle.gui.type;

import com.wellfail.buildbattle.GameFactory;
import com.wellfail.buildbattle.gui.Gui;
import com.wellfail.buildbattle.gui.GuiType;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import ru.bird.main.utils.ItemUtils;

import java.util.List;

public class TimeGui
extends Gui {
    private Gui gui;
    private static List<String> list = null;

    public TimeGui(Gui gui) {
        super(gui, 2, 4, "\u0412\u0440\u0435\u043c\u044f");
        this.gui = gui;
    }

    public void init() {
        this.fillItems();
        this.addAction(31, this.gui.getGui(GuiType.MANAGER));
    }

    private void fillItems() {
        this.inventory.setItem(0, ItemUtils.createItem((Material)Material.STAINED_CLAY, (int)1, (int)13, (String)"\u00a7a6 \u0427\u0430\u0441\u043e\u0432 \u0443\u0442\u0440\u0430", list));
        this.inventory.setItem(1, ItemUtils.createItem((Material)Material.STAINED_CLAY, (int)1, (int)14, (String)"\u00a7a9 \u0427\u0430\u0441\u043e\u0432 \u0443\u0442\u0440\u0430", list));
        this.inventory.setItem(2, ItemUtils.createItem((Material)Material.STAINED_CLAY, (int)1, (int)14, (String)"\u00a7a\u041f\u043e\u043b\u0434\u0435\u043d\u044c", list));
        this.inventory.setItem(3, ItemUtils.createItem((Material)Material.STAINED_CLAY, (int)1, (int)14, (String)"\u00a7a3 \u0427\u0430\u0441\u0430 \u0434\u043d\u044f", list));
        this.inventory.setItem(4, ItemUtils.createItem((Material)Material.STAINED_CLAY, (int)1, (int)14, (String)"\u00a7a6 \u0427\u0430\u0441\u043e\u0432 \u0432\u0435\u0447\u0435\u0440\u0430", list));
        this.inventory.setItem(5, ItemUtils.createItem((Material)Material.STAINED_CLAY, (int)1, (int)14, (String)"\u00a7a9 \u0427\u0430\u0441\u043e\u0432 \u0432\u0435\u0447\u0435\u0440\u0430", list));
        this.inventory.setItem(6, ItemUtils.createItem((Material)Material.STAINED_CLAY, (int)1, (int)14, (String)"\u00a7a\u041f\u043e\u043b\u043d\u043e\u0447\u044c", list));
        this.inventory.setItem(7, ItemUtils.createItem((Material)Material.STAINED_CLAY, (int)1, (int)14, (String)"\u00a7a3 \u0427\u0430\u0441\u0430 \u043d\u043e\u0447\u0438", list));
        this.inventory.setItem(8, ItemUtils.createItem((Material)Material.STAINED_CLAY, (int)1, (int)14, (String)"\u00a7a6 \u0427\u0430\u0441\u043e\u0432 \u0443\u0442\u0440\u0430", list));
        this.inventory.setItem(31, ItemUtils.createItem((Material)Material.ARROW, (String)"\u00a7a\u041d\u0430\u0437\u0430\u0434"));
    }

    @Override
    protected void onClick(InventoryClickEvent e) {
        long time;
        int slot = e.getSlot();
        switch (slot) {
            case 0: 
            case 8: {
                time = 0;
                break;
            }
            case 1: {
                time = 3000;
                break;
            }
            case 2: {
                time = 6000;
                break;
            }
            case 3: {
                time = 9000;
                break;
            }
            case 4: {
                time = 12000;
                break;
            }
            case 5: {
                time = 15000;
                break;
            }
            case 6: {
                time = 18000;
                break;
            }
            case 7: {
                time = 21000;
                break;
            }
            default: {
                return;
            }
        }
        for (int i = 0; i <= 8; ++i) {
            if (i != e.getSlot()) {
                this.inventory.getItem(i).setDurability((short) 14);
                continue;
            }
            this.inventory.getItem(i).setDurability((short) 13);
        }
        this.inventory.getItem(slot).setDurability((short) 13);
        GameFactory.getInstance().getRegion((Player)e.getWhoClicked()).setTime(time);
    }
}

