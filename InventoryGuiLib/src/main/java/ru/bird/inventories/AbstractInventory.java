package ru.bird.inventories;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class AbstractInventory implements Listener {

	private Inventory inventory;
	private JavaPlugin plugin;

	public AbstractInventory(JavaPlugin plugin, String title, int size) {
		this.plugin = plugin;
		this.inventory = Bukkit.createInventory(null, (size > 7 || size < 0 ? 7 : size) * 9, title);
		Bukkit.getPluginManager().registerEvents(this, this.plugin);
	}

	public abstract void onInventoryBuild(Player player, Inventory inventory);
	public abstract void onInventoryDestruct(Player player, Inventory inventory);

	public abstract void onSlotClicked(Player player, InventoryClickEvent event);

	public Inventory getBukkitInventory() {
		return inventory;
	}

	public void showTo(Player player) {
		player.closeInventory();
		player.openInventory(inventory);
	}

	@EventHandler
	public void bukkitClickEvent(InventoryClickEvent event) {
		if (event.getInventory().hashCode() == inventory.hashCode()) {
			this.onSlotClicked((Player) event.getWhoClicked(), event);
		}
	}

	@EventHandler
	public void bukkitOpenEvent(InventoryOpenEvent event) {
		this.onInventoryBuild((Player) event.getPlayer(), inventory);
		event.getHandlers().unregister(this);
	}

	@EventHandler
	public void bukkitCloseEvent(InventoryCloseEvent event) {
		this.onInventoryDestruct((Player) event.getPlayer(), inventory);
		event.getHandlers().unregister(this);
	}

}
