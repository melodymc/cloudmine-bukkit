/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.block.BlockBreakEvent
 *  org.bukkit.event.block.BlockPlaceEvent
 */
package ru.bird.main.event.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.game.GameSettings;
import ru.bird.main.event.listeners.FListener;

public class BlockListener
extends BasicListener {

    @EventHandler(priority=EventPriority.LOWEST)
    public void onBlockBreak(BlockBreakEvent e) {
        if (GameSettings.isBreak) {
            return;
        }
        e.setCancelled(true);
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onBlockPlace(BlockPlaceEvent e) {
        if (GameSettings.isPlace) {
            return;
        }
        e.setCancelled(true);
    }

    private boolean contains(int[] array, int id) {
        for (int i : array) {
            if (i != id) continue;
            return true;
        }
        return false;
    }
}

