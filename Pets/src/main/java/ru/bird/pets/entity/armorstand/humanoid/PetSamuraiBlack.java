package ru.bird.pets.entity.armorstand.humanoid;

import java.util.UUID;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.EulerAngle;

import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;

public class PetSamuraiBlack extends AbstractArmoredPet {

	public PetSamuraiBlack(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setLeftLegPose(new EulerAngle(0.3, 0, 0));
		armor.setRightLegPose(new EulerAngle(0.3, 0, 0));
		armor.setLeftArmPose(new EulerAngle(-1.57, 0, 0));
		armor.setRightArmPose(new EulerAngle(-1.57, 0, 0));
		armor.setArms(true);
		armor.setVisible(true);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}



	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getMainHand() {
		return new ItemStack(Material.IRON_SWORD);
	}
	
	@Override
	public ItemStack getOffHand() {
		return new ItemStack(Material.IRON_SWORD);
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		LeatherArmorMeta lim = (LeatherArmorMeta) leggings.getItemMeta();
		lim.setColor(Color.BLACK);
		leggings.setItemMeta(lim);
		return leggings;
	}

	@Override
	public ItemStack getChestplate() {
		ItemStack chestPlate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		LeatherArmorMeta cim = (LeatherArmorMeta) chestPlate.getItemMeta();
		cim.setColor(Color.BLACK);
		chestPlate.setItemMeta(cim);
		return chestPlate;
	}

	@Override
	public ItemStack getHelmet() {
		ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta meta = (SkullMeta) head.getItemMeta();
		meta.setOwner("BlackSamurai");
		head.setItemMeta(meta);
		return head;
	}

	@Override
	public void onLivingUpdate() {
		this.setLeftArmPose(-50 + 10 * MathUtils.cos(), 340, 0);
		this.setRightArmPose(-50 + 10 * MathUtils.cos(), 20, 0);
		this.sendWatcherUpdate();
	}

}
