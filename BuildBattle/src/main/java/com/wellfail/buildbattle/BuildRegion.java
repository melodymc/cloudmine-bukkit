package com.wellfail.buildbattle;

import com.wellfail.buildbattle.gui.Gui;
import com.wellfail.buildbattle.gui.GuiType;
import net.minecraft.server.v1_16_R2.ChunkCoordIntPair;
import net.minecraft.server.v1_16_R2.EntityHuman;
import net.minecraft.server.v1_16_R2.EntityPlayer;
import net.minecraft.server.v1_16_R2.World;
import org.bukkit.*;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_16_R2.CraftChunk;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.bird.main.game.Game;

import java.util.*;

public class BuildRegion {
    private Player player;
    private Location min;
    private Location max;
    private Location spawn;
    private long time;
    private Biome biome;
    private WeatherType weather;
    private boolean music;
    private List<Particle> effects;
    private Gui gui;
    private Map<Player, Integer> points = new HashMap<Player, Integer>();

    public void setPoints(Player p, int points) {
        this.points.put(p, points);
    }

    public Player getPlayer() {
        return this.player;
    }

    public List<Particle> getEffects() {
        return this.effects;
    }

    public int getScore() {
        int score = 0;
        for (int i : this.points.values()) {
            score += i;
        }
        return score;
    }

    public void openEffects() {
        if (this.player == null || !this.player.isOnline()) {
            return;
        }
        this.getGui(GuiType.PARTICLES).open(this.player);
    }

    public Gui getGui(GuiType type) {
        return this.gui.getGui(type);
    }

    public BuildRegion(Location one, Location two) {
        if (one.getWorld() != two.getWorld()) {
            throw new IllegalArgumentException("Ты че еблан? Ты 2 разных мира указал даун..  (с) Yooxa");
        }
        this.min = new Location(one.getWorld(), Math.min(one.getX(), two.getX()), Math.min(one.getY(), two.getY()), Math.min(one.getZ(), two.getZ()));
        this.max = new Location(one.getWorld(), Math.max(one.getX(), two.getX()), Math.max(one.getY(), two.getY()), Math.max(one.getZ(), two.getZ()));
        this.effects = new ArrayList<Particle>();
        this.spawn = new Location(this.min.getWorld(), this.max.getX() - (this.max.getX() - this.min.getX()) / 2.0, this.max.getY() - (this.max.getY() - this.min.getY()) / 2.0, this.max.getZ() - (this.max.getZ() - this.min.getZ()) / 2.0);
        this.weather = WeatherType.CLEAR;
        this.setBiome(Biome.PLAINS);
        this.fillFloor(159, (byte) 0);
        this.setTime(0);
        this.gui = new Gui(this);
    }

    public Location getSpawn() {
        return this.spawn;
    }

    public void setPlayer(Player player) {
        this.player = player;
        player.teleport(this.spawn);
        player.setGameMode(GameMode.CREATIVE);
        player.setFlying(true);
        player.setPlayerTime(this.time, false);
        player.setPlayerWeather(this.weather);
    }

    public boolean isInside(Location loc) {
        return (int) this.min.getX() <= (int) loc.getX() && (int) loc.getX() <= (int) this.max.getX() && (int) this.min.getY() <= (int) loc.getY() && (int) loc.getY() <= (int) this.max.getY() && (int) this.min.getZ() <= (int) loc.getZ() && (int) loc.getZ() <= (int) this.max.getZ();
    }

    public void fillFloor(int id, byte data) {
        if (id == 326) {
            id = 8;
        }
        if (id == 327) {
            id = 10;
        }
        for (Block block : this.getFloor()) {
            if (block.getTypeId() == id && block.getData() == data) continue;
            block.setTypeIdAndData(id, data, false);
        }
    }

    public Set<Block> getFloor() {
        HashSet<Block> set = new HashSet<Block>();
        for (int x = (int)this.min.getX(); x <= (int)this.max.getX(); ++x) {
            for (int z = (int)this.min.getZ(); z <= (int)this.max.getZ(); ++z) {
                set.add(this.min.getWorld().getBlockAt(new Location(this.min.getWorld(), (double)x, this.min.getY(), (double)z)));
            }
        }
        return set;
    }

    public Set<Block> getBlocks() {
        HashSet<Block> set = new HashSet<Block>();
        for (int x = (int)this.min.getX(); x <= (int)this.max.getX(); ++x) {
            for (int y = (int)this.min.getY(); y <= (int)this.max.getY(); ++y) {
                for (int z = (int)this.min.getZ(); z <= (int)this.max.getZ(); ++z) {
                    set.add(this.min.getWorld().getBlockAt(new Location(this.min.getWorld(), (double)x, (double)y, (double)z)));
                }
            }
        }
        return set;
    }

    public void fillFloor(ItemStack item) {
        this.fillFloor(item.getTypeId(), (byte)item.getDurability());
    }

    public void music() {
        boolean bl = this.music = !this.music;
        if (this.music) {
            this.spawn.getWorld().playEffect(this.spawn, Effect.RECORD_PLAY, 2259);
        }
    }


    public void setBiome(Biome biome) {
        if (this.biome != null && this.biome == biome) {
            return;
        }
        this.biome = biome;
        for (Block block : this.getFloor()) {
            block.setBiome(biome);
        }
        this.refreshChunks(); // TODO посмотреть работает ли
    }

    private void refreshChunks() {
        HashSet<Chunk> chunks = new HashSet<org.bukkit.Chunk>();
        this.getFloor().stream().filter(b -> !chunks.contains(b.getChunk())).forEach(b -> {
            chunks.add(b.getChunk());
        }
        );
//        int view = Bukkit.getServer().getViewDistance() << 4;
        for (Chunk chunk : chunks) {
            if(chunk.unload()) {
                chunk.load();
            }

//            World world = ((CraftChunk)chunk).getHandle().world;
//            for (EntityHuman ep : world.players) {
//                double diffx = Math.abs(ep.locX - (double)(chunk.getX() << 4));
//                double diffz = Math.abs(ep.locZ - (double)(chunk.getZ() << 4));
//                if (diffx > (double)view || diffz > (double)view) continue;
//                ((EntityPlayer)ep).chunkCoordIntPairQueue.add(new ChunkCoordIntPair(chunk.getX(), chunk.getZ()));
//            }
        }
    }

    public void setWeather(int type) {
        switch (type) {
            case 1: {
                this.setBiome(Biome.PLAINS);
                this.setWeather(WeatherType.DOWNFALL);
                this.player.sendMessage(Game.getPrefix() + "\u041f\u043e\u0433\u043e\u0434\u0430 - \u00a7e\u0414\u043e\u0436\u0434\u044c");
                break;
            }
            case 2: {
                this.setBiome(Biome.ICE_MOUNTAINS);
                this.setWeather(WeatherType.DOWNFALL);
                this.player.sendMessage(Game.getPrefix() + "\u041f\u043e\u0433\u043e\u0434\u0430 - \u00a7e\u0421\u043d\u0435\u0433");
                break;
            }
            case 3: {
                this.setWeather(WeatherType.CLEAR);
                this.player.sendMessage(Game.getPrefix() + "\u041f\u043e\u0433\u043e\u0434\u0430 - \u00a7e\u0421\u043e\u043b\u043d\u0435\u0447\u043d\u043e");
            }
        }
    }

//    public void addEffect(Effects1_8.ParticleType type, ItemStack item) {
//        if (this.effects.size() == 10) {
//            this.player.sendMessage(Game.getPrefix() + "\u00a7f\u0412\u044b \u0443\u0441\u0442\u0430\u043d\u043e\u0432\u0438\u043b\u0438 \u043c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u043e\u0435 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u044d\u0444\u0444\u0435\u043a\u0442\u043e\u0432");
//            return;
//        }
//        Particle effect = new Particle(this.player.getLocation(), type, item);
//        this.effects.add(effect);
//    }

//    public void removeEffect(int i) {
//        if (i > this.effects.size()) {
//            return;
//        }
//        Particle particle = this.effects.remove(i);
//        particle.remove();
//        ((ParticlesManagerGui)this.getGui(GuiType.PARTICLESLIST)).update();
//    }

    public void setWeather(WeatherType type) {
        this.weather = type;
        if (this.player != null && this.player.isOnline()) {
            this.player.setPlayerWeather(type);
        }
    }

    public void setTime(long time) {
        this.time = time;
        if (this.player != null && this.player.isOnline()) {
            this.player.setPlayerTime(time, false);
        }
    }

    public void teleport() {
        Location loc = this.spawn.clone();
        loc.setY(this.max.getY() - 2.0);
        //Title title = new Title("\u00a7e\u0423\u0447\u0430\u0441\u0442\u043e\u043a \u0438\u0433\u0440\u043e\u043a\u0430", this.player.getDisplayName(), 0, 3, 2);
        //title.broadcast();
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.teleport(loc);
            player.setPlayerWeather(this.weather);
            player.setPlayerTime(this.time, false);
            player.setFlying(true);
        }
    }
}

