package ru.bird.jobs.jobgame.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import ru.bird.jobs.jobgame.JobGame;
import ru.bird.jobs.jobgame.Worker;

public abstract class JobEventUnified extends Event
{
    private static final HandlerList handlers = new HandlerList();
    private JobGame jobInstance;

    public JobEventUnified(JobGame jobInstance)
    {
        this.jobInstance = jobInstance;
    }

    public JobGame getJobInstance()
    {
        return jobInstance;
    }

    public Worker getWorker()
    {
        return jobInstance.worker;
    }

    @Override
    public HandlerList getHandlers()
    {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }
}