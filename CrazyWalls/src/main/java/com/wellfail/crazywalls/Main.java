package com.wellfail.crazywalls;

import com.wellfail.crazywalls.game.GameFactory;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.game.GameSettings;
import ru.bird.main.utils.GameUtils;

public class Main extends JavaPlugin
{
    private static Main instance;

    public static Main getInstance() {
        return instance;
    }

    public void onLoad() {
        instance = this;
    }

    public void onEnable()
    {
        this.saveDefaultConfig();
        GameSettings.prefix = "\u00a7eCrazyWalls \u00a7f| ";
        GameSettings.wboardname = "CRAZY WALLS";
        GameSettings.damage = true;
        GameSettings.isSpectate = true;
        GameSettings.damage = true;
        GameSettings.pickup = true;
        GameSettings.inventory = true;
        GameSettings.drop = true;
        GameSettings.fallDamage = true;
        GameSettings.physical = false;
        GameSettings.itemSpawn = true;
        GameSettings.chest = true;
        GameSettings.isFoodChange = true;
        GameSettings.shop = true;
        GameSettings.explode = true;
        GameSettings.itemdrop = true;
        GameSettings.crops = true;
        GameSettings.gameTime = 840;
        if (this.getConfig().getBoolean("Setup")) {
            GameSettings.setup = true;
        }
        GameSettings.slots = this.getConfig().getInt("Slots");
        GameSettings.toStart = GameSettings.slots - 4;
        GameSettings.mapLocation = GameUtils.stringToLocation((String)this.getConfig().getString("Map"));
        new GameFactory();
    }
}

