// 
// Decompiled by Procyon v0.5.30
// 

package ru.bird.murdermistery.game;

import org.bukkit.entity.Player;
import ru.wellfail.wapi.logger.WLogger;

public class GPlayer
{
    private Player player;
    private int kills;
    private int fb;
    private int score;
    private Role role;

    public GPlayer(final Player player, Role role) {
        this.player = player;
        this.role = role;
    }

    public Player getPlayer() {
        return this.player;
    }

    public int getKills() {
        return this.kills;
    }

    public void setFb() {
        ++this.fb;
    }

    public Role getRole() {
        return this.role;
    }

    public void setScore(int score) {
        WLogger.info("Score set " + score + " player "+ player.getDisplayName());
        this.score = score;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getScore(){
        return this.score;
    }

    public void addScore(int score) {
        WLogger.info("Score add " + (this.score + score) + " player "+ player.getDisplayName());
        this.score += score;
    }

    public int getFb() {
        return this.fb;
    }

    public void setKills() {
        ++this.kills;
    }
}