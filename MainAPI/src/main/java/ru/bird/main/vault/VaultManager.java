package ru.bird.main.vault;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import ru.bird.main.API;
import ru.bird.main.groups.Groups;
import ru.bird.main.logger.BaseLogger;

public class VaultManager {

    private static Economy economy;
    private static Chat chat;
    private Permission permission;

    public void build(){
        if (!setupVault() ) {
            Bukkit.getServer().getPluginManager().disablePlugin(API.getInstance());
            return;
        }
    }

    private boolean setupVault() {
        return (setupEconomy() && setupPermission());
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> rsp = API.getInstance().getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            BaseLogger.error("Disabled because Vault vault not initialization!");
            return false;
        }
        economy = rsp.getProvider();
        return true;
    }

    private boolean setupPermission() {
        RegisteredServiceProvider<Permission> rsp = API.getInstance().getServer().getServicesManager().getRegistration(Permission.class);
        if (rsp == null) {
            BaseLogger.error("Disabled because Vault Permission not initialization!");
            return false;
        }
        permission = rsp.getProvider();
        return true;
    }

    private boolean setupChat(){
        RegisteredServiceProvider<Chat> rsp = API.getInstance().getServer().getServicesManager().getRegistration(Chat.class);
        if (rsp == null) {
            BaseLogger.error("Disabled because Vault chat not initialization!");
            return false;
        }
        chat = rsp.getProvider();
        return true;
    }

    public Economy getEconomy() {
        return economy;
    }

    public Chat getChat() {
        return chat;
    }

    public Permission getPermission() {
        return permission;
    }
}