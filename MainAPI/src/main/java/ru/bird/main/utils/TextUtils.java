package ru.bird.main.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;

public class TextUtils {

	public static String colorize(String text) {
		return ChatColor.translateAlternateColorCodes('&', text);
	}

	public static List<String> convert(String in, int lenght) {
		in = ChatColor.stripColor(in);
		String colour = "";
		if (in.startsWith("&")) {
			colour = in.substring(0, 2);
			in = in.substring(2, in.length());
		}
		StringBuilder sb = new StringBuilder(in);
		int i = 0;
		while (i + lenght < sb.length() && (i = sb.lastIndexOf(" ", i + lenght)) != -1) {
			sb.replace(i, i + 1, "#" + colour);
		}
		String[] returned = sb.toString().split("#");
		returned[0] = colour + returned[0];
		i = 0;
		for (String part : returned) {
			returned[i] = colorize(part);
			i++;
		}
		return Arrays.asList(returned);
	}

	public static List<String> convertKeepColor(String in, int lenght) {
		StringBuilder sb = new StringBuilder(in);
		int i = 0;
		while (i + lenght < sb.length() && (i = sb.lastIndexOf(" ", i + lenght)) != -1) {
			sb.replace(i, i + 1, "©");
		}
		String[] returned = sb.toString().split("©");
		return Arrays.asList(returned);
	}

	public static List<String> cloneList(List<String> list) {
		if (list == null) {
			return new ArrayList<String>();
		}
		List<String> clone = new ArrayList<String>(list.size());
		for (String item : list)
			clone.add(item);
		return clone;
	}

}
