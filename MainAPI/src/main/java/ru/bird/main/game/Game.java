package ru.bird.main.game;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.inventories.Item;
import ru.bird.inventories.ItemManager;
import ru.bird.inventories.ItemRunnable;
import ru.bird.main.API;
import ru.bird.main.actionbar.ActionBarApi;
import ru.bird.main.event.listeners.*;
import ru.bird.main.inventories.ShopManager;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.utils.BExecutorService;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.TaskManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class Game {
    private static Game instance;
    public static WaitingSidebar waitingBoard;
    private WaitingListener waiting;
    private StartListener start;
    private Listener listener;
    public Item lobbyItem;
    public Item shopItem;
    public Item teamChanger;
    public static List<Player> spectators;
    public List<Player> wins = new ArrayList<>();

    public static Game getInstance() {
        return instance;
    }

    public static String getPrefix() {
        return GameSettings.prefix;
    }

    public static void setPrefix(String s) {
        GameSettings.prefix = s;
    }

    public abstract void onRespawn(Player var1);

    protected abstract void onStartGame();

    protected abstract void onEndGame();

    public static void broadcast(String message) {
        Bukkit.broadcastMessage((Game.getPrefix() + message));
    }

    public Game(Listener listener) {
        instance = this;
        waitingBoard = new WaitingSidebar(GameSettings.wboardname.toUpperCase());
        this.listener = listener;
        new ItemManager(API.getInstance());
        new ActionBarApi().load();
        new GamerListener();
        if (!GameSettings.isWeatherChange) {
            new WeatherListener();
        }
        if (!GameSettings.isFoodChange) {
            new FoodListener();
        }
        if (!GameSettings.isTimeChange) {
            new TimeListener();
        }
        if (!GameSettings.pickup) {
            new PickupListener();
        }
        if (!GameSettings.itemdrop) {
            new DropListener();
        }
        if (!GameSettings.fallDamage) {
            new FallListener();
        }
        if (!GameSettings.physical) {
            new PhysicalListener();
        }
        if (!GameSettings.inventory) {
            new InventoryListener();
        }
        if (GameSettings.shop) {
            new ShopManager();
        }
        if (GameSettings.isSpectate) {
            new SpectatorListener();
        }
        if (!GameSettings.damage) {
            new DamageListener();
        }
        if (GameSettings.chest) {
            new ChestListener();
        }
        if (GameSettings.explode) {
            new ExplosionListener();
        }
        if (!GameSettings.itemSpawn) {
            new SpawnListener();
        }
        if (GameSettings.specthrow) {
            new SpecThrow();
        }
        new BlockListener();
        new LoginListener();
        if (GameSettings.death) {
            new DeathListener();
        }
        if (GameSettings.removearrowonhit) {
            new RemoveArrowOnHit();
        }
        this.start = new StartListener();
        if (GameSettings.setup) {
            new GameSetup().register();
        } else {
            this.waiting = new WaitingListener();
        }

        GameState.setCurrent(GameState.WAITING);
        new JoinListener();

        new GameManager();
//        new CommandListener();
        GameUtils.setupGhostLib();
        new GameModeListener();
        this.loadItems();
    }

    private void loadItems() {
        ItemStack toLobby = new ItemStack(Material.RED_BED);
        ItemMeta meta = toLobby.getItemMeta();
        meta.setDisplayName("§a[‣] §fВернуться в §e§lLOBBY §7(ПКМ)");
        toLobby.setItemMeta(meta);
        this.lobbyItem = new Item(toLobby, new ItemRunnable(){
            @Override
            public void onUse(PlayerInteractEvent e) {
                GameManager.redirectToLobby(e.getPlayer());
            }
            @Override
            public void onClick(InventoryClickEvent paramInventoryClickEvent) {}
        }, new Action[]{Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK});
    }

    public StartListener getStart() {
        return start;
    }

    public void registerListener(Listener listener) {
        if (listener == null) {
            return;
        }
        Bukkit.getServer().getPluginManager().registerEvents(listener, API.getInstance());
    }

    public void onGameStart() {
        ActionBarApi.getTasks().forEach((player, tasks) -> {
            BaseLogger.info("all actionbar tasks canceled for " + player.getName());
            tasks.forEach(BukkitRunnable::cancel);
        });
        JoinListener.run.cancel();
        HandlerList.unregisterAll(this.waiting);
        this.registerListener(this.listener);
        GameState.setCurrent(GameState.GAME);;
//        API.getInstance().getServerManagerClient().sendServerInfo(true, false);
        GameSettings.mapLocation.getWorld().setGameRuleValue("announceAdvancements", "false");
        Bukkit.getOnlinePlayers().forEach(player -> {
            player.getInventory().clear();
            player.setExp(0.0f);
            player.setLevel(0);
            player.setHealth(20.0);
            player.setFoodLevel(20);
            player.getActivePotionEffects().forEach(effect -> {
                player.removePotionEffect(effect.getType());
            });

        });
//        waitingBoard.remove();
        spectators.forEach(player -> {
            player.teleport(GameSettings.spectrLocation);
        });

        this.onStartGame();
    }

    public static void saveStat(String request) {
        TaskManager.addTask(() -> {
            try {
                API.getInstance().getSQLConnection().getConnection().prepareStatement(request).execute();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void endGame() {
        GameState.setCurrent(GameState.END);
        HandlerList.unregisterAll(this.listener);
        this.registerListener(this.waiting);
//        Bukkit.getOnlinePlayers().forEach(this::addDust);
        this.onEndGame();
//        this.wins.forEach(pl -> {});
        new Thread(() -> {
            try {
                Thread.sleep(5000);
                Gamer.clear();
                Thread.sleep(5000);
                GameManager.redirectToLobby();
                Thread.sleep(5000);
                while (BExecutorService.hasTask()) {
                    Thread.sleep(1000);
                }
                GameState.setCurrent(GameState.END);
                Bukkit.getServer().shutdown();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

//    private void addDust(Player pl) {
//        pl.sendMessage(pl.getDisplayName() + " добавлено 10 пыли");
//        PlayerData playerData = PlayerDataManager.getData(pl);
//        playerData.setMysteryDust(playerData.getMysteryDust() + 10);
//    }

    static {
        spectators = new ArrayList<Player>();
    }

}