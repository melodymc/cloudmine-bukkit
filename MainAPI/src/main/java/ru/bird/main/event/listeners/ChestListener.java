/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.block.Block
 *  org.bukkit.block.Chest
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.Event
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.block.BlockPlaceEvent
 *  org.bukkit.event.inventory.InventoryOpenEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryHolder
 */
package ru.bird.main.event.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import ru.bird.main.event.GenChestEvent;
import ru.bird.main.game.Gamer;

import java.util.HashSet;
import java.util.Set;

public class ChestListener
extends BasicListener {
    private Set<Location> chests = new HashSet<Location>();
    private Set<Location> placed = new HashSet<Location>();

    @EventHandler
    public void onInventoryOpenEvent(InventoryOpenEvent e) {
        Player player = (Player)e.getPlayer();
        Gamer gamer = Gamer.getGamer(player);
        if (gamer.isSpectator()) {
            return;
        }
        if (e.getInventory().getHolder() instanceof Chest) {
            Chest chest = (Chest)e.getInventory().getHolder();
            Location loc = chest.getLocation();
            if (this.placed.contains(loc)) {
                return;
            }
            if (this.chests.contains(loc)) {
                return;
            }
            this.chests.add(chest.getLocation());
            Bukkit.getPluginManager().callEvent(new GenChestEvent(player, chest));
        }
    }


    // TODO проверить генерацию предметов в скайварсе при ломании
//    @EventHandler
//    public void onBreakChest(BlockBreakEvent e) {
//        Player player = (Player)e.getPlayer();
//        Gamer gamer = Gamer.getGamer(player);
//        if (gamer.isSpectator()) {
//            return;
//        }
//        Block block = e.getBlock();
//        if (block instanceof Chest) {
//            Chest chest = (Chest)block;
//            Location loc = chest.getLocation();
//            if (this.placed.contains(loc)) {
//                return;
//            }
//            if (this.chests.contains(loc)) {
//                return;
//            }
//            this.chests.add(chest.getLocation());
//            Bukkit.getPluginManager().callEvent(new GenChestEvent(player, chest));
//        }
//    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        if (e.getBlock().getType() == Material.CHEST || e.getBlock().getType() == Material.TRAPPED_CHEST) {
            this.placed.add(e.getBlock().getLocation());
        }
    }
}

