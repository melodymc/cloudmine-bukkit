// 
// Decompiled by Procyon v0.5.30
// 

package com.wellfail.crazywalls.sidebar;

import ru.wellfail.wapi.Main;
import ru.wellfail.wapi.game.GameSettings;
import org.bukkit.Bukkit;
import ru.wellfail.wapi.utils.Utils;
import com.wellfail.crazywalls.game.GameFactory;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;
import ru.wellfail.wapi.board.BoardLine;
import java.util.List;
import ru.wellfail.wapi.board.Board;

public class SpectatorBoard extends Board
{
    private int d;
    private List<String> list;
    private BoardLine players;
    private BoardLine spectators;
    private BoardLine kills;
    private BoardLine greenwall;
    private BoardLine redwall;
    private BoardLine border;
    private boolean endred;
    private boolean endgreen;
    private boolean end;
    
    public SpectatorBoard() {
        this.endred = false;
        this.endgreen = false;
        this.end = false;
        final Set<BoardLine> lines = this.getLines();
        this.create((Set)lines, "§e§lCRAZY WALLS");
        (this.list = new ArrayList<String>()).add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§6§lC§e§lRAZY WALLS");
        this.list.add("§f§lC§6§lR§e§lAZY WALLS");
        this.list.add("§f§lCR§6§lA§e§lZY WALLS");
        this.list.add("§f§lCRA§6§lZ§e§lY WALLS");
        this.list.add("§f§lCRAZ§6§lY§e§l WALLS");
        this.list.add("§f§lCRAZY §6§lW§e§lALLS");
        this.list.add("§f§lCRAZY W§6§lA§e§lLLS");
        this.list.add("§f§lCRAZY WA§6§lL§e§lLS");
        this.list.add("§f§lCRAZY WAL§6§lL§e§lS");
        this.list.add("§f§lCRAZY WALL§6§lS");
        this.list.add("§f§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§f§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§f§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§f§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.update(() -> {
            if (this.d == this.list.size()) {
                this.d = 0;
            }
            this.setDisplay((String)this.list.get(this.d));
            ++this.d;
            return;
        }, 3L);
        this.startUpdate();
    }

    public Set<BoardLine> getLines() {
        final Set<BoardLine> set = new HashSet<BoardLine>();
        set.add(new BoardLine((Board)this, "§1", 10));
        set.add(this.greenwall = new BoardLine((Board)this, "§aЗеленая стена: §a" + Utils.getTimeToEnd(GameFactory.timer.getGreen()), 9));
        this.dynamicLine(this.greenwall.getNumber(), "§aЗеленая стена: §a", Utils.getTimeToEnd(GameFactory.timer.getGreen()));
        set.add(this.redwall = new BoardLine((Board)this, "§cКрасная стена: §c" + Utils.getTimeToEnd(GameFactory.timer.getRed()), 8));
        this.dynamicLine(this.redwall.getNumber(), "§cКрасная стена: §c", Utils.getTimeToEnd(GameFactory.timer.getRed()));
        set.add(new BoardLine((Board)this, "§2", 7));
        this.border = new BoardLine((Board)this, "Дезматч: §a" + Utils.getTimeToEnd(GameFactory.timer.getBorder()), 6);
        this.dynamicLine(this.border.getNumber(), "Дезматч: §a", Utils.getTimeToEnd(GameFactory.timer.getBorder()));
        set.add(this.border);
        set.add(new BoardLine((Board)this, "§3", 5));
        set.add(this.players = new BoardLine((Board)this, "Игроков: §a", 4));
        this.dynamicLine(this.players.getNumber(), "Игроков: §a", GameFactory.players.size() + "");
        set.add(this.spectators = new BoardLine((Board)this, "Наблюдателей: §a", 3));
        this.dynamicLine(this.spectators.getNumber(), "Наблюдателей: §a", Bukkit.getOnlinePlayers().size() - GameFactory.players.size() + "");
        set.add(new BoardLine((Board)this, "§4", 2));
        set.add(new BoardLine((Board)this, "Карта: §a" + GameSettings.mapLocation.getWorld().getName(), 2));
        set.add(new BoardLine((Board)this, "Сервер: §a" + Main.getUsername(), 1));
        this.update(() -> {
            if (GameFactory.timer.getBorder() == 0) {
                if (!this.end) {
                    this.setLine(this.border.getNumber(), "Конец:§a " + Utils.getTimeToEnd());
                    this.end = true;
                }
                this.dynamicLine(this.border.getNumber(), "Конец:§a ", Utils.getTimeToEnd());
            }
            else {
                this.dynamicLine(this.border.getNumber(), "Дезматч: §a", Utils.getTimeToEnd(GameFactory.timer.getBorder()));
            }
            if (GameFactory.timer.getGreen() == 0) {
                if (!this.endgreen) {
                    this.setLine(this.greenwall.getNumber(), "§7§mЗеленая стена: §7§m00:00§6");
                    this.endgreen = true;
                }
                else {
                    this.dynamicLine(this.greenwall.getNumber(), "§7§mЗеленая стена: §7§m00:00§6", "");
                }
            }
            else {
                this.dynamicLine(this.greenwall.getNumber(), "§aЗеленая стена: §a", Utils.getTimeToEnd(GameFactory.timer.getGreen()));
            }
            if (GameFactory.timer.getRed() == 0) {
                if (!this.endred) {
                    this.setLine(this.redwall.getNumber(), "§7§mКрасная стена: §7§m00:00§7");
                    this.endred = true;
                }
                else {
                    this.dynamicLine(this.redwall.getNumber(), "§7§mКрасная стена: §7§m00:00§7", "");
                }
            }
            else {
                this.dynamicLine(this.redwall.getNumber(), "§cКрасная стена: §c", Utils.getTimeToEnd(GameFactory.timer.getRed()));
            }
            return;
        }, 20L);
        this.update(() -> {
            this.dynamicLine(this.spectators.getNumber(), "Наблюдателей: §a", Bukkit.getOnlinePlayers().size() - Utils.getAlivePlayers().size() + "");
            this.dynamicLine(this.players.getNumber(), "Игроков: §a", Utils.getAlivePlayers().size() + "");
            return;
        }, 100L);
        return set;
    }
}
