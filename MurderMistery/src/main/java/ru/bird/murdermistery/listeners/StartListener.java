// 
// Decompiled by Procyon v0.5.30
// 

package ru.bird.murdermistery.listeners;

import ru.bird.murdermistery.game.GameFactory;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.Main;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import ru.wellfail.wapi.game.Game;
import java.util.concurrent.atomic.AtomicInteger;
import ru.wellfail.wapi.listeners.FListener;

public class StartListener extends FListener implements Runnable
{
    private boolean starting;
    private Thread thread;
    private static AtomicInteger time;
    private static int i;
    
    public void start() {
        StartListener.time.set(320);
        this.thread = new Thread(this);
        this.thread.start();
    }

    public void stop() {
        StartListener.time.set(0);
        thread.stop();
    }
    
    public static void setTime(final int t) {
        StartListener.i = t;
        StartListener.time.set(t);
    }
    
    public static int getTime() {
        return StartListener.time.get();
    }
    
    public void run() {
        if (this.starting) {
            return;
        }
        this.starting = true;
        try {
            while ((StartListener.i = StartListener.time.get()) > 0) {
                if (StartListener.i <= 15) {
                    Game.broadcast("Конец игры через §a" + StartListener.i + "§f секунд");
                }
                if (StartListener.i == 90) {
                    GameFactory.addCompas();
                }
                for (final Player player : Bukkit.getOnlinePlayers()) {
                    if (StartListener.i <= 10) {
                        player.playSound(player.getLocation(), Sound.BLOCK_COMPARATOR_CLICK, 1.0f, 1.0f);
                    }
                }
                Thread.sleep(1000L);
                StartListener.time.getAndDecrement();
            }
            new BukkitRunnable() {
                public void run() {
                    // убийца проиграл
                    GameFactory.win_type = true;
                    Game.getInstance().endGame();
                }
            }.runTask(Main.getInstance());
            if (GameSettings.gameTime == -1) {
                return;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    static {
        StartListener.time = new AtomicInteger();
        StartListener.i = GameSettings.gameTime;
    }
}
