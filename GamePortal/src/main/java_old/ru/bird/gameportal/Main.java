package ru.bird.gameportal;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEnterEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.API;
import ru.bird.main.socket.MessageManager;
import ru.bird.main.socket.SocketManager;
import ru.bird.main.socket.message.Server;
import ru.bird.main.socket.message.StatusListener;

import java.io.File;
import java.io.IOException;
import java.util.*;

public final class Main extends JavaPlugin implements Listener {

    private static final String CONFIG_PATH = "/config.yml";
    private YamlConfiguration config;
    private String portalCurrent;
    private List<Player> players;
    
    @Override
    public void onEnable() {
        players = new ArrayList<>();
        config();
        new StatusListener(this, portalCurrent);
        getServer().getPluginManager().registerEvents(this, this);
    }

    private static Server getFavoritServer(){
        Server best = null;
        Collection<Server> srvs = MessageManager.getServers().values();
        System.out.println("srvs " + srvs);
        for (Server server : srvs) {
            if (server.status || server.online >= server.slots) continue;
            if (best == null) {
                best = server;
            }
            if (server.online <= best.online) continue;
            best = server;
        }
        return best;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();
        players.remove(player);
    }

    @EventHandler
    public void portalEvent(EntityPortalEnterEvent event){
        if(!(event.getEntity() instanceof Player)) {
            return;
        }
        Player player = (Player) event.getEntity();
        if(players.contains(player)) {
            cancelPortal(player);
        }
        Server server = getFavoritServer();
        if(server != null) {
            int online = API.getInstance().getSocketManager().getOnlineServer(server.name);
            if(online != -1) {
                players.add(player);
                API.getInstance().getSocketManager().redirect(player, server.name);
            }
        }
    }

    @EventHandler
    public void onPortal(PlayerTeleportEvent event){
        Player player = event.getPlayer();
        if((event.getCause() == PlayerTeleportEvent.TeleportCause.END_PORTAL)){
            event.setCancelled(true);
            players.add(player);
            cancelPortal(player);
        }
    }

    private void cancelPortal(Player player) {
        Bukkit.getServer().dispatchCommand(player, "spawn");
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public void config() {
        File f = new File(this.getDataFolder().getAbsolutePath() + CONFIG_PATH);
        config = YamlConfiguration.loadConfiguration((File)f);
        config.addDefault("portal.current", "SW");
        try {
            this.config.options().copyDefaults(true);
            this.config.save(f);
        }
        catch (IOException e) {
            this.getLogger().warning("ERROR: can not save config to " + CONFIG_PATH + ". Please check it.");
        }
        portalCurrent = config.getString("portal.current");
    }
}
