package ru.bird.main.hologram;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.main.API;
import ru.bird.main.logger.BaseLogger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HologramsManager {

    private List<Hologram> holograms;
    private YamlConfiguration config;
    private static String CONFIG_PATH = "/holograms.yml";

    public void build() {
        holograms = new ArrayList<>();
        hologramsConfig();
//        startUpdateTimer();
    }

    public List<Hologram> getHolograms() {
        return holograms;
    }

    private void hologramsConfig() {
        File f = new File(API.getInstance().getDataFolder().getAbsolutePath() + CONFIG_PATH);
        config = YamlConfiguration.loadConfiguration(f);
        config.addDefault("hologram.update_period", 20);
        try {
            config.options().copyDefaults(true);
            config.save(f);
        } catch (IOException e) {
            BaseLogger.error("ERROR: can not save config to " + CONFIG_PATH + ". Please check it.");
        }
    }

    private void startUpdateTimer() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if(holograms != null) {
                    if(holograms.size() > 0) {
                        holograms.forEach(Hologram::startUpdate);
                    }
                }
            }
        }.runTaskTimer(
                API.getInstance(),
                0,
                config.getInt("hologram.update_period")
        );
    }

    public void registetHologram(Hologram hologram) {
        if(hologram != null) {
            holograms.add(hologram);
        } else {
            throw new NullPointerException();
        }
    }

    public void unregisterHologram(Hologram hologram){
        if(hologram != null) {
            hologram.getArmorStands().values().forEach(Entity::remove);
            holograms.remove(hologram);
        } else {
            throw new NullPointerException();
        }
    }
}