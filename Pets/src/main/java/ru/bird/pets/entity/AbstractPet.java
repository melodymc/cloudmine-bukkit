package ru.bird.pets.entity;

import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import net.minecraft.server.v1_16_R2.DataWatcher;
import net.minecraft.server.v1_16_R2.PacketPlayOutEntityMetadata;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import ru.bird.main.API;
import ru.bird.main.packets.WrapperPlayServerEntityHeadRotation;
import ru.bird.main.packets.WrapperPlayServerEntityTeleport;
import ru.bird.main.packets.WrapperPlayServerSpawnEntityLiving;
import ru.bird.pets.utils.PacketUtils;

import java.util.Random;
import java.util.UUID;

public abstract class AbstractPet {

	protected World			world		= Bukkit.getWorlds().get(0);
	protected Location		petLocation	= new Location(world, 0, 0, 0);
	protected Random		rand		= new Random();
	protected int			uniqueID;
	protected DataWatcher	watcher;
	private UUID			ownerID;
	private UUID			petUUID;

	private WrapperPlayServerSpawnEntityLiving	spawnEntityPacket	= new WrapperPlayServerSpawnEntityLiving();
	private WrapperPlayServerEntityTeleport		position			= new WrapperPlayServerEntityTeleport();
	private WrapperPlayServerEntityHeadRotation	rotation			= new WrapperPlayServerEntityHeadRotation();

	public AbstractPet(UUID ownerID, int uniqueID) {
		this.uniqueID = uniqueID;
		this.ownerID = ownerID;
		watcher = build();
		petUUID = UUID.randomUUID();
	}

	public UUID getPetUUID() {
		return petUUID;
	}

	public int getEntityID() {
		return uniqueID;
	}

	protected abstract DataWatcher build();

	public abstract EntityType getEntityType();

	protected abstract float getOffsetY();

	public Player getOwner() {
		return Bukkit.getPlayer(ownerID);
	}

	public void spawn() {
		Location locTo = getOwner().getLocation();
		double angle = locTo.getYaw() + getAngle();
		double newX = locTo.getX() - getRadius() * Math.sin(Math.toRadians(angle));
		double newY = locTo.getY() + getOffsetY();
		double newZ = locTo.getZ() + getRadius() * Math.cos(Math.toRadians(angle));
		float headRotation = ((CraftPlayer) getOwner()).getHandle().getHeadRotation();

		this.petLocation = new Location(getOwner().getWorld(), newX, newY, newZ);
		spawnEntityPacket.setEntityID(getEntityID());
		spawnEntityPacket.setUniqueId(getPetUUID());
		EntityType type = getEntityType();
		spawnEntityPacket.setType(type);
		spawnEntityPacket.setX(newX);
		spawnEntityPacket.setY(newY);
		spawnEntityPacket.setZ(newZ);
		spawnEntityPacket.setYaw(locTo.getYaw());
		spawnEntityPacket.setPitch(locTo.getPitch());
		spawnEntityPacket.setHeadPitch(headRotation);
		spawnEntityPacket.broadcastPacket();
		move(getOwner());
	}

	public double getAngle() {
		return 125D;
	}

	public double getRadius() {
		return 1D;
	}

	public abstract void move(Player player);

	public void livingUpdate(){
		onLivingUpdate();
		if(getOwner() != null) {
			move(getOwner());
		}
	}

	public abstract void onLivingUpdate();

	public Location getPetLocation() {
		return petLocation;
	}

	protected void sendWatcherUpdate() {
		PacketPlayOutEntityMetadata metadata = new PacketPlayOutEntityMetadata(this.getEntityID(), watcher, false);
		PacketUtils.broadcastNMSPacket(metadata);
	}

//	/**
//	 * количество обновлений за тик
//	 */
//	public int getTickUpdate() {
//		return 1;
//	}

    public WrapperPlayServerEntityTeleport getPosition() {
        return position;
    }

    public void setPosition(WrapperPlayServerEntityTeleport position) {
        this.position = position;
    }

    public WrapperPlayServerEntityHeadRotation getRotation() {
        return rotation;
    }

    public void setRotation(WrapperPlayServerEntityHeadRotation rotation) {
        this.rotation = rotation;
    }

}