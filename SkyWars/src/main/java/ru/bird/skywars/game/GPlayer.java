package ru.bird.skywars.game;

import org.bukkit.entity.Player;
import ru.bird.main.board.api.Board;

public class GPlayer {
    private Player player;
    private int kills = 0;
    private boolean firstChestOpen;
    private int glassCount;

    private Board board;
    public Board getBoard() { return board; }
    public void setBoard(Board board)
    {
        this.board = board;
    }

    public GPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return this.player;
    }

    public int getKills() {
        return this.kills;
    }

    public void setKills() {
        ++this.kills;
    }

    public boolean isFirstChestOpen() {
        return firstChestOpen;
    }

    public int getGlassCount() {
        return glassCount;
    }

    public void setGlassCount(int glassCount) {
        this.glassCount = glassCount;
    }

    public boolean minusGlass(){
        if(this.glassCount > 0) {
            this.glassCount -=1;
            return true;
        } else {
            return false;
        }
    }

    public void setFirstChestOpen(boolean firstChestOpen) {
        this.firstChestOpen = firstChestOpen;
    }
}