package ru.bird.pets.entity.normal;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Rabbit;

import net.minecraft.server.v1_16_R2.DataWatcher;
import ru.bird.pets.entity.AbstractStandardPet;

public class PetRabbit extends AbstractStandardPet {

	public PetRabbit(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		Rabbit entity = (Rabbit) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		entity.setBaby();
		DataWatcher data = ((CraftEntity) entity).getHandle().getDataWatcher();
		entity.remove();
		return data;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.RABBIT;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

}
