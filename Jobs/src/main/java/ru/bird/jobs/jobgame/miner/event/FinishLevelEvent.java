package ru.bird.jobs.jobgame.miner.event;

import ru.bird.jobs.jobgame.event.JobEventUnified;
import ru.bird.jobs.jobgame.miner.JobGameMiner;

public class FinishLevelEvent extends JobEventUnified
{
    public FinishLevelEvent(JobGameMiner instance)
    {
        super(instance);
    }
}
