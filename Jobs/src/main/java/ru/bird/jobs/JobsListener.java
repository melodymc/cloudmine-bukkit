package ru.bird.jobs;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import ru.bird.jobs.menu.MenuManager;

import java.util.UUID;

public class JobsListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        UUID uuid = player.getUniqueId();
        JobsUser u = StorageJob.load(uuid);
        String time = JobsPlugin.getTimeToUpdatePlot(u.getSumm());
        player.sendMessage(ChatColor.GRAY + "Ваша работа " + (u.getJobToString() != null ? ChatColor.GOLD + u.getJobToString() : ChatColor.RED + "Нет") + ChatColor.GRAY + " до обновления плота " + ChatColor.GOLD + time + ChatColor.GRAY +  ".");
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event){
        Player player = event.getPlayer();
        JobsUser ju = StorageJob.get(player);
        Block block = event.getBlock();
        Material type = block.getType();
        switch (type) {
            case IRON_ORE:
            case GOLD_ORE:
            case COAL_ORE:
            case EMERALD_ORE:
            case LEGACY_GLOWING_REDSTONE_ORE:
            case LAPIS_ORE:
            case NETHER_QUARTZ_ORE:
            case REDSTONE_ORE:
            case DIAMOND_ORE:
                if(!(ju.job == JobEnum.MINER)) { // not miner
                    player.sendMessage("Для добычи руд требуется профессия Шахтёр.");
                    player.sendMessage("Смена профессии будет доступна через " + JobsPlugin.getTimeToChangeJobStr(ju) + ".");
                    player.sendMessage("Команда для смена профессии /js start");
                    event.setCancelled(true);
                }
            break;

        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        UUID uuid = player.getUniqueId();
        JobsUser u = StorageJob.get(uuid);
        StorageJob.insert(u);
        u.remove();
//        if(MenuManager.playerSlots.get(uuid) != null) {
//            MenuManager.playerSlots.remove(uuid);
//        }
//        if(MenuManager.points.get(uuid) != null) {
//            MenuManager.points.remove(uuid);
//        }
    }
}