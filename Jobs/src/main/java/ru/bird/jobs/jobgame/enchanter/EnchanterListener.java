package ru.bird.jobs.jobgame.enchanter;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import ru.bird.jobs.jobgame.JobGame;
import ru.bird.jobs.jobgame.event.FinishJobEvent;
import ru.bird.jobs.jobgame.event.InterruptedJobEvent;
import ru.bird.jobs.jobgame.event.StartJobEvent;
import ru.bird.jobs.jobgame.event.TargetListener;

public final class EnchanterListener implements Listener, TargetListener
{
    @Override
    public Class<? extends JobGame> getCorrectType()
    {
        return JobGameEnchanter.class;
    }

    @EventHandler
    public void onStartEvent(StartJobEvent event)
    {
        if ( !isCorrect(event) ) return;

        Player onlinePlayer = event.getWorker().getOnlinePlayer();

        if (onlinePlayer != null)
        {
            onlinePlayer.sendMessage("[Зачарователь] Игра началась. Уровнь 1.");
        }
    }

    @EventHandler
    public void onInterruptedJobEvent(InterruptedJobEvent event)
    {
        if ( !isCorrect(event) ) return;

        Player onlinePlayer = event.getWorker().getOnlinePlayer();

        if (onlinePlayer != null)
        {
            onlinePlayer.sendMessage("[Зачарователь] Игра была прервана.");
        }
    }

    @EventHandler
    public void onFinishEvent(FinishJobEvent event)
    {
        if ( !isCorrect(event) ) return;

        Player onlinePlayer = event.getWorker().getOnlinePlayer();

        if (onlinePlayer != null)
        {
            onlinePlayer.sendMessage("[Зачарователь] Поздравляем, вы прошли все уровни!");
        }
    }
}
