package ru.bird.main.clock;

public abstract class AbstractTask {

	public abstract int getModule();

	public abstract void call();
	
}
