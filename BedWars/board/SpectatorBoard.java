/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.World
 *  org.bukkit.entity.Player
 *  ru.wellfail.wapi.Main
 *  ru.wellfail.wapi.board.Board
 *  ru.wellfail.wapi.board.BoardLine
 *  ru.wellfail.wapi.game.GameSettings
 */
package ru.bird.bedwars.board;

import ru.bird.bedwars.team.Team;
import ru.wellfail.wapi.Main;
import ru.wellfail.wapi.board.Board;
import ru.wellfail.wapi.board.BoardLine;
import ru.wellfail.wapi.game.GameSettings;

import java.util.*;

public class SpectatorBoard
extends Board {
    private int d;
    private List<String> list;
    private BoardLine rteam;
    private BoardLine bteam;
    private BoardLine gteam;
    private BoardLine yteam;
    Map<Team, BoardLine> teams = new HashMap<Team, BoardLine>();

    public SpectatorBoard() {
        Set<BoardLine> lines = this.getLines();
        this.create(lines, "В§eВ§lBEDWARS");
        this.list = new ArrayList<String>();
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§6§lB§e§lEDWARS");
        this.list.add("§f§lB§6§lE§e§lDWARS");
        this.list.add("§f§lBE§6§lD§e§lWARS");
        this.list.add("§f§lBED§6§lW§e§lARS");
        this.list.add("§f§lBEDW§6§lA§e§lRS");
        this.list.add("§f§lBEDWA§6§lR§e§lS");
        this.list.add("§e§lBEDWAR§6§lS");
        this.list.add("§f§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§f§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§f§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.update(() -> {
            if (this.d == this.list.size()) {
                this.d = 0;
            }
            this.setDisplay(this.list.get(this.d));
            ++this.d;
        }
        , 3);
        this.startUpdate();
    }

    private String canSpawn(Team team) {
        if (team.isCanSpawn()) {
            return "§a✔ ";
        }
        return "§c✘ ";
    }

    public synchronized Set<BoardLine> getLines() {
        HashSet<BoardLine> set = new HashSet<BoardLine>();
        set.add(new BoardLine((Board)this, "§1", 8));
        Iterator<Team> it = Team.teams.iterator();
        Team t = it.next();
        this.gteam = new BoardLine((Board)this, t.getName() + ": §a", 7);
        set.add(this.gteam);
        this.dynamicLine(this.gteam.getNumber(), (Object)t.getColor() + t.getName() + ": §a", "" + t.getPlayers().size() + "", this.canSpawn(t));
        this.teams.put(t, this.gteam);
        t = it.next();
        this.yteam = new BoardLine((Board)this, t.getName() + ": §a", 6);
        set.add(this.yteam);
        this.dynamicLine(this.yteam.getNumber(), (Object)t.getColor() + t.getName() + ": §a", "" + t.getPlayers().size() + "", this.canSpawn(t));
        this.teams.put(t, this.yteam);
        t = it.next();
        this.rteam = new BoardLine((Board)this, t.getName() + ": §a", 5);
        set.add(this.rteam);
        this.dynamicLine(this.rteam.getNumber(), (Object)t.getColor() + t.getName() + ": §a", "" + t.getPlayers().size() + "", this.canSpawn(t));
        this.teams.put(t, this.rteam);
        t = it.next();
        this.bteam = new BoardLine((Board)this, t.getName() + ": §a", 4);
        set.add(this.bteam);
        this.dynamicLine(this.bteam.getNumber(), (Object)t.getColor() + t.getName() + ": §a", "" + t.getPlayers().size() + "", this.canSpawn(t));
        this.teams.put(t, this.bteam);
        set.add(new BoardLine((Board)this, "§2", 3));
        set.add(new BoardLine((Board)this, "Карта: §a" + GameSettings.mapLocation.getWorld().getName(), 2));
        set.add(new BoardLine((Board)this, "Сервер: §a" + Main.getUsername(), 1));
        this.update(() -> {
            for (Map.Entry<Team, BoardLine> s : this.teams.entrySet()) {
                Team team1 = s.getKey();
                if (Team.teams.contains(team1)) {
                    this.dynamicLine(this.teams.get(s.getKey()).getNumber(), (Object)team1.getColor() + team1.getName() + ": §a", "" + team1.getPlayers().size() + "", this.canSpawn(team1));
                    continue;
                }
                this.teams.get(team1).setLine((Object)team1.getColor() + "§4");
            }
        }
        , 100);
        return set;
    }
}

