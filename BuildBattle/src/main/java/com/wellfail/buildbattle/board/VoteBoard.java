/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.player.PlayerJoinEvent
 *  ru.wellfail.wapi.board.Board
 *  ru.wellfail.wapi.board.BoardLine
 *  ru.bird.main.game.Game
 */
package com.wellfail.buildbattle.board;

import com.wellfail.buildbattle.GameFactory;
import org.bukkit.Bukkit;
import ru.bird.main.board.api.Board;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.utils.GameUtils;
import ru.pir.main.utils.BoardStringAnimator;

public class VoteBoard
extends Board {

    private final byte HEADER_UPDATE_RATE = 3;
    private final byte BOARD_UPDATE_RATE = 20;

    public VoteBoard() {
        super("§e§l" + GameSettings.wboardname.toUpperCase());

        addUpdaterHeader(HEADER_UPDATE_RATE, BoardStringAnimator.concatFrameBuffers(
                BoardStringAnimator.generateStatic(GameSettings.wboardname.toUpperCase(), "&e", 5),
                BoardStringAnimator.generateMetalShine(GameSettings.wboardname.toUpperCase(), "&e", "&6", "&f"),
                BoardStringAnimator.generateBlinking(GameSettings.wboardname.toUpperCase(), "&f", "&e", 5)
        ));
        write(0, "§2");
        write(1, "Игроков: §a " + (Bukkit.getOnlinePlayers().size() - Game.spectators.size()) + "");
        write(2, "§1");
        write(3, "Тема: §a " + GameFactory.getTheme());
        write(4, "§1");
        addUpdater(BOARD_UPDATE_RATE, board -> {
            int spec = Bukkit.getOnlinePlayers().size() - GameUtils.getAlivePlayers().size();

            board.modifyLine(1, "Игроков: §a " + (Bukkit.getOnlinePlayers().size() - Game.spectators.size()));
        });
        create();
    }

}

