/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.World
 */
package com.wellfail.crazywalls.utils;

import org.bukkit.World;

public class Point {
    private int x;
    private int z;
    private World world;

    public Point(int x, int z, World world) {
        this.x = x;
        this.z = z;
        this.world = world;
    }

    public World getWorld() {
        return this.world;
    }

    public int getX() {
        return this.x;
    }

    public int getZ() {
        return this.z;
    }
}

