// 
// Decompiled by Procyon v0.5.30
// 

package com.wellfail.crazywalls.utils;

import org.bukkit.Location;

public class Barrier
{
    private Location min;
    private Location max;
    
    public Barrier(final Location one, final Location two) {
        this.min = new Location(one.getWorld(), Math.min(one.getX(), two.getX()), Math.min(one.getY(), two.getY()), Math.min(one.getZ(), two.getZ()));
        this.max = new Location(one.getWorld(), Math.max(one.getX(), two.getX()), Math.max(one.getY(), two.getY()), Math.max(one.getZ(), two.getZ()));
    }
    
    public boolean isInside(final Location loc) {
        return (int)this.min.getX() <= (int)loc.getX() && (int)loc.getX() <= (int)this.max.getX() && (int)this.min.getY() <= (int)loc.getY() && (int)loc.getY() <= (int)this.max.getY() && (int)this.min.getZ() <= (int)loc.getZ() && (int)loc.getZ() <= (int)this.max.getZ();
    }
}
