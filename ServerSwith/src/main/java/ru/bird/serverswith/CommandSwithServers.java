/*
 * Decompiled with CFR 0_123.
 * 
 * Could not load the following classes:
 *  com.google.common.io.ByteArrayDataOutput
 *  com.google.common.io.ByteStreams
 *  org.bukkit.ru.bird.jobs.command.Command
 *  org.bukkit.ru.bird.jobs.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.Plugin
 *  ru.bird.chat.AbstractCommandmmand
 *  ru.bird.main.socket.SocketManager
 *  ru.bird.main.utils.FIterator
 */
package ru.bird.serverswith;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.bird.chat.AbstractCommand;
import ru.bird.main.API;
import ru.bird.main.utils.FIterator;

public class CommandSwithServers
extends AbstractCommand {

    private List<Player> cooldown;

    public CommandSwithServers() {
        super("swithservers");
        cooldown = new ArrayList<>();
    }

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length == 0) {
            return false;
        }

        if (!(sender instanceof Player)) {
            return false;
        }
        Player player = (Player) sender;
        if(!cooldown.contains(player)) {
            String server = args[0];
            SocketManager socketManager = API.getInstance().getSocketManager();
            Set<String> servers = socketManager.getServers(server, (byte) 1);
            FIterator<String> iterator = new FIterator<>(servers);
            if (servers.isEmpty()) {
                player.sendMessage("§e[Hub] Нет доступных лобби для режима " + server + ".");
                return false;
            } else {
                cooldown.add(player);
                Main.getInstance().getServer().getScheduler().runTaskLater(Main.getInstance(), () -> {
                    cooldown.remove(player);
                }, 2 * 20);
                socketManager.redirect(player, iterator.getNext());
                return true;
            }
        } else {
            return false;
        }
//        Bukkit.getServer().dispatchCommand(player, "spawn");

    }
}

