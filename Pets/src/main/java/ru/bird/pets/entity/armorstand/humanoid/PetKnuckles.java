package ru.bird.pets.entity.armorstand.humanoid;

import java.util.UUID;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.EulerAngle;

import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;

public class PetKnuckles extends AbstractArmoredPet {

	public PetKnuckles(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setLeftLegPose(new EulerAngle(0.3, 0, 0));
		armor.setRightLegPose(new EulerAngle(0.3, 0, 0));
		armor.setLeftArmPose(new EulerAngle(-1.57, 0, 0));
		armor.setRightArmPose(new EulerAngle(-1.57, 0, 0));
		armor.setArms(true);
		armor.setVisible(true);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getMainHand() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzdiOTU5MzE0MDUwMjE0OWMyOTQ2N2VlNTBjZDU1M2U3OTA5Y2QxOWYxOTEwM2JkZjQ1OTI1NjdiNDk4MCJ9fX0=");
	}
	
	@Override
	public ItemStack getOffHand() {
		return null;
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		LeatherArmorMeta lim = (LeatherArmorMeta) leggings.getItemMeta();
		lim.setColor(Color.RED);
		leggings.setItemMeta(lim);
		return leggings;
	}

	@Override
	public ItemStack getChestplate() {
		ItemStack chestPlate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		LeatherArmorMeta cim = (LeatherArmorMeta) chestPlate.getItemMeta();
		cim.setColor(Color.RED);
		chestPlate.setItemMeta(cim);
		return chestPlate;
	}

	@Override
	public ItemStack getHelmet() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTg3OWEzZDRmZjhhZThhN2I1YmEyYTc4OTcxMTM0MWI3OTRjZDMxY2M1NmVlZmJlMGEyNTIyNjFjYzNiZWYzIn19fQ==");
	}

	@Override
	public void onLivingUpdate() {
		this.setLeftArmPose(-90 + 10 * MathUtils.sin(), 0, -MathUtils.sin());
		this.setRightArmPose(-90 + 10 * MathUtils.cos(), 0, MathUtils.sin());
		this.setLeftLegPose(15 + 10 * MathUtils.cos(), 0, 0);
		this.setRightLegPose(15 + 10 * MathUtils.sin(), 0, 0);
		this.sendWatcherUpdate();
	}
}