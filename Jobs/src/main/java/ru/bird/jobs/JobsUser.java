package ru.bird.jobs;

import de.leonhard.storage.Json;
import ru.bird.jobs.utils.TimeUtils;
import ru.bird.storageUtils.player.BasicAccount;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.UUID;

public class JobsUser extends BasicAccount {

    public JobEnum job;
    private long time;
    private long loginTime;
    private boolean canUpdatePlot;
    private long jobTime;
    private boolean canChangeJob;
    public Json json;

    public JobsUser() {
        super();
    }

    public JobsUser(UUID uuid) {
        super(uuid);
    }

    public void remove() {
        StorageJob.removeJobsUser(getUUID());
    }

    public String getJobToString() {
        return job != null ? job.toString() : null;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getSumm(){
        return (time + getNum());
    }

    public long getTime() {
        return time;
    }

    public long getNum(){
        long num;
        if (canUpdatePlot) {
            num = 0;
        } else {
            num = (TimeUtils.getCurrentTime() - loginTime);
        }
        return num;
    }

    public void setJob(JobEnum job) {
        this.job = job;
    }

    public void setJobByString(String job) {
        this.job = JobEnum.valueOf(job);
    }

    public void setLoginTime(long loginTime) {
        this.loginTime = loginTime;
    }

    public void setCanUpdatePlot(boolean canUpdatePlot) {
        this.canUpdatePlot = canUpdatePlot;
    }

    public boolean isCanUpdatePlot() {
        return canUpdatePlot;
    }

    public long getJobTime() {
        return jobTime;
    }

    public long getJobTimeNum() {
        long jobnum;
        if(canChangeJob) {
            jobnum = 0;
        } else {
            jobnum = (TimeUtils.getCurrentTime() - loginTime);
        }
        return jobnum;
    }

    public boolean isCanChangeJob() {
        return canChangeJob;
    }

    public long getJobTimeSumm(){
        return (jobTime + getJobTimeNum());
    }

    public void setJobTime(long jobTime) {
        this.jobTime = jobTime;
    }

    public void setCanChangeJob(boolean canChangeJob) {
        this.canChangeJob = canChangeJob;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = super.serialize();
        map.put("job", job != null ? job.toString() : null);
        map.put("time", time);
        map.put("loginTime", loginTime);
        map.put("canUpdatePlot", canChangeJob);
        map.put("jobTime", jobTime);
        map.put("canChangeJob", canChangeJob);
        return map;
    }

    @Override
    public <T> T deserialize(Map<String, Object> args) throws IllegalArgumentException, NullPointerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        JobsUser user = new JobsUser(
            UUID.fromString((String) args.get("uuid"))
        );

        user.setJobByString((String) args.get("job"));
        user.setTime((long) args.get("time"));
        user.setLoginTime((long) args.get("loginTime"));
        user.setJobTime((long) args.get("jobTime"));
        return (T) user;
    }

    public void setJson(Json json) {
        this.json = json;
    }

}