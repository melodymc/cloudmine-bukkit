package ru.bird.holopath;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.API;
import ru.bird.main.hologram.Hologram;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.socket.MessageManager;
import ru.bird.main.socket.message.StatusListener;
import ru.bird.main.utils.GameUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Main extends JavaPlugin {

    private static final String CONFIG_PATH = "/config.yml";
    private YamlConfiguration config;
    public static List<String> chennals;
    public static Map<String, Number> onlineAll;
    private List<?> srcHolograms;
    private static Main plugin;
    public static List<String> datas;

    @Override
    public void onEnable() {
        plugin = this;
        onlineAll = new HashMap<>();
        config();
//        refreshAllOnline();
        setHolograms();
    }

    private void setHolograms() {
        List<Map<String, Object>> list = cast(srcHolograms);

        list.forEach(map-> {
            Hologram hologram = new Hologram(Location.deserialize(map), plugin, cast(map.get("lines")));
            hologram.setDoUpdate(new Runnable() {
                @Override
                public void run() {
                    if(Bukkit.getOnlinePlayers().size() > 0) {
                        refreshAllOnline();
                        BaseLogger.debug("all online " + onlineAll.toString());
                        Pattern pattern = Pattern.compile("(\\{(.+?)})");
                        hologram.getLines().stream()
                            .filter(pattern.asPredicate())
                            .forEach(line ->{
                                Matcher matcher = pattern.matcher(line);
                                ArmorStand armorStand = hologram.getArmorStands().get(line);
                                if(matcher.find()) {
                                    String placeholder = matcher.group(1);
                                    String value = placeholder.replace("{", "").replace("}", "");
                                    BaseLogger.debug("key " + value);
                                    Number online = onlineAll.get(value);
                                    BaseLogger.debug("online " + online);
                                    armorStand.setCustomName(GameUtils.colorSet(line.replace(placeholder, online.intValue() + "")));
                                }
                            });
                    }
                }
            });
            API.getInstance().getHologramsManager().registetHologram(hologram);
//            hologram.startUpdate();
        });

    }



    @SuppressWarnings("unchecked")
    public static <T extends List<?>> T cast(Object obj) {
        return (T) obj;
    }

    public void config() {
        File f = new File(this.getDataFolder().getAbsolutePath() + CONFIG_PATH);
        config = YamlConfiguration.loadConfiguration(f);
        ArrayList<String> defListChennals = new ArrayList<String>();
        defListChennals.add("BW");
        defListChennals.add("SW");
        defListChennals.add("BB");
        this.config.addDefault("chennals", defListChennals);
        ArrayList<Map<String, Object>> defListHolograms = new ArrayList<Map<String, Object>>();
        Map<String, Object> loc1 = new Location(Bukkit.getWorld("Lobby"), 3.5, 9.7, 7.5).serialize();
        List<String> list1 = new ArrayList<>();
        list1.add("&e&lSkyWars");
        list1.add("&u{SW} &bигроков онлайн");
        loc1.put("lines", list1);
        defListHolograms.add(loc1);
        Map<String, Object> loc2 = new Location(Bukkit.getWorld("Lobby"), 7.5, 9.7, 9.5).serialize();
        List<String> list2 = new ArrayList<>();
        list2.add("&e&lSurvival");
        list2.add("&u{BW} &bигроков онлайн");
        loc2.put("lines", list2);
        defListHolograms.add(loc2);
        Map<String, Object> loc3 = new Location(Bukkit.getWorld("Lobby"),11.5, 9.7, 7.5).serialize();
        List<String> list3 = new ArrayList<>();
        list3.add("&e&lBedWars");
        list3.add("&u{BW} &bигроков онлайн");
        loc3.put("lines", list3);
        defListHolograms.add(loc3);
        this.config.addDefault("holograms", defListHolograms);
        List<String> datasDefaultList = new ArrayList<>();
        datasDefaultList.add("Survival");
        this.config.addDefault("datas", datasDefaultList);
        try {
            this.config.options().copyDefaults(true);
            this.config.save(f);
        }
        catch (IOException e) {
            this.getLogger().warning("ERROR: can not save config to " + CONFIG_PATH + ". Please check it.");
        }
        chennals = config.getStringList("chennals");
        datas = config.getStringList("datas");
        srcHolograms = config.getList("holograms");
    }

    public static void refreshAllOnline() {
        onlineAll.clear();
        if(chennals != null) {
            for (final String s : chennals) {
                new StatusListener(plugin, s);
                int i = MessageManager.getGroupOnline(s);
                onlineAll.put(s, i);
            }
        }
        if(datas != null) {
            for(String s : datas) {
                int i = API.getInstance().getSocketManager().getOnlineServer(s);
                onlineAll.put(s, i);
            }
        }
    }

//    private String getGroupOnline(String s) {
//        Set<String> servers = SocketManager.getServers(s, (byte)1);
//        int online = 0;
//        for (String server : servers) {
//            online = API.getInstance().getSocketManager().getOnlineServer(server);
//        }
//        String str = String.valueOf(online);
//        System.out.println("servers " + s + " online " + online);
//        return str;
//    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public static Plugin get() {
        return plugin;
    }

}