/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.entity.Player
 */
package ru.bird.bedwars;

import ru.bird.bedwars.game.GameFactory;
import org.bukkit.entity.Player;

public class Utils {
    public static String teamColorName(Player player) {
        return GameFactory.players.get(player).getTeam().getColor() + player.getName();
    }
}