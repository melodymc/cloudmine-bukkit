package ru.bird.pets.entity.armorstand.mobs;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import ru.bird.main.API;
import ru.bird.pets.Pets;
import ru.bird.main.packets.WrapperPlayServerEntityTeleport;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;

public class PetGuardian extends AbstractArmoredPet {

	public PetGuardian(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setArms(false);
		armor.setVisible(false);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}



	@Override
	protected float getOffsetY() {
		return offset;
	}

	@Override
	public ItemStack getMainHand() {
		return null;
	}

	@Override
	public ItemStack getOffHand() {
		return null;
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		return null;
	}

	@Override
	public ItemStack getChestplate() {
		return null;
	}

	@Override
	public ItemStack getHelmet() {
		ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta meta = (SkullMeta) head.getItemMeta();
		meta.setOwner("Guardian");
		head.setItemMeta(meta);
		return head;
	}

	private float offset = 1F;

	@Override
	public void onLivingUpdate() {
		offset = (float) (1 + 0.2 * MathUtils.cos());
		Location locTo = getOwner().getLocation();
		double localAngle = locTo.getYaw() + getAngle();
		double newX = locTo.getX() - 1D * Math.sin(Math.toRadians(localAngle));
		double newY = locTo.getBlockY() + getOffsetY();
		double newZ = locTo.getZ() + 1D * Math.cos(Math.toRadians(localAngle));
		this.petLocation = new Location(getOwner().getWorld(), newX, newY, newZ);
		
		WrapperPlayServerEntityTeleport position = new WrapperPlayServerEntityTeleport();
		position.setEntityID(getEntityID());
		position.setX(newX);
		position.setY(newY);
		position.setZ(newZ);
		position.setYaw(locTo.getYaw());
		position.setPitch(locTo.getPitch());
		position.broadcastPacket();
		
		setHeadPose(5 * MathUtils.cos(), 0, 5 * MathUtils.sin());
		this.sendWatcherUpdate();

	}

}
