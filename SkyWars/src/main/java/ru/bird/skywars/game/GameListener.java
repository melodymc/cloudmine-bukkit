/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.block.Chest
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.entity.Entity
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.entity.EntityDamageByEntityEvent
 *  org.bukkit.event.entity.EntityDamageEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  ru.wellfail.wapi.events.GenChestEvent
 *  ru.wellfail.wapi.utils.Utils
 */
package ru.bird.skywars.game;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.main.API;
import ru.bird.main.event.DeathEvent;
import ru.bird.main.event.GenChestEvent;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.Title;
import ru.bird.skywars.Main;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class GameListener
implements Listener {

    @EventHandler
    public void onDeath(DeathEvent event){
        Bukkit.getServer().getScheduler().runTaskLater(API.getInstance(), new BukkitRunnable(){
            @Override
            public void run() {
                int count = GameUtils.getAlivePlayers().size();
                if(count > 1) {
                    String title = "§fОсталось §b§l" + (count) + " §fигроков";
                    String subtitle = "§e" + event.getPlayer().getDisplayName() + " §8§l⋙ §cумер ☠";
                    new Title(title, subtitle, 1, 2, 1).broadcast();
                }
            }
        }, 2);
    }

    @EventHandler
    public void onLeavesDecayEvent(LeavesDecayEvent e) {
        e.setCancelled(true);
    }

    @EventHandler(priority=EventPriority.HIGH)
    public void onChestGen(GenChestEvent e) {
        FileConfiguration config = GameFactory.configMain;
        Inventory inv = e.getChest().getInventory();
        Player player = e.getPlayer();
        GPlayer gPlayer = GameFactory.getGplayer(player);
        if(GameFactory.isFirstOnenChest(player)) {
            Set<String> keys = config.getConfigurationSection("Start.chestgen").getKeys(false);
            int startListSize = keys.size();
            int rn = ThreadLocalRandom.current().nextInt(0, startListSize);
            String path = "Start.chestgen." + keys.toArray()[rn] + ".items";
            BaseLogger.debug("path " + path);
            Set<String> headers = config.getConfigurationSection(path).getKeys(false);
            for(String header : headers){
                Map<String, Object> map = config.getConfigurationSection(path + "." + header).getValues(true);
                BaseLogger.debug("map " + map);
                ItemStack item = ItemStack.deserialize(map);
                BaseLogger.debug("item " + item);
                inv.addItem(item);
            }
            getItemsFromConfig("Chest", config, "Start.addedRand.min", "Start.addedRand.max", inv);
            gPlayer.setFirstChestOpen(false);
//            GameFactory.firstOpenChest.replace(player, true, false);
        } else {
            BaseLogger.info("type " + e.getChest().getType());
            String mainPath;
            if(e.getChest().getType() == Material.TRAPPED_CHEST) {
                mainPath = "Traped";
            } else {
                mainPath = "Chest";
            }
            getItemsFromConfig(mainPath, config,  mainPath + ".min",  mainPath + ".max", inv);
        }
    }

    private void getItemsFromConfig(String mainPath, FileConfiguration config, String min_con, String max_str, Inventory inv) {
        int min = config.getInt(min_con);
        System.out.println("min " + min);
        int max = config.getInt(max_str);
        System.out.println("max " + max);
        int rand = ThreadLocalRandom.current().nextInt(min, max);
        String path = mainPath + ".items";
        Set<String> headers = config.getConfigurationSection(path).getKeys(false);
        int i = 0;
        List<String> list = new ArrayList<String>(headers);
        Collections.shuffle(list);
        for(String header : list){
            if(i <= rand) {
                ItemStack item = ItemStack.deserialize(config.getConfigurationSection(path + "." + header).getValues(true));
                inv.addItem(item);
            }
            i++;
        }
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onDamage(final EntityDamageByEntityEvent e) {
        new BukkitRunnable(){
            public void run() {
                e.getEntity().setLastDamageCause(null);
            }
        }.runTaskLater(API.getInstance(), 100);
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void BlockBreakEvent(final BlockBreakEvent e) {
        Block block = e.getBlock();
        Player player = e.getPlayer();
        GPlayer gPlayer = GameFactory.getGplayer(player);
        ConfigurationSection all = GameFactory.allDrops;
        Set<String> allDropKeys = GameFactory.allDrops.getKeys(false);
        BaseLogger.info("1");
        final ItemStack[] currentItem = {null};
        final String[] path = {null};
        ItemStack two = new ItemStack(block.getType());
        two.setDurability(block.getData());
//        BaseLogger.info("two " + two);
        allDropKeys.forEach(item ->{
            path[0] = item;
            Map<String, Object> map = all.getConfigurationSection(path[0] + ".item").getValues(true);
//            BaseLogger.info("map " + map);
            ItemStack one = ItemStack.deserialize(map);
//            BaseLogger.info("one " + one);
            if(one != null && itemsLowProirityEquals(one, two)) {
                currentItem[0] = one;
            }
        });
//        BaseLogger.info("currentItem " + currentItem[0]);
//        BaseLogger.info("block " + block);
        if(currentItem[0] != null) {
            String symbol = "";
            int glassCount = gPlayer.getGlassCount();
            if(glassCount == 1) {
                symbol = "§c➀";
            }
            if(glassCount == 2) {
                symbol = "§6➁";
            }
            if(glassCount == 3) {
                symbol = "§a➂";
            }
            if(!gPlayer.minusGlass()) {
                e.setCancelled(true);
                block.setType(Material.BEDROCK);
                return;
            }
            e.setCancelled(true);
//            BaseLogger.info("[Эвент ломания] Начало");
            block.setType(two.getType());
            block.setData((byte) two.getDurability());
            List<String> effectHeaders = new LinkedList<>(all.getConfigurationSection(path[0] + ".effects").getKeys(false));
            AtomicReference<Map<String, Object>> currentEffect = new AtomicReference<>();
            AtomicInteger currentRandom = new AtomicInteger();
            effectHeaders.forEach(effectHeader -> {
                Map<String, Object> map = all.getConfigurationSection(path[0] + ".effects." + effectHeader).getValues(true);
                int chance = (int) map.get("chance");
                int rand = ThreadLocalRandom.current().nextInt(0, 101);
                if(chance >= rand) {
                    currentEffect.set(map);
                    currentRandom.set(rand);
                };
            });
            int duration = (int) currentEffect.get().get("duration");
            currentEffect.get().replace("duration", duration, duration * 20);
            PotionEffect eff = new PotionEffect(currentEffect.get());
//            player.sendMessage("chance " + currentRandom);
            new Title( symbol + "", "§f" + currentEffect.get().get("text"), 1, 2, 1).send(player);
//            BaseLogger.info("[Эвент ломания] eff " + eff.getType().toString());
            player.addPotionEffect(eff);
//            BaseLogger.info("[Эвент ломания] Конец");
            if(glassCount == 1) {
                e.setCancelled(true);
                block.setType(Material.BEDROCK);
            }
        }

    }

    private boolean itemsLowProirityEquals(ItemStack one, ItemStack two) {
        boolean bool = one.getType() == two.getType();
        BaseLogger.info("bool " + bool);
        boolean bool2 = one.getDurability() == two.getDurability();
        BaseLogger.info("bool1 " + bool2);
        return bool && bool2;
    }

//    @EventHandler(priority=EventPriority.LOWEST)
//    public void BlockBreakEvent(final BlockBreakEvent e) {
//        Block block = e.getBlock();
//        Material type = block.getType();
//        Player player = e.getPlayer();
//
//        Material getmat = Material.matchMaterial(elem);
//        if(type == getmat) {
//            e.setCancelled(true);
//            e.getBlock().setType(Material.AIR);
//            BaseLogger.debug("[Эвент ломания] Начало");
//            String[] items = Utils.random_list(config, "items." + elem + ".items");
//            BaseLogger.debug("[Эвент ломания] Выпал предмет "+ items[0]);
//            BaseLogger.debug("Длинна массива items "+ items.length);
//            if (!Objects.equals(items[0], "false")) {
//                String[] meta = items[0].split(":", 2);
//                String id = meta[0];
//                Material mat = Material.matchMaterial(id);
//                if (!(mat == null)) {
//                    if (meta.length == 2) {
//                        String m = meta[1];
//                        BaseLogger.debug("ID " + id + " SubId " + m);
//                        ItemStack stack = new ItemStack(mat);
//                        stack.setAmount(Integer.valueOf(items[1]));
//                        stack.setDurability(Short.parseShort(m));
//                        e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), stack);
//                    } else {
//                        ItemStack stack = new ItemStack(mat);
//                        stack.setAmount(Integer.valueOf(items[1]));
//                        e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), stack);
//
//                    }
//                } else {
//                    BaseLogger.debug("Неверно указан предмет, укажи правильный");
//                }
//            }
//            String[] effect = Utils.random_list(config, "items." + elem + ".effect");
//            BaseLogger.debug("[Эвент ломания] Выпал эффект "+ effect[0]);
//            BaseLogger.debug("Длинна массива effect "+ effect.length);
//            if(!(Objects.equals(effect[0], "false"))) {
//                PotionEffectType eff_type = PotionEffectType.getByName(effect[0].toUpperCase());
//                String[] numbers = effect[1].split("-", 2);
//                if (!(eff_type == null)) {
//                    if (numbers.length == 2) {
//                        Integer rand = ThreadLocalRandom.current().nextInt(Integer.parseInt(numbers[0]), Integer.parseInt(numbers[1]) + 1);
//                        BaseLogger.debug("Результат рандома чисел "+ numbers[0] +"-"+numbers[1]+" стало "+ rand);
//                        PotionEffect eff = new PotionEffect(eff_type, rand, Integer.parseInt(effect[2]));
//                        player.addPotionEffect(eff);
//                    } else {
//                        PotionEffect eff = new PotionEffect(eff_type, Integer.parseInt(effect[1]), Integer.parseInt(effect[2]));
//                        player.addPotionEffect(eff);
//                    }
//
//                } else {
//                    BaseLogger.info("Неверный эффект зелья - "+ effect[0] +" в конфиге, поправь");
//                }
//            }
//            String[] exp = Utils.random_list(config, "items." + elem + ".exp");
//            BaseLogger.debug("[Эвент ломания] Выпал опыт "+ exp[0]);
//            BaseLogger.debug("Длинна массива exp "+ exp.length);
//            if(!(Objects.equals(exp[0], "false"))) {
//                player.giveExpLevels(Integer.parseInt(exp[0]));
//            }
//        }
//
//    }
}

