package ru.bird.bedwars.shop;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import ru.bird.bedwars.Main;
import ru.bird.bedwars.config.Config;
import ru.bird.bedwars.game.GameFactory;
import ru.bird.bedwars.game.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.bird.main.game.Gamer;
import ru.bird.inventories.Item;
import ru.bird.inventories.ItemRunnable;
import ru.bird.inventories.menu.Menu;
import ru.bird.inventories.menu.interfaces.GuiAction;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.ItemUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Shop implements Listener {
    private Map<Inventory, Menu> guis = new HashMap<Inventory, Menu>();
    private Menu shopMenu;
    private static FileConfiguration config = GameFactory.shopConfig.getMemory();

    @EventHandler
    public void onNpcClick(PlayerInteractEntityEvent e) {
        Player player = e.getPlayer();
        Gamer gamer = Gamer.getGamer(player);
        if(e.getRightClicked() instanceof Villager) {
            if(!gamer.isSpectator()) {
                player.playSound(player.getLocation(), Sound.BLOCK_CHEST_OPEN, 4f, 4f);
                shopMenu.open(player);
            }
            e.setCancelled(true);
        }
    }

    public Shop() {
        Bukkit.getServer().getPluginManager().registerEvents(this, Main.getInstance());
        this.loadGuis();
    }

    private void loadGuis() {
        this.createMain();
        this.createGroups();
    }

    private void createGroups() {
        for (String id : Config.list.getKeys(false)) {
            String displayname = config.getString("Menu." + (id) + ".name");
            Menu menu = new Menu(displayname, 3);
            BaseLogger.info("Загрузка группы предметов " + displayname);
            guis.put(menu.getBukkitInventory(), menu);
            ConfigurationSection currentConfigSectionNPC = config.getConfigurationSection("Menu." + (id) + ".NPC");
            loadItems(currentConfigSectionNPC.getKeys(false), currentConfigSectionNPC.getCurrentPath(), menu);
        }
    }

    private void createMain() {
        String displayname = "Магазин";
        Menu gui = new Menu(displayname, 3);
        shopMenu = gui;
        loadItems(Config.list.getKeys(false), Config.list.getCurrentPath(), gui);
        guis.put(gui.getBukkitInventory(), gui);
    }

    private void loadItems(Set<String> list, String path, Menu menu) {
        BaseLogger.info("Грузим " + list.toString() + "\nPath " + path + "\nGUI: " + menu.getBukkitInventory().getName());
        AtomicInteger i = new AtomicInteger();
        list.forEach(el -> {
            Inventory inventory = menu.getBukkitInventory();
            if(menu == shopMenu) {
                ItemStack itm = itemFromConfig(path + "." + (el));
                inventory.setItem(i.get(), itm);
                menu.addAction(i.get(), new GuiAction() {
                    @Override
                    public void onClick(InventoryClickEvent event) {
                        event.setCancelled(true);
                        for (Inventory inv : guis.keySet()) {
                            if(Objects.equals(inv.getName(), itm.getItemMeta().getDisplayName())){
                                guis.get(inv).open((Player) event.getWhoClicked());
                            }
                        }
                    }
                });
            } else {
                ItemStack itm = itemFromConfig(path + "." + el + ".Item");
                int cost = config.getInt(path + "." + el + ".Cost") * 2;
                List<String> lore = new ArrayList<String>();
                BaseLogger.info("cost " + cost);
                lore.add("Цена ☄" + cost + " очков");
                ItemMeta meta = itm.getItemMeta();
                meta.setLore(lore);
                itm.setItemMeta(meta);
                inventory.setItem(i.get(), itm);
                menu.addAction(i.get(), e -> {
                    e.setCancelled(true);
                    Player player = (Player) e.getWhoClicked();
                    buy_item(itemFromConfig(path + "." + el + ".Item"), player, cost);
                    BaseLogger.info("Клик по предмету внутри группы");
                });
                ItemStack back = ItemUtils.createItem(Material.ARROW,"Назад");
                inventory.setItem(26, back);
                menu.addAction(26, e -> {
                    shopMenu.open((Player) e.getWhoClicked());
                });
            }
            i.getAndIncrement();
        });

    }

//    private void loadItems(Set<String> list_items, Menu gui, String id) {
//        Integer i = 0;
//        for (String item : list_items) {
//            for (Integer key : gui.guis.keySet()) {
//                if (allowtype.contains(key)) {
//                    BaseLogger.info("key" + key);
//                    for (Gui guii : gui.guis.values()) {
//                        if (guii == gui) {
//                            BaseLogger.info("Загрузка предмета "+ item + " итерация " + i);
//                            BaseLogger.info("gui" + gui.toString());
//                            if (key == 7) {
//                                ItemStack itm = itemFromConfig("Menu." + (item));
//                                BaseLogger.info("item "+ itm.toString());
//                                gui.getInventory().setItem(i, itm);
//                                gui.addAction(i, e -> {
//                                    e.setCancelled(true);
//                                    for (Inventory inv : guis.keySet()) {
//                                        if(Objects.equals(inv.getName(), itm.getItemMeta().getDisplayName())){
//                                            guis.get(inv).open((Player) e.getWhoClicked());
//                                        }
//                                    }
//                                    BaseLogger.info("Клик по предмету групп");
//                                });
//                            } else if (key == 8) {
//                                BaseLogger.info("itemm " + id);
//                                int cost = config.getInt("Menu." + (id) + ".NPC." + (item) + ".Cost") * 2;
//
//                                ItemStack itm = itemFromConfig("Menu." + (id) + ".NPC." + (item) + ".Item");
//                                List<String> lore = new ArrayList<String>();
//                                BaseLogger.info("cost " + cost);
//                                lore.add("Цена ☄" + cost + " очков");
//                                ItemMeta meta = itm.getItemMeta();
//                                meta.setLore(lore);
//                                itm.setItemMeta(meta);
//                                Inventory inv = gui.getBukkitInventory();
//                                inv.setItem(i, itm);
//                                gui.addAction(i, e -> {
//                                    e.setCancelled(true);
//                                    Player player = (Player) e.getWhoClicked();
//                                    buy_item(itemFromConfig("Menu." + (id) + ".NPC." + (item) + ".Item"), player, cost);
//                                    BaseLogger.info("Клик по предмету внутри группы");
//                                });
//                                ItemStack back = ItemUtils.createItem(Material.ARROW,"Назад");
//                                inv.setItem(26, back);
//                                gui.addAction(26, e -> {
//                                    shopMenu.open((Player) e.getWhoClicked());
//                                });
//                            }
//                            i++;
//                        }
//                    }
//                }
//            }
//        }
//    }

    private void buy_item(ItemStack itm, Player player, int cost) {
        BaseLogger.info("Попытка купить предмет");
        GamePlayer gamePlayer = GameFactory.getGamePlayer(player);
        int balance = gamePlayer.balance;
        if(gamePlayer.minusBalance(cost)){
            player.getInventory().addItem(itm);
        } else {
            player.sendMessage("§c§lМагазин §8⋙ §fНе хватает§8: §b☄" + (cost-balance) + " §fочков");
        }
    }

    public static Integer cost_item(ItemStack itm) {
        Integer amount = itm.getAmount();
        switch (itm.getType()){
            case CLAY_BRICK:
                return 5*amount;
            case IRON_INGOT:
                return 30*amount;
            case GOLD_INGOT:
                return 50*amount;
            default:
                return -1;
        }
    }

    private static ItemStack itemFromConfig(String path) {
        int id = config.getInt(path + ".id", 1);
        BaseLogger.info("path "+ path);
        BaseLogger.info("itemstack in config id "+ id);
        //String material = config.getString(path + ".id", "Air");
        short subID = (short)config.getInt(path + ".subid", 0);
        int amount = config.getInt(path + ".amount", 1);
        String name = config.getString(path + ".name", null);
        List<String> lore = config.getStringList(path + ".lore");
        List<String> enchants = config.getStringList(path + ".enchants");
        ItemStack item = new ItemStack(id, amount, subID);
        //ItemStack item = new ItemStack(Material.valueOf(material), amount, subID);
        ItemMeta meta = item.getItemMeta();
        if (name != null) {
            meta.setDisplayName(name);
        }
        if (lore != null) {
            meta.setLore(lore);
        }
        item.setItemMeta(meta);

        if(enchants != null) {
            HashMap<Enchantment, Integer> map1 = new HashMap<Enchantment, Integer>();
            Iterator<String> var12 = enchants.iterator();

            while(var12.hasNext()) {
                String line = var12.next();
                try {
                    String[] ench = line.split(":");
                    map1.put(Enchantment.getById(Integer.parseInt(ench[0])), Integer.valueOf(Integer.parseInt(ench[1])));
                } catch (Exception var15) {
                    ;
                }
            }
            item.addEnchantments(map1);
        }
        return item;
    }
}
