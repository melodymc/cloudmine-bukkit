package ru.bird.totems.Utils;

import org.bukkit.Location;
import org.bukkit.World;

import java.util.Objects;

public class LocationUtils {
    
    public static boolean equals(Location other, Location location) {
        World world = location.getWorld();
        World otherWorld = other.getWorld() == null ? null : other.getWorld();
        if (!Objects.equals(world, otherWorld)) {
            return false;
        } else if (Double.doubleToLongBits(location.getX()) != Double.doubleToLongBits(other.getX())) {
            return false;
        } else if (Double.doubleToLongBits(location.getY()) != Double.doubleToLongBits(other.getY())) {
            return false;
        }
        return Double.doubleToLongBits(location.getZ()) != Double.doubleToLongBits(other.getZ());
    }
    
}
