package ru.bird.pets.entity.armorstand.blocks;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import ru.bird.main.particles.ParticleEffect;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;

public class PetDiamond extends AbstractArmoredPet {

	public PetDiamond(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setArms(false);
		armor.setVisible(false);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getHelmet() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTYzMTU5N2RjZTRlNDA1MWU4ZDVhNTQzNjQxOTY2YWI1NGZiZjI1YTBlZDYwNDdmMTFlNjE0MGQ4OGJmNDhmIn19fQ==");
	}

	@Override
	public ItemStack getMainHand() {
		return null;
	}

	@Override
	public ItemStack getOffHand() {
		return null;
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		return null;
	}

	@Override
	public ItemStack getChestplate() {
		return null;
	}

	public ItemStack getInactiveHelmet() {
		return null;
	}

	@Override
	public void onLivingUpdate() {
		Location location = this.getPetLocation().clone().add(MathUtils.random(-0.2F, 0.2F), 0.8F + MathUtils.random(0.80F), MathUtils.random(-0.2F, 0.2F));
		this.setHeadPose(0 , 180 * MathUtils.cos(), 0);
		ParticleEffect.REDSTONE.sendColor(Bukkit.getOnlinePlayers(), location, Color.fromBGR(227, 227, 9));
		this.sendWatcherUpdate();
	}

}
