package ru.bird.pets.entity.normal;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.Cow;
import org.bukkit.entity.EntityType;

import net.minecraft.server.v1_16_R2.DataWatcher;
import ru.bird.pets.entity.AbstractStandardPet;

public class PetCow extends AbstractStandardPet {

	public PetCow(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		Cow entity = (Cow) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		entity.setBaby();
		DataWatcher data = ((CraftEntity) entity).getHandle().getDataWatcher();
		entity.remove();
		return data;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.COW;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

}
