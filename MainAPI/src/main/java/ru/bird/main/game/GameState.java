/*
 * Decompiled with CFR 0_115.
 */
package ru.bird.main.game;

import ru.bird.main.API;

public enum GameState {
    WAITING(0),
    STARTING(1),
    GAME(2),
    END(3);

    public static GameState current;
    private final int num;

    private GameState(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public static void setCurrent(GameState current) {
//        API.getInstance().getManagerRabitMQ().sendServerInfo(current);
        GameState.current = current;
    }

    static {
        current = WAITING;
    }
}

