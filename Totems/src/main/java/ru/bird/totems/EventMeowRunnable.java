package ru.bird.totems;

public interface EventMeowRunnable<E> {

    void meowRun(E event);

    default boolean validate(E eventMeow) {
        return true;
    }
}
