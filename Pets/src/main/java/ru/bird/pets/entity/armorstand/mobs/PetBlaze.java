package ru.bird.pets.entity.armorstand.mobs;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;

import ru.bird.main.particles.ParticleEffect;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;

public class PetBlaze extends AbstractArmoredPet {

	public PetBlaze(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setArms(true);
		armor.setVisible(false);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getMainHand() {
		return new ItemStack(Material.BLAZE_ROD);
	}

	@Override
	public ItemStack getOffHand() {
		return new ItemStack(Material.BLAZE_ROD);
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		return null;
	}

	@Override
	public ItemStack getChestplate() {
		return null;
	}

	@Override
	public ItemStack getHelmet() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjc4ZWYyZTRjZjJjNDFhMmQxNGJmZGU5Y2FmZjEwMjE5ZjViMWJmNWIzNWE0OWViNTFjNjQ2Nzg4MmNiNWYwIn19fQ==");
	}

	private float angle = 0;

	@Override
	public void onLivingUpdate() {
		angle += 10;
		angle = angle % 360;
		setLeftArmPose(angle, -angle, angle);
		setRightArmPose(-angle, angle, -angle);
		this.sendWatcherUpdate();
//		if (MathUtils.random(3) == 0) {
//			ParticleEffect.FLAME.sendToPlayers(Bukkit.getOnlinePlayers(), this.getPetLocation().clone().add(0, 1F, 0), rand.nextFloat() - 0.5F, rand.nextFloat() - 0.5F, rand.nextFloat() - 0.5F, 1 / 100, 1);
//		}
	}
}