/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package com.wellfail.crazywalls.shop;

import com.wellfail.crazywalls.shop.Cost;
import com.wellfail.crazywalls.utils.CWUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Item {
    public static Set<Item> items = new HashSet<Item>();
    private List<Cost> costs;
    private ItemStack item;
    private int slot;
    private boolean buy = true;

    public List<Cost> getCosts() {
        return this.costs;
    }

    public int getSlot() {
        return this.slot;
    }

    public ItemStack getItem() {
        return this.item;
    }

    public boolean isCanBuy() {
        return this.buy;
    }

    public void updateItem(Player player) {
        this.buy = true;
        ArrayList<String> newLore = new ArrayList<String>();
        ItemMeta meta = this.item.getItemMeta();
        newLore.add("§7Стоимость:");
        this.costs.forEach(cost -> {
            newLore.add(this.getColor(this.checkCost(cost, player)) + cost.getName() + ": " + cost.getAmount() + " (" + CWUtils.getAmountItem(cost.getCostItem(), player) + ")");
        });
        newLore.add(" ");
        if (this.buy) {
            newLore.add("§aНажмите, чтобы обменяться!");
        } else {
            newLore.add("§cНедостаточно ресурсов!");
        }
        meta.setLore(newLore);
        this.item.setItemMeta(meta);
    }

    private String getColor(boolean cost) {
        return cost ? "§a" : "§c";
    }

    public boolean checkCost(Cost cost, Player player) {
        if (CWUtils.getAmountItem(cost.getCostItem(), player) < cost.getAmount()) {
            this.buy = false;
            return false;
        }
        return true;
    }

    public void updateCost(Player player) {
        this.buy = true;
        this.updateItem(player);
    }

    public Item(List<Cost> costs, ItemStack item, int slot) {
        this.costs = costs;
        this.item = item;
        this.slot = slot;
        items.add(this);
    }
}

