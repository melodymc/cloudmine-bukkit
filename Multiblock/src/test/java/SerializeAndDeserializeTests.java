import nl.shanelab.multiblock.*;
import nl.shanelab.multiblock.patternobjects.PatternBlock;
import nl.shanelab.multiblock.patternobjects.PatternEntity;
import nl.shanelab.multiblock.patternobjects.PatternItem;
import nl.shanelab.multiblock.validators.CauldronValidator;
import nl.shanelab.multiblock.validators.DyeColorValidator;
import nl.shanelab.multiblock.validators.GrassSpeciesValidator;
import org.bukkit.DyeColor;
import org.bukkit.GrassSpecies;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.EntityType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.bird.storageUtils.utils.DeserializeUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class SerializeAndDeserializeTests {

    private <T extends ConfigurationSerializable> void deserializeForTest(Map<String, Object> args, String[] keysExample, Object[] valuesExample) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        T object = DeserializeUtils.staticDeserialize(args);
        Map<String, Object> serialized = object.serialize();
        Object[] keysArray = serialized.keySet().toArray();
        Object[] valuesArray = serialized.values().toArray();
        System.out.println("[0]: " + Arrays.toString(keysArray));
        System.out.println("[1]: " + Arrays.toString(keysExample));
        System.out.println("]0[: " + Arrays.toString(valuesArray));
        System.out.println("]1[: " + Arrays.toString(valuesExample));
        Assertions.assertTrue(
                Arrays.equals(keysExample, keysArray) &&
                        Arrays.equals(valuesExample, valuesArray)
        );
    }

    @Test
    public void materialWrapper() {
        Map<String, Object> materialWrapperSerialized = new MaterialWrapper(Material.STONE).serialize();
        try {
            deserializeForTest(materialWrapperSerialized, new String[]{"==", "material"}, new String[]{MaterialWrapper.class.getName(), Material.STONE.toString()});
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void patternBlockObject() {
        IMaterial iMaterial = new MaterialWrapper(Material.STONE);
        Map<String, Object> patternBlockSerialized = new PatternBlock(iMaterial, 2, 2, 8).serialize();
        try {
            Map<String, Object> iMaterialSerializedData = iMaterial.serialize();
            deserializeForTest(patternBlockSerialized, new String[]{"==", "relativeX", "relativeY", "relativeZ", "IMaterial"}, new Object[]{PatternBlock.class.getName(), 2, 2, 8, iMaterialSerializedData});
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void patternEntityObject() {
        Map<String, Object> patternEntitySerialized = new PatternEntity(EntityType.ARMOR_STAND, 2, 2, 8).serialize();
        try {
            String armorStandToString = EntityType.ARMOR_STAND.toString();
            deserializeForTest(patternEntitySerialized, new String[]{"==", "relativeX", "relativeY", "relativeZ", "entityType"}, new Object[]{PatternEntity.class.getName(), 2, 2, 8, armorStandToString});
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void patternItemObject() {
        Map<String, Object> patternItemSerialized = new PatternItem(Material.COAL, 2, 2, 8).serialize();
        try {
            Map<String, Object> coalMaterialToString = new MaterialWrapper(Material.COAL).serialize();
            deserializeForTest(patternItemSerialized, new String[]{"==", "relativeX", "relativeY", "relativeZ", "entityType", "IMaterial"}, new Object[]{PatternItem.class.getName(), 2, 2, 8, "DROPPED_ITEM", coalMaterialToString});
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void cauldronValidator() {
        Map<String, Object> cauldronValidator = new CauldronValidator(1).serialize();
        try {
            deserializeForTest(cauldronValidator, new String[]{"==", "stage"}, new Object[]{CauldronValidator.class.getName(), 1});
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void dyeColorValidator() {
        DyeColor dyeColor = DyeColor.BLUE;
        Map<String, Object> cauldronValidator = new DyeColorValidator(dyeColor).serialize();
        try {
            String whiteWoolEnumToString = Material.WHITE_WOOL.toString();
            deserializeForTest(cauldronValidator, new String[]{"==", "dyeColor", "material"}, new Object[]{DyeColorValidator.class.getName(), dyeColor.toString(), whiteWoolEnumToString});
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void grassSpeciesValidator() {
        GrassSpecies species = GrassSpecies.DEAD;
        Material material = Material.ACACIA_BOAT;
        Map<String, Object> grassSpeciesValidator = new GrassSpeciesValidator(species, material).serialize();
        try {
            deserializeForTest(grassSpeciesValidator, new String[]{"==", "species", "material"}, new Object[]{GrassSpeciesValidator.class.getName(), species.toString(), material.toString()});
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void multiBlockPattern() {
        IMaterial coreMaterial = new MaterialWrapper(Material.STONE);
        PatternObject[] objects = {new PatternBlock(Material.STONE, 0, 0, 1), new PatternBlock(Material.STONE, 0, 1, 0)};
        MultiBlockPatternFacing patternFacing = MultiBlockPatternFacing.CARDINAL;
        Map<String, Object> grassSpeciesValidator = new MultiBlockPattern(coreMaterial, patternFacing, objects).serialize();
        try {
            deserializeForTest(grassSpeciesValidator, new String[]{
                    "==",
                    "coreMaterial",
                    "objects",
                    "patternFacing"
            }, new Object[]{
                    MultiBlockPattern.class.getName(),
                    coreMaterial.serialize(),
                    Arrays.stream(objects).map(PatternObject::serialize).collect(Collectors.toList()),
                    patternFacing.toString()
            });
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

//    @Test
//    public void slabValidator() {
//        boolean up = true;
//        Material material = Material.PURPUR_SLAB;
//        Material stepType = Material.PURPUR_SLAB;
//        Map<String, Object> slabValidator = new SlabValidator(material, up, stepType).serialize();
//        try {
//            deserializeForTest(slabValidator, new String[]{"==", "material", "stepType", "up"}, new Object[]{GrassSpeciesValidator.class.getName(), stepType.toString(), up});
//        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
//            e.printStackTrace();
//            Assertions.fail();
//        }
//    }

}