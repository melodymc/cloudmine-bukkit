package io.cloudmc.restfullib.core;

public enum Permission
{
    GET, POST, PATCH, PUT, DELETE;
}