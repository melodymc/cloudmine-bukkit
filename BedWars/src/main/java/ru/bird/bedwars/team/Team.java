package ru.bird.bedwars.team;

import org.bukkit.*;
import ru.bird.bedwars.game.GameFactory;
import ru.bird.bedwars.game.GamePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import ru.bird.bedwars.sidebar.GameSidebar;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.Gamer;

import java.util.*;

public class Team {
    public static List<Team> teams = new ArrayList<Team>();
    public static Map<Player, Team> select = new HashMap<Player, Team>();
    private String offName;
    private int sub;
    private int slot;
    private Set<Player> players = new HashSet<Player>();
    private ChatColor color;
    private String name;
    private Location spawn;
    private List<Location> bed;
    private boolean canSpawn;
    private Inventory ender;
    private boolean win = true;
    private boolean first;

    public ChatColor getColor() {
        return this.color;
    }

    public String getName() {
        return this.name;
    }

    public String getOffName() {
        return offName;
    }

    public int getSlot() {
        return this.slot;
    }

    public int getSub() {
        return this.sub;
    }

    public boolean isCanSpawn() {
        return this.canSpawn;
    }

    public Inventory getChest() {
        return this.ender;
    }

    public Location getSpawn() {
        return this.spawn;
    }

    public Set<Player> getPlayers() {
        return this.players;
    }

    public int getSize() {
        return this.players.size();
    }

    public boolean breakBed(String player, Location lc) {
        if (!this.isBed(lc)) {
            return false;
        }
        for (Location loc : this.bed) {
            loc.getBlock().setType(Material.AIR);
        }
        this.canSpawn = false;
        Game.broadcast((String)("Кровать команды " + this.getColor() + this.getName() + "§f уничтожена игроком " + player));
        this.sendMessage(GameSettings.prefix + "Ваша кровать сломана. Вы больше не сможете возродиться!");
        return true;
    }

    public static String canSpawn(Team team) {
        if (team.isCanSpawn()) {
            return "§a⚑ ";
        }
        return "§c☠ ";
    }

    public boolean isBed(Location loc) {
        if (!this.canSpawn) {
            return false;
        }
        for (Location lc : this.bed) {
            boolean bool = equalsLoc(lc, loc);
            if (!bool) continue;
            return true;
        }
        return false;
    }

    private boolean equalsLoc(Location one, Location two) {
        return one.getBlockX() == two.getBlockX() && one.getBlockY() == two.getBlockY() && one.getBlockZ() == two.getBlockZ() || one.distance(two) <= 5;
    }

    public boolean addPlayer(Player player) {
        if (this.players.contains(player)) {
            return false;
        }
        this.players.add(player);
        GameFactory.players.put(player, new GamePlayer(player, this));
        select.put(player, this);
        return true;
    }

    public void removePlayer(Player player) {
        this.players.remove(player);
        select.remove(player);
    }

    public void win() {
        this.addMoney(50);
        this.players.forEach(player -> {
            GameFactory.players.get(player).setWin();
        });
    }

    private void addMoney(int amount) {
        for (Player player : this.players) {
            Gamer gamer = Gamer.getGamer((Player)player);
            gamer.addMoney(amount);
        }
    }

    public void teleportToSpawn() {
        this.players.forEach(player -> {
            if (!this.spawn.getChunk().isLoaded()) {
                this.spawn.getChunk().load();
            }
            player.teleport(this.spawn);
            new GameSidebar(player).send(player);
        });
    }

    public void setTags() {
        this.players.forEach(player -> {
            String name = player.getName();
            if (name.length() > 14) {
                name = name.substring(0, 14);
            }
            player.setPlayerListName(this.color + name);
        }
        );
    }

    public void sendMessage(String message) {
        this.players.stream().filter(OfflinePlayer::isOnline).forEach(player -> {
            player.sendMessage(message);
        });
    }

    public Team(String name, String offName, Location spawn, List<Location> bed, ChatColor color, int sub, int slot) {
        this.slot = slot;
        this.sub = sub;
        this.color = color;
        this.name = name;
        this.offName = offName;
        this.spawn = spawn;
        this.bed = bed;
        this.canSpawn = true;
        this.ender = Bukkit.createInventory(null, 27, ("§rСундук команды " + this.getColor() + this.getName()));
    }

    public int getKills() {
        return players.stream().mapToInt(player -> Gamer.getGamer(player).getKills()).sum();
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }
}