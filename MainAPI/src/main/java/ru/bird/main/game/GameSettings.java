/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 */
package ru.bird.main.game;

import org.bukkit.Location;
import ru.bird.main.board.api.Board;

import java.util.List;

public abstract class GameSettings {
    public static boolean isWeatherChange = false;
    public static boolean isFoodChange = false;
    public static boolean isTimeChange = false;
    public static boolean drop = true;
    public static boolean fallDamage = false;
    public static boolean pickup = false;
    public static boolean physical = false;
    public static boolean inventory = false;
    public static boolean damage = false;
    public static boolean setup = false;
    public static boolean shop = false;
    public static boolean isBreak = false;
    public static boolean isPlace = false;
    public static boolean chest = false;
    public static boolean isSpectate = false;
    public static boolean itemSpawn = false;
    public static boolean explode = false;
    public static boolean nockickarmorstand;
    public static Board spectatorSidebar;
    public static String game;
    public static int worldTime;
    public static Location respawnLocation;
    public static Location mapLocation;
    public static Location Lobby;
    public static boolean respawn;
    public static int respawnTime;
    public static int toStart;
    public static int slots;
    public static int gameTime;
    public static String prefix;
    public static String wboardname;
    public static int partyLimit;
    public static Location spectrLocation;
    public static boolean perks;
    public static boolean teamChanger;
    public static boolean itemdrop;
    public static boolean death;
    public static boolean removearrowonhit;
    public static boolean deathnomsg;
    public static boolean axetrowing;
    public static boolean endgamelastplayer;
    public static boolean hiddenicks;
    public static boolean crops;
    public static boolean specthrow;

    static {
        worldTime = 5000;
        respawn = false;
        respawnTime = 5;
        toStart = 6;
        slots = 12;
        gameTime = 1200;
        prefix = "Arcade";
        wboardname = "Arcade";
        partyLimit = 4;
        perks = false;
        teamChanger = false;
        itemdrop = false;
        death = true;
        deathnomsg = true;
        removearrowonhit = false;
        axetrowing = false;
        endgamelastplayer = true;
        nockickarmorstand = false;
        hiddenicks = false;
        crops = false;
        specthrow = true;
    }
}

