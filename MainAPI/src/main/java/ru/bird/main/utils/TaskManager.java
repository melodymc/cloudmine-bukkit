package ru.bird.main.utils;

public abstract class TaskManager {
    public static BExecutorService service = new BExecutorService();

    public static void addTask(Runnable runnable) {
        service.execute(runnable);
    }
}