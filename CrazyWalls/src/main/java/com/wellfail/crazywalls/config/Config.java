/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.Material
 */
package com.wellfail.crazywalls.config;

import com.wellfail.crazywalls.utils.Barrier;
import com.wellfail.crazywalls.wall.Wall;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.Material;

public class Config {
    public static int wallHeight;
    public static List<Location> locations;
    public static int delayColb;
    public static List<Wall> redWalls;
    public static List<Wall> greenWalls;
    public static String traped;
    public static String chest;
    public static Material replace;
    public static List<Location> shops;
    public static Location center;
    public static int radius;
    public static Barrier barrier;
}

