package ru.bird.pets.entity.armorstand.blocks;

import net.minecraft.server.v1_16_R2.DataWatcher;
import net.minecraft.server.v1_16_R2.EnumItemSlot;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import ru.bird.main.particles.ParticleEffect;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.MathUtils;

import java.util.UUID;

public class PetFurnace extends AbstractArmoredPet {

    public PetFurnace(UUID ownerID, int uniqueID) { super(ownerID, uniqueID); }

    @Override
    public ItemStack getMainHand() {
        return null;
    }

    @Override
    public ItemStack getOffHand() {
        return null;
    }

    @Override
    public ItemStack getBoots() {
        return null;
    }

    @Override
    public ItemStack getLeggings() {
        return null;
    }

    @Override
    public ItemStack getChestplate() {
        return null;
    }

    @Override
    public ItemStack getHelmet() {
        return flame();
    }

    private ItemStack flame() {
        return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTEyY2U3OTgyNDZhZGRiY2E2NThmZmJlMWZmYzA1N2I1NmQ0YjM4N2E3MTk1YmY5NTlhYjRjOTdlYTNlNSJ9fX0=");
    }

    private ItemStack flameOff() {
        return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDE3YjhiNDNmOGM0YjVjZmViOTE5YzlmOGZlOTNmMjZjZWI2ZDJiMTMzYzJhYjFlYjMzOWJkNjYyMWZkMzA5YyJ9fX0=");
    }

    @Override
    protected DataWatcher build() {
        ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
        armor.setSmall(true);
        armor.setBasePlate(false);
        armor.setArms(false);
        armor.setVisible(false);
        DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
        armor.remove();
        return data;
    }

    @Override
    public EntityType getEntityType() {
        return EntityType.ARMOR_STAND;
    }

    @Override
    protected float getOffsetY() {
        return 1F;
    }

    private int stage = 0;

    @Override
    public void onLivingUpdate() {
        stage++;
        switch (stage) {
            case 0:
            case 20:
                sendEquipmentPacket(EnumItemSlot.HEAD, flame());
                if (MathUtils.random(3) == 0) {
                    ParticleEffect.FLAME.sendToPlayers(Bukkit.getOnlinePlayers(), this.getPetLocation().clone().add(0, 1F, 0), rand.nextFloat() - 0.5F, rand.nextFloat() - 0.5F, rand.nextFloat() - 0.5F, 1 / 100, 2);
                }
            break;
            case 21:
            case 39:
                sendEquipmentPacket(EnumItemSlot.HEAD, flameOff());
                stage = 0;
                break;
        }
    }
}
