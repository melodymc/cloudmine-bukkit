package ru.bird.totems.structures;

import com.ticxo.modelengine.api.model.ModeledEntity;
import com.ticxo.modelengine.api.model.component.ModelOption;
import nl.shanelab.multiblock.*;
import nl.shanelab.multiblock.patternobjects.PatternBlock;
import nl.shanelab.multiblock.patternobjects.PatternItem;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.Material;
import org.bukkit.util.Vector;
import ru.bird.totems.Main;

import javax.annotation.Nonnull;

public class RawChickenTomb implements IMultiBlock {

    @Override
    public void onActivate(Plugin plugin, Location coreBlockLocation, Player player, MultiBlockActivation activation) {
        System.out.println("test");
        MultiBlockActivationType activationType = activation.getType();
        if (activationType == MultiBlockActivationType.CORE_PLACED) {
            MultiBlockPatternFacing facing = activation.getFacing();
            player.sendMessage(ChatColor.GREEN + "Good. Facing of tomb: " + facing);
            Vector vector;
            switch(facing) {
                case NORTH: vector = new Vector(0, 0, 0.5); break;
                case EAST: vector = new Vector(-0.5, 0, 0); break;
                case SOUTH: vector = new Vector(0, 0, -0.5); break;
                case WEST: vector = new Vector(0.5, 0, 0); break;
                default:
                    vector = new Vector(0, 0, 0);
                    break;
            }
            player.getWorld().strikeLightningEffect(coreBlockLocation.add(vector));
        } else if (activationType == MultiBlockActivationType.CORE_INTERACTED) {
            player.sendMessage(ChatColor.RED + "Interacted with example multiblock");
        }
    }

    @Nonnull
    @Override
    public MultiBlockPattern getMultiBlockPattern() {
        return new MultiBlockPattern(Material.OAK_PLANKS,
                new PatternBlock(Material.OAK_PLANKS, 0, 1, 0),
                new PatternBlock(Material.OAK_PLANKS, 0, 2, 0),
                new PatternBlock(Material.OAK_FENCE, -1, 2, 0),
                new PatternBlock(Material.OAK_FENCE, 1, 2, 0),
                new PatternItem(Material.CHICKEN, 0, 0, 1)
        );
    }
}