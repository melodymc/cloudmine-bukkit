package ru.bird.main.utils;

import java.util.Collection;
import java.util.Iterator;

public class FIterator<T> {
    private Collection<T> collection;
    private Iterator<T> iterator;

    public Collection<T> getCollection() {
        return this.collection;
    }

    public FIterator(Collection<T> collection) {
        this.collection = collection;
        this.iterator = collection.iterator();
    }

    public T getNext() {
        if (!this.iterator.hasNext()) {
            this.iterator = this.collection.iterator();
        }
        return this.iterator.next();
    }
}