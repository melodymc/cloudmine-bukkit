package ru.bird.inventories;

import io.netty.util.internal.ConcurrentSet;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ItemManager
        implements Listener {
    private static final Map<JavaPlugin, Set<Item>> items = new HashMap<>();
    private final JavaPlugin plugin;

    public void addItem(Item item) {
        items.get(this.plugin).add(item);
    }

    public ItemManager(JavaPlugin plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, this.plugin);
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onInteract(PlayerInteractEvent e) {
        items.get(this.plugin).stream().filter(item -> this.equal(item.getItem(), e.getPlayer().getItemInHand(), item.getDynamicData(), item.getDynamicName())).forEach(item -> {
            for (Action action : item.getActions()) {
                if (action != e.getAction()) continue;
                item.getRunnable().onUse(e);
                return;
            }
        });
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onInteract(InventoryClickEvent e) {
        items.get(this.plugin).forEach(item -> {
            if (this.equal(item.getItem(), e.getCurrentItem(), item.getDynamicData(), item.getDynamicName())) {
                item.getRunnable().onClick(e);
                return;
            }
        });
    }

    private boolean equal(ItemStack a, ItemStack b, boolean dynamicData, boolean dynamicName) {
        if (a == null || b == null) {
            return false;
        }
        if (a.getType() != b.getType()) {
            return false;
        }
        if (a.getAmount() != b.getAmount()) {
            return false;
        }
        if (!dynamicData && !a.getData().equals((Object)b.getData())) {
            return false;
        }
        if (!a.getEnchantments().equals(b.getEnchantments())) {
            return false;
        }
        if (!dynamicName && a.hasItemMeta() && a.getItemMeta().hasDisplayName() && !a.getItemMeta().getDisplayName().equals(b.getItemMeta().getDisplayName())) {
            return false;
        }
        return true;
    }
}

