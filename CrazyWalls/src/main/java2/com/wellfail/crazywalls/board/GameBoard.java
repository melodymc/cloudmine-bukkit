// 
// Decompiled by Procyon v0.5.30
// 

package com.wellfail.crazywalls.sidebar;

import ru.wellfail.wapi.game.Gamer;
import org.bukkit.Bukkit;
import ru.wellfail.wapi.utils.Utils;
import com.wellfail.crazywalls.game.GameFactory;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;
import ru.wellfail.wapi.board.BoardLine;
import java.util.List;
import org.bukkit.entity.Player;
import ru.wellfail.wapi.board.Board;

public class GameBoard extends Board
{
    private int d;
    private Player player;
    private List<String> list;
    private BoardLine players;
    private BoardLine spectators;
    private BoardLine kills;
    private BoardLine greenwall;
    private BoardLine redwall;
    private BoardLine border;
    private boolean endred;
    private boolean endgreen;
    private boolean end;
    
    public GameBoard(final Player player) {
        this.endred = false;
        this.endgreen = false;
        this.end = false;
        this.player = player;
        final Set<BoardLine> lines = this.getLines();
        this.create((Set)lines, "§e§lCRAZY WALLS");
        (this.list = new ArrayList<String>()).add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§6§lC§e§lRAZY WALLS");
        this.list.add("§f§lC§6§lR§e§lAZY WALLS");
        this.list.add("§f§lCR§6§lA§e§lZY WALLS");
        this.list.add("§f§lCRA§6§lZ§e§lY WALLS");
        this.list.add("§f§lCRAZ§6§lY§e§l WALLS");
        this.list.add("§f§lCRAZY §6§lW§e§lALLS");
        this.list.add("§f§lCRAZY W§6§lA§e§lLLS");
        this.list.add("§f§lCRAZY WA§6§lL§e§lLS");
        this.list.add("§f§lCRAZY WAL§6§lL§e§lS");
        this.list.add("§f§lCRAZY WALL§6§lS");
        this.list.add("§f§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§f§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§f§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§f§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.list.add("§e§lCRAZY WALLS");
        this.update(() -> {
            if (this.d == this.list.size()) {
                this.d = 0;
            }
            this.setDisplay((String)this.list.get(this.d));
            ++this.d;
            return;
        }, 3L);
        this.setHealth(player);
        this.set(player);
        this.updateHealth(player);
        this.startUpdate();
    }
    
    public Set<BoardLine> getLines() {
        final Set<BoardLine> set = new HashSet<BoardLine>();
        set.add(new BoardLine((Board)this, "§1", 10));
        set.add(this.greenwall = new BoardLine((Board)this, "§a\u0417\u0435\u043b\u0435\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: §a" + Utils.getTimeToEnd(GameFactory.timer.getGreen()), 9));
        this.dynamicLine(this.greenwall.getNumber(), "§a\u0417\u0435\u043b\u0435\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: §a", Utils.getTimeToEnd(GameFactory.timer.getGreen()));
        set.add(this.redwall = new BoardLine((Board)this, "§c\u041a\u0440\u0430\u0441\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: §c" + Utils.getTimeToEnd(GameFactory.timer.getRed()), 8));
        this.dynamicLine(this.redwall.getNumber(), "§c\u041a\u0440\u0430\u0441\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: §c", Utils.getTimeToEnd(GameFactory.timer.getRed()));
        set.add(new BoardLine((Board)this, "§2", 7));
        this.border = new BoardLine((Board)this, "\u0414\u0435\u0437\u043c\u0430\u0442\u0447: §a" + Utils.getTimeToEnd(GameFactory.timer.getBorder()), 6);
        this.dynamicLine(this.border.getNumber(), "\u0414\u0435\u0437\u043c\u0430\u0442\u0447: §a", Utils.getTimeToEnd(GameFactory.timer.getBorder()));
        set.add(this.border);
        set.add(new BoardLine((Board)this, "§3", 5));
        set.add(this.players = new BoardLine((Board)this, "\u0418\u0433\u0440\u043e\u043a\u043e\u0432: §a", 4));
        this.dynamicLine(this.players.getNumber(), "\u0418\u0433\u0440\u043e\u043a\u043e\u0432: §a", GameFactory.players.size() + "");
        set.add(this.spectators = new BoardLine((Board)this, "\u041d\u0430\u0431\u043b\u044e\u0434\u0430\u0442\u0435\u043b\u0435\u0439: §a", 3));
        this.dynamicLine(this.spectators.getNumber(), "\u041d\u0430\u0431\u043b\u044e\u0434\u0430\u0442\u0435\u043b\u0435\u0439: §a", Bukkit.getOnlinePlayers().size() - GameFactory.players.size() + "");
        set.add(this.kills = new BoardLine((Board)this, "\u0423\u0431\u0438\u0439\u0441\u0442\u0432: §a", 1));
        this.dynamicLine(this.kills.getNumber(), "\u0423\u0431\u0438\u0439\u0441\u0442\u0432: §a", Gamer.getGamer(this.player).getKills() + "");
        set.add(new BoardLine((Board)this, "§4", 2));
        this.update(() -> {
            if (GameFactory.timer.getBorder() == 0) {
                if (!this.end) {
                    this.setLine(this.border.getNumber(), "\u041a\u043e\u043d\u0435\u0446:§a " + Utils.getTimeToEnd());
                    this.end = true;
                }
                this.dynamicLine(this.border.getNumber(), "\u041a\u043e\u043d\u0435\u0446:§a ", Utils.getTimeToEnd());
            }
            else {
                this.dynamicLine(this.border.getNumber(), "\u0414\u0435\u0437\u043c\u0430\u0442\u0447: §a", Utils.getTimeToEnd(GameFactory.timer.getBorder()));
            }
            if (GameFactory.timer.getGreen() == 0) {
                if (!this.endgreen) {
                    this.setLine(this.greenwall.getNumber(), "§7§m\u0417\u0435\u043b\u0435\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: §7§m00:00§6");
                    this.endgreen = true;
                }
                else {
                    this.dynamicLine(this.greenwall.getNumber(), "§7§m\u0417\u0435\u043b\u0435\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: §7§m00:00§6", "");
                }
            }
            else {
                this.dynamicLine(this.greenwall.getNumber(), "§a\u0417\u0435\u043b\u0435\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: §a", Utils.getTimeToEnd(GameFactory.timer.getGreen()));
            }
            if (GameFactory.timer.getRed() == 0) {
                if (!this.endred) {
                    this.setLine(this.redwall.getNumber(), "§7§m\u041a\u0440\u0430\u0441\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: §7§m00:00§7");
                    this.endred = true;
                }
                else {
                    this.dynamicLine(this.redwall.getNumber(), "§7§m\u041a\u0440\u0430\u0441\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: §7§m00:00§7", "");
                }
            }
            else {
                this.dynamicLine(this.redwall.getNumber(), "§c\u041a\u0440\u0430\u0441\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430: §c", Utils.getTimeToEnd(GameFactory.timer.getRed()));
            }
            return;
        }, 20L);
        this.update(() -> {
            this.dynamicLine(this.spectators.getNumber(), "\u041d\u0430\u0431\u043b\u044e\u0434\u0430\u0442\u0435\u043b\u0435\u0439: §a", Bukkit.getOnlinePlayers().size() - Utils.getAlivePlayers().size() + "");
            this.dynamicLine(this.players.getNumber(), "\u0418\u0433\u0440\u043e\u043a\u043e\u0432: §a", Utils.getAlivePlayers().size() + "");
            this.dynamicLine(this.kills.getNumber(), "\u0423\u0431\u0438\u0439\u0441\u0442\u0432: §a", Gamer.getGamer(this.player).getKills() + "");
            return;
        }, 100L);
        return set;
    }
}
