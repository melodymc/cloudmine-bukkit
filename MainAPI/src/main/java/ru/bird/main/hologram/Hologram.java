package ru.bird.main.hologram;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.Plugin;
import ru.bird.main.API;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hologram {
    private Location location;
    private List<String> lines;
    private Map<String, ArmorStand> armorStands;
    private Plugin plugin;
    private Runnable doUpdate;

    public Hologram(Location location, Plugin plugin, List<String> lines) {
        this.location = location;
        this.plugin = plugin;
        this.lines = lines;
        this.armorStands = new HashMap<>();
        spawn();
    }

    public Hologram(Location location, Plugin plugin, String... lines) {
        this.location = location;
        this.plugin = plugin;
        this.lines = Arrays.asList(lines);
        this.armorStands = new HashMap<>();
        spawn();
    }

    public void setDoUpdate(Runnable doUpdate) {
        this.doUpdate = doUpdate;
    }

    private void spawn(){
        lines.forEach(this::spawnArmorStand);
    }

    private void spawnArmorStand(String line) {
        ArmorStand as = (ArmorStand) location.getWorld().spawnEntity(location.add(0.0, -0.26, 0.0), EntityType.ARMOR_STAND); //Spawn the ArmorStand

        as.setGravity(false); //Make sure it doesn't fall
        as.setCanPickupItems(false); //I'm not sure what happens if you leave this as it is, but you might as well disable it
        as.setCustomName(line.replace("&", ChatColor.COLOR_CHAR + "")); //Set this to the text you want
        as.setCustomNameVisible(true); //This makes the text appear no matter if your looking at the entity or not
        as.setVisible(false); //Makes the ArmorStand invisible

        armorStands.put(line, as);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<String> getLines() {
        return lines;
    }

//    public void setLines(List<String> lines) {
//        this.lines = lines;
//    }

    public Map<String, ArmorStand> getArmorStands() {
        return armorStands;
    }

    public void startUpdate() {
        if(doUpdate != null) {
            API.getInstance().getServer().getScheduler().runTask(plugin, doUpdate);
        }
    }
}