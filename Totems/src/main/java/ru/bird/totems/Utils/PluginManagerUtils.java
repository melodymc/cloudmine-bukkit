package ru.bird.totems.Utils;

import ru.bird.totems.EventMeowRunnable;
import org.bukkit.Bukkit;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class PluginManagerUtils {

    public static <E extends Event & Cancellable> void oneTimeEvent(Class<? extends Event> eventClass, JavaPlugin plugin, EventPriority priority, EventMeowRunnable<E> eventMeowRunnable) throws ClassCastException {
        Bukkit.getServer().getPluginManager().registerEvent(eventClass, new Listener() {}, priority, (Listener listener, Event event) -> {
            E eventMeow = (E) event;
            eventMeow.setCancelled(true);
            eventMeow.getHandlers().unregister(listener);
            if(eventMeowRunnable.validate(eventMeow)) {
                eventMeowRunnable.meowRun(eventMeow);
            } else {
                PluginManagerUtils.oneTimeEvent(eventClass, plugin, priority, eventMeowRunnable);
            }
        }, plugin);
    }


}
