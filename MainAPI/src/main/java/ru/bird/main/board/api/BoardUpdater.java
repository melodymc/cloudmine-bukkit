package ru.bird.main.board.api;

public interface BoardUpdater {
    void update(Board board);
}
