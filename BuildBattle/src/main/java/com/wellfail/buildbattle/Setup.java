/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.ru.bird.jobs.command.Command
 *  org.bukkit.ru.bird.jobs.command.CommandExecutor
 *  org.bukkit.ru.bird.jobs.command.CommandSender
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.entity.Player
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.buildbattle;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import ru.bird.main.utils.GameUtils;

public class Setup
implements CommandExecutor {
    private HashMap<String, Location[]> regions = new HashMap<>();
    private String current;

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.isOp()) {
            return true;
        }
        if (args.length < 2) {
            sender.sendMessage("/bsetup [region] 1");
            sender.sendMessage("/bsetup [region] 2");
            return true;
        }
        Player player = (Player)sender;
        String region = args[0];
        Location[] locs = this.regions.get(region);
        if (locs == null) {
            locs = new Location[2];
        }
        switch (args[1].toLowerCase()) {
            case "1": {
                locs[0] = player.getLocation();
                this.regions.put(region, locs);
                break;
            }
            case "2": {
                locs[1] = player.getLocation();
                this.regions.put(region, locs);
                break;
            }
            case "save": {
                this.save();
                break;
            }
            default: {
                player.sendMessage("\u042f \u043d\u0435 \u0437\u043d\u0430\u044e \u0447\u0442\u043e \u0434\u0435\u043b\u0430\u0442\u044c");
                return true;
            }
        }
        player.sendMessage("\u0412\u0441\u0435 \u043d\u043e\u0440\u043c");
        return true;
    }

    private void save() {
        FileConfiguration config = Main.getInstance().getConfig();
        for (Map.Entry<String, Location[]> entry : this.regions.entrySet()) {
            int i = 1;
            for (Location loc : entry.getValue()) {
                config.set("Regions." + entry.getKey() + "." + i, GameUtils.locationToString(loc, false));
                ++i;
            }
        }
        Main.getInstance().saveConfig();
    }
}

