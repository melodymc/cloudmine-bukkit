// 
// Decompiled by Procyon v0.5.30
// 

package com.wellfail.crazywalls.utils;

import org.bukkit.plugin.Plugin;
import ru.wellfail.wapi.Main;
import ru.wellfail.wapi.game.GameSettings;
import org.bukkit.scheduler.BukkitRunnable;
import java.util.Iterator;
import org.bukkit.inventory.meta.ItemMeta;
import ru.wellfail.wapi.game.Gamer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import com.wellfail.crazywalls.config.Config;
import ru.wellfail.wapi.utils.Utils;
import java.util.ArrayList;
import com.wellfail.crazywalls.wall.Wall;
import java.util.List;
import org.bukkit.configuration.file.FileConfiguration;

public class CWUtils
{
    public static List<Wall> getWalls(final FileConfiguration config, final String wall) {
        final List<Wall> wals = new ArrayList<Wall>();
        final String[] locs;
        final List<Wall> list;
        config.getStringList(wall).stream().forEach(line -> {
            locs = line.split("/");
            list.add(new Wall(Utils.stringToLocation(locs[0]), Utils.stringToLocation(locs[1]), Config.wallHeight));
            return;
        });
        return wals;
    }
    
    public static void sendStartMessages(final Player p) {
        p.sendMessage("§a§l\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac");
        p.sendMessage("");
        p.sendMessage("                                   §c§lCrazyWalls");
        p.sendMessage("");
        p.sendMessage("                      §e\u041f\u043e\u0441\u0442\u0430\u0440\u0430\u0439\u0441\u044f \u0443\u043d\u0438\u0447\u0442\u043e\u0436\u0438\u0442\u044c \u0432\u0441\u0435\u0445, \u043a\u0440\u043e\u043c\u0435 \u0441\u0435\u0431\u044f.");
        p.sendMessage("                      §e\u0412\u0441\u0435 \u043d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e\u0435 \u0442\u044b \u043d\u0430\u0439\u0434\u0435\u0448\u044c \u0432 \u0441\u0443\u043d\u0434\u0443\u043a\u0435...");
        p.sendMessage("");
        p.sendMessage("§a§l\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac\u25ac");
    }
    
    public static void giveStartItems(final Player player) {
        player.getInventory().setItem(0, new ItemStack(Material.STONE_SWORD));
        final ItemStack pick = new ItemStack(Material.IRON_PICKAXE);
        final ItemMeta meta = pick.getItemMeta();
        meta.addEnchant(Enchantment.DIG_SPEED, 2, true);
        pick.setItemMeta(meta);
        player.getInventory().setItem(1, pick);
        player.getInventory().setItem(2, new ItemStack(Material.IRON_AXE));
        if (Gamer.getGamer(player).getPurchase() != null) {
            for (final ItemStack item : Gamer.getGamer(player).getPurchase().getItems()) {
                if (item == null) {
                    continue;
                }
                player.getInventory().addItem(new ItemStack[] { item });
            }
        }
    }
    
    public static void removeCell(final Player p) {
        new BukkitRunnable() {
            public void run() {
                Utils.removeBlocks(p.getLocation());
                GameSettings.isBreak = true;
                GameSettings.isPlace = true;
            }
        }.runTaskLater((Plugin)Main.getInstance(), (long)Config.delayColb);
    }
    
    public static int getAmountItem(final ItemStack item, final Player player) {
        int amount = 0;
        for (final ItemStack it : player.getInventory().getContents()) {
            if (it != null) {
                if (item.getType() == it.getType()) {
                    amount += it.getAmount();
                }
            }
        }
        return amount;
    }
}
