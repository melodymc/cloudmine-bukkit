/*
 * Decompiled with CFR 0_122.
 *
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.player.PlayerJoinEvent
 *  ru.wellfail.wapi.board.Board
 *  ru.wellfail.wapi.board.BoardLine
 *  ru.bird.main.game.Game
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.buildbattle.board;

import com.wellfail.buildbattle.GameFactory;
import org.bukkit.Bukkit;
import ru.bird.main.board.api.Board;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.utils.GameUtils;
import ru.pir.main.utils.BoardStringAnimator;

public class GameBoard extends Board
{
    public GameBoard()
    {
        super(GameSettings.wboardname.toUpperCase());

        String gameTitle = GameSettings.wboardname.toUpperCase();
        addUpdaterHeader(3, BoardStringAnimator.concatFrameBuffers(
                BoardStringAnimator.generateStatic(gameTitle, "&e", 5),
                BoardStringAnimator.generateMetalShine(gameTitle, "&e", "&6", "&f"),
                BoardStringAnimator.generateBlinking(gameTitle, "&f", "&e", 5)
        ));
        write(0);
        write(1, "Игроков: §a " + (Bukkit.getOnlinePlayers().size() - Game.spectators.size()) + "");
        write(2);
        write(3, "Тема: §a " + GameFactory.getTheme());
        write(4);
        write(5, "Время: §a 05:00");
        write(6);

        addUpdater(20, board -> {
            int playerCount = Bukkit.getOnlinePlayers().size() - Game.spectators.size();
            board.modifyLine(1, "Игроков: §a " + playerCount);
            board.modifyLine(5, "Время: §a " + GameUtils.getTimeToEnd());
        });
        create();
    }
}