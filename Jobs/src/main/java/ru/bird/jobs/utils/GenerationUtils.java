//package ru.bird.jobs.utils;
//
//import com.plotsquared.core.generator.ClassicPlotWorld;
//import com.plotsquared.core.plot.PlotArea;
//import org.bukkit.Bukkit;
//import org.bukkit.Location;
//import org.bukkit.Material;
//import org.bukkit.World;
//import org.bukkit.block.Biome;
//import org.bukkit.block.Block;
//import org.bukkit.entity.Player;
//import ru.bird.jobs.Generation;
//
//import com.plotsquared.core.plot.Plot;
//import ru.bird.jobs.JobsPlugin;
//
//import java.util.Random;
//
//public class GenerationUtils {
//
//    public static void generateShaft(Player player, Plot plot, Biome biome) {
//        if (BiomeHandler.isRunning) {
//            player.sendMessage("Генерация уже запущена.");
//        } else {
//            BiomeHandler.getNewGenerator(biome, (new Random(System.nanoTime())).nextLong());
//            PlotArea plotworld = plot.getArea();
//            final int height;
//            if (plotworld instanceof ClassicPlotWorld) {
//                height = ((ClassicPlotWorld) plotworld).PLOT_HEIGHT;
//            } else {
//                height = 64;
//            }
//
//            final World world = Bukkit.getWorld(plot.getArea().worldname);
//            final Set<RegionWrapper> allRegions = plot.getRegions();
//            final ArrayDeque<RegionWrapper> regions = new ArrayDeque<RegionWrapper>(allRegions);
//            if (regions.size() == 0) {
//                player.sendMessage("&aDone!");
//            } else {
//                RegionWrapper region = regions.poll();
//                Location pos1 = new Location(world, (double) region.minX, (double) region.minY, (double) region.minZ);
//                Location pos2 = new Location(world, (double) region.maxX, (double) region.maxY, (double) region.maxZ);
//                BiomeSelection selection = new BiomeSelection(world, pos1, pos2, height);
//
//                BiomeHandler.generate(selection, player, (Runnable) () -> {
//                    if (checkForShitGeneration(pos1, pos2, player)) {
//                        setBlocks(pos1, pos2, Material.AIR);
//                        Bukkit.getServer().getScheduler().runTaskLater(JobsPlugin.get(), () -> generateShaft(player, plot, biome), 10);
//                    } else {
//                        generateStandart(world, region, pos1);
//                        player.sendMessage("Done");
//                    }
//                });
//            }
//        }
//    }
//    private static void generateStandart(World world, RegionWrapper region, Location pos1) {
//        setBlocks(new Location(world, (double) region.minX - 1, (double) region.minY - 1, (double) region.minZ - 1),
//                new Location(world, (double) region.maxX + 1, (double) region.minY - 1, (double) region.maxZ + 1),
//                Material.BEDROCK
//        );
//    }
//
//    private static boolean checkForShitGeneration(Location pos1, Location pos2, Player player) {
//        Generation generation = checkValid(new Generation(pos1, pos2, Material.WATER));
//        double water = generation.getPersentWater();
//        double maxY = generation.getMaxY();
//        player.sendMessage("water: " + water + "% maxY: "+ maxY);
//        return (water > 5) || (maxY > 85) || (maxY < 64);
//    }
//
//    public static void setBlocks(Location min, Location max, Material material) {
//        for (int x = min.getBlockX(); x <= max.getBlockX(); x++) {
//            for (int y = min.getBlockY(); y <= max.getBlockY(); y++) {
//                for (int z = min.getBlockZ(); z <= max.getBlockZ(); z++) {
//                    Block block = min.getWorld().getBlockAt(new Location(min.getWorld(), x, y, z));
//                    block.setType(material);
//                }
//            }
//        }
//    }
//
//    public static Generation checkValid(Generation generation) {
//        int currentBlock = 0;
//        int allblocks = 0;
//        double maxY = 0;
//        for (int x = generation.getMin().getBlockX(); x <= generation.getMax().getBlockX(); x++) {
//            for (int y = generation.getMin().getBlockY(); y <= generation.getMax().getBlockY(); y++) {
//                for (int z = generation.getMin().getBlockZ(); z <= generation.getMax().getBlockZ(); z++) {
//                    Material mat = generation.getMin().getWorld().getBlockAt(new Location(generation.getMax().getWorld(), x, y, z)).getType();
//                    if(mat != Material.AIR) {
//                        if (mat == generation.getMaterial()) {
//                            currentBlock++;
//                        }
//                        if(y > maxY){
//                            maxY = y;
//                        }
//                        allblocks++;
//                    }
//                }
//            }
//        }
//        double value;
//        try {
//            value = 100.0 / (allblocks / currentBlock);
//        } catch (ArithmeticException e) {
//            value = 0;
//        }
//        generation.setPersentWater(value);
//        generation.setMaxY(maxY);
//        return generation;
//    }
//}
