package nl.shanelab.multiblock.validators;

import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.block.Block;
import org.bukkit.material.WoodenStep;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class WoodSlabValidator extends TreeSpeciesValidator {

	private boolean up;
	
	private boolean sided;
	
	public WoodSlabValidator(TreeSpecies species) {
		this(species, false);
		this.sided = false;
	}
	
	public WoodSlabValidator(TreeSpecies species, boolean up) {
		super(species, Material.LEGACY_WOOD_STEP);
		
		this.up = up;
		
		this.sided = true;
	}

	private WoodSlabValidator(TreeSpecies species, Material material, boolean sided, boolean up) {
		super(species, material);
		this.up = up;
		this.sided = sided;
	}

	@Override
	public boolean validateBlock(Block block) {
		boolean flag = super.validateBlock(block);
		
		System.out.println("sided: " + sided);

		return flag && (!sided || (sided && ((WoodenStep)block.getState().getData()).isInverted() == up));
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> result = super.serialize();
		result.put("sided", sided);
		result.put("up", up);
		return result;
	}

	@Override
	public <T> T deserialize(Map<String, Object> args) throws IllegalArgumentException, NullPointerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return (T) new WoodSlabValidator(TreeSpecies.valueOf((String) args.get("treeSpecies")), Material.valueOf((String) args.get("material")), (boolean) args.get("sided"), (boolean) args.get("up"));
	}
}