// 
// Decompiled by Procyon v0.5.30
// 

package ru.bird.murdermistery.game;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import ru.bird.murdermistery.listeners.Spawner;
import ru.bird.murdermistery.board.GameBoard;
import ru.bird.murdermistery.Main;
import ru.bird.murdermistery.config.Config;
import ru.bird.murdermistery.listeners.AddWeaponListener;
import ru.bird.murdermistery.listeners.SpectatorBoard;
import ru.bird.murdermistery.listeners.StartListener;
import ru.wellfail.wapi.actionbar.ActionBarApi;
import ru.wellfail.wapi.game.GameWeaponSettings;
import ru.wellfail.wapi.logger.WLogger;
import ru.wellfail.wapi.utils.Title;

import java.util.*;

import org.bukkit.Bukkit;
import ru.wellfail.wapi.game.Gamer;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import ru.wellfail.wapi.board.Board;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.utils.Utils;
import java.util.concurrent.ConcurrentHashMap;

import ru.wellfail.wapi.listeners.DamageListener;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.Location;
import ru.wellfail.wapi.utils.FIterator;
import org.bukkit.entity.Player;
import ru.wellfail.wapi.game.Game;

public class GameFactory extends Game
{
    public static Map<Player, GPlayer> players;
    private static GameFactory instance;
    public static Boolean compas = false;
    public static Player compaspl;
    public static ItemStack compasitem;
    private FIterator<Location> spawns;
    private static GameBoard board;
    private ConfigurationSection shop;
    private static DamageListener dmg;
    private final String create = "ALTER TABLE stats ADD sgfb INT DEFAULT 0, ADD sgdeaths INT DEFAULT 0,ADD sgwins INT DEFAULT 0, ADD sggames INT DEFAULT 0, ADD sgkills INT DEFAULT 0;";
    private Player lastAlive;
    public static Role villager;
    public static Role commisar;
    public static Role killer;
    public static List<Player> villagers = new ArrayList<>();
    public static Boolean win_type;
    public static Player killer_pl;
    public static Player commisar_pl;
    public static Location bow;
    public static ArrayList<ArmorStand> bowlist = new ArrayList();

    private StartListener startListener;
    private AddWeaponListener weaponlistener;
    private Spawner spawner;
    private ru.wellfail.wapi.Main wapi;

    public GameFactory() {
        super("MM", new GameListener());
        GameFactory.players = new ConcurrentHashMap<Player, GPlayer>();
        this.loadConfig();
        this.loadRoles();
    }

    public static int getPlayers() {
        return GameFactory.players.size();
    }

    public static GameFactory getInstance() {
        return GameFactory.instance;
    }
    
    public static GPlayer getGplayer(final Player player) {
        return GameFactory.players.get(player);
    }

    private void loadConfig() {
        try {
            final FileConfiguration config = Main.getInstance().getConfig();
            this.shop = config.getConfigurationSection("Shop");
            GameSettings.respawnLocation = Utils.stringToLocation(config.getString("Lobby"));
            GameSettings.spectrLocation = Utils.stringToLocation(config.getString("Spectator"));
            Config.gold = Utils.stringListToLocations(config.getStringList("Items.Gold"));
            final Set<Location> set = config.getStringList("Spawns").stream().map(Utils::stringToLocation).collect(Collectors.toCollection(() -> new LinkedHashSet()));
            this.spawns = (FIterator<Location>)new FIterator((Collection)set);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        Board.spectator = new SpectatorBoard();
    }

    private void loadRoles() {
        List<ItemStack> comisar_start_item = new ArrayList<ItemStack>();
        ItemStack item = Utils.createItem(Material.BOW, "Лук детектива");
        item.addEnchantment(Enchantment.ARROW_INFINITE, 1);
        comisar_start_item.add(item);
        comisar_start_item.add(new ItemStack(Material.ARROW));
        commisar = new Role("Детектив", comisar_start_item);
        List<ItemStack> killer_start_item = new ArrayList<ItemStack>();
        ItemStack killer_sword = Utils.createItem(Material.STONE_SWORD, "Меч убийцы");
        killer_start_item.add(killer_sword);
        GameWeaponSettings.throwableMaterial.add(killer_sword);
        killer = new Role("Убийца", killer_start_item);
        List<ItemStack> villager_start_item = new ArrayList<ItemStack>();
        villager = new Role("Житель", villager_start_item);
    }

    public void onRespawn(final Player player) {}

    private void ever30secondAddScore() {
        Main.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
            public void run() {
                for(Player player : Utils.getAlivePlayers()) {
                    GPlayer gPlayer = getGplayer(player);
                    if(gPlayer.getRole() == villager || gPlayer.getRole() == commisar) {
                        gPlayer.addScore(250);
                        ActionBarApi.sendActionBar(player, "§e+150 очков (выжить 30 секунд)", 5);
                    }
                }
            }
        }, 600);// 60 L == 3 sec, 20 ticks == 1 sec
    }
    
    protected void onStartGame() {
        this.startListener = new StartListener();
        this.startListener.start();
        this.ever30secondAddScore();
        boolean oficer_swith = true;
        boolean killer_swith = true;
        GameFactory.compasitem = Utils.createItem(Material.COMPASS, "Компас убийцы");
        for (final Player p : Bukkit.getOnlinePlayers()) {
            Role role;
            if (oficer_swith) {
                Title title = new Title("§1Твоя роль Детектив", "§6ЦЕЛЬ: §eВычислить и убить скрытного убийцу!", 1, 3, 2);
                title.send(p);
                commisar_pl = p;
                role = commisar;
                villagers.add(p);
                oficer_swith = false;

            } else if (killer_swith) {
                Title title = new Title("§cТвоя роль Убийца", "§6ЦЕЛЬ: §eУбить всех мирных жителей и комисара!", 1, 3, 2);
                title.send(p);
                role = killer;
                killer_pl = p;
                killer_swith = false;
            } else {
                Title title = new Title("§eТвоя роль Житель", "§6ЦЕЛЬ: §eОстаться в живых до окончания таймера!", 1, 3, 2);
                title.send(p);
                villagers.add(p);
                role = villager;
            }
            GPlayer gPlayer = new GPlayer(p, role);
            GameFactory.players.put(p, gPlayer);
            WLogger.info(gPlayer.getRole().getDisplayname() + " set "+ p.getDisplayName());
            final Location loc = this.spawns.getNext();
            if (!loc.getChunk().isLoaded()) {
                loc.getChunk().load();
            }
            p.teleport(loc);
            p.sendMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
            p.sendMessage("");
            p.sendMessage("                                   §c§lMurder Mystery");
            p.sendMessage("");
            p.sendMessage("               §eБорьба идет между мирными жителями и убийцей.");
            p.sendMessage("               §eВ начале случайныи игрок - Комисар, другой Убийца.");
            p.sendMessage("               §eПосле смерти Комисара жители могут взять его лук и стать им.");
            p.sendMessage("");
            p.sendMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
            GameFactory.board = new GameBoard(p);
//            Bukkit.getOnlinePlayers().forEach(pl -> {
//                if(pl != p) {
//                    wapi.getTagManager().hide(pl);
//                }
//            });
        }
        this.weaponlistener = new AddWeaponListener();
        weaponlistener.start();
        new Spawner(Config.gold);
    }

    private static void removeBow() {
        for(Entity en : GameFactory.bowlist) {
            en.remove();
        }
    }

    protected void onEndGame() {
        GameFactory.compas = false;
        removeBow();
        this.startListener.stop();
        this.weaponlistener.stop();
        this.lastAlive = Utils.getLastAlive();
        if (this.lastAlive == null) {
            return;
        }
        if(killer_pl != null && commisar_pl != null) {
            Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("                         §c§lMurder Mystery");
            Bukkit.broadcastMessage("");
            if (win_type) {
                Bukkit.broadcastMessage("                         §6Комисар " + commisar_pl.getDisplayName());
                Bukkit.broadcastMessage("                         §mУбийца " + killer_pl.getDisplayName());
            } else {
                Bukkit.broadcastMessage("                         §mКомисар " + commisar_pl.getDisplayName());
                Bukkit.broadcastMessage("                         §6Убийца " + killer_pl.getDisplayName());
            }
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");

            final Collection<Gamer> gamers = Gamer.getGamers();
            for (final Gamer gamer : gamers) {
                if (GameFactory.players != null && gamer != null && gamer.getPlayer() != null) {
                    if (GameFactory.players.get(gamer.getPlayer()) == null) {
                        continue;
                    }
                    Player player = gamer.getPlayer();
                    Role role = GameFactory.getGplayer(player).getRole();
                    boolean good_role = role == commisar || role == villager;
                    if (good_role && GameFactory.win_type) {
                        new Title("§eYOU WIN!", "§6Убийца был остановлен!", 1, 3, 2).send(player);
                        gamer.addMoney(75);
                    }
                    if (good_role && !GameFactory.win_type) {
                        new Title("§eYOU LOST!", "§6Вы не смогли убить скрытого убийцу!", 1, 3, 2).send(player);
                    }
                    if (role == killer && !GameFactory.win_type) {
                        new Title("§eYOU WIN!", "§6Все мирные жители мертвы!", 1, 3, 2).send(player);
                        gamer.addMoney(75);
                    }
                    if (role == killer && GameFactory.win_type) {
                        new Title("§eYOU LOST!", "§6Вы не смогли убить мирных жителей!", 1, 3, 2).send(player);
                    }
                }
            }
        } else {
            Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("                         §c§lMurder Mystery");
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("                         §cОшибка");
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
            Bukkit.getServer().shutdown();
        }
    }

    public static void addCompas() {
        GameFactory.compas = true;
        int randomNum = ThreadLocalRandom.current().nextInt(0, villagers.size());
        compaspl = villagers.get(randomNum);
    }
}