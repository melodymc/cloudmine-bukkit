package ru.bird.totems.structures;

import de.leonhard.storage.Json;
import nl.shanelab.multiblock.*;
import nl.shanelab.multiblock.patternobjects.PatternBlock;
import ru.bird.storageUtils.utils.DeserializeUtils;
import nl.shanelab.multiblock.utils.MultiBlockPatternUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;
import ru.bird.totems.Main;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class TestTombFromFile implements IMultiBlock {

    @Override
    public void onActivate(Plugin plugin, Location coreBlockLocation, Player player, MultiBlockActivation activation) {
        System.out.println("test");
        MultiBlockActivationType activationType = activation.getType();
        MultiBlockPatternFacing facing = activation.getFacing();
        if (activationType == MultiBlockActivationType.CORE_PLACED) {
            List<Vector> vectorList = getMultiBlockPattern().getPatternObjects().stream()
                    .filter(patternObject -> patternObject instanceof PatternBlock)
                    .map(patternObject -> new Vector(patternObject.getX(), patternObject.getY(), patternObject.getZ()))
                    .collect(Collectors.toList());
                    MultiBlockPatternUtils.byCoreBlockLocationGetRealLocations(coreBlockLocation, vectorList, facing).stream()
                    .map(location -> Objects.requireNonNull(coreBlockLocation.getWorld()).getBlockAt(location))
                    .forEach(block -> {
                        block.setType(Material.BEDROCK);
                    });
            player.sendMessage(ChatColor.GREEN + "Найден блок из конфига, заменяю на бедрок. Сторона: " + facing);
        } else if (activationType == MultiBlockActivationType.CORE_INTERACTED) {
            player.sendMessage(ChatColor.YELLOW + "Interacted with example multiblock");
        }
    }

    @Nonnull
    @Override
    public MultiBlockPattern getMultiBlockPattern() {
        Main plugin = Main.getThisClass();
        Json config = plugin.loadPatternConfig("testPetPattern");
//        System.out.println("path " + config.getFile().getPath());
        Map<String, Object> map = config.getFileData().toMap();
        try {
            return DeserializeUtils.staticDeserialize(map);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }
}