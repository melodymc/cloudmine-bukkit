package ru.bird.main.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class BasicEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    public BasicEvent(boolean async) {
        super(async);
    }

    public BasicEvent() {
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
