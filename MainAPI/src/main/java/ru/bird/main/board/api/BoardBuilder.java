package ru.bird.main.board.api;

import java.util.List;

public class BoardBuilder {

    private Board board;

    BoardBuilder(Board board) {
        this.board = board;
    }

    public BoardBuilder write(int index, String text) {
        board.write(index, text);
        return this;
    }

    public BoardBuilder write(int index, int num) {
        board.write(index, num + "");
        return this;
    }

    public BoardBuilder updater(long interval, BoardUpdater updater) {
        board.addUpdater(interval, updater);
        return this;
    }

    public BoardBuilder updaterHeader(long interval, List<String> list) {
        board.addUpdaterHeader(interval, list);
        return this;
    }

    public Board build() {
        board.create();
        return board;
    }
}
