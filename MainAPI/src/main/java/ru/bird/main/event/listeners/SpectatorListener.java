/*
 * Decompiled with CFR 0_115.
 *
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.block.Action
 *  org.bukkit.event.block.BlockBreakEvent
 *  org.bukkit.event.block.BlockPlaceEvent
 *  org.bukkit.event.entity.EntityDamageByEntityEvent
 *  org.bukkit.event.entity.EntityDamageEvent
 *  org.bukkit.event.entity.FoodLevelChangeEvent
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.player.AsyncPlayerChatEvent
 *  org.bukkit.event.player.PlayerDropItemEvent
 *  org.bukkit.event.player.PlayerExpChangeEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.event.player.PlayerMoveEvent
 *  org.bukkit.event.player.PlayerPickupItemEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryHolder
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.inventory.meta.SkullMeta
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  org.bukkit.util.Vector
 */
package ru.bird.main.event.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import ru.bird.main.API;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.GameState;
import ru.bird.main.game.Gamer;
import ru.bird.main.groups.Groups;
import ru.bird.inventories.Item;
import ru.bird.inventories.ItemRunnable;
import ru.bird.main.utils.GameUtils;

import java.util.ArrayList;
import java.util.HashSet;

public class SpectatorListener
extends BasicListener {
    private Inventory inventory;
    private static Item item;

    public static Item getItem() {
        return item;
    }

    public SpectatorListener() {
        ItemStack compass = new ItemStack(Material.COMPASS);
        ItemMeta meta = compass.getItemMeta();
        meta.setDisplayName("§aТелепортер §7(ПКМ)");
        compass.setItemMeta(meta);
        item = new Item(compass, new ItemRunnable(){

            @Override
            public void onUse(PlayerInteractEvent e) {
                e.getPlayer().openInventory(inventory);
            }

            @Override
            public void onClick(InventoryClickEvent paramInventoryClickEvent) {
            }
        }, new Action[]{Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK});
        this.inventory = Bukkit.createInventory(null, 45, "§rТелепортер");
//        this.startInventoryUpdate();
    }

    private void startInventoryUpdate() {
        new Thread(() -> {
            while (GameState.current != GameState.GAME) {
                try {
                    Thread.sleep(5000);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            while (GameState.current == GameState.GAME) {
                try {
                    Thread.sleep(5000);
                    final HashSet<ItemStack> items = new HashSet<ItemStack>();
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        Gamer gamer = Gamer.getGamer(player);
                        if (gamer.isSpectator()) continue;
                        ItemStack item = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);
                        SkullMeta meta = (SkullMeta)item.getItemMeta();
                        meta.setDisplayName(Groups.getFullName(player));
                        ArrayList<String> lore = new ArrayList<String>();
                        lore.add("");
                        lore.add("§7Нажмите, чтобы телепортироваться");
                        meta.setLore(lore);
                        meta.setOwner(player.getName());
                        item.setItemMeta(meta);
                        items.add(item);
                    }
                    new BukkitRunnable(){
                        public void run() {
                            inventory.clear();
                            int i = 1;
                            for (ItemStack item : items) {
                                if (i == 8) {
                                    i = 10;
                                }
                                if (i == 17) {
                                    i = 19;
                                }
                                if (i == 26) break;
                                inventory.setItem(i, item);
                                ++i;
                            }
                        }
                    }.runTask(API.getInstance());
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if (event.getTo().getY() <= 0.0 && Gamer.getGamer(event.getPlayer()).isSpectator() && GameSettings.spectrLocation != null) {
            event.getPlayer().teleport(GameSettings.spectrLocation);
            event.getPlayer().setFallDistance(0.0f);
            event.getPlayer().setVelocity(new Vector(0, 0, 0));
            event.getPlayer().setAllowFlight(true);
            event.getPlayer().setFlying(true);
        }
    }

    @EventHandler
    public void onExpChange(PlayerExpChangeEvent event) {
        if (Gamer.getGamer(event.getPlayer()).isSpectator()) {
            event.setAmount(0);
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if (e.isCancelled()) {
            return;
        }
        Gamer gamer = Gamer.getGamer(e.getPlayer());
        if (gamer != null && gamer.isSpectator()) {
            e.getRecipients().clear();
            for (Player player : Bukkit.getOnlinePlayers()) {
                Gamer g = Gamer.getGamer(player);
                if (!g.isSpectator()) continue;
                e.getRecipients().add(player);
            }
            Player player = e.getPlayer();
            String prefix = Groups.getPrefix(player) + " ".replace("&", "§");
            String suffix = Groups.getSuffix(player);
            if(suffix == null) {
                suffix = "";
            }
            e.setFormat("§8[§fНаблюдателям§8] §7" + prefix + e.getPlayer().getName() + suffix.replace("&", "§") + ": " + e.getMessage());
            return;
        }
    }

    @EventHandler
    public void onGuiClick(InventoryClickEvent e) {
        if (!e.getInventory().equals(this.inventory)) { // TODO проверить работу в одной из мини-игр
            return;
        }
        ItemStack item = e.getCurrentItem();
        if (item == null || item.getType() != Material.PLAYER_HEAD) {
            return;
        }
        SkullMeta meta = (SkullMeta)item.getItemMeta();
        Player player = Bukkit.getPlayer(meta.getOwner());
        if (player == null) {
            return;
        }
        e.getWhoClicked().teleport(player);
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onDamageByEntity(EntityDamageByEntityEvent e) {
        if (GameUtils.isSpectator(e.getDamager())) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onDamage(EntityDamageEvent e) {
        if (GameUtils.isSpectator(e.getEntity())) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onBreak(BlockBreakEvent e) {
        if (GameUtils.isSpectator(e.getPlayer())) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onPlace(BlockPlaceEvent e) {
        if (GameUtils.isSpectator(e.getPlayer())) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onDrop(PlayerDropItemEvent e) {
        if (GameUtils.isSpectator(e.getPlayer())) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onPickup(PlayerPickupItemEvent e) {
        if (GameUtils.isSpectator(e.getPlayer())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent e) {
        Player player = (Player)e.getEntity();
        if (GameUtils.isSpectator(player)) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.HIGHEST)
    public void onInventoryClick(InventoryClickEvent e) {
        if (GameUtils.isSpectator(e.getWhoClicked())) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.HIGHEST)
    public void onInteract(PlayerInteractEvent e) {
        if (GameUtils.isSpectator(e.getPlayer())) {
            e.setCancelled(true);
        }
    }

}

