package ru.bird.bedwars.sidebar;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.bedwars.game.GameFactory;
import ru.bird.bedwars.game.GamePlayer;
import ru.bird.bedwars.team.Team;
import ru.bird.main.board.api.PersonalBoard;
import ru.bird.main.game.Gamer;

import java.util.*;

public class GameSidebar extends PersonalBoard {

    public GameSidebar(Player player){
        super("В§eВ§lBEDWARS", player);
        GameFactory.getGamePlayer(player).setGameSidebar(this);
        ArrayList<String> list = new ArrayList<>();
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§6§lB§e§lEDWARS");
        list.add("§f§lB§6§lE§e§lDWARS");
        list.add("§f§lBE§6§lD§e§lWARS");
        list.add("§f§lBED§6§lW§e§lARS");
        list.add("§f§lBEDW§6§lA§e§lRS");
        list.add("§f§lBEDWA§6§lR§e§lS");
        list.add("§e§lBEDWAR§6§lS");
        list.add("§f§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§f§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§f§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        GamePlayer gPlayer = GameFactory.getGamePlayer(player);
        Team team = gPlayer.getTeam();
        Gamer gamer = Gamer.getGamer(player);
        write(11, ChatColor.DARK_GREEN + "");
        write(10, " §e›§fТвоя команда§8: " + team.getColor() + team.getOffName());
        write(9, " §fТвоих смертей§8: §b§l" + gamer.getDeaths());
        write(8, " §fТвоих убийств§8: §b§l" + gamer.getKills());
        write(7, " §fКомандных убийств§8: §b§l" + team.getKills());
        write(6, ChatColor.GREEN + "");
        write(5, getTeamText(0));
        write(4, getTeamText(1));
        write(3, getTeamText(2));
        write(2, getTeamText(3));
        write(1, ChatColor.BLACK + "");
        addUpdaterHeader(3, list);
        addUpdater(20, board -> {
            board.modifyLine(9, " §fТвоих смертей§8: §b§l" + gamer.getDeaths());
            board.modifyLine(8, " §fТвоих убийств§8: §b§l" + gamer.getKills());
            board.modifyLine(7, " §fКомандных убийств§8: §b§l" + team.getKills());
            board.modifyLine(5, getTeamText(0));
            board.modifyLine(4, getTeamText(1));
            board.modifyLine(3, getTeamText(2));
            board.modifyLine(2, getTeamText(3));
        });
        create();
    }

    private String getTeamText(int i) {
        List<Team> teams = Team.teams;
        return teams.get(i).getColor() + teams.get(i).getName() + ": §6§l" + teams.get(i).getPlayers().size() + " " + Team.canSpawn(teams.get(i));
    }

    public void setNewData() {
        new BukkitRunnable(){
            @Override
            public void run() {
                Gamer gamer = Gamer.getGamer(getOwner());
                GamePlayer gamePlayer = GameFactory.getGamePlayer(getOwner());
                Team team = gamePlayer.getTeam();
            }
        }.run();
    }

    public void hide() {
        remove();
    }
}