package ru.bird.pets.entity.armorstand.humanoid;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import ru.bird.main.particles.ParticleEffect;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;

public class PetHerobrine extends AbstractArmoredPet {

	public PetHerobrine(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);

		armor.setArms(true);
		armor.setVisible(true);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}



	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getMainHand() {
		return new ItemStack(Material.IRON_PICKAXE);
	}


	@Override
	public ItemStack getOffHand() {
		return null;
	}

	
	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		LeatherArmorMeta lim = (LeatherArmorMeta) leggings.getItemMeta();
		lim.setColor(Color.BLACK);
		leggings.setItemMeta(lim);
		return leggings;
	}

	@Override
	public ItemStack getChestplate() {
		ItemStack chestPlate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		LeatherArmorMeta cim = (LeatherArmorMeta) chestPlate.getItemMeta();
		cim.setColor(Color.BLACK);
		chestPlate.setItemMeta(cim);
		return chestPlate;
	}

	@Override
	public ItemStack getHelmet() {
		ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta meta = (SkullMeta) head.getItemMeta();
		meta.setOwner("Herobrine");
		head.setItemMeta(meta);
		return head;
	}

	@Override
	public void onLivingUpdate() {
		setLeftLegPose(20 + 20 * MathUtils.cos(), 0, 0);
		setRightLegPose(20 + 10 * MathUtils.sin(), 0, 0);
		setLeftArmPose(20 * MathUtils.cos(), 0, -10 + 10 * MathUtils.sin());
		setRightArmPose(20 * MathUtils.cos(), 0, 10 + 10 * MathUtils.cos());
		this.sendWatcherUpdate();
		ParticleEffect.SMOKE_NORMAL.sendToPlayers(Bukkit.getOnlinePlayers(), this.getPetLocation().clone().add(0, 0.5F, 0), rand.nextFloat() - 0.5F, rand.nextFloat() - 0.5F, rand.nextFloat() - 0.5F, 1 / 100, 1);
	}

}
