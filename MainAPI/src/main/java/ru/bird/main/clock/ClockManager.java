package ru.bird.main.clock;

import java.util.List;

import com.google.common.collect.Lists;

public class ClockManager implements Runnable {

	private static ClockManager	clockManager;
	private List<AbstractTask>	tasks	= Lists.newArrayList();

	private int countdown = 0;

	public ClockManager() {
		clockManager = this;
		addTask(new TaskSinCosUpdater());
	}

	public void addTask(AbstractTask task) {
		if (task.getModule() < 1) {
			tasks.add(task);
		} else {
			System.err.println(("Warning, invalid task : " + task.getClass().getSimpleName()));
		}
	}

	@Override
	public void run() {
		++countdown;
		countdown = countdown % (Integer.MAX_VALUE - 1);
		for (AbstractTask task : tasks) {
			if ((1 + countdown) % task.getModule() == 0) {
				task.call();
			}
		}
	}
	
	public static ClockManager getClockManager(){
		return clockManager;
	}

}