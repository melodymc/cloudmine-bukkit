package ru.bird.gameportal;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.bird.chat.AbstractCommand;
import ru.bird.main.utils.GameUtils;

public class CurrentServerCommand extends AbstractCommand {

    private final String current;

    public CurrentServerCommand(String current) {
        super("currentserver");
        this.current = current;
        register();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        Player player = (Player) sender;
        GameUtils.redirectToGroup(player, current);
        return true;
    }
}