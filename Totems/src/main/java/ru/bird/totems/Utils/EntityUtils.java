package ru.bird.totems.Utils;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.util.List;

public class EntityUtils {

    public static Entity getEntity(EntityType entityType, Location location) {
        List<Entity> entities = (List<Entity>) location.getWorld().getNearbyEntities(location, 1, 1, 1);

        Entity entity = null;
        for (Entity e : entities) {
            if (entity == null) {
                if (e.getType() == entityType) {
                    entity = e;
                }
            }
        }

        return entity;
    }

}
