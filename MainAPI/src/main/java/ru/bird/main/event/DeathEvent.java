package ru.bird.main.event;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class DeathEvent extends BasicEvent {
    private Player player;
    private String killer;
    private String pl;
    private Player kill;
    private boolean message = true;
    private boolean deny = false;
    private Location location;

    public boolean isMessage() {
        return this.message;
    }

    public void setMessage(boolean message) {
        this.message = message;
    }

    public Player getKill() {
        return this.kill;
    }

    public void setKill(Player kill) {
        this.kill = kill;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setDeny(boolean deny) {
        this.deny = deny;
    }

    public void setLocation(Location loc) {
        this.location = loc;
    }

    public boolean isDeny() {
        return this.deny;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setPl(String pl) {
        this.pl = pl;
    }

    public void setkiller(String killer) {
        this.killer = killer;
    }

    public String getKiller() {
        return this.killer;
    }

    public String getPl() {
        return this.pl;
    }

    public DeathEvent(Player player) {
        this.player = player;
    }
}