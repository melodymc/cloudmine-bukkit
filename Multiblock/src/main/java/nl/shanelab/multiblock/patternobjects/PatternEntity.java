package nl.shanelab.multiblock.patternobjects;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.util.Vector;

import nl.shanelab.multiblock.PatternObject;

/**
 * Entity pattern for the multiblock structure
 * 
 * @author ShaneCraft
 *
 */
public class PatternEntity extends PatternObject {

	private EntityType entityType;
	public PatternEntity() {}

	public PatternEntity(@Nonnull EntityType entityType, int x, int y, int z) {
		this(entityType, new Vector(x, y, z));
	}
	
	public PatternEntity(@Nonnull EntityType entityType, Vector relativeVec) {
		super(relativeVec);
		
		if (entityType == EntityType.DROPPED_ITEM && !(this instanceof PatternItem)) {
			throw new IllegalArgumentException("Use PatternItem to use items in your multiblock pattern");
		}
		
		this.entityType = entityType;
	}

	@Override
	protected PatternObject createRotatedClone(Vector vector) {
		return new PatternEntity(entityType, vector);
	}

	@Override
	public boolean isValid(Location location) {
		return getEntity(location) != null;
	}
	
	protected Entity getEntity(Location location) {
		if (location == null) {
			return null;
		}
		
		List<Entity> entities = (List<Entity>) location.getWorld().getNearbyEntities(location, 1, 1, 1);
		
		Entity entity = null;
		for (Entity e : entities) {
			if (entity == null) {
				if (e.getType() == entityType) {
					entity = e;
				}
			}
		}
		
		return entity;
	}
	
	public EntityType getEntityType() {
		return entityType;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> list = super.serialize();
		list.put("entityType", entityType.toString());
		return list;
	}

	@Override
	public <T> T deserialize(Map<String, Object> args) throws IllegalArgumentException, NullPointerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		EntityType entityType = EntityType.valueOf((String) args.get("entityType"));
		return (T) new PatternEntity(entityType, (int) args.get("relativeX"), (int) args.get("relativeY"), (int)  args.get("relativeZ"));
	}
}
