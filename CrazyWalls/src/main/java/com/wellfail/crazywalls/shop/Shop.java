/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.HandlerList
 *  org.bukkit.event.Listener
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  ru.wellfail.wapi.Main
 *  ru.wellfail.wapi.gui.Gui
 *  ru.wellfail.wapi.gui.GuiAction
 */
package com.wellfail.crazywalls.shop;

import com.wellfail.crazywalls.Main;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.inventories.menu.Menu;

public class Shop
extends Menu {
    public Shop() {
        super("Магазин", 6);
//        HandlerList.unregisterAll(this);
    }

    public void open(Player player) {
        Item.items.forEach(item -> {
            item.updateItem(player);
            getBukkitInventory().setItem(item.getSlot(), item.getItem());
            this.addAction(item.getSlot(), e -> {
                e.setCancelled(true);
                this.buy((Player)e.getWhoClicked(), item);
            });
        });
        showTo(player);
//        player.openInventory(getBukkitInventory());
    }

    public void buy(Player player, Item item) {
        item.updateCost(player);
        if (!item.isCanBuy()) {
            player.sendMessage("У вас недостаточно ресурсов для покупки.");
            return;
        }
        item.getCosts().forEach(cost -> {
            Material type = cost.getCostItem().getType();
            int current = cost.getAmount();
            for (ItemStack it : player.getInventory().getContents()) {
                if (it == null || it.getType() != type) continue;
                if (it.getAmount() > current) {
                    it.setAmount(it.getAmount() - current);
                    break;
                }
                if (it.getAmount() == current) {
                    player.getInventory().remove(it);
                    it.setType(Material.AIR);
                    break;
                }
                if (it.getAmount() >= current) continue;
                current -= it.getAmount();
                it.setType(Material.AIR);
            }
        }
        );
        player.getInventory().addItem(new ItemStack(item.getItem().getType(), item.getItem().getAmount(), item.getItem().getDurability()));
        this.reOpen(player);
    }

    private void reOpen(final Player player) {
        player.closeInventory();
        new BukkitRunnable(){

            public void run() {
                Shop.this.open(player);
            }
        }.runTaskLater(Main.getInstance(), 1);
    }

}

