// 
// Decompiled by Procyon v0.5.30
// 

package com.wellfail.crazywalls.game;

import ru.wellfail.wapi.game.Gamer;
import org.bukkit.entity.Player;

public class GamePlayer
{
    private int kills;
    private Player player;
    private int fb;
    
    public GamePlayer(final Player player) {
        this.player = player;
        this.kills = 0;
        this.fb = 0;
    }
    
    public void setKills() {
        ++this.kills;
    }
    
    public void setFb() {
        ++this.fb;
    }
    
    public int getFb() {
        return Gamer.getGamer(this.player).getFb();
    }
    
    public int getKills() {
        return this.kills;
    }
    
    public Player getPlayer() {
        return this.player;
    }
}
