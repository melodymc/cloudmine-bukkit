package ru.bird.jobs.jobgame.event;

import ru.bird.jobs.jobgame.JobGame;

public class StartJobEvent extends JobEventUnified
{
    public StartJobEvent(JobGame instance)
    {
        super(instance);
    }
}
