/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.milkbowl.vault.economy.Economy
 *  net.milkbowl.vault.economy.EconomyResponse
 *  org.bukkit.Bukkit
 *  org.bukkit.Material
 *  org.bukkit.configuration.Configuration
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.configuration.file.FileConfigurationOptions
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.entity.Player
 *  org.bukkit.event.block.Action
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryHolder
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  ru.wellfail.wapi.Main
 *  ru.wellfail.wapi.game.Game
 *  ru.wellfail.wapi.game.GameSettings
 *  ru.wellfail.wapi.game.Gamer
 *  ru.wellfail.wapi.gui.Item
 *  ru.wellfail.wapi.gui.ItemRunnable
 *  ru.wellfail.wapi.shop.Kit
 *  ru.wellfail.wapi.shop.ShopManager
 */
package com.wellfail.crazywalls.kits;

import com.wellfail.crazywalls.Main;
import com.wellfail.crazywalls.game.GameFactory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.bird.main.API;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.Gamer;
import ru.bird.inventories.Item;
import ru.bird.inventories.ItemRunnable;
import ru.bird.inventories.Kit;
import ru.bird.inventories.ShopManager;
import ru.bird.main.logger.BaseLogger;

import java.io.*;

public class KitLoader {
    private FileConfiguration config;
    private File locationsFile = null;

    public KitLoader() {
        this.init();
        this.loadShop(this.config);
        this.load();
    }

    public void load() {
        ItemStack shop = new ItemStack(Material.CHEST);
        ItemMeta meta = shop.getItemMeta();
        meta.setDisplayName("§aМагазин наборов §7(ПКМ)");
        shop.setItemMeta(meta);
        GameFactory.getInstance().shopItem = new Item(shop, new ItemRunnable(){

            public void onUse(PlayerInteractEvent e) {
                Gamer gamer = Gamer.getGamer((Player)e.getPlayer());
                if (gamer.getPurchase() != null && !gamer.isCancelbuy()) {
                    e.getPlayer().sendMessage(GameSettings.prefix + "Нажмите еще раз, чтобы аннулировать покупку набора " + gamer.getPurchase().getName());
                    e.setCancelled(true);
                    e.getPlayer().closeInventory();
                    gamer.setCancelbuy(true);
                    return;
                }
                if (gamer.getPurchase() != null) {
                    e.getPlayer().sendMessage(GameSettings.prefix + "Вам были возвращены деньги за покупку набора " + gamer.getPurchase().getName());
                    API.getInstance().getVaultManager().getEconomy().depositPlayer(e.getPlayer().getName(), (double)gamer.getPurchase().getPrice());
                    gamer.setCancelbuy(false);
                    gamer.setPurchase(null);
                    e.setCancelled(true);
                    e.getPlayer().closeInventory();
                    return;
                }
                Inventory inv = Bukkit.createInventory((InventoryHolder)null, (int)45, (String)"§rМагазин наборов");
                for (int i = 0; i < 45; ++i) {
                    Kit kit = ShopManager.instance.getKit(i);
                    if (kit == null) continue;
                    inv.setItem(i, kit.getItem());
                }
                e.getPlayer().openInventory(inv);
            }

            public void onClick(InventoryClickEvent paramInventoryClickEvent) {
            }
        }, new Action[]{Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK});
    }

    private void loadShop(FileConfiguration config) {
        for (String s : config.getConfigurationSection("Shop").getKeys(false)) {
            ShopManager.instance.addKit(new Kit(config.getString("Shop." + s + ".kit"), config.getStringList("Shop." + s + ".lore"), config.getInt("Shop." + s + ".perm")));
        }
    }

    private void init() {
        this.locationsFile = new File(Main.getInstance().getDataFolder(), "kits.yml");
        this.config = YamlConfiguration.loadConfiguration(this.locationsFile);
        InputStream defConfigStream1 = Main.getInstance().getResource("kits.yml");
        if (defConfigStream1 != null) {
            YamlConfiguration defConfig1 = null;
            try
            {
                defConfig1 = YamlConfiguration.loadConfiguration(new BufferedReader(new InputStreamReader(defConfigStream1, "UTF-8")));
                this.config.setDefaults(defConfig1);

            } catch (UnsupportedEncodingException e)
            {
                BaseLogger.error("[Main] Не загружен kits.yml");
                e.printStackTrace();
            }
        }

        this.config.options().copyDefaults(true);
        this.saveLocationsConfig();
    }

    public void saveLocationsConfig() {
        if (this.locationsFile == null || this.config == null) {
            return;
        }
        try {
            this.config.save(this.locationsFile);
        }
        catch (IOException ex) {
            System.out.println("Ошибка при чтении файла конфигурации.");
        }
    }

}

