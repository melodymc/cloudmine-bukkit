package ru.bird.pets.entity.normal;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.CaveSpider;
import org.bukkit.entity.EntityType;

import net.minecraft.server.v1_16_R2.DataWatcher;
import ru.bird.pets.entity.AbstractStandardPet;

public class PetCaveSpider extends AbstractStandardPet {

	public PetCaveSpider(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		CaveSpider entity = (CaveSpider) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		DataWatcher data = ((CraftEntity) entity).getHandle().getDataWatcher();
		entity.remove();
		return data;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.CAVE_SPIDER;
	}

	@Override
	protected float getOffsetY() {
		return 0F;
	}

}
