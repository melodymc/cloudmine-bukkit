package ru.bird.jobs;

import de.leonhard.storage.Json;
import de.leonhard.storage.internal.FileData;
import de.leonhard.storage.internal.settings.ReloadSettings;
import org.bukkit.entity.Player;
import ru.bird.jobs.utils.StorageUtils;
import ru.bird.storageUtils.utils.DeserializeUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class StorageJob {
    public static Map<UUID, JobsUser> jobsusers = new HashMap<>();

    public static void insertAll() {
        if(!jobsusers.isEmpty()) {
            for (JobsUser u : jobsusers.values()) {
                insert(u);
            }
        }
    }

    public static void insert(JobsUser u) {
        Json json = u.json;
        System.out.println("json path " + json.getFilePath());
        json.addDefaultsFromMap(u.serialize());
        System.out.println("map count " + json.getData().values().stream().count());
    }

    public static JobsUser load(UUID uuid) {
        Json json = StorageUtils.jobsUserJson(uuid.toString());
        FileData fileData = json.getFileData();
        JobsUser u = new JobsUser(uuid);
        u.setJson(json);
        try {
            try {
                u = DeserializeUtils.staticDeserialize(fileData.toMap());
                JobsPlugin.checkCanPlotUpdate(u);
                JobsPlugin.checkCanJobChange(u);
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
                return null;
            }
        } catch (NullPointerException e) {
            u.setJob(null);
            u.setTime(0);
            u.setJobTime(0);
            u.setCanUpdatePlot(true);
            u.setCanChangeJob(true);
            insert(u);
        }
        jobsusers.put(u.getUUID(), u);
        return u;
    }

    public static JobsUser get(Player player) {
        return get(player.getUniqueId());
    }

    public static JobsUser get(UUID uuid){
        return jobsusers.get(uuid);
    }

    public static void removeJobsUser(UUID uuid) {
        JobsUser l = jobsusers.get(uuid);
        jobsusers.remove(uuid, l);
    }

}