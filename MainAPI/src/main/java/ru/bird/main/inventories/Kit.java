package ru.bird.main.inventories;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.bird.inventories.Item;
import ru.bird.inventories.ItemRunnable;
import ru.bird.main.game.GameSettings;
import ru.bird.main.groups.Groups;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.utils.GameUtils;

import java.util.*;

public class Kit {
    private String kit;
    private String name;
    private List<String> lore;
    private int price;
    private int id;
    private ItemStack item;
    private Set<ItemStack> items;
    private int perm;

    public String getName() {
        return this.name;
    }

    public int getPrice() {
        return this.price;
    }

    public int getId() {
        return this.id;
    }

    public ItemStack getItem() {
        return this.item;
    }

    public Set<ItemStack> getItems() {
        return this.items;
    }

    public int getPerm() {
        return this.perm;
    }

    public Kit(String kit, List<String> lore, int perm) {
        this.lore = lore;
        this.kit = kit;
        this.perm = perm;
        this.items = new HashSet<>();
        this.stringToItems(kit);
        this.setLoreItem();
    }

    private void stringToItems(String kit) {
        ArrayList<String> items = new ArrayList<String>(Arrays.asList(kit.split(";")));
        if (items.size() < 5) {
            BaseLogger.error("Size error");
            return;
        }
        this.name = items.get(0);
        items.remove(0);
        this.id = Integer.valueOf(items.get(0));
        items.remove(0);
        this.item = new ItemStack(Material.getMaterial(items.get(0)), 1);
        items.remove(0);
        this.price = Integer.valueOf(items.get(0));
        items.remove(0);
        StringBuffer line = new StringBuffer();
        items.forEach(item -> {
            line.append(item + ";");
        });
        for (ItemStack item2 : GameUtils.stringToItems(Bukkit.createInventory(null, 45), line.toString(), false, true)) {
            this.items.add(item2);
        }
    }

    private void setLoreItem() {
        ItemMeta meta = this.item.getItemMeta();
        meta.setLore(this.lore);
        meta.setDisplayName(this.name);
        this.item.setItemMeta(meta);
        new Item(this.item, new ItemRunnable(){

            @Override
            public void onUse(PlayerInteractEvent e) {
            }

            @Override
            public void onClick(InventoryClickEvent e) {
                if (e.getCurrentItem() == null) {
                    return;
                }
                Player player = (Player)e.getWhoClicked();
                Kit kit = ShopManager.instance.getKit(e.getRawSlot());
                if (Groups.getLevel(player) < kit.getPerm()) {
                    player.sendMessage(GameSettings.prefix + "Недостаточно прав для покупки данного набора");
                    e.setCancelled(true);
                    player.closeInventory();
                    return;
                }
                ShopManager.instance.buy(player, e.getRawSlot());
                e.setCancelled(true);
                player.closeInventory();
            }
        }, new Action[]{Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK});
    }

}