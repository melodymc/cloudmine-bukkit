package ru.bird.inventories;

import org.bukkit.event.block.Action;
import org.bukkit.inventory.ItemStack;

public class Item {
    private ItemStack item;
    private ItemRunnable runnable;
    private Action[] actions;
    private boolean dynamicData;
    private boolean dynamicName;

    public ItemRunnable getRunnable() {
        return this.runnable;
    }

    public ItemStack getItem() {
        return this.item;
    }

    public Action[] getActions() {
        return this.actions;
    }

    public boolean getDynamicData() {
        return this.dynamicData;
    }

    public boolean getDynamicName() {
        return this.dynamicName;
    }

    public Item(ItemStack item, ItemRunnable run, Action[] actions) {
        this.item = item;
        this.runnable = run;
        this.actions = actions;
    }

    public Item(ItemStack item, ItemRunnable run, Action[] actions, boolean dynamicData) {
        this.item = item;
        this.runnable = run;
        this.actions = actions;
        this.dynamicData = dynamicData;
    }

    public Item(ItemStack item, ItemRunnable run, Action[] actions, boolean dynamicData, boolean dynamicName) {
        this.item = item;
        this.runnable = run;
        this.actions = actions;
        this.dynamicData = dynamicData;
        this.dynamicName = dynamicName;
    }
}

