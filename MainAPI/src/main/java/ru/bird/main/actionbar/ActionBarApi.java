package ru.bird.main.actionbar;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.main.API;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class ActionBarApi implements Listener {
    public static JavaPlugin plugin;
    public static boolean works = true;
    public static String nmsver;
    private static Map<Player, List<BukkitRunnable>> tasks;
    private static boolean useOldMethods = false;

    public static Map<Player, List<BukkitRunnable>> getTasks() {
        return tasks;
    }

    public void load() {
        plugin = API.getInstance();
        Server server = plugin.getServer();
        tasks = new HashMap<Player, List<BukkitRunnable>>();
        nmsver = server.getClass().getPackage().getName();
        nmsver = nmsver.substring(nmsver.lastIndexOf(".") + 1);

        if (nmsver.equalsIgnoreCase("v1_8_R1") || nmsver.startsWith("v1_7_")) { // Not sure if 1_7 works for the protocol hack?
            useOldMethods = true;
        }
    }

    public static void sendActionBar(Player player, String message, boolean sendToChat) {
        if (!player.isOnline()) {
            return; // Player may have logged out
        }
        ActionBarEvent actionBarEvent = new ActionBarEvent(player, message);
        Bukkit.getPluginManager().callEvent(actionBarEvent);
        if (actionBarEvent.isCancelled())
            return;

        if (nmsver.startsWith("v1_12_")) {
            sendActionBarPost112(player, message);
        } else {
            sendActionBarPre112(player, message);
        }
        if(sendToChat) {
            player.sendMessage(message);
        }
    }

    private static void sendActionBarPost112(Player player, String message) {
        if (!player.isOnline()) {
            return; // Player may have logged out
        }

        try {
            Class<?> craftPlayerClass = Class.forName("org.bukkit.craftbukkit." + nmsver + ".entity.CraftPlayer");
            Object craftPlayer = craftPlayerClass.cast(player);
            Object ppoc;
            Class<?> c4 = Class.forName("net.minecraft.server." + nmsver + ".PacketPlayOutChat");
            Class<?> c5 = Class.forName("net.minecraft.server." + nmsver + ".Packet");
            Class<?> c2 = Class.forName("net.minecraft.server." + nmsver + ".ChatComponentText");
            Class<?> c3 = Class.forName("net.minecraft.server." + nmsver + ".IChatBaseComponent");
            Class<?> chatMessageTypeClass = Class.forName("net.minecraft.server." + nmsver + ".ChatMessageType");
            Object[] chatMessageTypes = chatMessageTypeClass.getEnumConstants();
            Object chatMessageType = null;
            for (Object obj : chatMessageTypes) {
                if (obj.toString().equals("GAME_INFO")) {
                    chatMessageType = obj;
                }
            }
            Object o = c2.getConstructor(new Class<?>[]{String.class}).newInstance(message);
            ppoc = c4.getConstructor(new Class<?>[]{c3, chatMessageTypeClass}).newInstance(o, chatMessageType);
            Method m1 = craftPlayerClass.getDeclaredMethod("getHandle");
            Object h = m1.invoke(craftPlayer);
            Field f1 = h.getClass().getDeclaredField("playerConnection");
            Object pc = f1.get(h);
            Method m5 = pc.getClass().getDeclaredMethod("sendPacket", c5);
            m5.invoke(pc, ppoc);
        } catch (Exception ex) {
            ex.printStackTrace();
            works = false;
        }
    }

    private static void sendActionBarPre112(Player player, String message) {
        if (!player.isOnline()) {
            return; // Player may have logged out
        }

        try {
            Class<?> craftPlayerClass = Class.forName("org.bukkit.craftbukkit." + nmsver + ".entity.CraftPlayer");
            Object craftPlayer = craftPlayerClass.cast(player);
            Object ppoc;
            Class<?> c4 = Class.forName("net.minecraft.server." + nmsver + ".PacketPlayOutChat");
            Class<?> c5 = Class.forName("net.minecraft.server." + nmsver + ".Packet");
            if (useOldMethods) {
                Class<?> c2 = Class.forName("net.minecraft.server." + nmsver + ".ChatSerializer");
                Class<?> c3 = Class.forName("net.minecraft.server." + nmsver + ".IChatBaseComponent");
                Method m3 = c2.getDeclaredMethod("a", String.class);
                Object cbc = c3.cast(m3.invoke(c2, "{\"text\": \"" + message + "\"}"));
                ppoc = c4.getConstructor(new Class<?>[]{c3, byte.class}).newInstance(cbc, (byte) 2);
            } else {
                Class<?> c2 = Class.forName("net.minecraft.server." + nmsver + ".ChatComponentText");
                Class<?> c3 = Class.forName("net.minecraft.server." + nmsver + ".IChatBaseComponent");
                Object o = c2.getConstructor(new Class<?>[]{String.class}).newInstance(message);
                ppoc = c4.getConstructor(new Class<?>[]{c3, byte.class}).newInstance(o, (byte) 2);
            }
            Method m1 = craftPlayerClass.getDeclaredMethod("getHandle");
            Object h = m1.invoke(craftPlayer);
            Field f1 = h.getClass().getDeclaredField("playerConnection");
            Object pc = f1.get(h);
            Method m5 = pc.getClass().getDeclaredMethod("sendPacket", c5);
            m5.invoke(pc, ppoc);
        } catch (Exception ex) {
            ex.printStackTrace();
            works = false;
        }
    }

    public static void sendActionBar(final Player player, final String message, int duration, boolean toChat) {
        if(tasks.containsKey(player)) {
            tasks.get(player).forEach(BukkitRunnable::cancel);
        }
        sendActionBar(player, message, toChat);

//        if (duration >= 0) {
//            // Sends empty message at the end of the duration. Allows messages shorter than 3 seconds, ensures precision.
//            new BukkitRunnable() {
//                @Override
//                public void run() {
//                    sendActionBar(player, "", toChat);
//                }
//            }.runTaskLater(plugin, duration + 1);
//        }

        // Re-sends the messages every 3 seconds so it doesn't go away from the player's screen.
        while (duration > 40) {
            duration -= 40;
            BukkitRunnable task = new BukkitRunnable() {
                @Override
                public void run() {
                    sendActionBar(player, message, toChat);
                }
            };
            List<BukkitRunnable> list = tasks.get(player);
            if(list == null) {
                list = new ArrayList<BukkitRunnable>();
            }
            list.add(task);
            tasks.put(player, list);
            task.runTaskLater(plugin, (long) duration);
        }
    }

    public static void sendActionBarToAllPlayers(String message, boolean toChat) {
        sendActionBarToAllPlayers(message, -1, toChat);
    }

    public static void sendActionBarToAllPlayers(String message, int duration, boolean toChat) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            sendActionBar(p, message, duration, toChat);
        }
    }

    public static void addTask(Player player, BukkitRunnable task) {
        tasks.get(player).forEach(runnable ->{
            if(runnable != task) {
                runnable.cancel();
            }
        });
        List<BukkitRunnable> list = new ArrayList<>();
        list.add(task);
        tasks.put(player, list);
    }
}