/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package com.wellfail.crazywalls.shop;

import com.wellfail.crazywalls.shop.Cost;
import com.wellfail.crazywalls.utils.CWUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Item {
    public static Set<Item> items = new HashSet<Item>();
    private List<Cost> costs;
    private ItemStack item;
    private int slot;
    private boolean buy = true;

    public List<Cost> getCosts() {
        return this.costs;
    }

    public int getSlot() {
        return this.slot;
    }

    public ItemStack getItem() {
        return this.item;
    }

    public boolean isCanBuy() {
        return this.buy;
    }

    public void updateItem(Player player) {
        this.buy = true;
        ArrayList<String> newLore = new ArrayList<String>();
        ItemMeta meta = this.item.getItemMeta();
        newLore.add("\u00a77\u0421\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c:");
        this.costs.stream().forEach(cost -> {
            newLore.add(this.getColor(this.checkCost(cost, player)) + cost.getName() + ": " + cost.getAmount() + " (" + CWUtils.getAmountItem(cost.getCostItem(), player) + ")");
        }
        );
        newLore.add(" ");
        if (this.buy) {
            newLore.add("\u00a7a\u041d\u0430\u0436\u043c\u0438\u0442\u0435, \u0447\u0442\u043e\u0431\u044b \u043e\u0431\u043c\u0435\u043d\u044f\u0442\u044c\u0441\u044f!");
        } else {
            newLore.add("\u00a7c\u041d\u0435\u0434\u043e\u0441\u0442\u0430\u0442\u043e\u0447\u043d\u043e \u0440\u0435\u0441\u0443\u0440\u0441\u043e\u0432!");
        }
        meta.setLore(newLore);
        this.item.setItemMeta(meta);
    }

    private String getColor(boolean cost) {
        String color = cost ? "\u00a7a" : "\u00a7c";
        return color;
    }

    public boolean checkCost(Cost cost, Player player) {
        if (CWUtils.getAmountItem(cost.getCostItem(), player) < cost.getAmount()) {
            this.buy = false;
            return false;
        }
        return true;
    }

    public void updateCost(Player player) {
        this.buy = true;
        this.updateItem(player);
    }

    public Item(List<Cost> costs, ItemStack item, int slot) {
        this.costs = costs;
        this.item = item;
        this.slot = slot;
        items.add(this);
    }
}

