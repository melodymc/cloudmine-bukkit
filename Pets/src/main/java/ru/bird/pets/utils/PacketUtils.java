package ru.bird.pets.utils;

import net.minecraft.server.v1_16_R2.Packet;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class PacketUtils {

    public static void broadcastNMSPacket(Packet<?> packet) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            CraftPlayer cp = (CraftPlayer) p;
            cp.getHandle().playerConnection.sendPacket(packet);
        }
    }

}
