package ru.bird.bedwars.sidebar;

import de.dytanic.cloudnet.bridge.CloudServer;
import org.bukkit.ChatColor;
import ru.bird.bedwars.team.Team;
import ru.bird.main.board.api.Board;
import ru.bird.main.game.GameSettings;

import java.util.ArrayList;
import java.util.List;

public class SpectatorBoard extends Board {

    public SpectatorBoard(){
        super("В§eВ§lBEDWARS");
        ArrayList<String> list = new ArrayList<>();
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§6§lB§e§lEDWARS");
        list.add("§f§lB§6§lE§e§lDWARS");
        list.add("§f§lBE§6§lD§e§lWARS");
        list.add("§f§lBED§6§lW§e§lARS");
        list.add("§f§lBEDW§6§lA§e§lRS");
        list.add("§f§lBEDWA§6§lR§e§lS");
        list.add("§e§lBEDWAR§6§lS");
        list.add("§f§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§f§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§f§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        list.add("§e§lBEDWARS");
        write(9, ChatColor.DARK_GREEN + "");
        write(8, " §fКарта: §a" + GameSettings.mapLocation.getWorld().getName());
        write(7, " §fСервер: §a" + CloudServer.getInstance().getMotd());
        write(6, ChatColor.GREEN + "");
        write(5, getTeamText(0));
        write(4, getTeamText(1));
        write(3, getTeamText(2));
        write(2, getTeamText(3));
        write(1, ChatColor.BLACK + "");
        addUpdaterHeader(3, list);
        addUpdater(20, board -> {
            board.modifyLine(5, getTeamText(0));
            board.modifyLine(4, getTeamText(1));
            board.modifyLine(3, getTeamText(2));
            board.modifyLine(2, getTeamText(3));
        });
        create();
    }

    private String getTeamText(int i) {
        List<Team> teams = Team.teams;
        return teams.get(i).getColor() + teams.get(i).getName() + ": §6§l" + teams.get(i).getPlayers().size() + " " + Team.canSpawn(teams.get(i));
    }

    public void hide() {
        remove();
    }

}