package com.wellfail.crazywalls.game;

import org.bukkit.entity.Player;
import ru.bird.main.board.api.Board;
import ru.bird.main.game.Gamer;

public class GPlayer
{
    private int kills;
    private Player player;
    private int fb;

    private Board board;
    public Board getBoard() { return board; }
    public void setBoard(Board board)
    {
        this.board = board;
    }

    public GPlayer(Player player) {
        this.player = player;
        this.kills = 0;
        this.fb = 0;
    }

    public void setKills() {
        ++this.kills;
    }

    public void setFb() {
        ++this.fb;
    }

    public int getFb() {
        return this.fb;//Gamer.getGamer(this.player).getFb();
    }

    public int getKills() {
        return this.kills;
    }

    public Player getPlayer() {
        return this.player;
    }
}

