package ru.bird.pets.entity.armorstand.blocks;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.EulerAngle;

import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;

public class PetDropper extends AbstractArmoredPet {

	public PetDropper(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setLeftLegPose(new EulerAngle(0.3, 0, 0));
		armor.setRightLegPose(new EulerAngle(0.3, 0, 0));
		armor.setLeftArmPose(new EulerAngle(-1.57, 0, 0));
		armor.setRightArmPose(new EulerAngle(-1.57, 0, 0));
		armor.setArms(false);
		armor.setVisible(false);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getMainHand() {
		return null;
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		return null;
	}

	@Override
	public ItemStack getChestplate() {
		return null;
	}

	@Override
	public ItemStack getHelmet() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMjVlOTE1MmVmZDg5MmY2MGQ3ZTBkN2U1MzM2OWUwNDc3OWVkMzExMWUyZmIyNzUyYjZmNGMyNmRmNTQwYWVkYyJ9fX0=");
	}

	private Item lastDropped = null;

	private int state = 1;
	
	@Override
	public void onLivingUpdate() {
		++state;
		if(state % 31 == 0){
			state = 1;
		} else {
			return;
		}
		if (lastDropped == null) {
			ItemStack item = new ItemStack(Material.values()[256 + MathUtils.random(120)]);
			lastDropped = this.getOwner().getWorld().dropItem(this.getPetLocation().clone().add(0, 1.2, 0), item);
			lastDropped.setPickupDelay(Integer.MAX_VALUE);
		} else {
			lastDropped.remove();
			lastDropped = null;
		}

	}

	@Override
	public ItemStack getOffHand() {
		return null;
	}

}