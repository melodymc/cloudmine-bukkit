package ru.bird.main.clock;

public class TaskSinCosUpdater extends AbstractTask {

	public static float	COS;
	public static float	SIN;

	private float state = 0;

	@Override
	public int getModule() {
		return 1;
	}

	@Override
	public void call() {
		state += 0.1F;
		if (state >= 2 * Math.PI) {
			state = 0;
		}
		COS = (float) Math.cos(state);
		SIN = (float) Math.sin(state);
	}

}
