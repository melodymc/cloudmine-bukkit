package ru.bird.main.event;

import org.bukkit.block.Chest;
import org.bukkit.entity.Player;

public class GenChestEvent extends BasicEvent {
    private Player player;
    private Chest chest;

    public Player getPlayer() {
        return this.player;
    }

    public Chest getChest() {
        return this.chest;
    }

    public GenChestEvent(Player player, Chest chest) {
        this.player = player;
        this.chest = chest;
    }
}
