package ru.bird.serverswith;

import org.bukkit.plugin.java.JavaPlugin;

public final class Main
extends JavaPlugin {
    private static JavaPlugin instance;

    public static JavaPlugin getInstance() {
        return instance;
    }

    public void onEnable() {
        new CommandSwithServer().register();
        new CommandSwithServers().register();
        new CommandSwithHubs().register();
        new CommandShowOnlineServer().register();
        new CommandShowOnlineServers().register();
        instance = this;
    }
}

