package ru.bird.jobs.utils;

import com.mojang.datafixers.util.Pair;

import java.util.ArrayList;
import java.util.List;

import static ru.bird.jobs.utils.Directions.fromId;

public final class InvUtils
{
    public static int edgeSlot(int slot, Directions dir, int width, int height)
    {
        switch (dir)
        {
            case DOWN: return (height-1) * width + slot % width;
            case UP: return slot % width;
            case RIGHT: return (slot / width) * width + width - 1;
            case LEFT: return (slot / width) * width;

            default: return -1;
        }
    }

    public static List<Integer> getAllRow(int slot, int width)
    {
        ArrayList<Integer> slots = new ArrayList<>();

        int row = slot / width; int startWith = row * width;
        for (int i = 0; i < width; ++i)
        {
            slots.add(startWith + i);
        }

        return slots;
    }

    public static List<Integer> getAllColumn(int slot, int width, int height)
    {
        ArrayList<Integer> slots = new ArrayList<>();

        int column = slot % width;
        for (int i = column; i < width * height; i += width)
        {
            slots.add(i);
        }

        return slots;
    }

    public static List<Integer> getAllNearestSlots(int slot, int width, int height)
    {
        ArrayList<Integer> slots = new ArrayList<>();

        for (int i = 0; i < 8; ++i)
        {
            int nearestSlot = getNearestSlot(slot, width, height, fromId(i));

            if (nearestSlot >= 0) slots.add(nearestSlot);
        }

        return slots;
    }

    public static int getNearestSlot(int slot, int width, int height, Directions dir)
    {
        int x = slot % width + dir.horizontalShifts(); int y = slot / width + dir.verticalShifts();

        return (x < 0 || x > width-1 || y < 0 || y > height-1) ? -1 : xy2index(x, y, width);
    }

    public static int xy2index(int x, int y, int width)
    {
        return y * width + x;
    }

    public static Pair<Integer, Integer> index2xy(int index, int width)
    {
        return new Pair<>(index % width, index / width);
    }
}
