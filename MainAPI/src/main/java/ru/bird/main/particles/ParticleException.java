package ru.bird.main.particles;

public class ParticleException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ParticleException() {
	}

	public ParticleException(String message) {
		super(message);
	}

	public ParticleException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParticleException(Throwable cause) {
		super(cause);
	}

	public ParticleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
