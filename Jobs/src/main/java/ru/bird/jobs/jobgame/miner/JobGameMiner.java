package ru.bird.jobs.jobgame.miner;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.bird.inventories.menu.Menu;
import ru.bird.inventories.menu.interfaces.GuiAction;
import ru.bird.inventories.menu.interfaces.GuiCallback;
import ru.bird.jobs.JobsPlugin;
import ru.bird.jobs.jobgame.JobGame;
import ru.bird.jobs.jobgame.event.FinishJobEvent;
import ru.bird.jobs.jobgame.event.InterruptedJobEvent;
import ru.bird.jobs.jobgame.event.StartJobEvent;
import ru.bird.jobs.jobgame.generator.GeneratorGroup;
import ru.bird.jobs.jobgame.generator.GeneratorItem;
import ru.bird.jobs.jobgame.generator.LevelGenerator;
import ru.bird.jobs.jobgame.generator.patterns.SimpleSimulator;
import ru.bird.jobs.jobgame.miner.event.AppearExitEvent;
import ru.bird.jobs.jobgame.miner.event.CollectItemEvent;
import ru.bird.jobs.jobgame.miner.event.FinishLevelEvent;
import ru.bird.jobs.jobgame.miner.event.TntExplodedEvent;
import ru.bird.jobs.utils.InvUtils;
import ru.bird.jobs.utils.ItemStackUtils;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class JobGameMiner extends JobGame
{
    /* CONSTANT DATA */
    private final Set<Material> trashItems;
    private final List<ItemData> items;

    public final double EXIT_SPAWN_CHANCE = 0.02d;
    public final int EXIT_EXTRA_CLICKS = 3;
    public final int MAX_LEVEL = 10;
    public final int INIT_MINE_DURABILITY = 1000;

    public final Function<Integer, Integer> MINE_DAMAGE = (tnt) -> 10 * tnt;

    /* MAINTENANCE DATA */
    public final Material EXIT_ITEM = Material.LADDER;
    public final Menu gui = new Menu(JobsPlugin.get(), "Шахтёр", 6);
    private LevelGenerator generator;

    /* GAME VARIABLES */
    private int mineDurability;
    private int currentLevel;
    private boolean exitIsExist;

    public JobGameMiner(UUID playerUuid)
    {
        super(playerUuid);

        this.items = new ArrayList<>();

        GeneratorGroup group = new GeneratorGroup();
        addItem(group, new ItemData(new GeneratorItem(Material.EMERALD_ORE, 0.03d, 1, 5), 10));
        addItem(group, new ItemData(new GeneratorItem(Material.DIAMOND_ORE, 0.05d, 1, 5), 6));
        addItem(group, new ItemData(new GeneratorItem(Material.GOLD_ORE, 0.12d, 1, 5), 3));
        addItem(group, new ItemData(new GeneratorItem(Material.IRON_ORE, 0.15d, 1, 5), 2));
        addItem(group, new ItemData(new GeneratorItem(Material.COAL_ORE, 0.24d, 1, 5), 1));
        addItem(group, new ItemData(new GeneratorItem(Material.OBSIDIAN, 0.02d, 8, 32), 1));
        addItem(group, new ItemData(new GeneratorItem(Material.STONE, 0.10d, 1, 2), 0, true, 100, 0.005d));
        addItem(group, new ItemData(new GeneratorItem(Material.AIR, 0.29d, 0, 0), 0, true, 0, 0));

        this.generator = new LevelGenerator(group);
        generator.setSimulator( new SimpleSimulator() );

        trashItems = items.stream()
                .filter(data -> data.isTrash).map(data -> data.aggregatedItem.material)
                .collect(Collectors.toSet());
        trashItems.add(Material.TNT);

        gui.setGuiOpen(onGuiBuildEventHandler);
        gui.setGuiClose(onGuiDestructEventHandler);
        gui.setGuiClick(onGuiClickEventHandler);
        gui.allowInteract(Material.TNT);

        mineDurability = INIT_MINE_DURABILITY;
        currentLevel = 1;
        exitIsExist = false;

        worker.newJobInstance(this);
    }

    public void addItem(GeneratorGroup group, ItemData data)
    {
        group.defineItem(data);
        items.add(data);
    }

    /* Utils */
    public ItemData findByMaterial(Material request)
    {
        Optional<ItemData> foundItemData = items.stream().filter( (m) -> m.aggregatedItem.material == request ).findAny();

        return foundItemData.orElse(ItemData.IMPOSSIBLE);
    }

    /* It the same that is 'inventory.isEmpty()' */
    /* Because the function fail with NPE        */
    private boolean inventoryIsEmpty(Inventory inventory)
    {
        for (ItemStack stack : inventory.getContents())
        {
            if (stack != null && stack.getAmount() > 0 && !trashItems.contains(stack.getType()))
            {
                return false;
            }
        }

        return true;
    }

    /* GUI event handlers */
    private final GuiCallback onGuiBuildEventHandler = (player, container) -> {
        Bukkit.getServer().getPluginManager().callEvent( new StartJobEvent(this) );

        generator.generateLevel(container);
    };

    private final GuiCallback onGuiDestructEventHandler = (player, container) -> {
        if (currentLevel <= MAX_LEVEL)
        {
            Bukkit.getServer().getPluginManager().callEvent( new InterruptedJobEvent(this) );
            worker.newJobInstance(JobGame.EMPTY);
        }
    };

    private final BiConsumer<Player, InventoryClickEvent> onGuiClickEventHandler = (player, event) -> {
        if (event.getCurrentItem() == null) return;
        if (event.getClickedInventory() != gui.getBukkitInventory()) return;

        int slot = event.getSlot();

        Player onlineWorker = worker.getOnlinePlayer();
        Inventory inventory = event.getClickedInventory();

        ItemStack item = inventory.getItem(slot);
        Material itemType = item.getType();

        if (event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.TNT)
        {
            int damage = MINE_DAMAGE.apply(item.getAmount());
            mineDurability -= damage;

            item.setAmount(0);

            List<Integer> slots = InvUtils.getAllNearestSlots(slot, 9, 6);

            for (int i: slots)
            {
                ItemStack is = inventory.getItem(i);

                if (is != null && is.getAmount() > 0)
                {
                    gui.onSlotClicked(onlineWorker, new InventoryClickEvent(event.getView(), event.getSlotType(), i, ClickType.LEFT, InventoryAction.PICKUP_ONE));
                }
            }

            Bukkit.getServer().getPluginManager().callEvent( new TntExplodedEvent(this, damage) );

            if (mineDurability <= 0)
            {
                onlineWorker.closeInventory();
            }
        }
        else
        {
            item.setAmount(item.getAmount() - 1);
            if (item.getAmount() == 0)
            {
                ItemData data = findByMaterial(itemType);

                double randomValue = ThreadLocalRandom.current().nextDouble();
                if (!exitIsExist && randomValue < EXIT_SPAWN_CHANCE)
                {
                    exitIsExist = true;
                    inventory.setItem(slot, ItemStackUtils.createItem(EXIT_ITEM, EXIT_EXTRA_CLICKS));

                    Bukkit.getServer().getPluginManager().callEvent( new AppearExitEvent(this, slot, inventory, true) );
                }

                randomValue = ThreadLocalRandom.current().nextDouble();
                worker.setPoints(worker.getPoints() + data.reward);

                Bukkit.getServer().getPluginManager().callEvent( new CollectItemEvent(this, data.reward, slot, inventory, data, false) );

                if (randomValue < data.bonusChance)
                {
                    worker.setPoints(worker.getPoints() + data.bonus);

                    Bukkit.getServer().getPluginManager().callEvent( new CollectItemEvent(this, data.bonus, slot, inventory, data, true) );
                }

                if ( inventoryIsEmpty(inventory) || itemType == EXIT_ITEM)
                {
                    if (itemType != EXIT_ITEM)
                    {
                        exitIsExist = true;
                        inventory.setItem(slot, ItemStackUtils.createItem(EXIT_ITEM, 1));

                        Bukkit.getServer().getPluginManager().callEvent( new AppearExitEvent(this, slot, inventory, false) );
                        return;
                    }

                    currentLevel += 1;

                    if (currentLevel > MAX_LEVEL)
                    {
                        onlineWorker.closeInventory();

                        worker.newJobInstance(JobGame.EMPTY);

                        Bukkit.getServer().getPluginManager().callEvent( new FinishJobEvent(this) );
                    }
                    else
                    {
                        Bukkit.getServer().getPluginManager().callEvent( new FinishLevelEvent(this) );

                        generator.generateLevel(inventory);
                        exitIsExist = false;
                    }
                }
            }

            item = inventory.getItem(slot);
            if (item == null)
            {
                gui.removeAction(slot);
            }
        }
    };

    /* Getting game data */
    public List<ItemData> getItems()
    {
        return Collections.unmodifiableList(items);
    }

    public int getCurrentLevel()
    {
        return currentLevel;
    }

    public int getMineDurability()
    {
        return mineDurability;
    }
}
