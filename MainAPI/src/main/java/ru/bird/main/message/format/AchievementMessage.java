package ru.bird.main.message.format;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.advancement.Advancement;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.main.utils.GameUtils;

import java.util.*;

public class AchievementMessage implements ConfigurationSerializable {

    private NamespacedKey id;
    private String icon;
    private String header, footer;
    private JavaPlugin javaPlugin;

    public AchievementMessage(Map<?, ?> list, JavaPlugin main) {
        this((String) list.get("header"),
                (String) list.get("footer"),
                (String) list.get("icon"), main);
    }

    public AchievementMessage(String header, String footer, String icon, JavaPlugin javaPlugin) {
        this.header = header;
        this.footer = footer;
        this.icon = icon;
        this.javaPlugin = javaPlugin;

        this.id = new NamespacedKey(javaPlugin, "chatty" + new Random().nextInt(1000000) + 1);
    }

    public void show(Player player)	{
        show(Collections.singletonList(player));
    }

    public void show(Collection<? extends Player> players) {
        this.register();

        for (Player player : players)
            this.grant(player);

        new BukkitRunnable() {

            @Override
            public void run() {
                for (Player player : players)
                    revoke(player);

                unregister();
            }

        }.runTaskLater(javaPlugin, 20);
    }

    private void register() {
        try {
            Bukkit.getUnsafe().loadAdvancement(id, this.json());
        } catch (IllegalArgumentException ignored){ }
    }

    private void unregister()	{
        Bukkit.getUnsafe().removeAdvancement(id);
    }

    private void grant(Player player) {
        Advancement advancement = Bukkit.getAdvancement(id);
        AdvancementProgress progress = player.getAdvancementProgress(advancement);
        if (!progress.isDone())	{
            for (String criteria : progress.getRemainingCriteria())	{
                progress.awardCriteria(criteria);
            }
        };

    }

    private void revoke(Player player)	{
        Advancement advancement = Bukkit.getAdvancement(id);
        AdvancementProgress progress = player.getAdvancementProgress(advancement);
        if (progress.isDone())	{
            for (String criteria : progress.getAwardedCriteria()) {
                progress.revokeCriteria(criteria);
            }
        }
    }

    private String json() {
        JsonObject json = new JsonObject();

        JsonObject display = new JsonObject();

        JsonObject icon = new JsonObject();
        icon.addProperty("item", this.icon);

        display.add("icon", icon);
        display.addProperty("title", GameUtils.colorSet(this.header + "\n" + this.footer));
        display.addProperty("description", "Chatty Announcement");
        display.addProperty("background", "minecraft:textures/entity/banner_base.png");
        display.addProperty("frame", "task");
        display.addProperty("announce_to_chat", false);
        display.addProperty("show_toast", true);
        display.addProperty("hidden", true);

        JsonObject trigger = new JsonObject();
        trigger.addProperty("trigger", "minecraft:impossible");

        JsonObject criteria = new JsonObject();
        criteria.add("impossible", trigger);

        json.add("criteria", criteria);
        json.add("display", display);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        return gson.toJson(json);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();

        map.put("icon", icon);
        map.put("header", header);
        map.put("footer", footer);

        return map;
    }
}