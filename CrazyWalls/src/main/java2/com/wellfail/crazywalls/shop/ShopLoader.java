/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.World
 *  org.bukkit.configuration.Configuration
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.configuration.file.FileConfigurationOptions
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.EntityType
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.plugin.Plugin
 *  ru.wellfail.holograms.Holograms
 *  ru.wellfail.npc.NPCCreator
 */
package com.wellfail.crazywalls.shop;

import com.wellfail.crazywalls.CrazyWalls;
import com.wellfail.crazywalls.config.Config;
import com.wellfail.crazywalls.shop.Cost;
import com.wellfail.crazywalls.shop.Item;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.FileConfigurationOptions;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import ru.wellfail.holograms.Holograms;
import ru.wellfail.npc.NPCCreator;

public class ShopLoader {
    public static List<Holograms> hologramses = new ArrayList<Holograms>();
    private FileConfiguration config;
    private File locationsFile = null;

    public ShopLoader() {
        this.init();
        this.loadShop();
    }

    private void loadShop() {
        for (String s : this.config.getConfigurationSection("Shop").getKeys(false)) {
            String name = this.config.getString("Shop." + s + ".name");
            int amount = this.config.getInt("Shop." + s + ".amount");
            int slot = this.config.getInt("Shop." + s + ".slot");
            String cid = this.config.getString("Shop." + s + ".id");
            String[] split = cid.split(":");
            ItemStack item = split.length == 1 ? new ItemStack(Integer.valueOf(split[0]).intValue(), amount) : new ItemStack(Integer.valueOf(split[0]).intValue(), amount, Short.valueOf(split[1]).shortValue());
            List costs = this.config.getStringList("Shop." + s + ".cost");
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(name);
            item.setItemMeta(meta);
            new Item(this.getCostList(costs), item, slot);
        }
        new NPCCreator();
        Config.shops.get(0).getWorld().getEntities().stream().filter(entity -> entity.getType() == EntityType.VILLAGER).forEach(entity1 -> {
            entity1.remove();
        }
        );
        String[] Text = new String[]{"\u00a7b\u041c\u0430\u0433\u0430\u0437\u0438\u043d", "\u00a77(\u00a7a\u0420\u0435\u0441\u0443\u0440\u0441\u044b \u00a7e\u279c \u00a7b\u041f\u0440\u0435\u0434\u043c\u0435\u0442\u044b\u00a77)"};
        Config.shops.stream().forEach(shop -> {
            NPCCreator.createNPC((Location)shop);
            Holograms holo = new Holograms(Text, shop.add(0.0, 0.2, 0.0), (Plugin)CrazyWalls.getInstance());
            hologramses.add(holo);
        }
        );
    }

    private List<Cost> getCostList(List<String> list) {
        ArrayList<Cost> costs = new ArrayList<Cost>();
        list.stream().forEach(line -> {
            String[] args = line.split(";");
            if (args.length == 4) {
                ItemStack cost = new ItemStack(Integer.valueOf(args[0]).intValue(), Integer.valueOf(args[1], Integer.parseInt(args[2])).intValue());
                costs.add(new Cost(cost, Integer.valueOf(args[1]), args[3]));
            } else if (args.length == 3) {
                ItemStack cost = new ItemStack(Integer.valueOf(args[0]).intValue(), Integer.valueOf(args[1]).intValue());
                costs.add(new Cost(cost, Integer.valueOf(args[1]), args[2]));
            }
        }
        );
        return costs;
    }

    public void saveLocationsConfig() {
        if (this.locationsFile == null || this.config == null) {
            return;
        }
        try {
            this.config.save(this.locationsFile);
        }
        catch (IOException ex) {
            System.out.println("\u041e\u0448\u0438\u0431\u043a\u0430 \u043f\u0440\u0438 \u0447\u0442\u0435\u043d\u0438\u0438 \u0444\u0430\u0439\u043b\u0430 \u043a\u043e\u043d\u0444\u0438\u0433\u0443\u0440\u0430\u0446\u0438\u0438.");
        }
    }

    private void init() {
        this.locationsFile = new File(CrazyWalls.getInstance().getDataFolder(), "shop.yml");
        this.config = YamlConfiguration.loadConfiguration((File)this.locationsFile);
        InputStream defConfigStream1 = CrazyWalls.getInstance().getResource("shop.yml");
        if (defConfigStream1 != null) {
            YamlConfiguration defConfig1 = YamlConfiguration.loadConfiguration((InputStream)defConfigStream1);
            this.config.setDefaults((Configuration)defConfig1);
        }
        this.config.options().copyDefaults(true);
        this.saveLocationsConfig();
    }
}

