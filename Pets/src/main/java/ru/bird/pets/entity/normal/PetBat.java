package ru.bird.pets.entity.normal;

import net.minecraft.server.v1_16_R2.DataWatcher;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.Bat;
import org.bukkit.entity.EntityType;
import ru.bird.pets.entity.AbstractStandardPet;

import java.util.UUID;

public class PetBat extends AbstractStandardPet {

	public PetBat(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		Bat entity = (Bat) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		entity.setAwake(true);
		DataWatcher data = ((CraftEntity) entity).getHandle().getDataWatcher();
		entity.remove();
		return data;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.BAT;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

}
