package ru.bird.pets.commands;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginDescriptionFile;
import ru.bird.chat.AbstractCommand;
import ru.bird.inventories.menu.PageMenus;
import ru.bird.inventories.menu.interfaces.GuiAction;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.ItemUtils;
import ru.bird.main.utils.MathUtils;
import ru.bird.pets.Pets;
import ru.bird.pets.PetsDB;
import ru.bird.pets.entity.EnumPet;
import ru.bird.pets.player.PetAccount;

import java.util.*;
import java.util.stream.Collectors;

public class PetsCommands extends AbstractCommand {
    public PetsCommands() {
        super("pets");
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        try {
            if(args.length == 1) {
                ArrayList<String> helpTab = new ArrayList<>();
                if(sender.hasPermission("mg.pets.help.user")) {
                    helpTab.add("select");
                    helpTab.add("list");
//                    helpTab.add("list all");
                    helpTab.add("hide");
                }
                if(sender.hasPermission("mg.pets.help.admin")) {
                    helpTab.add("add");
                    helpTab.add("remove");
                    helpTab.add("set");
                    helpTab.add("spawn");
                }
                return helpTab;
            }
            String subcommand = args[0];
            args = Arrays.copyOfRange(args, 1, args.length);
            switch (subcommand){
                case "hide":
                case "list":
                    if(args.length == 1) {
                        return getAllPlayersName();
                    }
                    return new ArrayList<>();
                case "select":
//                sender.sendMessage("length " + args.length);
                    if(args.length == 1) {
                        if(sender instanceof Player) {
                            Player player = (Player) sender;
                            PetAccount acc = PetsDB.get(player);
                            return acc.getAllowedPets().stream().map(Enum::name).collect(Collectors.toList());
                        } else {
                            sender.sendMessage("Вы не игрок? Фига, ну тогда, пусто чё ещё тебе дать)");
                            return new ArrayList<>();
                        }
                    }
                    return new ArrayList<>();
                case "set":
                    if (args.length == 1) {
                        return getAllPlayersName();
                    }
                    if(args.length == 2) {
                        Player player = Bukkit.getPlayer(args[0]);
                        PetAccount acc = PetsDB.get(player);
                        if(acc != null) {
                            List<String> list = Arrays.stream(EnumPet.values())
                                    .filter(pet -> acc.getAllowedPets().contains(pet))
                                    .map(Enum::name)
                                    .collect(Collectors.toList());
                            if(list.size() > 0) {
                                return list;
                            } else {
                                sender.sendMessage(ChatColor.RED + "Нет доступных петов.");
                                TextComponent msg = new TextComponent("Добавить пета.");
                                msg.setColor(net.md_5.bungee.api.ChatColor.GOLD);
                                msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("<Игрок> <Пет>").create()));
                                msg.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/pets add"));
                                sender.spigot().sendMessage(msg);
                                return new ArrayList<>();
                            }
                        } else {
                            if(sender.hasPermission("mg.pets.admin.messages.nullaccount")) {
                                sender.sendMessage("У цели не загружен аккаунт петов. Set");
                            } else {
                                return new ArrayList<>();
                            }
                        }
                    }
                    return new ArrayList<>();
                case "add":
                    if (args.length == 1) {
                        return getAllPlayersName();
                    }
                    if(args.length == 2) {
                        Player player = Bukkit.getPlayer(args[0]);
                        PetAccount acc = PetsDB.get(player);
                        if(acc != null) {
                            List<String> list = Arrays.stream(EnumPet.values())
                                    .filter(pet -> !acc.getAllowedPets().contains(pet))
                                    .map(Enum::name)
                                    .collect(Collectors.toList());
                            if(list.size() > 0) {
                                return list;
                            } else {
                                sender.sendMessage(ChatColor.RED + "Все поступные петы уже добавлены игроку.");
                                return new ArrayList<>();
                            }
                        } else {
                            if(sender.hasPermission("mg.pets.admin.messages.nullaccount")) {
                                sender.sendMessage("У цели не загружен аккаунт петов. Add");
                            } else {
                                return new ArrayList<>();
                            }
                        }
                    }
                    return new ArrayList<>();
                case "remove":
//                sender.sendMessage("length " + args.length);
                    if (args.length == 1) {
                        return getAllPlayersName();
                    }
                    if(args.length == 2) {
                        Player target = Bukkit.getPlayer(args[0]);
                        PetAccount acc = PetsDB.get(target);
                        List<String> list = acc.getAllowedPets().stream().map(Enum::name).collect(Collectors.toList());
                        if(list.size() > 0) {
                            return list;
                        } else {
                            sender.sendMessage(ChatColor.RED + "Нет доступных петов для удаления.");
                            return new ArrayList<>();
                        }
                    }
                    return new ArrayList<>();
                case "spawn":
                    return Arrays.stream(EnumPet.values())
                            .map(Enum::name)
                            .collect(Collectors.toList());
                default:
                    sender.sendMessage(ChatColor.RED + "Этот аргумент не поддерживает TAB");
                    return new ArrayList<>();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            if(sender.hasPermission("mg.pets.admin.messages.taberror")) {
                sender.sendMessage("Ошибка автозаполнения.");
            }
            return new ArrayList<>();
        }
    }

    private List<String> getAllPlayersName() {
        return Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player)){
            sender.sendMessage("only for player");
            return false;
        }
        if(!sender.hasPermission("mg.entity.command")) {
            sender.sendMessage("Нет доступа к этой команде.");
            return false;
        }
        if(args.length == 0) {
            help(label, sender);
            return false;
        }
        Player player = (Player) sender;
        String subcommand = args[0];
        if(!sender.hasPermission("mg.pets.command." + subcommand)) {
            sender.sendMessage("Нет доступа к этой саб-команде.");
            return false;
        }
        args = Arrays.copyOfRange(args, 1, args.length);
        switch (subcommand){
            case "spawn":
                if(args.length == 0) {
                    player.sendMessage("Кого заспавнить? ");
                    return false;
                }
                return spawn(player, args[0]);
            case "select":
                if(args.length == 0) {
                    player.sendMessage("Кого выбрать? <" + ChatColor.GREEN + "Работает TAB" + ChatColor.WHITE + ">");
                    return false;
                }
                return set(player, player.getName(), args[0], true);
            case "hide":
                if(args.length == 0) {
                    return hidePet(player, player.getName());
                }
                if(args.length == 1 && player.hasPermission("pets.hide.other")) {
                    return hidePet(player, args[0]);
                }
                return false;
            case "set":
                if(args.length == 0) {
                    player.sendMessage("Кому установить? Работает <" + ChatColor.GREEN + "TAB" + ChatColor.WHITE + ">");
                    return false;
                }
                if(args.length == 1) {
                    player.sendMessage("Кого установить? Работает <" + ChatColor.GREEN + "TAB" + ChatColor.WHITE + ">");
                    return false;
                }
                return set(player, args[0], args[1], false);
            case "list":
                if(args.length == 0) {
                    return list(player, player);
                }
                if(args.length == 1) {
                    String subsubcommand = args[0];
                    switch (subsubcommand) {
                        case "all":
                            return list_all(player);
                        default:
                            Player target = Bukkit.getPlayer(subsubcommand);
                            if(target != null && target.isOnline()) {
                                return list(player, target);
                            } else {
                                sender.sendMessage("Игрока нет в на этой арене");
                                return false;
                            }
                    }
                }
                return false;
            case "add":
                if(args.length == 0) {
                    player.sendMessage("Кому добавить? <" + ChatColor.GREEN + "Никнейм" + ChatColor.WHITE + ">");
                    return false;
                }
                if(args.length == 1) {
                    player.sendMessage("Кого добавить? <" + ChatColor.GREEN + "Работает TAB" + ChatColor.WHITE + ">");
                    return false;
                }
                return add(player, args[0], args[1]);
            case "remove":
                if(args.length == 0) {
                    player.sendMessage("Кому удалить? <" + ChatColor.GREEN + "Никнейм" + ChatColor.WHITE + ">");
                    return false;
                }
                if(args.length == 1) {
                    player.sendMessage("Кого удалить? <" + ChatColor.GREEN + "Работает " + ChatColor.GREEN + "TAB" + ChatColor.WHITE + ">");
                    return false;
                }
                return remove(player, args[0], args[1]);
//            case "page":
//                return page(player);
            default:
                player.sendMessage(ChatColor.GRAY + "Нет такой саб-команды.");
                return true;
        }
    }

    private boolean page(Player player) {
        List<ItemStack> list = new ArrayList<>();
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64)));
        list.add(ItemUtils.createItem(Material.APPLE, MathUtils.random(1, 64), 0, "Лууууук"));
        list.add(ItemUtils.createItem(Material.BOW, 1));
        PageMenus menus = new PageMenus("Проверка", list, new GuiAction() {
            @Override
            public void onClick(InventoryClickEvent var1) {
                var1.getWhoClicked().sendMessage("name click item " + var1.getCurrentItem().getItemMeta().getDisplayName());
            }
        }, null);
        if(menus.openStart(player)) {
            return true;
        } else {
            player.sendMessage("Ошибка ?");
            return false;
        }
    }

    private boolean hidePet(Player player, String to) {
        Player to_player;
        if(!player.getName().equals(to)) {
            to_player = Bukkit.getPlayer(to);
            if(to_player == null || !to_player.isOnline()) {
                player.sendMessage("Игрока нет в сети");
                return false;
            }
        } else {
            to_player = player;
        }
        player.sendMessage("Пет был " + (Pets.getPetsManager().hiddenPet(to_player) ? ChatColor.RED + "Скрыт" : ChatColor.GREEN + "Показан"));
        return true;
    }

    private boolean set(Player player, String to, String name, boolean al) {
        EnumPet pet;
        try {
            pet = EnumPet.valueOf(name);
        } catch (Exception e){
            player.sendMessage(ChatColor.GRAY + "Такого питомца нет в списке.");
            return false;
        }
        Player to_player;
        if(!player.getName().equals(to)) {
            to_player = Bukkit.getPlayer(to);
            if(to_player == null || !to_player.isOnline()) {
                player.sendMessage("Игрока нет в сети");
                return false;
            }
        } else {
            to_player = player;
        }
        PetAccount acc = PetsDB.get(to_player);
        if(acc != null) {
            if (acc.getActivePet() != pet && acc.getAllowedPets().contains(pet) && al || !al) {
                acc.setActivePet(pet);
                player.sendMessage(ChatColor.GREEN + "Готово.");
                return true;
            } else {
                player.sendMessage(ChatColor.RED + "Ошибка.");
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean add(Player player, String to, String name) {
        EnumPet pet;
        try {
            pet = EnumPet.valueOf(name);
        } catch (Exception e){
            player.sendMessage(ChatColor.GRAY + "Такого питомца нет в списке.");
            return false;
        }
        Player to_player;
        if(!player.getName().equals(to)) {
            to_player = Bukkit.getPlayer(to);
            if(to_player == null || !to_player.isOnline()) {
                player.sendMessage("Игрока нет в сети");
                return false;
            }
        } else {
            to_player = player;
        }
        PetAccount acc = PetsDB.get(to_player);
        if(!acc.getAllowedPets().contains(pet)) {
            acc.addAlowdedPets(pet);
            to_player.sendMessage("Вам был добавлен пет " + pet.getDisplayName() + ChatColor.WHITE + ".");
            TextComponent msg = new TextComponent("Выбрать этого пета.");
            msg.setColor(net.md_5.bungee.api.ChatColor.GOLD);
            msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Кликабельно").create()));
            msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/pets select " + pet.name()));
            to_player.spigot().sendMessage(msg);
            return true;
        } else {
            player.sendMessage(ChatColor.GRAY + "У цели уже есть данный питомец.");
            return false;
        }
    }

    private boolean remove(Player player, String to, String name) {
        EnumPet pet;
        try {
            pet = EnumPet.valueOf(name);
        } catch (Exception e){
            player.sendMessage(ChatColor.GRAY + "Такого питомца нет в списке.");
            return false;
        }
        Player to_player;
        if(!player.getName().equals(to)) {
            to_player = Bukkit.getPlayer(to);
            if(to_player == null || !to_player.isOnline()) {
                player.sendMessage("Игрока нет в сети");
                return false;
            }
        } else {
            to_player = player;
        }
        PetAccount acc = PetsDB.get(to_player);
        if(acc.getAllowedPets().contains(pet)) {
            to_player.sendMessage("У вас был удалён пет " + pet.getDisplayName() + ChatColor.WHITE + ".");
            if(acc.getAllowedPets().size() > 0) {
                TextComponent msg = new TextComponent("Выбрать другого пета.");
                msg.setColor(net.md_5.bungee.api.ChatColor.GOLD);
                msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Кликабельно").create()));
                msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/pets list"));
                to_player.spigot().sendMessage(msg);
            }
            if(acc.getActivePet() == pet) {
                Pets.getPetsManager().hiddenPet(to_player);
            }
            acc.removeAlowdedPets(pet);
            return true;
        } else {
            player.sendMessage(ChatColor.RED + "У цели уже не такого питомца.");
            return false;
        }
    }

//    private boolean remove(Player player, String name) {
//        EnumPet pet;
//        try{
//            pet = EnumPet.valueOf(name);
//        } catch (Exception e){
//            player.sendMessage("Такого питомца нет в списке");
//            return false;
//        }
//        PetsDB.get(player).removeAlowdedPets(pet);
//        return true;
//    }

    private void help(String label, CommandSender sender) {
        PluginDescriptionFile desc = Pets.getPlugin().getDescription();
        sender.sendMessage(ChatColor.GOLD + "Доступные команды " + ChatColor.GREEN + desc.getName() + ChatColor.WHITE + " v" + desc.getVersion());
        if(sender.hasPermission("mg.pets.help.admin")) {
            sender.sendMessage(new String[]{
                    ChatColor.BOLD + "/" + label + ChatColor.GRAY + " add - " + ChatColor.GREEN + " добавить в доступные петы",
                    ChatColor.BOLD + "/" + label + ChatColor.GRAY + " set - " + ChatColor.GREEN + " установить пет из доступных игроку",
                    ChatColor.BOLD + "/" + label + ChatColor.GRAY + " remove - " + ChatColor.GREEN + " удалить из доступных петов",
                    ChatColor.BOLD + "/" + label + ChatColor.GRAY + " spawn - " + ChatColor.GREEN + " создать пета без привязки к аккаунту",
                    ChatColor.BOLD + "/" + label + ChatColor.GRAY + " list - " + ChatColor.GREEN + " посмотреть петы игрока (с параметром в виде ника)",
                    ChatColor.BOLD + "/" + label + ChatColor.GRAY + " list all - " + ChatColor.GREEN + " посмотреть всех доступных петов сервера",
                    ChatColor.BOLD + "/" + label + ChatColor.GRAY + " hide - " + ChatColor.GREEN + " принудительно скрыть игроку пета"
            });
        }
        if(sender.hasPermission("mg.pets.help.user")){
            sender.sendMessage(new String[]{
                    ChatColor.BOLD + "/" + label + ChatColor.GRAY + " select - " + ChatColor.GREEN + " выбрать пета",
                    ChatColor.BOLD + "/" + label + ChatColor.GRAY + " hide - " + ChatColor.GREEN + " скрыть выбраного питомца",
                    ChatColor.BOLD + "/" + label + ChatColor.GRAY + " list - " + ChatColor.GREEN + " посмотреть свои доступные петы",
            });
        }
    }

    private boolean list(Player player, Player showTo) {
        PageMenus pageMenu;
        if(player.equals(showTo)) {
            pageMenu = new PageMenus("Мои петы", PetsDB.get(player).getAllowedPets().stream().map(EnumPet::getItem).collect(Collectors.toList()), action -> {
                Player pl = (Player) action.getWhoClicked();
                ItemStack item = action.getCurrentItem();
                List<EnumPet> rawItems = Arrays.stream(EnumPet.values()).filter(enumPet -> enumPet.getItem().getItemMeta().getDisplayName().equals(item.getItemMeta().getDisplayName())).collect(Collectors.toList());
                EnumPet pet = rawItems.get(0);
                PetAccount acc = PetsDB.get(pl);
                if(!acc.getActivePet().equals(pet)) {
                    acc.setActivePet(pet);
                } else {
                    pl.sendMessage("Уже выбран в данный момент");
                }
            }, (player1, inventory) -> {
                PetAccount acc = PetsDB.get(player1);
                Arrays.stream(inventory.getContents()).filter(Objects::nonNull).forEach(item -> {
                    ItemMeta meta = item.getItemMeta();
                    List<String> lore = meta.getLore();
                    if(lore == null) {
                        lore = new ArrayList<>();
                    }
                    if(item.equals(acc.getActivePet().getItem())) {
                        lore.add(GameUtils.colorSet("&eВыбран"));
                    } else {
                        lore.add(GameUtils.colorSet("&7Выбрать"));
                    }
                    meta.setLore(lore);
                    item.setItemMeta(meta);
                });
            });
        } else {
            pageMenu = new PageMenus(
                "Петы игрока " + player.getName(),
                PetsDB.get(player).getAllowedPets().stream().map(EnumPet::getItem).collect(Collectors.toList()),
                null,
                (player1, inventory) -> {
                    PetAccount acc = PetsDB.get(player1);
                    Arrays.stream(inventory.getContents()).filter(Objects::nonNull).forEach(item -> {
                        ItemMeta meta = item.getItemMeta();
                        List<String> lore = meta.getLore();
                        if(lore == null) {
                            lore = new ArrayList<>();
                        }
                        if(item.equals(acc.getActivePet().getItem())) {
                            lore.add(GameUtils.colorSet("&eВыбран"));
                        }
                        meta.setLore(lore);
                        item.setItemMeta(meta);
                    });
                }
            );
        }
        pageMenu.openStart(showTo);
        return true;
    }

    private boolean list_all(Player player) {
        if(player.hasPermission("mg.pets.admin.listall")){
            PageMenus pages = new PageMenus("Все доступные петы", Arrays.stream(EnumPet.values()).map(EnumPet::getItem).collect(Collectors.toList()), event -> {
                Player lp = (Player) event.getWhoClicked();
                lp.sendMessage("Ты (" + lp.getName() +  ") кликнул на предмет " + event.getCurrentItem().getItemMeta().getDisplayName());
            }, null);
            pages.openStart(player);
            return true;
        }
        return false;
    }

    private boolean list_str(Player player) {
        EnumPet[] values = EnumPet.values();
        StringBuilder pets = new StringBuilder();
        for(EnumPet pet : values){
            pets.append(values.length == 1 ? pet : pet + ", ");
        }
        player.sendMessage(pets.toString());
        return true;
    }

    private boolean spawn(Player player, String name) {
        EnumPet pet;
        try{
            pet = EnumPet.valueOf(name);
        } catch (Exception e){
            player.sendMessage("Такого питомца нет в списке");
            return false;
        }
//        AbstractPet apet = pet.create(player, 1);
//        apet.spawn();
        Pets.getPetsManager().spawnPetFor(player, pet);
        player.sendMessage("Создан " + pet.name());
        return true;
    }
}
