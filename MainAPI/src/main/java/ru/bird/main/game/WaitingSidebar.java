package ru.bird.main.game;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import ru.bird.main.API;
import ru.bird.main.board.api.Board;
import ru.bird.main.event.TimerTickEvent;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.utils.GameUtils;
import ru.pir.main.utils.BoardStringAnimator;

import java.util.Arrays;
import java.util.List;

public class WaitingSidebar extends Board {

    private byte d = 0;

    public WaitingSidebar(String title) {
        super(title);
        addUpdaterHeader(3, BoardStringAnimator.concatFrameBuffers(
                BoardStringAnimator.generateStatic(title, "&e", 5),
                BoardStringAnimator.generateMetalShine(title, "&e", "&6", "&f"),
                BoardStringAnimator.generateBlinking(title, "&f", "&e", 5)
        ));
        write(0, "123");

//        addUpdater(15, board -> {
//            if(d >= loadingList.size()) {
//                d = 0;
//            }
//            board.modifyLine(4, loadingList.get(d));
//            d++;
//        });
        create();
    }

}