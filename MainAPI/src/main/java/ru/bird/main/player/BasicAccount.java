package ru.bird.main.player;

import java.util.UUID;

public class BasicAccount {

    private UUID uuid;
    public static final BasicAccount empty = new BasicAccount();

    public BasicAccount() {}

    public BasicAccount(UUID uuid) {
        this.uuid = uuid;
    }

    public boolean isEmpty() {
        return this == empty;
    }

    public UUID getUUID() {
        return uuid;
    }

    public void getUUID(UUID uuid) {
        this.uuid = uuid;
    }
}