/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.entity.Player
 *  ru.wellfail.wapi.board.Board
 *  ru.wellfail.wapi.board.BoardLine
 *  ru.wellfail.wapi.game.Gamer
 */
package ru.bird.bedwars.board;

import ru.bird.bedwars.game.GameFactory;
import ru.bird.bedwars.team.Team;
import org.bukkit.entity.Player;
import ru.wellfail.wapi.board.Board;
import ru.wellfail.wapi.board.BoardLine;
import ru.wellfail.wapi.game.Gamer;

import java.util.*;

public class GameBoard
extends Board {
    private int d;
    private Player player;
    private List<String> list;
//    private BoardLine deaths;
    private BoardLine kills;
    //private BoardLine bedkills;
    private BoardLine teamKills;
    private BoardLine rteam;
    private BoardLine bteam;
    private BoardLine gteam;
    private BoardLine yteam;

    int s;
    Map<Team, BoardLine> teams = new HashMap<Team, BoardLine>();

    public GameBoard(Player player) {
        this.player = player;
        Set<BoardLine> lines = this.getLines();
        this.create(lines, "В§eВ§lBEDWARS");
        this.list = new ArrayList<String>();
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§6§lB§e§lEDWARS");
        this.list.add("§f§lB§6§lE§e§lDWARS");
        this.list.add("§f§lBE§6§lD§e§lWARS");
        this.list.add("§f§lBED§6§lW§e§lARS");
        this.list.add("§f§lBEDW§6§lA§e§lRS");
        this.list.add("§f§lBEDWA§6§lR§e§lS");
        this.list.add("§e§lBEDWAR§6§lS");
        this.list.add("§f§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§f§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§f§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.list.add("§e§lBEDWARS");
        this.update(() -> {
            if (this.d == this.list.size()) {
                this.d = 0;
            }
            this.setDisplay(this.list.get(this.d));
            ++this.d;
        }
        , 3);
        this.set(player);
        this.startUpdate();
    }

    private String canSpawn(Team team) {
        if (team.isCanSpawn()) {
            return "§a⚑ ";
        }
        return "§c☠ ";
    }

//    private String getTeam(ChatColor c) {
//        if (c.equals(ChatColor.GREEN)) {
//            return "Зеленые";
//        }
//        if (c.equals(ChatColor.BLUE)) {
//            return "Синие";
//        }
//        if (c.equals(ChatColor.YELLOW)) {
//            return "Желтые";
//        }
//        if (c.equals(ChatColor.RED)) {
//            return "Красные";
//        }
//        return "error";
//    }

    public synchronized Set<BoardLine> getLines() {
        //GamePlayer gamer = GameFactory.players.get(this.player);
        HashSet<BoardLine> set = new HashSet<BoardLine>();
        set.add(new BoardLine(this, "§1", 10));
        Iterator<Team> it = Team.teams.iterator();
        Team t = it.next();
        this.gteam = new BoardLine(this, t.getName() + ": §6§l", 9);
        set.add(this.gteam);
        this.dynamicLine(this.gteam.getNumber(), t.getColor() + t.getName() + ": §6§l", "" + t.getPlayers().size() + "", this.canSpawn(t));
        this.teams.put(t, this.gteam);
        t = it.next();
        this.yteam = new BoardLine(this, t.getName() + ": §6§l", 8);
        set.add(this.yteam);
        this.dynamicLine(this.yteam.getNumber(), t.getColor() + t.getName() + ": §6§l", "" + t.getPlayers().size() + "", this.canSpawn(t));
        this.teams.put(t, this.yteam);
        t = it.next();
        this.rteam = new BoardLine(this, t.getName() + ": §6§l", 7);
        set.add(this.rteam);
        this.dynamicLine(this.rteam.getNumber(), t.getColor() + t.getName() + ": §6§l", "" + t.getPlayers().size() + "", this.canSpawn(t));
        this.teams.put(t, this.rteam);
        t = it.next();
        this.bteam = new BoardLine(this, t.getName() + ": §6§l", 6);
        set.add(this.bteam);
        this.dynamicLine(this.bteam.getNumber(), t.getColor() + t.getName() + ": §6§l", "" + t.getPlayers().size() + "", this.canSpawn(t));
        this.teams.put(t, this.bteam);
        set.add(new BoardLine(this, "§2", 5));

//        this.deaths = new BoardLine(this, " §fТвоих смертей§8: §b§l", 5);
//        this.dynamicLine(this.deaths.getNumber(), " §fТвоих смертей§8: §b§l",Gamer.getGamer(this.player).getDeaths() + "");
//        set.add(this.deaths);

        this.kills = new BoardLine(this, " §fТвоих убийств§8: §b§l", 4);
        this.dynamicLine(this.kills.getNumber(), " §fТвоих убийств§8: §b§l", Gamer.getGamer(this.player).getKills() + "");
        set.add(this.kills);

        this.teamKills = new BoardLine(this, " §fКомандных убийств§8: §b§l", 3);
        this.dynamicLine(this.teamKills.getNumber(), " §fКомандных убийств§8: §b§l",GameFactory.players.get(this.player).getTeam().getKills() + "");
        set.add(this.teamKills);

        set.add(new BoardLine(this, "§3", 2));
        //String team = this.getTeam(GameFactory.players.get(this.player).getTeam().getColor());
        set.add(new BoardLine(this, "§e› §fТвоя команда§8: " + GameFactory.players.get(this.player).getTeam().getColor() + "⚐", 1));
        this.update(() -> {
            if (this.player == null || GameFactory.players.get(this.player) == null) {
                this.remove();
            }
            this.dynamicLine(this.kills.getNumber(), " §fТвоих убийств§8: §b§l", Gamer.getGamer(this.player).getKills() + "");
            this.dynamicLine(this.teamKills.getNumber(), " §fКомандных убийств§8: §b§l", GameFactory.players.get(this.player).getTeam().getKills() +"");
//            this.dynamicLine(this.deaths.getNumber(), " §fТвоих смертей§8: §b§l", Gamer.getGamer(this.player).getDeaths() +"");


            for (Map.Entry<Team, BoardLine> s : this.teams.entrySet()) {
                Team team1 = s.getKey();
                if (Team.teams.contains(team1)) {
                    this.dynamicLine(this.teams.get(s.getKey()).getNumber(), team1.getColor() + team1.getName() + ": §6§l", "" + team1.getPlayers().size() + "", this.canSpawn(team1));
                    continue;
                }
                this.teams.get(team1).setLine("§7§m" + team1.getOffName());
            }
        }, 20);

        return set;
    }
}

