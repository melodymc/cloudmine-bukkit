package ru.bird.main;

import org.apache.logging.log4j.LogManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;

import org.bukkit.plugin.messaging.Messenger;
import ru.bird.main.board.ProtocolBoard;
import ru.bird.main.clock.ClockManager;
import ru.bird.main.connection.SQLConnection;
//import ru.bird.main.server.ManagerRabbitMQ;
import ru.bird.main.vault.VaultManager;
import ru.bird.main.hologram.HologramsManager;
import ru.bird.inventories.PageManager;
import ru.bird.main.logger.BaseLogger;

public class APIInitialiser extends JavaPlugin {

	private ProtocolManager	protocolManager;
	private APIManager apiManager;
	private VaultManager vaultManager;
	private YamlConfiguration config;
	private SQLConnection SQLConnection;
	private HologramsManager hologramsManager;
	private PageManager pageManager;
	private ProtocolBoard protocolBoard;
//	private ManagerRabbitMQ managerRabitMQ;

	protected void initProtocolManager() {
		getLogger().info("[ProtocolLibrary] load ProtocolLibrary");
		protocolManager = ProtocolLibrary.getProtocolManager();
		getLogger().info("[ProtocolLibrary] done loaded ProtocolLibrary");
		getLogger().info("[ProtocolBoard] load ProtocolBoard");
		protocolBoard = new ProtocolBoard();
		getLogger().info("[ProtocolBoard] done loaded ProtocolBoard");
	}

	public ProtocolBoard getProtocolBoard() {
		return protocolBoard;
	}

	//	protected void initHeadManager() {
//		headManager = new HeadManager();
//		try {
//			headManager.build();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}

//	public void initSocket() {
//		socketManager = new SocketManager();
//		socketManager.build();
//	}

	public void initVault() {
		vaultManager = new VaultManager();
		vaultManager.build();
	}

	public VaultManager getVaultManager() {
		return vaultManager;
	}

	protected void initClock() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new ClockManager(), 1, 0);
	}

	public ProtocolManager getProtocolManager() {
		return protocolManager;
	}


//	public SocketManager getSocketManager() {
//		return socketManager;
//	}

	public void initApiManager(API instance) {
		apiManager = new APIManager();
		apiManager.build(instance);
	}

	public void initGame() {
		Messenger msgmanager = API.getInstance().getServer().getMessenger();
		msgmanager.registerOutgoingPluginChannel(API.getInstance(), "BungeeCord");
		BaseLogger.setLogger(LogManager.getLogger("BaseLogger"));
//		new Groups();
//		new MainCommand();
//		try {
//			String systemname = Bukkit.getWorldContainer().getCanonicalFile().getName();
//			if(systemname.equals("paper1") || systemname.equals("paper3")) {
//				API.getInstance().getManagerRabitMQ().write("servers");
//			} else {
//				Schedulers.async().runLater(() -> {
//					BaseLogger.info("Return on send server info " + API.getInstance().getManagerRabitMQ().sendServerInfo(GameState.WAITING));
//				}, 1);
//				Schedulers.async().runLater(() -> {
//					BaseLogger.info("Return on send server info " + API.getInstance().getManagerRabitMQ().sendServerInfo(GameState.STARTING));
//				}, 2);
//				Schedulers.async().runLater(() -> {
//					BaseLogger.info("Return on send server info " + API.getInstance().getManagerRabitMQ().sendServerInfo(GameState.END));
//				}, 2);
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

		SQLConnection = new SQLConnection();
		pageManager = new PageManager();
	}

	public SQLConnection getSQLConnection() {
		return SQLConnection;
	}

	public void initHolograms() {
		hologramsManager = new HologramsManager();
		hologramsManager.build();
	}

    public HologramsManager getHologramsManager() {
        return hologramsManager;
    }

	public PageManager getPageManager() {
		return pageManager;
	}

//	public void initRabbitMQ() {
//		managerRabitMQ = new ManagerRabbitMQ();
//		managerRabitMQ.build();
//	}
//
//	public ManagerRabbitMQ getManagerRabitMQ() {
//		return managerRabitMQ;
//	}
}