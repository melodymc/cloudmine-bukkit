/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.World
 *  org.bukkit.block.Block
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.FallingBlock
 *  org.bukkit.entity.TNTPrimed
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.block.BlockPlaceEvent
 *  org.bukkit.event.entity.EntityChangeBlockEvent
 *  org.bukkit.event.entity.EntityExplodeEvent
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  org.bukkit.util.Vector
 */
package ru.bird.main.event.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import ru.bird.main.API;

import java.util.ArrayList;
import java.util.List;

public class ExplosionListener
extends BasicListener {
    private static List<FallingBlock> blocks = new ArrayList<FallingBlock>();

    @EventHandler
    public void onTntPlace(BlockPlaceEvent event) {
        if (event.getBlockPlaced().getType() == Material.TNT) {
            Block tnt = event.getBlockPlaced();
            tnt.setType(Material.AIR);
            TNTPrimed tntPrimed = (TNTPrimed)tnt.getWorld().spawn(tnt.getLocation().add(0.0, 1.0, 0.0), (Class)TNTPrimed.class);
            tntPrimed.setFuseTicks(40);
        }
    }

    @EventHandler
    public void onExplode(EntityExplodeEvent event) {
        for (Block b : event.blockList()) {
            float x = -1.5f + (float)(Math.random() * 4.0);
            float y = -0.7f + (float)(Math.random() * 2.4);
            float z = -1.5f + (float)(Math.random() * 4.0);
            final FallingBlock block = b.getWorld().spawnFallingBlock(b.getLocation(), b.getType(), b.getData());
            block.setDropItem(false);
            block.setVelocity(new Vector(x, y, z));
            new BukkitRunnable(){

                public void run() {
                    block.remove();
                }
            }.runTaskLaterAsynchronously((Plugin) API.getInstance(), 20);
            b.setType(Material.AIR);
        }
        if (event.getEntity() == null) {
            event.blockList().clear();
        }
    }

    @EventHandler
    public void onTntHitGround(EntityChangeBlockEvent event) {
        if (event.getEntity() instanceof FallingBlock) {
            event.getEntity().remove();
        }
    }

}

