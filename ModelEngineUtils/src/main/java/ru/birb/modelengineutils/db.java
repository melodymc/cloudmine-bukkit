package ru.birb.modelengineutils;

import java.util.LinkedHashMap;
import java.util.HashMap;
import org.bukkit.Material;
import java.util.List;

public class db
{
    private String modelId;
    private boolean useModelEngine;
    private int b;
    private int c;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private boolean apEnable;
    private List e;
    private List f;
    private boolean l;
    private String m;
    private List g;
    private String animationItemNameIdle;
    private String animationItemNameWalk;
    private String animationItemNameAttack;
    private int animationCustomModelDataIdle;
    private int animationCustomModelDataWalk;
    private int animationCustomModelDataAttack;
    private int d;
    private double modelLocationH;
    private Material a;
    private Material b;
    private Material c;
    private double d;
    private boolean m;
    private boolean n;
    private double e;
    private boolean o;
    private int e;
    private double f;
    private List h;
    private String n;
    private String o;
    private String p;
    private boolean p;
    private String q;
    private double g;
    private double h;
    private double i;
    private List i;
    private int f;
    private double j;
    private double k;
    private int g;
    private int i;
    private int j;
    private boolean q;
    private String r;
    private String s;
    private boolean r;
    private String t;
    private int k;
    private List j;
    private double l;
    private double m;
    private double n;
    private boolean s;
    private List k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int z;
    private int A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private int G;
    private int H;
    private int I;
    private int J;
    private int K;
    private int L;
    private int M;
    private int N;
    private int O;
    private int P;
    private int Q;
    private int R;
    private int S;
    private int T;
    private int U;
    private int V;
    private int W;
    private int X;
    private int Y;
    private int Z;
    private List animationModelList;
    public static HashMap m;

    public List getAnimationModelList() {
        return this.animationModelList;
    }

    public void setAnimationModelList(final List animationModelList) {
        this.animationModelList = animationModelList;
    }

    public db(final String modelId) {
        this.modelId = modelId;
    }

    public void register() {
        db.m.put(this.modelId, this);
        de.o.add(this.modelId);
    }

    public static void clear() {
        db.m.clear();
        de.o.clear();
    }

    public String a() {
        return this.g;
    }

    public double a() {
        return this.modelLocationH;
    }

    public void setModelLocationH(final double modelLocationH) {
        this.modelLocationH = modelLocationH;
    }

    public void e(final String g) {
        this.g = g;
    }

    public String b() {
        return this.h;
    }

    public void f(final String h) {
        this.h = h;
    }

    public String c() {
        return this.i;
    }

    public void g(final String i) {
        this.i = i;
    }

    public String d() {
        return this.j;
    }

    public void h(final String j) {
        this.j = j;
    }

    public String e() {
        return this.k;
    }

    public void i(final String k) {
        this.k = k;
    }

    public String f() {
        return this.animationItemNameIdle;
    }

    public void setAnimationItemNameIdle(final String animationItemNameIdle) {
        this.animationItemNameIdle = animationItemNameIdle;
    }

    public String g() {
        return this.animationItemNameWalk;
    }

    public void setAnimationItemNameWalk(final String animationItemNameWalk) {
        this.animationItemNameWalk = animationItemNameWalk;
    }

    public int b() {
        return this.d;
    }

    public void a(final int d) {
        this.d = d;
    }

    public String h() {
        return this.animationItemNameAttack;
    }

    public void setAnimationItemNameAttack(final String animationItemNameAttack) {
        this.animationItemNameAttack = animationItemNameAttack;
    }

    public int c() {
        return this.animationCustomModelDataIdle;
    }

    public void setAnimationCustomModelDataIdle(final int animationCustomModelDataIdle) {
        this.animationCustomModelDataIdle = animationCustomModelDataIdle;
    }

    public Material a() {
        return this.c;
    }

    public Material b() {
        return this.a;
    }

    public Material c() {
        return this.b;
    }

    public void a(final Material c) {
        if (c != null) {
            this.c = c;
            return;
        }
        this.c = Material.DIAMOND_HOE;
    }

    public void b(final Material a) {
        if (a != null) {
            this.a = a;
            return;
        }
        this.a = Material.DIAMOND_HOE;
    }

    public void c(final Material b) {
        if (b != null) {
            this.b = b;
            return;
        }
        this.b = Material.DIAMOND_HOE;
    }

    public int d() {
        return this.animationCustomModelDataWalk;
    }

    public void setAnimationCustomModelDataWalk(final int animationCustomModelDataWalk) {
        this.animationCustomModelDataWalk = animationCustomModelDataWalk;
    }

    public int e() {
        return this.animationCustomModelDataAttack;
    }

    public void setAnimationCustomModelDataAttack(final int animationCustomModelDataAttack) {
        this.animationCustomModelDataAttack = animationCustomModelDataAttack;
    }

    public void a(final boolean apEnable) {
        this.apEnable = apEnable;
    }

    public boolean d() {
        return this.apEnable;
    }

    public void b(final List e) {
        this.e = e;
    }

    public List a() {
        return this.e;
    }

    public void c(final List f) {
        this.f = f;
    }

    public List b() {
        return this.f;
    }

    public void b(final boolean l) {
        this.l = l;
    }

    public boolean e() {
        return this.l;
    }

    public List c() {
        return this.g;
    }

    public void d(final List g) {
        this.g = g;
    }

    public void j(final String m) {
        this.m = m;
    }

    public String i() {
        return this.m;
    }

    public double b() {
        return this.d;
    }

    public boolean f() {
        return this.m;
    }

    public boolean g() {
        return this.o;
    }

    public boolean h() {
        return this.n;
    }

    public double c() {
        return this.e;
    }

    public String getModelId() {
        return this.modelId;
    }

    public void setModelId(final String modelId) {
        this.modelId = modelId;
    }

    public double d() {
        return this.f;
    }

    public void a(final double d) {
        this.d = d;
    }

    public void c(final boolean m) {
        this.m = m;
    }

    public void d(final boolean n) {
        this.n = n;
    }

    public int f() {
        return this.e;
    }

    public void b(final double e) {
        this.e = e;
    }

    public boolean i() {
        return this.s;
    }

    public void e(final boolean o) {
        this.o = o;
    }

    public boolean j() {
        return this.r;
    }

    public List d() {
        return this.h;
    }

    public String j() {
        return this.s;
    }

    public boolean k() {
        return this.q;
    }

    public String k() {
        return this.r;
    }

    public void b(final int e) {
        this.e = e;
    }

    public boolean l() {
        return this.p;
    }

    public double e() {
        return this.l;
    }

    public double f() {
        return this.m;
    }

    public double g() {
        return this.n;
    }

    public double h() {
        return this.g;
    }

    public double i() {
        return this.h;
    }

    public double j() {
        return this.i;
    }

    public int getExp() {
        return this.i;
    }

    public int g() {
        return this.g;
    }

    public int h() {
        return this.f;
    }

    public int i() {
        return this.k;
    }

    public double k() {
        return this.k;
    }

    public int getMaxLevel() {
        return this.j;
    }

    public void c(final double f) {
        this.f = f;
    }

    public double l() {
        return this.j;
    }

    public List e() {
        return this.k;
    }

    public List f() {
        return this.j;
    }

    public String l() {
        return this.n;
    }

    public List g() {
        return this.i;
    }

    public String m() {
        return this.t;
    }

    public String n() {
        return this.o;
    }

    public String o() {
        return this.p;
    }

    public String p() {
        return this.q;
    }

    public void f(final boolean s) {
        this.s = s;
    }

    public void e(final List k) {
        this.k = k;
    }

    public void k(final String n) {
        this.n = n;
    }

    public void f(final List h) {
        this.h = h;
    }

    public void setExp(final int i) {
        this.i = i;
    }

    public void c(final int g) {
        this.g = g;
    }

    public void d(final int f) {
        this.f = f;
    }

    public void g(final boolean r) {
        this.r = r;
    }

    public void l(final String t) {
        this.t = t;
    }

    public void g(final List j) {
        this.j = j;
    }

    public void e(final int k) {
        this.k = k;
    }

    public void d(final double l) {
        this.l = l;
    }

    public void e(final double m) {
        this.m = m;
    }

    public void f(final double n) {
        this.n = n;
    }

    public void g(final double k) {
        this.k = k;
    }

    public void h(final double j) {
        this.j = j;
    }

    public void f(final int j) {
        this.j = j;
    }

    public void m(final String o) {
        this.o = o;
    }

    public void n(final String p) {
        this.p = p;
    }

    public void o(final String s) {
        this.s = s;
    }

    public void h(final boolean q) {
        this.q = q;
    }

    public void p(final String r) {
        this.r = r;
    }

    public void i(final boolean p) {
        this.p = p;
    }

    public void h(final List i) {
        this.i = i;
    }

    public void i(final double g) {
        this.g = g;
    }

    public void q(final String q) {
        this.q = q;
    }

    public void j(final double h) {
        this.h = h;
    }

    public void k(final double i) {
        this.i = i;
    }

    public int j() {
        return this.H;
    }

    public int k() {
        return this.F;
    }

    public int l() {
        return this.G;
    }

    public int m() {
        return this.Q;
    }

    public int n() {
        return this.O;
    }

    public int o() {
        return this.P;
    }

    public int p() {
        return this.M;
    }

    public int q() {
        return this.I;
    }

    public int r() {
        return this.J;
    }

    public int s() {
        return this.N;
    }

    public int t() {
        return this.K;
    }

    public int u() {
        return this.L;
    }

    public int v() {
        return this.z;
    }

    public int w() {
        return this.s;
    }

    public int x() {
        return this.t;
    }

    public int y() {
        return this.A;
    }

    public int z() {
        return this.u;
    }

    public int A() {
        return this.v;
    }

    public int B() {
        return this.n;
    }

    public int C() {
        return this.l;
    }

    public int D() {
        return this.m;
    }

    public int E() {
        return this.r;
    }

    public int F() {
        return this.o;
    }

    public int G() {
        return this.p;
    }

    public int H() {
        return this.D;
    }

    public int I() {
        return this.E;
    }

    public int J() {
        return this.B;
    }

    public int K() {
        return this.C;
    }

    public int L() {
        return this.T;
    }

    public int M() {
        return this.R;
    }

    public int N() {
        return this.S;
    }

    public int O() {
        return this.Y;
    }

    public int P() {
        return this.U;
    }

    public int Q() {
        return this.V;
    }

    public int R() {
        return this.Z;
    }

    public int S() {
        return this.W;
    }

    public int T() {
        return this.X;
    }

    public void g(final int h) {
        this.H = h;
    }

    public void h(final int f) {
        this.F = f;
    }

    public void i(final int g) {
        this.G = g;
    }

    public void j(final int q) {
        this.Q = q;
    }

    public void k(final int t) {
        this.T = t;
    }

    public void l(final int o) {
        this.O = o;
    }

    public void m(final int p) {
        this.P = p;
    }

    public void n(final int m) {
        this.M = m;
    }

    public void o(final int i) {
        this.I = i;
    }

    public void p(final int j) {
        this.J = j;
    }

    public void q(final int n) {
        this.N = n;
    }

    public void r(final int k) {
        this.K = k;
    }

    public void s(final int l) {
        this.L = l;
    }

    public void t(final int z) {
        this.z = z;
    }

    public void u(final int s) {
        this.s = s;
    }

    public void v(final int t) {
        this.t = t;
    }

    public void w(final int a) {
        this.A = a;
    }

    public void x(final int u) {
        this.u = u;
    }

    public void y(final int v) {
        this.v = v;
    }

    public void z(final int n) {
        this.n = n;
    }

    public void A(final int l) {
        this.l = l;
    }

    public void B(final int m) {
        this.m = m;
    }

    public void C(final int r) {
        this.r = r;
    }

    public void D(final int o) {
        this.o = o;
    }

    public void E(final int p) {
        this.p = p;
    }

    public void F(final int d) {
        this.D = d;
    }

    public void G(final int e) {
        this.E = e;
    }

    public void H(final int b) {
        this.B = b;
    }

    public void I(final int c) {
        this.C = c;
    }

    public void J(final int r) {
        this.R = r;
    }

    public void K(final int s) {
        this.S = s;
    }

    public void L(final int y) {
        this.Y = y;
    }

    public void M(final int u) {
        this.U = u;
    }

    public void N(final int v) {
        this.V = v;
    }

    public void O(final int z) {
        this.Z = z;
    }

    public void P(final int w) {
        this.W = w;
    }

    public void Q(final int x) {
        this.X = x;
    }

    public int U() {
        return this.q;
    }

    public void R(final int q) {
        this.q = q;
    }

    public String q() {
        return this.l;
    }

    public void r(final String l) {
        this.l = l;
    }

    public boolean isUseModelEngine() {
        return this.useModelEngine;
    }

    public void setUseModelEngine(final boolean useModelEngine) {
        this.useModelEngine = useModelEngine;
    }

    public int V() {
        return this.b;
    }

    public void S(final int b) {
        this.b = b;
    }

    public int W() {
        return this.c;
    }

    public void T(final int c) {
        this.c = c;
    }

    static {
        db.m = new LinkedHashMap();
    }
}