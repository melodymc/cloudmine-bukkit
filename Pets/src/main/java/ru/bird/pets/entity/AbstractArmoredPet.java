
package ru.bird.pets.entity;

import java.util.*;
import java.util.stream.Collectors;

import com.comphenix.protocol.ProtocolManager;
import com.mojang.datafixers.util.Pair;
import net.minecraft.server.v1_16_R2.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_16_R2.inventory.CraftItemStack;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import ru.bird.main.API;
import ru.bird.main.packets.WrapperPlayServerEntityHeadRotation;
import ru.bird.main.packets.WrapperPlayServerEntityTeleport;
import ru.bird.pets.utils.PacketUtils;

public abstract class AbstractArmoredPet extends AbstractPet {

	public AbstractArmoredPet(UUID ownerID, int uniqueID) {
		super(ownerID, uniqueID);
	}

	@Override
	public EntityType getEntityType(){
		return EntityType.ARMOR_STAND;
	};

	public ItemStack getMainHand() {
		return null;
	}

	public ItemStack getOffHand() {
		return null;
	}

	public ItemStack getBoots() {
		return null;
	}

	public ItemStack getLeggings() {
		return null;
	}

	public ItemStack getChestplate() {
		return null;
	}

	public ItemStack getHelmet() {
		return null;
	};

	@Override
	public void move(Player player){
		Location loc = player.getLocation();
		double decalage = getAngle();
		double angle = loc.getYaw() + decalage;
		double newX = loc.getX() - getRadius() * Math.sin(Math.toRadians(angle));
		double newY = loc.getBlockY() + getOffsetY();
		double newZ = loc.getZ() + getRadius() * Math.cos(Math.toRadians(angle));
		this.petLocation = new Location(getOwner().getWorld(), newX, newY, newZ);
		WrapperPlayServerEntityTeleport position = getPosition();
		position.setEntityID(getEntityID());
		position.setX(newX);
		position.setY(newY);
		position.setZ(newZ);
		position.setYaw(loc.getYaw());
		position.setPitch(loc.getPitch());
		ProtocolManager manager = API.getInstance().getProtocolManager();
		manager.broadcastServerPacket(position.getHandle());

		float headRotation = ((CraftPlayer) player).getHandle().getHeadRotation();
		WrapperPlayServerEntityHeadRotation rotation = getRotation();
		rotation.setEntityID(getEntityID());
		rotation.setHeadYaw(((byte) MathHelper.d(headRotation * 256.0F / 360.0F)));
		manager.broadcastServerPacket(rotation.getHandle());
	}

	@Override
	public void spawn() {
		super.spawn();
		net.minecraft.server.v1_16_R2.ItemStack held = CraftItemStack.asNMSCopy(getMainHand());
		net.minecraft.server.v1_16_R2.ItemStack offheld = CraftItemStack.asNMSCopy(getOffHand());
		net.minecraft.server.v1_16_R2.ItemStack boots = CraftItemStack.asNMSCopy(getBoots());
		net.minecraft.server.v1_16_R2.ItemStack leggings = CraftItemStack.asNMSCopy(getLeggings());
		net.minecraft.server.v1_16_R2.ItemStack chestplate = CraftItemStack.asNMSCopy(getChestplate());
		net.minecraft.server.v1_16_R2.ItemStack helmet = CraftItemStack.asNMSCopy(getHelmet());
		List<Pair<EnumItemSlot, net.minecraft.server.v1_16_R2.ItemStack>> equipment = new ArrayList<>();
		if (held != null) {
			equipment.add(new Pair<>(EnumItemSlot.MAINHAND, held));
		}
		if (offheld != null) {
			equipment.add(new Pair<>(EnumItemSlot.OFFHAND, held));
		}
		if (boots != null) {
			equipment.add(new Pair<>(EnumItemSlot.FEET, held));
		}
		if (leggings != null) {
			equipment.add(new Pair<>(EnumItemSlot.LEGS, held));
		}
		if (chestplate != null) {
			equipment.add(new Pair<>(EnumItemSlot.CHEST, chestplate));
		}
		if (helmet != null) {
			equipment.add(new Pair<>(EnumItemSlot.HEAD, helmet));
		}
		sendEquipmentPacket(equipment);
	}

	protected void sendEquipmentPacket(EnumItemSlot slot, ItemStack itemStack) {
		List<Pair<EnumItemSlot, net.minecraft.server.v1_16_R2.ItemStack>> equipment = new ArrayList<>();
		equipment.add(new Pair<>(slot, CraftItemStack.asNMSCopy(itemStack)));
		sendEquipmentPacket(equipment);
	}

	protected void sendEquipmentPacket(List<Pair<EnumItemSlot, net.minecraft.server.v1_16_R2.ItemStack>> equipment) {
		PacketPlayOutEntityEquipment entityData = new PacketPlayOutEntityEquipment(uniqueID, equipment);
		PacketUtils.broadcastNMSPacket(entityData);
	}

	public void setHeadPose(float al, float bt, float sg) {
		Vector3f vector3f = new Vector3f(al, bt, sg);
		this.watcher.set(EntityArmorStand.c, vector3f);
	}

	public void setBodyPose(float al, float bt, float sg) {
		Vector3f vector3f = new Vector3f(al, bt, sg);
		this.watcher.set(EntityArmorStand.d, vector3f);
	}

	public void setLeftArmPose(float al, float bt, float sg) {
		Vector3f vector3f = new Vector3f(al, bt, sg);
		this.watcher.set(EntityArmorStand.e, vector3f);
	}

	public void setRightArmPose(float al, float bt, float sg) {
		Vector3f vector3f = new Vector3f(al, bt, sg);
		this.watcher.set(EntityArmorStand.f, vector3f);
	}

	public void setLeftLegPose(float al, float bt, float sg) {
		Vector3f vector3f = new Vector3f(al, bt, sg);
		this.watcher.set(EntityArmorStand.g, vector3f);
	}

	public void setRightLegPose(float al, float bt, float sg) {
		Vector3f vector3f = new Vector3f(al, bt, sg);
		this.watcher.set(EntityArmorStand.e, vector3f);
	}

}