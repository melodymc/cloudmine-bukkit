/*
 * Decompiled with CFR 0_123.
 * 
 * Could not load the following classes:
 *  com.google.common.io.ByteArrayDataOutput
 *  com.google.common.io.ByteStreams
 *  org.bukkit.ru.bird.jobs.command.Command
 *  org.bukkit.ru.bird.jobs.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.Plugin
 *  ru.bird.chat.AbstractCommandmmand
 */
package ru.bird.serverswith;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.bird.chat.AbstractCommand;
import ru.bird.main.API;

import java.util.ArrayList;
import java.util.List;

public class CommandSwithServer
extends AbstractCommand {
    private List<Player> cooldown;

    public CommandSwithServer() {
        super("swithserver");
        cooldown = new ArrayList<>();
    }

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length == 0) {
            return false;
        }
        if (!(sender instanceof Player)) {
            return false;
        }
        Player player = (Player)sender;
        if(!cooldown.contains(player)) {
            String server = args[0];
            SocketManager socketManager = API.getInstance().getSocketManager();
            if (socketManager.getOnlineServer(server) != -1) {
                cooldown.add(player);
                Main.getInstance().getServer().getScheduler().runTaskLater(Main.getInstance(), () -> {
                    cooldown.remove(player);
                }, 2 * 20);
                socketManager.redirect(player, server);
                return true;
            } else {
                player.sendMessage("Сервер в данный момент недоступен.");
                return false;
            }
        } else {
            return false;
        }
    }
}