package nl.shanelab.multiblock;

import javax.annotation.Nonnull;

import ru.bird.storageUtils.interfaces.Deserializable;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.util.Vector;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Pattern for the multiblock structure
 * 
 * NORTH: 	negative on Z-axis
 * EAST: 	positive on X-axis
 * SOUTH: 	positive on Z-axis
 * WEST: 	negative on X-axis
 * 
 * @author ShaneCraft
 *
 */
public abstract class PatternObject implements ConfigurationSerializable, Deserializable {
	
	private final int relativeX, relativeY, relativeZ;

	public PatternObject() {
		this.relativeX = 0;
		this.relativeY = 0;
		this.relativeZ = 0;
	}

	public PatternObject(int relativeX, int relativeY, int relativeZ) {
		this.relativeX = relativeX;
		this.relativeY = relativeY;
		this.relativeZ = relativeZ;
	}
	
	public PatternObject(@Nonnull Vector relativeVec) {
		this.relativeX = relativeVec.getBlockX();
		this.relativeY = relativeVec.getBlockY();
		this.relativeZ = relativeVec.getBlockZ();
	}
	
	protected abstract PatternObject createRotatedClone(Vector vector);
	
	public abstract boolean isValid(Location location);
	
	public final int getX() {
		return relativeX;
	}
	
	public final int getY() {
		return relativeY;
	}
	
	public final int getZ() {
		return relativeZ;
	}
	
	public final Vector getLocation() {
		return new Vector(relativeX, relativeY, relativeZ);
	}

	public PatternObject rotate(MultiBlockPatternFacing patternFacing) {
		if(patternFacing != MultiBlockPatternFacing.NORTH || patternFacing != MultiBlockPatternFacing.CARDINAL) {
			return rotate(patternFacing.getRotate());
		}
		return this;
	}

	public PatternObject rotate(int n) {
		n = n > 360 ? 0 : n;
		
		double rad = Math.toRadians((double) n);
		
		double rx = (relativeX * Math.cos(rad)) - (relativeZ * Math.sin(rad));
	    double rz = (relativeX * Math.sin(rad)) + (relativeZ * Math.cos(rad));
	    
	    int x = (int) Math.round(rx);
	    int z = (int) Math.round(rz);
		
		return createRotatedClone(new Vector(x, relativeY, z));
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> result = new LinkedHashMap();
		result.put("==", getClass().getName());
		result.put("relativeX", this.getX());
		result.put("relativeY", this.getY());
		result.put("relativeZ", this.getZ());
		return result;
	}

}
