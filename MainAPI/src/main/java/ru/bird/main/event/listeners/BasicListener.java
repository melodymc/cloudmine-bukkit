package ru.bird.main.event.listeners;

import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.API;

public class BasicListener implements Listener {

    public BasicListener() {
        Plugin plugin = API.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }


    public BasicListener(JavaPlugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

}
