package nl.shanelab.multiblock.patternobjects;

import javax.annotation.Nonnull;

import ru.bird.storageUtils.utils.DeserializeUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.util.Vector;

import nl.shanelab.multiblock.IMaterial;
import nl.shanelab.multiblock.MaterialWrapper;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Item (entity) pattern for the multiblock structure
 * 
 * @author ShaneCraft
 *
 */
public class PatternItem extends PatternEntity {

	public PatternItem() {}

	private IMaterial IMaterial;
	
	public PatternItem(@Nonnull Material itemMaterial, int x, int y, int z) {
		this(itemMaterial, new Vector(x, y, z));
	}
	
	public PatternItem(@Nonnull Material itemMaterial, Vector relativeVec) {
		super(EntityType.DROPPED_ITEM, relativeVec);
		
		this.IMaterial = new MaterialWrapper(itemMaterial);
	}

	public PatternItem(@Nonnull IMaterial material, int x, int y, int z) {
		this(material, new Vector(x, y, z));
	}
	
	public PatternItem(@Nonnull IMaterial material, Vector relativeVec) {
		super(EntityType.DROPPED_ITEM, relativeVec);
		
		this.IMaterial = material;
	}
	
	@Override
	protected PatternItem createRotatedClone(Vector vector) {
		return new PatternItem(IMaterial, vector);
	}
	
	@Override
	public boolean isValid(Location location) {
		Entity entity = getEntity(location);
		
		if (entity != null && entity instanceof Item) {
			return ((Item) entity).getItemStack().getType() == IMaterial.getType();
		}
		
		return false;
	}

	public Material getBukkitItem() {
		return IMaterial.getType();
	}

	public IMaterial getIMaterial() {
		return IMaterial;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> list = super.serialize();
		list.put("IMaterial", getIMaterial().serialize());
		return list;
	}

	@Override
	public <T> T deserialize(Map<String, Object> args) throws IllegalArgumentException, NullPointerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		IMaterial IMaterial = DeserializeUtils.staticDeserialize((Map<String, Object>) args.get("IMaterial"));
		return (T) new PatternItem(IMaterial, (int) args.get("relativeX"), (int) args.get("relativeY"), (int)  args.get("relativeZ"));
	}
}
