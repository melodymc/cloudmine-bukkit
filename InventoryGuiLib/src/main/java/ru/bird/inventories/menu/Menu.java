package ru.bird.inventories.menu;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.inventories.AbstractInventory;
import ru.bird.inventories.menu.interfaces.GuiAction;
import ru.bird.inventories.menu.interfaces.GuiCallback;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

public class Menu extends AbstractInventory {

    private Map<Integer, GuiAction> slots;
    private GuiCallback guiOpen, guiClose;
    private BiConsumer<Player, InventoryClickEvent> guiClick;
    private Set<Material> allowInteraction;

    public Menu(JavaPlugin plugin,String name, int size) {
        super(plugin, name, size);
        this.slots = new HashMap<>();
        this.allowInteraction = new HashSet<>();
    }

    @Override
    public void onInventoryBuild(Player player, Inventory inventory){
        if(guiOpen != null) {
            guiOpen.execute(player, inventory);
        }
    }

    @Override
    public void onInventoryDestruct(Player player, Inventory inventory)
    {
        if (guiClose != null)
        {
            guiClose.execute(player, inventory);
        }
    }

    public void open(Player player){
        player.openInventory(getBukkitInventory());
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent e) {
        e.setCancelled(true);
    }

    public void clearAction() { this.slots.clear(); }

    public void addAction(int slot, GuiAction action) {
        this.slots.put(slot, action);
    }

    public void removeAction(int slot)
    {
        slots.remove(slot);
    }

    @Override
    public void onSlotClicked(Player player, InventoryClickEvent e) {
        if (guiClick != null)
        {
            guiClick.accept(player, e);
        }

        if (e.getCurrentItem() == null) {
            return;
        }

        e.setCancelled( !interactIsAllowed( e.getCurrentItem().getType() ) );

        if (getBukkitInventory() != e.getClickedInventory())
        {
            return;
        }

        GuiAction action = slots.get(e.getSlot());
        if (action == null) {
            onClick(e);
        } else {
            action.onClick(e);
        }
    }

    protected void onClick(InventoryClickEvent e) {
        e.setCancelled( !interactIsAllowed( e.getCurrentItem().getType() ) );
    }

    public GuiCallback getGuiOpen() {
        return guiOpen;
    }

    public GuiCallback getGuiClose()
    {
        return guiClose;
    }

    public void setGuiOpen(GuiCallback guiOpen) {
        this.guiOpen = guiOpen;
    }

    public void setGuiClose(GuiCallback guiClose)
    {
        this.guiClose = guiClose;
    }

    public void setGuiClick(BiConsumer<Player, InventoryClickEvent> guiClick)
    {
        this.guiClick = guiClick;
    }

    public BiConsumer<Player, InventoryClickEvent> getGuiClick()
    {
        return guiClick;
    }

    public boolean allowInteract(Material material)
    {
        return allowInteraction.add(material);
    }

    public boolean denyInteract(Material material)
    {
        return allowInteraction.remove(material);
    }

    public boolean interactIsAllowed(Material material)
    {
        return allowInteraction.contains(material);
    }
}
