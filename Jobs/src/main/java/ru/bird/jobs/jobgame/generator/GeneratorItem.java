package ru.bird.jobs.jobgame.generator;

import org.bukkit.Material;

public class GeneratorItem implements GenerableItem<GeneratorItem>
{
    public final static GeneratorItem IMPOSSIBLE = new GeneratorItem(Material.AIR, 0.0d, 0, 0);

    public final Material material;
    public final double spawnChance;
    public final int minCount, maxCount;

    public GeneratorItem(Material material, double spawnChance, int minCount, int maxCount)
    {
        this.material = material;
        this.spawnChance = spawnChance;
        this.minCount = minCount; this.maxCount = maxCount;
    }

    public GeneratorItem(Material material, double spawnChance)
    {
        this(material, spawnChance, 1, 1);
    }

    @Override
    public GeneratorItem getAggregatedItem()
    {
        return this;
    }

    @Override
    public <B> void invokeExtractor(ItemExtractor<B> extractor)
    {
        extractor.invoke(this);
    }
}
