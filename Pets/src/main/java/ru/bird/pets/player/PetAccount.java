package ru.bird.pets.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.player.BasicAccount;
import ru.bird.pets.Pets;
import ru.bird.pets.entity.EnumPet;

import java.util.*;

public class PetAccount extends BasicAccount {

    private EnumPet activePet;
    private List<EnumPet> allowedPets;
    private boolean connect;
    public static final PetAccount empty = new PetAccount();
    private boolean needInsert;

    public PetAccount(){}

    public PetAccount(UUID uuid){
        super(uuid);
    }

    public EnumPet getActivePet() {
        return activePet;
    }

    public List<EnumPet> getAllowedPets() {
        return allowedPets;
    }

    public Player getBukkitPlayer(){
        return Bukkit.getPlayer(getUUID());
    }

    public void setActivePet(EnumPet activePet) {
        if(!activePet.equals(this.activePet)) {
            this.activePet = activePet;
            this.setNeedInsert();
            Pets.getPetsManager().spawnPetFor(getBukkitPlayer(), activePet);
        }
    }

    public void setAllowedPets(List<EnumPet> allowedPets) {
        this.allowedPets = allowedPets;
    }

    public void setAllowedPets(String allowedPets_str) {
        List<String> loc = new ArrayList<>(Arrays.asList(allowedPets_str.split(",")));
        List<EnumPet> loc2 = new ArrayList<>();
        loc.forEach(lo ->{
            loc2.add(EnumPet.valueOf(lo));
        });
        this.allowedPets = loc2;
        BaseLogger.info("allowedPets " + allowedPets.toString());
    }

    public void setActivePet(String activePet_str) {
        try {
            setActivePet(EnumPet.valueOf(activePet_str));
        } catch (Exception ex) {
            BaseLogger.error("Не найден такой пет.");
        }
    }

    public EnumPet randomPet() {
//        List<EnumPet> newList = Arrays.asList(EnumPet.values());
        List<EnumPet> newList = new ArrayList<>();
        newList.add(EnumPet.COW);
        newList.add(EnumPet.OCELOT);
        newList.add(EnumPet.GOLEM);
        newList.add(EnumPet.PIG);
        newList.add(EnumPet.GHOST);
        newList.add(EnumPet.VILLAGER);
        newList.add(EnumPet.PIG_ZOMBIE);
        Collections.shuffle(newList);
        return newList.get(0);
    }

    public void removeAlowdedPets(EnumPet pet) {
        if(allowedPets.remove(pet)) {
            this.setNeedInsert();
        }
    }

    public void setConnect(boolean connect) {
        if(this.connect != connect) {
            this.connect = connect;
            this.setNeedInsert();
        }
    }

    public boolean isConnect() {
        return connect;
    }

    public void addAlowdedPets(EnumPet pet) {
        if(allowedPets.add(pet)) {
            this.setNeedInsert();
        }
    }

    public boolean isNeedInsert() {
        return this.needInsert;
    }

    private void setNeedInsert() {
        if(!this.needInsert) {
            this.needInsert = true;
        }
    }
}