package ru.bird.jobs;

import org.bukkit.Location;
import org.bukkit.Material;

public class Generation {
    private Location min;
    private Location max;
    private Material material;

    private double persentWater;
    private double maxY;

    public Generation(Location min, Location max, Material material) {
        this.min = min;
        this.max = max;
        this.material = material;
    }

    public Location getMin() {
        return min;
    }

    public void setMin(Location min) {
        this.min = min;
    }

    public Location getMax() {
        return max;
    }

    public void setMax(Location max) {
        this.max = max;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public double getPersentWater() {
        return persentWater;
    }

    public void setPersentWater(double persentWater) {
        this.persentWater = persentWater;
    }

    public double getMaxY() {
        return maxY;
    }

    public void setMaxY(double maxY) {
        this.maxY = maxY;
    }
}
