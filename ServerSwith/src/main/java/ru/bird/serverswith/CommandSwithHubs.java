/*
 * Decompiled with CFR 0_123.
 * 
 * Could not load the following classes:
 *  com.google.common.io.ByteArrayDataOutput
 *  com.google.common.io.ByteStreams
 *  org.bukkit.ru.bird.jobs.command.Command
 *  org.bukkit.ru.bird.jobs.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.Plugin
 *  ru.bird.chat.commandstractCommand
 *  ru.bird.main.socket.SocketManager
 *  ru.bird.main.utils.FIterator
 */
package ru.bird.serverswith;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.bird.chat.AbstractCommand;
import ru.bird.main.API;
import ru.bird.main.utils.FIterator;

import java.util.List;

public class CommandSwithHubs
extends AbstractCommand {
    public CommandSwithHubs() {
        super("hubs");
    }

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }
        Player player = (Player)sender;
        API api = API.getInstance();
        List<String> servers = api.getSocketManager().getServersByRegexp("Lobby", "Lobby-([0-9].*)");
        FIterator<String> iterator = new FIterator<String>(servers);
//        Bukkit.getServer().dispatchCommand(player, "spawn");
        if (servers.isEmpty()) {
            player.sendMessage("§e[Lobby] Нет доступных хабов.");
            return false;
        } else {
            api.getSocketManager().redirect(player, iterator.getNext());
            return true;
        }
    }
}