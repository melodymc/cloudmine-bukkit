package ru.bird.lobby.lobby;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.actionbar.ActionBarApi;
import ru.bird.main.logger.BaseLogger;

import java.io.File;
import java.io.IOException;

public final class Main extends JavaPlugin {

    private static Main plugin;
    private YamlConfiguration config;

    @Override
    public void onEnable() {
        plugin = this;
        new ActionBarApi().load();
        new LobbyListener();
        config();
    }

    public void config() {
        File f = new File(plugin.getDataFolder().getAbsolutePath() + "/config.yml");
        config = YamlConfiguration.loadConfiguration(f);
        config.addDefault("title.msg", "&a‣ &fВыбирай &bрежим&f и иди в портал");
        try {
            config.options().copyDefaults(true);
            config.save(f);
        } catch (IOException e) {
            BaseLogger.error("ERROR: can not save config.yml. Please check it.");
        }
    }

    @Override
    public YamlConfiguration getConfig() {
        return config;
    }

    public static Main getPlugin() {
        return plugin;
    }
}
