package ru.bird.jobs.jobgame.event;

import ru.bird.jobs.jobgame.JobGame;

public class FinishJobEvent extends JobEventUnified
{
    public FinishJobEvent(JobGame instance)
    {
        super(instance);
    }
}
