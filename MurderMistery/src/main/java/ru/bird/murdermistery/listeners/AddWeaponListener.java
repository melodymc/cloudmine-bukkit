// 
// Decompiled by Procyon v0.5.30
// 

package ru.bird.murdermistery.listeners;

import org.bukkit.inventory.ItemStack;
import ru.bird.murdermistery.game.GPlayer;
import ru.bird.murdermistery.game.GameFactory;
import ru.bird.murdermistery.game.Role;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.Main;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import ru.wellfail.wapi.game.Game;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import ru.wellfail.wapi.listeners.FListener;

public class AddWeaponListener extends FListener implements Runnable
{
    private boolean starting;
    private Thread thread;
    private static AtomicInteger time;
    private static int i;

    public void start() {
        AddWeaponListener.time.set(10);
        this.thread = new Thread(this);
        this.thread.start();
    }

    public void stop() {
        AddWeaponListener.time.set(0);
        thread.stop();
    }

    public static void setTime(final int t) {
        AddWeaponListener.i = t;
        AddWeaponListener.time.set(t);
    }

    public static int getTime() {
        return AddWeaponListener.time.get();
    }

    public void run() {
        if (this.starting) {
            return;
        }
        this.starting = true;
        try {
            while ((AddWeaponListener.i = AddWeaponListener.time.get()) > 0) {

                if (AddWeaponListener.i <= 5 || AddWeaponListener.i == 10) {
                    Game.broadcast("Через §a" + AddWeaponListener.i + "§f секунд у детектива и убийцы появится оружие");
                }
                Thread.sleep(1000L);
                AddWeaponListener.time.getAndDecrement();
            }
            new BukkitRunnable() {
                public void run() {
                    Bukkit.getOnlinePlayers().forEach(player -> {
                        Role role = GameFactory.getGplayer(player).getRole();
                        for(ItemStack itm : role.getStartInv()) {
                            player.getInventory().addItem(itm);
                        }
                        if(role == GameFactory.killer){
                            player.getInventory().setHeldItemSlot(1);
                        } else if(role == GameFactory.commisar) {
                            player.getInventory().setHeldItemSlot(2);
                        }
                    });
                }
            }.runTask(Main.getInstance());
            if (GameSettings.gameTime == -1) {
                return;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    static {
        AddWeaponListener.time = new AtomicInteger();
        AddWeaponListener.i = GameSettings.gameTime;
    }
}
