/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.block.Biome
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  ru.bird.main.game.Game
 *  ru.wellfail.wapi.utils.Utils
 */
package com.wellfail.buildbattle.gui.type;

import com.wellfail.buildbattle.GameFactory;
import com.wellfail.buildbattle.gui.Gui;
import com.wellfail.buildbattle.gui.GuiType;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import ru.bird.main.game.Game;
import ru.bird.main.utils.ItemUtils;

import java.util.List;

public class BiomeGui
extends Gui {
    private Gui gui;

    public BiomeGui(Gui gui) {
        super(gui, 4, 4, "Биомы");
        this.gui = gui;
    }

    public void init() {
        this.fillItems();
        this.addAction(31, this.gui.getGui(GuiType.MANAGER));
    }

    private void fillItems() {
        this.inventory.setItem(0, ItemUtils.createItem((Material)Material.GRASS, (String)"§aРавнина"));
        this.inventory.setItem(1, ItemUtils.createItem((Material)Material.SAND, (int)1, (int)1, (String)"§aКаньон", (List<String>)null));
        this.inventory.setItem(2, ItemUtils.createItem((Material)Material.WATER_BUCKET, (String)"§aОкеан"));
        this.inventory.setItem(3, ItemUtils.createItem((Material)Material.SAND, (String)"§aПустыня"));
        this.inventory.setItem(4, ItemUtils.createItem((Material)Material.LOG, (String)"§aЛес"));
        this.inventory.setItem(5, ItemUtils.createItem((Material)Material.VINE, (String)"§aДжунгли"));
        this.inventory.setItem(6, ItemUtils.createItem((Material)Material.PACKED_ICE, (String)"§aЗимний биом"));
        this.inventory.setItem(7, ItemUtils.createItem((Material)Material.WATER_LILY, (String)"§aБолото"));
        this.inventory.setItem(8, ItemUtils.createItem((Material)Material.LOG_2, (String)"§aСаванна"));
        this.inventory.setItem(31, ItemUtils.createItem((Material)Material.ARROW, (String)"§aНазад"));
    }

    @Override
    protected void onClick(InventoryClickEvent e) {
        Biome biome;
        int slot = e.getSlot();
        switch (slot) {
            case 0: {
                biome = Biome.PLAINS;
                break;
            }
            case 1: {
                biome = Biome.MESA;
                break;
            }
            case 2: {
                biome = Biome.OCEAN;
                break;
            }
            case 3: {
                biome = Biome.DESERT;
                break;
            }
            case 4: {
                biome = Biome.FOREST;
                break;
            }
            case 5: {
                biome = Biome.JUNGLE;
                break;
            }
            case 6: {
                biome = Biome.ICE_MOUNTAINS;
                break;
            }
            case 7: {
                biome = Biome.SWAMPLAND;
                break;
            }
            case 8: {
                biome = Biome.SAVANNA;
                break;
            }
            default: {
                return;
            }
        }
        GameFactory.getInstance().getRegion((Player)e.getWhoClicked()).setBiome(biome);
        ((Player)e.getWhoClicked()).sendMessage(Game.getPrefix() + "Установлен биом - " + e.getCurrentItem().getItemMeta().getDisplayName());
    }
}

