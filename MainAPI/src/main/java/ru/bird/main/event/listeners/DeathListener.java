/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.entity.Arrow
 *  org.bukkit.entity.Egg
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.FishHook
 *  org.bukkit.entity.Player
 *  org.bukkit.entity.Snowball
 *  org.bukkit.event.Event
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.entity.EntityDamageByEntityEvent
 *  org.bukkit.event.entity.EntityDamageEvent
 *  org.bukkit.event.entity.EntityDamageEvent$DamageCause
 *  org.bukkit.event.entity.PlayerDeathEvent
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.projectiles.ProjectileSource
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  org.bukkit.util.Vector
 */
package ru.bird.main.event.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.*;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import ru.bird.main.API;
import ru.bird.main.event.DeathEvent;
import ru.bird.main.event.KillEvent;
import ru.bird.main.groups.Groups;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.GameState;
import ru.bird.main.game.Gamer;
import ru.bird.main.logger.BaseLogger;

import java.util.HashMap;
import java.util.Map;

public class DeathListener
extends BasicListener {
    private Map<Player, Player> damagers = new HashMap<Player, Player>();
    private boolean fb = false;

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        e.setDeathMessage(null);
        e.getEntity().setHealth(20.0);
        if (!GameSettings.drop) {
            e.getDrops().clear();
        }
        Player player = e.getEntity();
        Player killer = player.getKiller();
        Bukkit.getPluginManager().callEvent(new KillEvent(player, killer));

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onKill(final KillEvent e) {
        final Player player = e.getPlayer();
        final DeathEvent event = new DeathEvent(player);
        event.setKill(e.getKiller());
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isDeny()) {
            Gamer.getGamer(player).setSpectator(true);
        }

        new BukkitRunnable(){
            public void run() {
                if (player.isOnline()) {
                    player.setVelocity(new Vector(0, 0, 0));
                    player.setHealth(20.0);
                    player.setFireTicks(0);
//                    GameUtils.removeArrows(player);
                    Game.getInstance().onRespawn(player);
                }
                if (e.getKiller() == null) {
                    Gamer.getGamer(e.getPlayer()).setDeaths();
                    if(GameSettings.deathnomsg) {
                        if (event.getPl() != null) {
                            Game.broadcast("Игрок " + event.getPl() + "§f умер");
                        } else {
                            Game.broadcast("Игрок " + Groups.getFullName(player) + "§f умер");
                        }
                    }
                    if (!event.isDeny()) {
                        player.teleport(GameSettings.spectrLocation);
                        player.setAllowFlight(true);
                        player.setFlying(true);
                    }
                } else {
                    BaseLogger.info("2 1");
                    Player killer = e.getKiller();
                    Gamer.getGamer(killer).setKills();
                    if (DeathListener.this.fb) {
                        if (event.getKiller() != null && event.getPl() != null) {
                            if(GameSettings.deathnomsg) {
                                Game.broadcast(event.getPl() + "§f был убит игроком " + event.getKiller());
                            } else {
                                if(GameSettings.deathnomsg) {
                                    Game.broadcast(Groups.getFullName(e.getPlayer()) + "§f был убит игроком " + Groups.getFullName(killer));
                                }
                            }
                        }
                    } else {
                        if (event.getKiller() != null && event.getPl() != null) {
                            if(GameSettings.deathnomsg) {
                                Game.broadcast(event.getKiller() + "§f пролил первую кровь убив " + event.getPl());
                            }
                        } else {
                            if(GameSettings.deathnomsg) {
                                Game.broadcast(Groups.getFullName(killer) + "§f пролил первую кровь убив " + Groups.getFullName(e.getPlayer()));
                            }
                        }
                        Gamer.getGamer(killer).setFb();
                        DeathListener.this.fb = true;
                    }

                }
                if (event.isMessage() && GameUtils.getAlivePlayers().size() != 1) {
                    if(GameSettings.deathnomsg) {
                        Game.broadcast("На арене осталось §a" + GameUtils.getAlivePlayers().size() + "§f игроков");
                    }
                }
                if (GameState.current != GameState.END && GameUtils.getAlivePlayers().size() <= 1 && GameSettings.endgamelastplayer) {
                    Game.getInstance().endGame();
                }
//                if(GameSettings.crops) {
//                    BaseLogger.info("crops spawn "+ Groups.getFullName(player));
//                }
            }
        }.runTaskLater(API.getInstance(), 2);
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void ondamage(EntityDamageEvent e) {
        if (e.getCause() != EntityDamageEvent.DamageCause.VOID) {
            return;
        }
        if (e.getEntity() instanceof Player) {
            if (Gamer.getGamer((Player)e.getEntity()).isSpectator()) {
                return;
            }
            e.setCancelled(true);
            Player player = (Player)e.getEntity();
            player.getKiller();
            Bukkit.getPluginManager().callEvent((Event)new KillEvent(player, this.damagers.get(player)));
        }
    }

    @EventHandler
    public void DamageByEntity(final EntityDamageByEntityEvent e) {
        if(e.getEntity() instanceof Player) {
            if(e.getDamager() instanceof Player) {
                this.damagers.put((Player)e.getEntity(), (Player)e.getDamager());
            }

            if(e.getDamager() instanceof Arrow) {
                Arrow arrow = (Arrow)e.getDamager();
                if(arrow.getShooter() instanceof Player) {
                    this.damagers.put((Player)e.getEntity(), (Player)arrow.getShooter());
                }
            }

            if(e.getDamager() instanceof Egg) {
                Egg arrow1 = (Egg)e.getDamager();
                if(arrow1.getShooter() instanceof Player) {
                    this.damagers.put((Player)e.getEntity(), (Player)arrow1.getShooter());
                }
            }

            if(e.getDamager() instanceof Snowball) {
                Snowball arrow2 = (Snowball)e.getDamager();
                if(arrow2.getShooter() instanceof Player) {
                    this.damagers.put((Player)e.getEntity(), (Player)arrow2.getShooter());
                }
            }

            if(e.getDamager() instanceof FishHook) {
                FishHook arrow3 = (FishHook)e.getDamager();
                if(arrow3.getShooter() instanceof Player) {
                    this.damagers.put((Player)e.getEntity(), (Player)arrow3.getShooter());
                }
            }

            (new BukkitRunnable() {
                public void run() {
                    DeathListener.this.damagers.remove(e.getEntity());
                }
            }).runTaskLater(API.getInstance(), 200L);
        }
    }

}

