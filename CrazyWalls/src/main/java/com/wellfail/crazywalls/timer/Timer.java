package com.wellfail.crazywalls.timer;

import com.wellfail.crazywalls.config.Config;
import com.wellfail.crazywalls.wall.Wall;
import org.bukkit.Location;
import org.bukkit.WorldBorder;
import org.bukkit.event.EventHandler;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.main.API;
import ru.bird.main.event.TimerTickEvent;
import ru.bird.main.event.listeners.BasicListener;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.logger.BaseLogger;

public class Timer extends BasicListener
{
    private int green;
    private int red;
    private int border;
    private boolean startetBoarder = false;
    private boolean destroyedGreen = false;
    private boolean destroyedRed = false;

    public Timer(int time) {
        this.green = time - 210;
        this.red = time - 90;
        this.border = time;
        this.setTime(GameSettings.gameTime);
    }

    public int getGreen() {
        return this.green;
    }

    public int getRed() {
        return this.red;
    }

    public int getBorder() {
        return this.border;
    }

    public void setTime(int time) {
        if (time - 570 >= 0) {
            this.green = time - 570;
        }
        if (time - 450 >= 0) {
            this.red = time - 450;
        }
        if (time - 360 >= 0) {
            this.border = time - 360;
        }
    }

    @EventHandler
    public void onTick(TimerTickEvent event) {
        this.setTime(event.getTime());
        if (this.getGreen() == 60) {
            Game.broadcast("§aЗеленая стена §fупадет через §a1 §fминуту!");
        }
        if (this.getGreen() == 0 && !this.destroyedGreen) {
            Config.greenWalls.forEach(Wall::destroy);
            this.destroyedGreen = true;
        }
        if (this.getRed() == 60) {
            Game.broadcast("§cКрасная стена §fупадет через §a1 §fминуту!");
        }
        if (this.getRed() == 0 && !this.destroyedRed) {
            Config.redWalls.forEach(Wall::destroy);
            this.destroyedRed = true;
        }
        if (this.getBorder() == 60) {
            Game.broadcast("§fДезматч начнется через §a1 §fминуту!");
        }
        if (this.getBorder() == 0 && !this.startetBoarder) {
            Location center = Config.center;

            WorldBorder border = center.getWorld().getWorldBorder();
            border.setCenter(center);
            border.setDamageAmount(20.0);
            border.setWarningTime(5);
            border.setWarningDistance(1);
            border.setSize(Config.radius);

            // TODO: Потенциальная утечка памяти.
            new BukkitRunnable(){
                public void run() {
                    if (border.getSize() > 5.0) {
                        border.setSize(border.getSize() - 0.1);
                    } else {
                        this.cancel();
                    }
                }
            }.runTaskTimer(API.getInstance(), 0, 720 / Config.radius);

            this.startetBoarder = true;
        }
    }
}

