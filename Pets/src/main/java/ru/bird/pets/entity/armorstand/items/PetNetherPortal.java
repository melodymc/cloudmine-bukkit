package ru.bird.pets.entity.armorstand.items;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import ru.bird.main.API;
import ru.bird.pets.Pets;
import ru.bird.main.packets.WrapperPlayServerEntityTeleport;
import net.minecraft.server.v1_16_R2.DataWatcher;
import ru.bird.main.particles.ParticleEffect;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.MathUtils;

public class PetNetherPortal extends AbstractArmoredPet {

	public PetNetherPortal(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setArms(false);
		armor.setVisible(false);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}

	@Override
	public ItemStack getHelmet() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjBiZmMyNTc3ZjZlMjZjNmM2ZjczNjVjMmM0MDc2YmNjZWU2NTMxMjQ5ODkzODJjZTkzYmNhNGZjOWUzOWIifX19");
	}

	@Override
	public ItemStack getMainHand() {
		return null;
	}

	@Override
	public ItemStack getOffHand() {
		return null;
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		return null;
	}

	@Override
	public ItemStack getChestplate() {
		return null;
	}

	public ItemStack getInactiveHelmet() {
		return null;
	}

	@Override
	public double getAngle() {
		return angle;
	}

	@Override
	protected float getOffsetY() {
		return 1.4F;
	}
	
	@Override
	public double getRadius() {
		return 0.3F;
	}

	private double angle = 0;

	@Override
	public void onLivingUpdate() {
		angle += 5;
		angle = angle % 360;
		Location locTo = getOwner().getLocation();
		double localAngle = locTo.getYaw() + getAngle();
		double newX = locTo.getX() - getRadius() * Math.sin(Math.toRadians(localAngle));
		double newY = locTo.getBlockY() + getOffsetY();
		double newZ = locTo.getZ() + getRadius() * Math.cos(Math.toRadians(localAngle));
		this.petLocation = new Location(getOwner().getWorld(), newX, newY, newZ);
		
		WrapperPlayServerEntityTeleport position = new WrapperPlayServerEntityTeleport();
		position.setEntityID(getEntityID());
		position.setX(newX);
		position.setY(newY);
		position.setZ(newZ);
		position.setYaw(locTo.getYaw());
		position.setPitch(locTo.getPitch());
		position.broadcastPacket();
		Location location = this.getPetLocation().clone().add(MathUtils.random(-0.2F, 0.2F), 0.8F + MathUtils.random(0.80F), MathUtils.random(-0.2F, 0.2F));
		ParticleEffect.REDSTONE.sendColor(Bukkit.getOnlinePlayers(), location, Color.fromBGR(148,0,211));
		this.setHeadPose(0, (float) -angle, 0);
		this.sendWatcherUpdate();
	}
}