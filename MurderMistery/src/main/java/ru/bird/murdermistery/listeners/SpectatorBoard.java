// 
// Decompiled by Procyon v0.5.30
// 

package ru.bird.murdermistery.listeners;

import ru.bird.murdermistery.game.GameFactory;
import ru.wellfail.wapi.utils.Utils;
import ru.wellfail.wapi.Main;
import ru.wellfail.wapi.game.GameSettings;
import org.bukkit.Bukkit;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;
import ru.wellfail.wapi.board.BoardLine;
import java.util.List;
import ru.wellfail.wapi.board.Board;

public class SpectatorBoard extends Board
{
    private int d;
    private List<String> list = GameSettings.animation;
    private BoardLine players;
    private BoardLine spectators;
    int s;
    
    public SpectatorBoard() {
        final Set<BoardLine> lines = this.getLines();
        this.create(lines, "§e§lMURDER MYSTERY");
        this.update(() -> {
            if (this.d == this.list.size()) {
                this.d = 0;
            }
            this.setDisplay((String)this.list.get(this.d));
            ++this.d;
            return;
        }, 3L);
        this.startUpdate();
    }
    
    public Set<BoardLine> getLines() {
        final Set<BoardLine> set = new HashSet<BoardLine>();
        set.add(new BoardLine((Board)this, "§1", 6));
        set.add(this.players = new BoardLine((Board)this, "\u0418\u0433\u0440\u043e\u043a\u043e\u0432: §a", 5));
        this.dynamicLine(this.players.getNumber(), "\u0418\u0433\u0440\u043e\u043a\u043e\u0432: §a", GameFactory.players.size() + "");
        set.add(this.spectators = new BoardLine((Board)this, "\u041d\u0430\u0431\u043b\u044e\u0434\u0430\u0435\u0442: §a", 4));
        final int spectator = Bukkit.getOnlinePlayers().size() - GameFactory.getPlayers();
        this.dynamicLine(this.players.getNumber(), "\u041d\u0430\u0431\u043b\u044e\u0434\u0430\u0442\u0435\u043b\u0435\u0439: §a", spectator + "");
        set.add(new BoardLine((Board)this, "§3", 3));
        set.add(new BoardLine((Board)this, "\u041a\u0430\u0440\u0442\u0430: §a" + GameSettings.mapLocation.getWorld().getName(), 2));
        set.add(new BoardLine((Board)this, "\u0421\u0435\u0440\u0432\u0435\u0440: §a" + Main.getUsername(), 1));
        final int[] spectator2 = new int[1];
        this.update(() -> {
            this.dynamicLine(this.players.getNumber(), "\u0418\u0433\u0440\u043e\u043a\u043e\u0432: §a", "" + Utils.getAlivePlayers().size());
            spectator2[0] = Bukkit.getOnlinePlayers().size() - Utils.getAlivePlayers().size();
            this.dynamicLine(this.spectators.getNumber(), "\u041d\u0430\u0431\u043b\u044e\u0434\u0430\u0442\u0435\u043b\u0435\u0439: §a", "" + spectator2[0]);
            return;
        }, 100L);
        return set;
    }
}
