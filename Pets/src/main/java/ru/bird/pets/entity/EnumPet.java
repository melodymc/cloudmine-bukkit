package ru.bird.pets.entity;

import java.util.UUID;

import javafx.scene.input.KeyCombination;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.ItemUtils;
import ru.bird.pets.entity.armorstand.blocks.*;
import ru.bird.pets.entity.armorstand.cart.*;
import ru.bird.pets.entity.armorstand.humanoid.*;
import ru.bird.pets.entity.armorstand.items.*;
import ru.bird.pets.entity.armorstand.mobs.*;
import ru.bird.pets.entity.armorstand.planets.*;
import ru.bird.pets.entity.normal.*;

public enum EnumPet {
	PIG(PetPig.class, "&d&lСвинка", ItemUtils.createItem(Material.PIG_SPAWN_EGG, 2, (short) 12)),
	COW(PetCow.class, "&6&lКоровка", ItemUtils.createItem(Material.COW_SPAWN_EGG, 3, (short) 11)),
	GUARDIAN(PetGuardian.class, "&3&lЗащитник"),
	CHICKEN(PetChicken.class, "&7&lКурочка"),
	BAT(PetBat.class, "&9&lЛетучая мышь"),
	STEAMPUNK_GIRL(PetSteampunkGirl.class, "&d&lДевочка техник"),
	ENDERMITE(PetEndermite.class, "&9&lЭндер чишуйница"),
	MAGMA_CUBE(PetMagnaCube.class, "&4&lЛавовый смайл"),
	GOLEM(PetGolem.class, "&b&lЛедяной голем"),
	MUSHROOM_COW(PetMushroomCow.class, "&4&lМухххоморка"),
	OCELOT(PetOcelot.class, "&6&lОцелот"),
	RABBIT(PetRabbit.class, "&7&lКролик"),
	SHEEP(PetSheep.class, "&5&lОвечка"),
	SILVERFISH(PetSilverfish.class, "&7&lЧешуйница"),
	SLIME(PetSlime.class, "&a&lСлизень"),
	VILLAGER(PetVillager.class, "&6&lЖитель"),
	ZOMBIE(PetZombie.class, "&2&lZомби"),
	WOLF(PetWolf.class, "&f&lСобачка"),
	CUBE_COMPAION(PetCubeCompanion.class, "&6&lКуб компаньон"),
	PIG_ZOMBIE(PetPigZombie.class, "&6&lСвино-зомби"),
	CAVE_SPIDER(PetCaveSpider.class, "&3&kПещерный паук"),
	GHOST(PetGhost.class, "&5&lПризрак"),
	HEROBRINE(PetHerobrine.class, "&c&lХеробрин &c☠"),
	FOOTBALLER(PetFootballer.class, "&6&lФутболист"),
	POKEBALL(PetPokeball.class, "&c&lПокебол"),
	FOOTBALL(PetFootBall.class, "&6&lФутбольный мяч"),
	DROPPER(PetDropper.class, "&7&lРаздатчик"),
	REDSTONE_LAMP(PetRedstoneLamp.class, "&c&lМаяк из красного камня"),
	KNUCKLES(PetKnuckles.class, "&c&lУГАНДА НАКЛС"),
	GRAYSAMURAI(PetSamuraiGray.class, "&c&lСамурай шаолиня"),
	BLACKSAMURAI(PetSamuraiBlack.class, "&b&lСамурай монах"),
	CYANBLUESAMURAI(PetSamuraiCyan.class, "&b&lСамурай воды"),
	EARTH_PLANET(PetEarth.class, "&a&lПланета Земля"),
	JUNGLE_PLANET(PetJungle.class, "&a&lПланета Джингли"),
	NETHER_PORTAL(PetNetherPortal.class, "&b&lАдский портал"),
	FURNACE(PetFurnace.class, "&7&lПечка", new ItemStack(Material.FURNACE)),
	DIAMOND(PetDiamond.class, "&b&lАлмазик"),
	ARCHER_QUEEN(PetArcherQueen.class, "&6&lЛучница"),
	TNT_CART(PetTntCart.class, "&c&lTNT вагонетка"),
	FURNACE_CART(PetFurnaceCart.class, "&6&lПечка вагонетка"),
	DIAMOND_CART(PetDiamondOreCart.class, "&b&lАлмазная вагонетка"),
	EMERALD_CART(PetEmeraldCart.class, "&a&lИзумрудная вагонетка"),
	WITHER(PetWither.class, "&6&lИссушитель"),
	BLAZE(PetBlaze.class, "&e&lИфрит"),
	SPACEMAN(PetSpaceman.class, "&e&lГагарин"),
	LINK(PetLink.class, "&e&lЛинк");

	private Class<? extends AbstractPet> petClass;
	private String displayName;
	private ItemStack item;

	EnumPet(Class<? extends AbstractPet> petClass, String displayName) {
		this.petClass = petClass;
		this.displayName = displayName;
	}

	EnumPet(Class<? extends AbstractPet> petClass, String displayName, Material material) {
		this.petClass = petClass;
		this.displayName = displayName;
		this.item = new ItemStack(material);
	}

	EnumPet(Class<? extends AbstractPet> petClass, String displayName, ItemStack item) {
		this.petClass = petClass;
		this.displayName = displayName;
		this.item = item;
	}

	public Class<? extends AbstractPet> getPetClass() {
		return petClass;
	}

	public AbstractPet create(Player owner, int uniqueID) {
		try {
			return petClass.getConstructor(UUID.class, int.class).newInstance(owner.getUniqueId(), uniqueID);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getDisplayName() {
		return GameUtils.colorSet(displayName);
	}

	public int getRarity() {
		return getItem().getAmount();
	}

	public ItemStack getItem() {
		if(item != null) {
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(getDisplayName());
			item.setItemMeta(meta);
			return item;
		} else {
			ItemStack itm = new ItemStack(Material.APPLE);
			ItemMeta meta = itm.getItemMeta();
			meta.setDisplayName(getDisplayName());
			itm.setItemMeta(meta);
			return itm;
		}
	}
}