/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  net.milkbowl.vault.economy.Economy
 *  net.milkbowl.vault.economy.EconomyResponse
 *  org.bukkit.Bukkit
 *  org.bukkit.OfflinePlayer
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.Player
 *  org.bukkit.entity.Player$Spigot
 *  org.bukkit.event.Event
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.plugin.PluginManager
 *  org.bukkit.potion.PotionEffect
 *  org.bukkit.potion.PotionEffectType
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  org.bukkit.scoreboard.Scoreboard
 *  org.bukkit.scoreboard.Team
 *  org.bukkit.util.Vector
 */
package ru.bird.main.game;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;
import ru.bird.main.API;
import ru.bird.main.event.listeners.SpectatorListener;
import ru.bird.main.groups.Groups;
import ru.bird.main.inventories.Kit;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.utils.GameUtils;

public class Gamer {
    private Player player;
    private Kit purchase;
    private boolean cancelbuy;
    private int money;
    private boolean spectator;
    private boolean gamemode;
    private boolean remove = false;
    private boolean fb = false;
    private int deaths = 0;
    private int kills = 0;
    private static Map<String, Gamer> gamers = new ConcurrentHashMap<String, Gamer>();

    public String getName() {
        return player.getName();
    }

    public int getKills() {
        return this.kills;
    }

    public int getDeaths() {
        return this.deaths;
    }

    public void setDeaths() {
        ++this.deaths;
    }

    public void setKills() {
        ++this.kills;
    }

    public void setFb() {
        this.fb = true;
    }

    public boolean getFb() {
        return this.fb;
    }

    public boolean isSpectator() {
        return this.spectator;
    }

    public void setPurchase(Kit purchase) {
        this.purchase = purchase;
    }

    public boolean isCancelbuy() {
        return this.cancelbuy;
    }

    public void setCancelbuy(boolean cancelbuy) {
        this.cancelbuy = cancelbuy;
    }

    public boolean isGamemode() {
        return this.gamemode;
    }

    public void setGamemode(boolean gamemode) {
        this.gamemode = gamemode;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean isOnline() {
        return player != null && player.isOnline();
    }

    public double getMultiplay() {
        return Groups.getMultiply(Bukkit.getPlayer(getName()));
    }
//
//    public void setMultiply(double multiply) {
//        this.multiply = multiply;
//    }

    public static Gamer getGamer(Player player) {
        return gamers.get(player.getName().toLowerCase());
    }

    public static Gamer getGamer(String name) {
        return gamers.get(name.toLowerCase());
    }

    public void setSpectator(boolean spectator) {
        if (this.spectator == spectator) {
            return;
        }
        this.spectator = spectator;
        if (!spectator) {
            this.removeGhost();
            player.getInventory().clear();
            player.setAllowFlight(false);
            player.setFlying(false);
            player.removePotionEffect(PotionEffectType.INVISIBILITY);
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.showPlayer(player);
            }
            return;
        }
        new BukkitRunnable(){
            public void run() {
                if (!isOnline()) {
                    return;
                }
                player.getInventory().clear();
                player.setHealth(20.0);
                player.setFoodLevel(20);
                player.getInventory().setHelmet(null);
                player.getInventory().setChestplate(null);
                player.getInventory().setLeggings(null);
                player.getInventory().setBoots(null);
                for (PotionEffect pt : player.getActivePotionEffects()) {
                    player.removePotionEffect(pt.getType());
                }
                player.setAllowFlight(true);
                player.setFlying(true);
                player.getInventory().setItem(8, Game.getInstance().lobbyItem.getItem());
                player.getInventory().setItem(0, SpectatorListener.getItem().getItem());
                Bukkit.getOnlinePlayers().forEach(player -> {
                    if (!GameUtils.isSpectator(player)) {
                        player.hidePlayer(player);
                    } else {
                        player.showPlayer(player);
                    }
                });
                setGhost(true);
                BaseLogger.info("sidebar " + GameSettings.spectatorSidebar);
                if(GameSettings.spectatorSidebar != null) {
                    GameSettings.spectatorSidebar.send(player);
                }
            }
        }.runTaskLater(API.getInstance(), 2);
    }

    public void setGhost(boolean onlySpectators) {
        player.setFireTicks(0);
        player.setFoodLevel(20);
        player.setAllowFlight(true);
        player.setFlying(true);
        player.setVelocity(new Vector(0, 0, 0));
        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 100000000, 0));
        Bukkit.getOnlinePlayers().forEach(player -> {
            Gamer gamer = Gamer.getGamer(player);
            if (gamer != null && (!onlySpectators || gamer.isSpectator())) {
                Team team;
                Scoreboard board = player.getScoreboard();
                if (board == null) {
                    board = Bukkit.getScoreboardManager().getNewScoreboard();
                    player.setScoreboard(board);
                }
                if ((team = board.getTeam("Ghosts")) == null) {
                    team = board.registerNewTeam("Ghosts");
                    team.setCanSeeFriendlyInvisibles(true);
                }
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    Gamer g = Gamer.getGamer(pl);
                    if (!g.isSpectator()) continue;
                    team.addPlayer(g.getPlayer());
                }
                team.addPlayer(player);
                player.spigot().setCollidesWithEntities(true);
                GameUtils.setWidthHeight(player, 0.0f, 0.0f, 0.0f);
                player.setPlayerListName("\u00a7f" + player.getName());
//                GameUtils.removeArrows(player);
            }
        }
        );
    }

    public void removeGhost() {
        player.removePotionEffect(PotionEffectType.INVISIBILITY);
        Bukkit.getOnlinePlayers().forEach(player -> {
            Gamer gamer = Gamer.getGamer(player);
            if (gamer != null) {
                Team team;
                Scoreboard board = player.getScoreboard();
                if (board != null && (team = board.getTeam("Ghosts")) != null && team.hasPlayer(player)) {
                    team.removePlayer(player);
                }
                GameUtils.setWidthHeight(player, 0.0f, 0.6f, 1.8f);
            }
        }
        );
    }

    public void addMoney(int count) {
        count = (int)((double)count * getMultiplay());
        this.money += count;
    }

    public Kit getPurchase() {
        return this.purchase;
    }

    public Gamer(Player player) {
        this.player = player;
        Gamer gamer = this;
        new BukkitRunnable() {
            @Override
            public void run() {

                gamers.put(getName().toLowerCase(), gamer);
            }
        }.runTask(API.getInstance());
//        new Thread(() -> {
//            if (this.player == null) {
//                throw new IllegalArgumentException("Player cannot be null");
//            }
//            long start = System.currentTimeMillis();
//            gamers.put(getName().toLowerCase(), this);
//            long time = System.currentTimeMillis() - start;
////            final GamerLoadEvent event = new GamerLoadEvent(this, time);
////            new BukkitRunnable(){
////
////                public void run() {
////                    if (!isOnline()) {
////                        return;
////                    }
////                    Bukkit.getServer().getPluginManager().callEvent((event)event);
////                }
////            }.runTask((Plugin)API.getInstance());
//        }
//        ).start();
    }

    public static void clear() {
        for (Gamer gamer : gamers.values()) {
            gamer.remove();
        }
    }

    public void remove() {
        if (this.remove) {
            return;
        }
        this.remove = true;
        this.saveBalance();
        player = null;
    }

    private void saveBalance() {
        API.getInstance().getVaultManager().getEconomy().depositPlayer(getName(), (double)money);
        if (isOnline()) {
            player.sendMessage(Game.getPrefix() + "Вы заработали §a" + money + "§f монет");
        }
    }

    public static Collection<Gamer> getGamers() {
        HashSet<Gamer> var1 = new HashSet<Gamer>(gamers.values());
        return var1;
    }

    public static void remove(Player player) {
        Gamer gamer = Gamer.getGamer(player);
        if (gamer != null) {
            gamer.remove();
        }
    }

}

