package ru.bird.main.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ItemUtils {

    public static ItemStack createItem(Material material, String displayname){
        return createItem(material, 1, (short) 0, displayname, new ArrayList<>());
    }

    public static ItemStack createItem(Material material, String displayname, String[] lore){
        return createItem(material, 1, (short) 0, displayname, Arrays.asList(lore));
    }

    public static ItemStack createItem(Material material, int amount){
        return createItem(material, amount, (short) 0, null, new ArrayList<>());
    }
    
    public static ItemStack createItem(Material material, int amount, short durability){
        return createItem(material, amount, durability, null, new ArrayList<>());
    }

    public static ItemStack createItem(Material material, int amount, int durability, String displayname){
        return createItem(material, amount, (short) durability, displayname);
    }

    public static ItemStack createItem(Material material, int amount, short durability, String displayname){
        return createItem(material, amount, durability, displayname, new ArrayList<>());
    }

    public static ItemStack createItem(Material material, int amount, int durability, String displayname, List<String> lore) {
        return createItem(material, amount, (short) durability, displayname, lore);
    }

    public static ItemStack createItem(Material material, int amount, short durability, String displayname, String[] lore) {
        return createItem(material, amount, (short) durability, displayname, Arrays.asList(lore));
    }

    public static ItemStack createItem(Material material, int amount, short durability, String displayname, List<String> lore){
        ItemStack item = new ItemStack(material);
        if(amount != 0) {
            item.setAmount(amount);
        }
        if(durability != 0) {
            item.setDurability(durability);
        }
        ItemMeta meta = item.getItemMeta();
        if(displayname != null){
            meta.setDisplayName(displayname);
        }
        if(lore != null && !lore.isEmpty()){
            meta.setLore(lore);
        }
        item.setItemMeta(meta);
        return item;
    }

}
