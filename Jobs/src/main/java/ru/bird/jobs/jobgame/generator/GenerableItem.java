package ru.bird.jobs.jobgame.generator;

public interface GenerableItem<A extends GenerableItem<A>>
{
    A getAggregatedItem();
    <B> void invokeExtractor(ItemExtractor<B> extractor);
}
