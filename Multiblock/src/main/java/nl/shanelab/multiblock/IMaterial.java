package nl.shanelab.multiblock;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;


public interface IMaterial extends ConfigurationSerializable {

	Material getType();
	boolean isValidBlock(Block block);

}
