package ru.bird.pets.entity.armorstand.planets;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import ru.bird.main.API;
import ru.bird.pets.Pets;
import ru.bird.main.packets.WrapperPlayServerEntityTeleport;
import net.minecraft.server.v1_16_R2.DataWatcher;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;

public class PetEarth extends AbstractArmoredPet {

	public PetEarth(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setArms(false);
		armor.setVisible(false);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getMainHand() {
		return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDdkNjhiYjE0NGUxNTlmZmRiMGJiMmFiZGQ1ODNmZjM4OWFlNzEwNjgyY2E3N2U2NTM1MzkzYWUyMjEzN2EifX19");
	}

	@Override
	public ItemStack getOffHand() {
		return null;
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		return null;
	}

	@Override
	public ItemStack getChestplate() {
		return null;
	}

	@Override
	public ItemStack getHelmet() {
		ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta meta = (SkullMeta) head.getItemMeta();
		meta.setOwner("Dipicrylamine");
		head.setItemMeta(meta);
		return head;
	}

	@Override
	public double getAngle() {
		return angle;
	}

	private double angle = 0;

	@Override
	public void onLivingUpdate() {
		angle += 10;
		angle = angle % 360;
		Location locTo = getOwner().getLocation();
		double localAngle = locTo.getYaw() + getAngle();
		double newX = locTo.getX() - 1D * Math.sin(Math.toRadians(localAngle));
		double newY = locTo.getBlockY() + getOffsetY();
		double newZ = locTo.getZ() + 1D * Math.cos(Math.toRadians(localAngle));
		this.petLocation = new Location(getOwner().getWorld(), newX, newY, newZ);

		WrapperPlayServerEntityTeleport position = new WrapperPlayServerEntityTeleport();
		position.setEntityID(getEntityID());
		position.setX(newX);
		position.setY(newY);
		position.setZ(newZ);
		position.setYaw(locTo.getYaw());
		position.setPitch(locTo.getPitch());
		position.broadcastPacket();

		this.setHeadPose(0, (float) angle + 120F, 0);
		this.setLeftArmPose(0, (float) angle, 230);
		this.setRightArmPose(0, (float) angle, 130);
		this.sendWatcherUpdate();
	}

}
