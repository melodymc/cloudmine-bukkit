package ru.bird.jobs.jobgame.miner.event;

import ru.bird.jobs.jobgame.JobGame;
import ru.bird.jobs.jobgame.event.JobEventUnified;

public class TntExplodedEvent extends JobEventUnified
{
    private int damage;

    public TntExplodedEvent(JobGame instance, int damage)
    {
        super(instance);

        this.damage = damage;
    }

    public int getDamage()
    {
        return damage;
    }
}
