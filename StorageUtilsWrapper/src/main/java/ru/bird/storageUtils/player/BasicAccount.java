package ru.bird.storageUtils.player;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import ru.bird.storageUtils.interfaces.Deserializable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Птица on 03.10.2017.
 */
public class BasicAccount implements Deserializable, ConfigurationSerializable {

    private UUID uuid;

    public BasicAccount() {}

    public BasicAccount(UUID uuid) {
        this.uuid = uuid;
    }
    public UUID getUUID() {
        return uuid;
    }

    public void getUUID(UUID uuid) {
        this.uuid = uuid;
    }


    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();
        result.put("==", getClass().getName());
        result.put("uuid", uuid.toString());
        return result;
    }

}