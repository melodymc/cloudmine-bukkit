// 
// Decompiled by Procyon v0.5.30
// 

package com.wellfail.crazywalls;

import java.util.List;
import com.wellfail.crazywalls.game.GameFactory;
import ru.wellfail.wapi.utils.Utils;
import java.util.ArrayList;
import ru.wellfail.wapi.game.GameSettings;
import org.bukkit.plugin.java.JavaPlugin;

public class CrazyWalls extends JavaPlugin
{
    private static CrazyWalls instance;
    
    public static CrazyWalls getInstance() {
        return CrazyWalls.instance;
    }
    
    public void onLoad() {
        CrazyWalls.instance = this;
    }
    
    public void onEnable() {
        this.setConfigurationWapi();
    }
    
    private void setConfigurationWapi() {
        this.saveDefaultConfig();
        GameSettings.prefix = "§eCrazyWalls §f| ";
        GameSettings.wboardname = "§e§lCRAZY WALLS";
        GameSettings.damage = true;
        GameSettings.isSpectate = true;
        GameSettings.damage = true;
        GameSettings.pickup = true;
        GameSettings.inventory = true;
        GameSettings.drop = true;
        GameSettings.fallDamage = true;
        GameSettings.physical = false;
        GameSettings.itemSpawn = true;
        GameSettings.chest = true;
        GameSettings.isFoodChange = true;
        GameSettings.shop = true;
        GameSettings.explode = true;
        GameSettings.itemdrop = true;
        final List<String> list = new ArrayList<String>();
        list.add("§e§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        list.add("§6§lC§e§lRAZY WALLS");
        list.add("§f§lC§6§lR§e§lAZY WALLS");
        list.add("§f§lCR§6§lA§e§lZY WALLS");
        list.add("§f§lCRA§6§lZ§e§lY WALLS");
        list.add("§f§lCRAZ§6§lY§e§l WALLS");
        list.add("§f§lCRAZY §6§lW§e§lALLS");
        list.add("§f§lCRAZY W§6§lA§e§lLLS");
        list.add("§f§lCRAZY WA§6§lL§e§lLS");
        list.add("§f§lCRAZY WAL§6§lL§e§lS");
        list.add("§f§lCRAZY WALL§6§lS");
        list.add("§f§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        list.add("§f§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        list.add("§f§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        list.add("§f§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        list.add("§e§lCRAZY WALLS");
        GameSettings.animation = list;
        GameSettings.gameTime = 840;
        if (this.getConfig().getBoolean("Setup")) {
            GameSettings.setup = true;
        }
        GameSettings.slots = this.getConfig().getInt("Slots");
        GameSettings.toStart = GameSettings.slots - 4;
        GameSettings.mapLocation = Utils.stringToLocation(this.getConfig().getString("Map"));
        new GameFactory();
    }
}
