/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.block.Block
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.block.BlockBreakEvent
 *  org.bukkit.event.block.BlockExplodeEvent
 *  org.bukkit.event.block.BlockPlaceEvent
 *  org.bukkit.event.entity.EntityExplodeEvent
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.inventory.InventoryType
 *  org.bukkit.event.player.PlayerBucketEmptyEvent
 *  org.bukkit.event.player.PlayerBucketFillEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.event.player.PlayerMoveEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  ru.wellfail.wapi.utils.Effects1_8
 *  ru.wellfail.wapi.utils.Effects1_8$ParticleType
 */
package com.wellfail.buildbattle.listeners;

import com.wellfail.buildbattle.BuildRegion;
import com.wellfail.buildbattle.GameFactory;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

public class GameListener
implements Listener {
//    private static Map<String, Effects1_8.ParticleType> effects = new HashMap<String, Effects1_8.ParticleType>();

    @EventHandler(priority=EventPriority.LOWEST)
    public void onBlockBreak(BlockBreakEvent e) {
        BuildRegion region = GameFactory.getInstance().getRegion(e.getPlayer());
        if (region.isInside(e.getBlock().getLocation())) {
            return;
        }
        e.setCancelled(true);
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onBlockPlace(BlockPlaceEvent e) {
        BuildRegion region = GameFactory.getInstance().getRegion(e.getPlayer());
        if (region.isInside(e.getBlock().getLocation())) {
            return;
        }
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockExpose(BlockExplodeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onEntityExpose(EntityExplodeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBukcetFill(PlayerBucketFillEvent e) {
        BuildRegion region = GameFactory.getInstance().getRegion(e.getPlayer());
        if (region.isInside(e.getBlockClicked().getLocation())) {
            return;
        }
        e.setCancelled(true);
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onUseEnderPerl(PlayerInteractEvent e) {
        if(e.getPlayer().getInventory().getItemInHand().getData().getItemType() == Material.ENDER_PEARL) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBucketEmpty(PlayerBucketEmptyEvent e) {
        BuildRegion region = GameFactory.getInstance().getRegion(e.getPlayer());
        if (region.isInside(e.getBlockClicked().getLocation())) {
            return;
        }
        e.setCancelled(true);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (e.getTo().getX() == e.getFrom().getX() && e.getTo().getY() == e.getFrom().getY() && e.getTo().getZ() == e.getFrom().getZ()) {
            return;
        }
        for (BuildRegion region : GameFactory.getInstance().getRegions()) {
            if (!region.isInside(e.getTo())) continue;
            return;
        }
        e.getPlayer().teleport(e.getFrom());
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.isShiftClick()) {
            e.setCancelled(true);
            return;
        }
        if (e.getInventory().getType() == InventoryType.PLAYER) {
            if (e.getSlot() == 8) {
                e.setCancelled(true);
                ((Player)e.getWhoClicked()).updateInventory();
            }
        } else if (e.getRawSlot() == 89) {
            e.setCancelled(true);
            ((Player)e.getWhoClicked()).updateInventory();
        }
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onEffectUse(PlayerInteractEvent e) {
        ItemStack item = e.getPlayer().getItemInHand();
        if (item == null) {
            return;
        }
        if (item.getItemMeta() == null) {
            return;
        }
        String display = item.getItemMeta().getDisplayName();
        if (display == null) {
            return;
        }
//        Effects1_8.ParticleType type = effects.get(display);
//        if (type == null) {
//            return;
//        }
        e.setCancelled(true);
//        GameFactory.getInstance().getRegion(e.getPlayer()).addEffect(type, item);
    }

//    static {
//        effects.put("\u00a7a\u0421\u043b\u0438\u0437\u044c", Effects1_8.ParticleType.SLIME);
//        effects.put("\u00a7a\u041b\u0430\u0432\u0430", Effects1_8.ParticleType.LAVA);
//        effects.put("\u00a7a\u0412\u043e\u0434\u0430", Effects1_8.ParticleType.DRIP_WATER);
//        effects.put("\u00a7a\u041d\u043e\u0442\u044b", Effects1_8.ParticleType.NOTE);
//        effects.put("\u00a7a\u0421\u0435\u0440\u0434\u0435\u0447\u043a\u0438", Effects1_8.ParticleType.HEART);
//        effects.put("\u00a7a\u0421\u0447\u0430\u0441\u0442\u043b\u0438\u0432\u044b\u0439 \u0436\u0438\u0442\u0435\u043b\u044c", Effects1_8.ParticleType.VILLAGER_HAPPY);
//        effects.put("\u00a7a\u041e\u0433\u043e\u043d\u044c", Effects1_8.ParticleType.FLAME);
//        effects.put("\u00a7a\u0417\u0430\u043a\u043b\u0438\u043d\u0430\u043d\u0438\u0435", Effects1_8.ParticleType.ENCHANTMENT_TABLE);
//        effects.put("\u00a7a\u041e\u0431\u043b\u0430\u043a\u0430", Effects1_8.ParticleType.CLOUD);
//        effects.put("\u00a7a\u0418\u0441\u043a\u0440\u044b", Effects1_8.ParticleType.SPELL_MOB);
//        effects.put("\u00a7a\u0412\u0437\u0440\u044b\u0432", Effects1_8.ParticleType.EXPLOSION_HUGE);
//        effects.put("\u00a7a\u041a\u0440\u0438\u0442\u044b", Effects1_8.ParticleType.CRIT_MAGIC);
//        effects.put("\u00a7a\u041f\u0443\u0437\u044b\u0440\u0438", Effects1_8.ParticleType.WATER_BUBBLE);
//        effects.put("\u00a7a\u041c\u0430\u0433\u0438\u044f", Effects1_8.ParticleType.SPELL_WITCH);
//        effects.put("\u00a7a\u0412\u0441\u043f\u044b\u0448\u043a\u0438", Effects1_8.ParticleType.CRIT);
//        effects.put("\u00a7a\u0417\u043b\u043e\u0439 \u0436\u0438\u0442\u0435\u043b\u044c", Effects1_8.ParticleType.VILLAGER_ANGRY);
//        effects.put("\u00a7a\u0424\u0435\u0439\u0435\u0440\u0432\u0435\u0440\u043a", Effects1_8.ParticleType.FIREWORKS_SPARK);
//        effects.put("\u00a7a\u0420\u0435\u0434\u0441\u0442\u043e\u0443\u043d", Effects1_8.ParticleType.REDSTONE);
//        effects.put("\u00a7a\u0421\u043d\u0435\u0433", Effects1_8.ParticleType.SNOWBALL);
//    }
}

