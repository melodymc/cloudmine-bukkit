package nl.shanelab.multiblock;

import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import ru.bird.storageUtils.interfaces.Deserializable;

import java.util.LinkedHashMap;
import java.util.Map;

public interface IMaterialValidator extends ConfigurationSerializable, Deserializable {

	boolean validateBlock(Block block);

	@Override
	default Map<String, Object> serialize() {
		Map<String, Object> result = new LinkedHashMap();
		result.put("==", getClass().getName());
		return result;
	}
}