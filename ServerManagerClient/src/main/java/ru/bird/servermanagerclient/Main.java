package ru.bird.servermanagerclient;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

public final class Main extends JavaPlugin {

    private YamlConfiguration config;

    @Override
    public void onEnable() {
        config();
        connect();
    }

    public void config() {
        String CONFIG_PATH = "/server.yml";
        File f = new File(getDataFolder().getAbsolutePath() + CONFIG_PATH);
        config = YamlConfiguration.loadConfiguration(f);
        config.addDefault("connection.host", "localhost");
        config.addDefault("connection.username", "username");
        config.addDefault("connection.password", "changeme");
        config.addDefault("connection.database", "database");
        try {
            config.options().copyDefaults(true);
            config.save(f);
        } catch (IOException e) {
            getLogger().warning("ERROR: can not save config to " + CONFIG_PATH + ". Please check it.");
        }
    }

    public void connect() {
        try {
            Connection connection = DriverManager.getConnection("JDBC:mysql://" + getConectInfo("host") + "/" + getConectInfo("database") + "?useUnicode=true&characterEncoding=utf-8", getConectInfo("username"), getConectInfo("password"));
            getLogger().info("[Servers] Соединение с базой данных установлено");
            sendServerInfo(connection);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            getLogger().warning("[Servers] Ошибка при соединении с базой данных. Ошибка - " + ex.getMessage());
        }
    }

    public void sendServerInfo(Connection connection) {
        try {
            String systemname = Bukkit.getWorldContainer().getCanonicalFile().getName();
            PreparedStatement st = connection.prepareStatement("CALL server_put(?, ?, ?, ?, ?, ?);");
            st.setString(1, systemname);
            st.setString(2, Bukkit.getServer().getIp().equals("") ? "127.0.0.1" : Bukkit.getServer().getIp());
            st.setInt(3, Bukkit.getServer().getPort());
            st.setString(4, systemname);
            st.setBoolean(5, true);
            st.setBoolean(6, true);
            st.executeUpdate();
            st.close();
            connection.close();
        } catch (SQLException e) {
            getLogger().warning("[Servers] Ошибка при отправке данных о сервере в базу");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getConectInfo(String str) {
        return config.getString("connection." + str);
    }
}