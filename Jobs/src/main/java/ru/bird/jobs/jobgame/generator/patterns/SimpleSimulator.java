package ru.bird.jobs.jobgame.generator.patterns;

import org.bukkit.inventory.Inventory;
import ru.bird.jobs.jobgame.generator.GeneratorGroup;
import ru.bird.jobs.jobgame.generator.GeneratorItem;

public class SimpleSimulator implements Simulator
{
    @Override
    public GeneratorItem simulate(Inventory levelData, GeneratorGroup group, int slotId, double randomValue)
    {
        // The code simulate spawn item by throwing random point at unit segment.
        double accumulatedValue = 0.0d;
        for(GeneratorItem item: group.getGroup())
        {
            if (randomValue > accumulatedValue && randomValue < accumulatedValue + item.spawnChance)
            {
                return item;
            }

            accumulatedValue += item.spawnChance;
        }

        // Mathematically impossible if sum of all spawn probabilities equals one.
        return GeneratorItem.IMPOSSIBLE;
    }
}
