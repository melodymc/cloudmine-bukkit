package ru.bird.main.event;

public class TimerTickEvent extends BasicEvent {

    private int time;

    public TimerTickEvent(int time) {
        this.time = time;
    }

    public int getTime() {
        return time;
    }
}