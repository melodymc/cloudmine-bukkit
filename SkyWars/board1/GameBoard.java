/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.entity.Player
 *  ru.wellfail.wapi.board.Board
 *  ru.wellfail.wapi.board.BoardLine
 *  ru.wellfail.wapi.game.Gamer
 *  ru.wellfail.wapi.utils.Utils
 */
package ru.bird.skywars.board;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.bird.skywars.game.GameFactory;

public class GameBoard
extends Board {
    private int d;
    private Player player;
    private List<String> list;
    private BoardLine players;
    private BoardLine spectators;
    private BoardLine kills;
    int s;

    public GameBoard(Player player) {
        this.player = player;
        Set<BoardLine> lines = this.getLines();
        this.create(lines, "§e§lSKYWARS");
        this.list = new ArrayList<String>();
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§6§lS§e§lKYWARS");
        this.list.add("§f§lS§6§lK§e§lYWARS");
        this.list.add("§f§lSK§6§lY§e§lWARS");
        this.list.add("§f§lSKY§6§lW§e§lARS");
        this.list.add("§f§lSKYW§6§lA§e§lRS");
        this.list.add("§f§lSKYWA§6§lR§e§lS");
        this.list.add("§e§lSKYWAR§6§lS");
        this.list.add("§f§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§f§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§f§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.update(new Runnable(){

            @Override
            public void run() {
                if (GameBoard.this.d == GameBoard.this.list.size()) {
                    GameBoard.this.d = 0;
                }
                GameBoard.this.setDisplay(GameBoard.this.list.get(GameBoard.this.d));
                GameBoard.this.d++;
            }
        }, 3);
//        this.setHealth(player);
        this.set(player);
//        this.updateHealth(player);
        this.startUpdate();
    }

    public Set<BoardLine> getLines() {
        HashSet<BoardLine> set = new HashSet<BoardLine>();
        set.add(new BoardLine((Board)this, "§1", 7));
        this.players = new BoardLine((Board)this, "Игроков: §a", 6);
        set.add(this.players);
        this.dynamicLine(this.players.getNumber(), "Игроков: §a", "" + GameFactory.players.size() + "");
        this.spectators = new BoardLine((Board)this, "Наблюдает: §a", 5);
        set.add(this.spectators);
        int spectator = Bukkit.getOnlinePlayers().size() - GameFactory.getPlayers();
        this.dynamicLine(this.players.getNumber(), "Наблюдателей: §a", "" + spectator + "");
        set.add(new BoardLine((Board)this, "§3", 4));
        this.kills = new BoardLine((Board)this, "§fУбийств: §a⚔", 3);
        set.add(this.kills);
        this.dynamicLine(this.players.getNumber(), "§fУбийств: §a⚔", "" + Gamer.getGamer((Player)this.player).getKills() + "");
        this.update(() -> {
            this.dynamicLine(this.kills.getNumber(), "§fУбийств: §a⚔", "" + Gamer.getGamer((Player)this.player).getKills());
            this.dynamicLine(this.players.getNumber(), "Игроков: §a", "" + Utils.getAlivePlayers().size());
            int spectator1 = Bukkit.getOnlinePlayers().size() - Utils.getAlivePlayers().size();
            this.dynamicLine(this.spectators.getNumber(), "Наблюдателей: §a", "" + spectator1);
        }
        , 100);
        return set;
    }

}

