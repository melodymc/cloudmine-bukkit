package ru.bird.jobs.jobgame.generator.patterns;

import org.bukkit.inventory.Inventory;
import ru.bird.jobs.jobgame.generator.GeneratorGroup;
import ru.bird.jobs.jobgame.generator.GeneratorItem;

public interface Simulator
{
    GeneratorItem simulate(Inventory levelData, GeneratorGroup group, int slotId, double randomValue);
}
