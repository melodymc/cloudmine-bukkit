package nl.shanelab.multiblock.validators;

import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.block.Block;
import org.bukkit.material.Wood;

import nl.shanelab.multiblock.IMaterialValidator;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class TreeSpeciesValidator implements IMaterialValidator {
	
	private TreeSpecies species;
	
	private Material material;

	public TreeSpeciesValidator() {}
	
	public TreeSpeciesValidator(TreeSpecies species) {
		this(species, Material.OAK_WOOD);
	}
	
	public TreeSpeciesValidator(TreeSpecies species, Material material) {
		this.species = species;
		this.material = material;
	}

	@Override
	public boolean validateBlock(Block block) {
		if (block == null || block.getType() != material) {
			return false;
		}
		
		return ((Wood)block.getState().getData()).getSpecies() == species;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> result = IMaterialValidator.super.serialize();
		result.put("treeSpecies", species.toString());
		result.put("material", material.toString());
		return result;
	}

	@Override
	public <T> T deserialize(Map<String, Object> args) throws IllegalArgumentException, NullPointerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return (T) new TreeSpeciesValidator(TreeSpecies.valueOf((String) args.get("treeSpecies")), Material.valueOf((String) args.get("material")));
	}
}
