package ru.bird.pets;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.minecraft.server.v1_16_R2.EntityPig;
import net.minecraft.server.v1_16_R2.EntityTypes;
import net.minecraft.server.v1_16_R2.PacketPlayOutEntityDestroy;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_16_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.pets.utils.PacketUtils;
import ru.bird.pets.entity.AbstractPet;
import ru.bird.pets.entity.EnumPet;
import ru.bird.pets.listeners.PetsPluginListener;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PetsManager {

	private static HashMap<UUID, AbstractPet> playersPets = Maps.newHashMap();

	public PetsManager() {
		new PetsDB();
//		Pets.getPlugin().getServer().getMessenger().registerOutgoingPluginChannel(Pets.getPlugin(), "BungeeManager");
		new BukkitRunnable() {
            @Override
            public void run() {
				playersPets.values().forEach(AbstractPet::livingUpdate);
            }
        }.runTaskTimer(Pets.getPlugin(), 0, 1);
		new PetsPluginListener();
	}

	public AbstractPet spawnPetFor(Player player, EnumPet pet) {
		player.closeInventory();

		int petUniqueID = getUniqueEntityID();
		AbstractPet aPet = pet.create(player, petUniqueID);
		if (isUsingPet(player)) {
			hiddenPet(player);
		}
		if (petUniqueID > Integer.MIN_VALUE / 4) {
			petUniqueID = Integer.MIN_VALUE;
		}
		playersPets.put(player.getUniqueId(), aPet);
		aPet.spawn();
		return aPet;
	}

	public AbstractPet getPlayerPet(Player p) {
		return playersPets.get(p.getUniqueId());
	}

	public boolean isUsingPet(Player p) {
		return playersPets.containsKey(p.getUniqueId());
	}

	public boolean hiddenPet(Player p) {
		boolean isUsingPet = isUsingPet(p);
		if (isUsingPet) {
			AbstractPet pet = getPlayerPet(p);
			PacketPlayOutEntityDestroy killPacket = new PacketPlayOutEntityDestroy(pet.getEntityID());
			for (Player player : Bukkit.getOnlinePlayers()) {
				((CraftPlayer) player).getHandle().playerConnection.sendPacket(killPacket);
			}
			playersPets.remove(p.getUniqueId());
		} else {
			spawnPetFor(p, PetsDB.get(p).getActivePet());
		}
		return isUsingPet;
	}

	public void showActivePetsFor(Player player) {
		for (AbstractPet pet : playersPets.values()) {
			pet.spawn();
		}
	}

	public void purgeAll() {
		List<Integer> integer = Lists.newArrayList();
		for (AbstractPet pet : playersPets.values()) {
			integer.add(pet.getEntityID());
		}
		int ids[] = integer.stream().mapToInt(Integer::valueOf).toArray();
		PacketPlayOutEntityDestroy killPacket = new PacketPlayOutEntityDestroy(ids);
		PacketUtils.broadcastNMSPacket(killPacket);
	}

	public Collection<AbstractPet> getActivePets() {
		return playersPets.values();
	}
	
	public int getUniqueEntityID() {
		EntityPig entity = new EntityPig(EntityTypes.PIG, ((CraftWorld) Bukkit.getWorlds().get(0)).getHandle());
		entity.getBukkitEntity().remove();
		return entity.getId();
	}

}