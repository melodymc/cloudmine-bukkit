/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.World
 *  org.bukkit.entity.Player
 *  ru.wellfail.wapi.Main
 *  ru.wellfail.wapi.board.Board
 *  ru.wellfail.wapi.board.BoardLine
 *  ru.wellfail.wapi.game.GameSettings
 *  ru.wellfail.wapi.utils.Utils
 */
package ru.bird.skywars.board;

import org.bukkit.Bukkit;
import ru.bird.skywars.game.GameFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SpectatorBoard
extends Board {
    private int d;
    private List<String> list;
    private BoardLine players;
    private BoardLine spectators;
    int s;

    public SpectatorBoard() {
        Set<BoardLine> lines = this.getLines();
        this.create(lines, "§e§lSKYWARS");
        this.list = new ArrayList<String>();
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§6§lS§e§lKYWARS");
        this.list.add("§f§lS§6§lK§e§lYWARS");
        this.list.add("§f§lSK§6§lY§e§lWARS");
        this.list.add("§f§lSKY§6§lW§e§lARS");
        this.list.add("§f§lSKYW§6§lA§e§lRS");
        this.list.add("§f§lSKYWA§6§lR§e§lS");
        this.list.add("§e§lSKYWAR§6§lS");
        this.list.add("§f§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§f§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§f§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.list.add("§e§lSKYWARS");
        this.update(() -> {
            if (this.d == this.list.size()) {
                this.d = 0;
            }
            this.setDisplay(this.list.get(this.d));
            ++this.d;
        }, 3);
        this.startUpdate();
    }

    public Set<BoardLine> getLines() {
        HashSet<BoardLine> set = new HashSet<BoardLine>();
        set.add(new BoardLine((Board)this, "§1", 6));
        this.players = new BoardLine((Board)this, "Игроков: §a", 5);
        set.add(this.players);
        this.dynamicLine(this.players.getNumber(), "Игроков: §a", "" + GameFactory.players.size() + "");
        this.spectators = new BoardLine((Board)this, "Наблюдает: §a", 4);
        set.add(this.spectators);
        int spectator = Bukkit.getOnlinePlayers().size() - GameFactory.getPlayers();
        this.dynamicLine(this.players.getNumber(), "Наблюдателей: §a", "" + spectator + "");
        set.add(new BoardLine((Board)this, "§3", 3));
        set.add(new BoardLine((Board)this, "Карта: §a" + GameSettings.mapLocation.getWorld().getName(), 2));
        set.add(new BoardLine((Board)this, "Сервер: §a" + Main.getUsername(), 1));
        this.update(() -> {
            this.dynamicLine(this.players.getNumber(), "Игроков: §a", "" + Utils.getAlivePlayers().size());
            int spectator1 = Bukkit.getOnlinePlayers().size() - Utils.getAlivePlayers().size();
            this.dynamicLine(this.spectators.getNumber(), "Наблюдателей: §a", "" + spectator1);
        }, 100);
        return set;
    }
}