/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.HandlerList
 *  org.bukkit.event.Listener
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  ru.wellfail.wapi.Main
 *  ru.wellfail.wapi.gui.Gui
 *  ru.wellfail.wapi.gui.GuiAction
 */
package com.wellfail.crazywalls.shop;

import com.wellfail.crazywalls.shop.Cost;
import com.wellfail.crazywalls.shop.Item;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import ru.wellfail.wapi.Main;
import ru.wellfail.wapi.gui.Gui;
import ru.wellfail.wapi.gui.GuiAction;

public class Shop
extends Gui {
    public Shop() {
        super(6, "\u041c\u0430\u0433\u0430\u0437\u0438\u043d");
        HandlerList.unregisterAll((Listener)this);
    }

    public void clickEvent(InventoryClickEvent e) {
        if (e.isCancelled()) {
            return;
        }
        if (e.getRawSlot() < 0 || e.getRawSlot() >= this.inventory.getSize()) {
            return;
        }
        if (e.getCurrentItem() == null) {
            return;
        }
        e.setCancelled(true);
        GuiAction action = (GuiAction)this.slots.get(e.getSlot());
        if (action == null) {
            this.onClick(e);
        } else {
            action.onClick(e);
        }
    }

    public void open(Player player) {
        Item.items.stream().forEach(item -> {
            Item it = item;
            it.updateItem(player);
            this.inventory.setItem(it.getSlot(), it.getItem());
            this.addAction(it.getSlot(), e -> {
                e.setCancelled(true);
                this.buy((Player)e.getWhoClicked(), it);
            }
            );
        }
        );
        player.openInventory(this.inventory);
    }

    public void buy(Player player, Item item) {
        item.updateCost(player);
        if (!item.isCanBuy()) {
            player.sendMessage("\u0423 \u0432\u0430\u0441 \u043d\u0435\u0434\u043e\u0441\u0442\u0430\u0442\u043e\u0447\u043d\u043e \u0440\u0435\u0441\u0443\u0440\u0441\u043e\u0432 \u0434\u043b\u044f \u043f\u043e\u043a\u0443\u043f\u043a\u0438.");
            return;
        }
        item.getCosts().stream().forEach(cost -> {
            Material type = cost.getCostItem().getType();
            int current = cost.getAmount();
            for (ItemStack it : player.getInventory().getContents()) {
                if (it == null || it.getType() != type) continue;
                if (it.getAmount() > current) {
                    it.setAmount(it.getAmount() - current);
                    break;
                }
                if (it.getAmount() == current) {
                    player.getInventory().remove(it);
                    it.setType(Material.AIR);
                    break;
                }
                if (it.getAmount() >= current) continue;
                current -= it.getAmount();
                it.setType(Material.AIR);
            }
        }
        );
        player.getInventory().addItem(new ItemStack[]{new ItemStack(item.getItem().getType(), item.getItem().getAmount(), item.getItem().getDurability())});
        this.reOpen(player);
    }

    private void reOpen(final Player player) {
        player.closeInventory();
        new BukkitRunnable(){

            public void run() {
                Shop.this.open(player);
            }
        }.runTaskLater((Plugin)Main.getInstance(), 1);
    }

}

