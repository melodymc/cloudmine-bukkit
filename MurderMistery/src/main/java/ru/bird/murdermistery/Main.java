package ru.bird.murdermistery;

import java.util.List;

import ru.bird.main.game.GameSettings;
import ru.bird.main.utils.GameUtils;
import ru.bird.murdermistery.game.GameFactory;
import java.util.ArrayList;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin
{
    private static Main instance;
    private Player winner;
    public static boolean move;
    
    public static Main getInstance() {
        return Main.instance;
    }
    
    public void onLoad() {
        Main.instance = this;
    }
    
    public void onEnable() {
        this.saveDefaultConfig();
        GameSettings.prefix = "§eMurder Mystery §f| ";
        GameSettings.wboardname = "§e§lMURDER MYSTERY";
        GameSettings.damage = true;
        GameSettings.isSpectate = true;
        GameSettings.pickup = true;
        GameSettings.shop = false;
        GameSettings.inventory = false;
        GameSettings.drop = false;
        GameSettings.fallDamage = false;
        GameSettings.physical = true;
        GameSettings.itemSpawn = true;
        GameSettings.chest = false;
        GameSettings.isBreak = false;
        GameSettings.isPlace = false;
        GameSettings.itemdrop = false;
        GameSettings.nockickarmorstand = true;
        GameSettings.isFoodChange = false;
        GameSettings.teamChanger = false;
        GameSettings.death = true;
        GameSettings.deathnomsg = false;
        GameSettings.removearrowonhit = true;
        GameSettings.axetrowing = true;
        GameSettings.endgamelastplayer = false;
        GameSettings.hiddenicks = true;
        GameSettings.crops = true;
        GameSettings.gameTime = -1;
        if (this.getConfig().getBoolean("Setup")) {
            GameSettings.setup = true;
        }
        GameSettings.slots = this.getConfig().getInt("Slots");
        GameSettings.toStart = GameSettings.slots - 4;
        GameSettings.mapLocation = GameUtils.stringToLocation(this.getConfig().getString("Map"));
        new GameFactory();
    }

    public void setWinner(final Player player) {
        this.winner = player;
    }

    static {
        Main.move = true;
    }
}