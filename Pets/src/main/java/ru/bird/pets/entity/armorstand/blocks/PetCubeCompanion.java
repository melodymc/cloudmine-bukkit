package ru.bird.pets.entity.armorstand.blocks;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.EulerAngle;

import ru.bird.main.particles.ParticleEffect;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;
import net.minecraft.server.v1_16_R2.EnumItemSlot;

public class PetCubeCompanion extends AbstractArmoredPet {

    public PetCubeCompanion(UUID id, int uniqueID) {
        super(id, uniqueID);
    }

    @Override
    protected DataWatcher build() {
        ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
        armor.setSmall(true);
        armor.setBasePlate(false);
        armor.setLeftLegPose(new EulerAngle(0.3, 0, 0));
        armor.setRightLegPose(new EulerAngle(0.3, 0, 0));
        armor.setLeftArmPose(new EulerAngle(-1.57, 0, 0));
        armor.setRightArmPose(new EulerAngle(-1.57, 0, 0));
        armor.setArms(false);
        armor.setVisible(false);
        DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
        armor.remove();
        return data;
    }

    @Override
    protected float getOffsetY() {
        return 1.2F;
    }

    @Override
    public ItemStack getMainHand() {
        return null;
    }

    @Override
    public ItemStack getOffHand() {
        return null;
    }

    @Override
    public ItemStack getBoots() {
        return null;
    }

    @Override
    public ItemStack getLeggings() {
        return null;
    }

    @Override
    public ItemStack getChestplate() {
        return null;
    }

    @Override
    public ItemStack getHelmet() {
        return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWI3OGU1NWUzZWE2YjM2ODJiZDdjZWFkNDNlYjFjOTFiMjUyN2E4MWFhMjg5NGYwOTU4MDFmNmVlNDdhMyJ9fX0=");
    }

    public ItemStack getInactiveHelmet() {
        return GameUtils.getHead("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzYzYzcyN2Q3NzVmNWYzMGViMjM2ZDM3NDg5ZGU1NGZhNjljZGI5NGQ3OTNmNzk2ZmMyNWM5YjRmYWY5MzAyNSJ9fX0=");
    }

    private float	angle	= 0;
    private boolean	active	= true;

    @Override
    public void onLivingUpdate() {
        angle += 5;
        if (angle % 360 == 0) {
            active = !active;
            angle = 0;
            if (!active) {
                sendEquipmentPacket(EnumItemSlot.HEAD, getInactiveHelmet());
            } else {
                sendEquipmentPacket(EnumItemSlot.HEAD, getHelmet());
            }
        }
        Location location = this.getPetLocation().clone().add(MathUtils.random(-0.5F, 0.5F), 0.5F + MathUtils.random(0.85F), MathUtils.random(-0.5F, 0.5F));
        if (active) {
            ParticleEffect.VILLAGER_HAPPY.sendToPlayers(Collections.singletonList(getOwner()), location, 0.1F, 0, 0.1F, 1 / 100F, 0);
            ParticleEffect.VILLAGER_ANGRY.sendToPlayers(Bukkit.getOnlinePlayers().stream().filter( player -> player != getOwner()).collect(Collectors.toList()), location, 0.1F, 0, 0.1F, 1 / 100F, 0);
        }
        this.setHeadPose(0, angle, 0);
        this.sendWatcherUpdate();
    }
}