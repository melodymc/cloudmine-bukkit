package nl.shanelab.multiblock.validators;

import org.bukkit.GrassSpecies;
import org.bukkit.Material;
import org.bukkit.block.Block;

import nl.shanelab.multiblock.IMaterialValidator;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class GrassSpeciesValidator implements IMaterialValidator {

	public GrassSpeciesValidator() {}
	
	private GrassSpecies species;
	
	private Material material;

	public GrassSpeciesValidator(GrassSpecies species) {
		this(species, Material.LEGACY_DOUBLE_PLANT);
	}
	
	public GrassSpeciesValidator(GrassSpecies species, Material material) {
		this.species = species;
		this.material = material;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean validateBlock(Block block) {
		if (block == null || block.getType() != material) {
			return false;
		}
		
		byte data = block.getData();
		if (material == Material.LEGACY_DOUBLE_PLANT) {
			// block data LONG_GRASS
			// 0 = dead
			// 1 = normal
			// 2 = fern-like
			
			// block data DOUBLE_PLANT
			// 2 = normal taller
			// 3 = fern-like taller
			data = (byte) (data - 1); // subtract 1, and it matches the same type (dirty but w/e)
		}
		
		return GrassSpecies.getByData(data) == species;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> result = IMaterialValidator.super.serialize();
		result.put("species", species.toString());
		result.put("material", material.toString());
		return result;
	}

	@Override
	public <T> T deserialize(Map<String, Object> args) throws IllegalArgumentException, NullPointerException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return (T) new GrassSpeciesValidator(GrassSpecies.valueOf((String) args.get("species")), Material.valueOf((String) args.get("material")));
	}
}
