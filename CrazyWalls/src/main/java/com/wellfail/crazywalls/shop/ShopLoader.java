/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.World
 *  org.bukkit.configuration.Configuration
 *  org.bukkit.configuration.ConfigurationSection
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.configuration.file.FileConfigurationOptions
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.EntityType
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.plugin.Plugin
 *  ru.wellfail.holograms.Holograms
 *  ru.wellfail.npc.NPCCreator
 */
package com.wellfail.crazywalls.shop;

import com.wellfail.crazywalls.Main;
import com.wellfail.crazywalls.config.Config;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.bird.main.hologram.Hologram;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.npc.NPCCreator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ShopLoader {
    public static List<Hologram> holograms = new ArrayList<Hologram>();
    private FileConfiguration config;
    private File locationsFile = null;

    public ShopLoader() {
        this.init();
        this.loadShop();
    }

    private void loadShop() {
//        BaseLogger.info("[-->>>SMOTRI SUDA PIDR<<<---]" + Boolean.toString(this.config.getConfigurationSection("Shop") == null));

        for (String s : this.config.getConfigurationSection("Shop").getKeys(false)) {
            String name = this.config.getString("Shop." + s + ".name");
            int amount = this.config.getInt("Shop." + s + ".amount");
            int slot = this.config.getInt("Shop." + s + ".slot");
            String cid = this.config.getString("Shop." + s + ".id");
            String[] split = cid.split(":");
            ItemStack item = split.length == 1 ? new ItemStack(Integer.valueOf(split[0]), amount) : new ItemStack(Integer.valueOf(split[0]), amount, Short.valueOf(split[1]));
            List<String> costs = this.config.getStringList("Shop." + s + ".cost");
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(name);
            item.setItemMeta(meta);
            new Item(this.getCostList(costs), item, slot);
        }
        Config.shops.get(0).getWorld().getEntities().stream().filter(entity -> entity.getType() == EntityType.VILLAGER).forEach(Entity::remove);

        String[] text = new String[]{"§bМагазин", "§7(§aРесурсы §e➜ §bПредметы§7)"};
        Config.shops.forEach(shop -> {
            NPCCreator npc = new NPCCreator(shop);
            npc.createNPC(true);
            holograms.add(new Hologram(shop.add(0.0, 0.2, 0.0), Main.getInstance(), text));

        }
        );
    }

    private List<Cost> getCostList(List<String> list) {
        ArrayList<Cost> costs = new ArrayList<Cost>();
        list.forEach(line -> {
            String[] args = line.split(";");
            if (args.length == 4) {
                ItemStack cost = new ItemStack(Integer.valueOf(args[0]), Integer.valueOf(args[1], Integer.parseInt(args[2])));
                costs.add(new Cost(cost, Integer.valueOf(args[1]), args[3]));
            } else if (args.length == 3) {
                ItemStack cost = new ItemStack(Integer.valueOf(args[0]), Integer.valueOf(args[1]));
                costs.add(new Cost(cost, Integer.valueOf(args[1]), args[2]));
            }
        });
        return costs;
    }

    public void saveLocationsConfig() {
        if (this.locationsFile == null || this.config == null) {
            return;
        }
        try {
            this.config.save(this.locationsFile);
        }
        catch (IOException ex) {
            System.out.println("Ошибка при чтении файла конфигурации.");
        }
    }

    private void init() {
        this.locationsFile = new File(Main.getInstance().getDataFolder(), "shop.yml");
        this.config = YamlConfiguration.loadConfiguration(this.locationsFile);
        InputStream defConfigStream1 = Main.getInstance().getResource("shop.yml");
        if (defConfigStream1 != null) {
            YamlConfiguration defConfig1 = null;
            try
            {
                defConfig1 = YamlConfiguration.loadConfiguration(new BufferedReader(new InputStreamReader(defConfigStream1, "UTF-8")));
                this.config.setDefaults(defConfig1);
            } catch (UnsupportedEncodingException e)
            {
                BaseLogger.error("[Main] Не загружен shop.yml");
                e.printStackTrace();
            }
        }
        this.config.options().copyDefaults(true);
        this.saveLocationsConfig();
    }
}

