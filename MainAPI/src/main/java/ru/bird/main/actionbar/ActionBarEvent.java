package ru.bird.main.actionbar;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import ru.bird.main.event.BasicEvent;
import ru.bird.main.event.listeners.BasicListener;

public class ActionBarEvent extends BasicEvent implements Cancellable {
    private final Player player;
    private String message;
    private boolean cancelled = false;

    public ActionBarEvent(Player player, String message) {
        this.player = player;
        this.message = message;
    }

    public Player getPlayer() {
        return player;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

}