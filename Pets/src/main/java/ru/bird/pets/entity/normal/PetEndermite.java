package ru.bird.pets.entity.normal;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.Endermite;
import org.bukkit.entity.EntityType;

import net.minecraft.server.v1_16_R2.DataWatcher;
import ru.bird.pets.entity.AbstractStandardPet;

public class PetEndermite extends AbstractStandardPet {

	public PetEndermite(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		Endermite entity = (Endermite) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		DataWatcher data = ((CraftEntity) entity).getHandle().getDataWatcher();
		entity.remove();
		return data;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.ENDERMITE;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

}