package ru.bird.pets;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import ru.bird.main.API;
import ru.bird.pets.Pets;
import ru.bird.main.logger.BaseLogger;
import ru.bird.pets.entity.EnumPet;
import ru.bird.pets.player.PetAccount;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class PetsDB {
    private static HashMap<UUID, PetAccount> petsAccounts;
    private static Connection connection;

    public PetsDB(){
        try {
            connection = API.getInstance().getSQLConnection().getConnection();
        } catch (SQLException e) {
            BaseLogger.error("Не получено соединение из MainAPI");
            e.printStackTrace();
        }
    }

    public static HashMap<UUID, PetAccount> getPetsAccounts() {
        if(petsAccounts == null) {
            petsAccounts = new HashMap<>();
        }
        return petsAccounts;
    }

    public static ResultSet search(UUID uuid) throws SQLException {
        PreparedStatement st = connection.prepareStatement("SELECT activePet,allowedPets FROM `accounts` WHERE uuid = ?");
        st.setString(1, uuid.toString());
        return st.executeQuery();
    }

    public static void load(UUID uuid) {
        try{
            PetAccount u = new PetAccount(uuid);
            ResultSet rs = search(uuid);
            getPetsAccounts().put(u.getUUID(), u);
            if (rs.next()) {
                setData(u, rs);
            } else {
                setData(u);
            }
        } catch (SQLException e) {
            BaseLogger.error("Ошибка при загрузке игрока");
            e.printStackTrace();
        }
    }

    public static void setData(PetAccount u, ResultSet rs) {
        try {
            u.setActivePet(rs.getString("activePet"));
            u.setAllowedPets(rs.getString("allowedPets"));
//            if(!rs.getBoolean("connect")) {
//                u.setConnect(true);
//                EnumPet pet = u.getActivePet();
//                u.getBukkitPlayer().sendMessage("Загружен пет " + pet.getDisplayName() + "\n " + ChatColor.GRAY + " Редкость: " + ChatColor.GREEN + pet.getRarity());
//            }
        } catch (SQLException e){
            BaseLogger.error("При загрузке игрока " + u.getUUID() + " произошла ошибка");
            e.printStackTrace();
        }
    }

    public static void setData(PetAccount u){
        u.setActivePet(u.randomPet());
        u.setAllowedPets(Collections.singletonList(u.getActivePet()));
        u.setConnect(true);
        EnumPet pet = u.getActivePet();
        u.getBukkitPlayer().sendMessage("Создан случайный пет " + pet.getDisplayName() + "\n " + ChatColor.GRAY + " Редкость: " + ChatColor.GREEN + pet.getRarity());
    }

    public static void insert(PetAccount u) {
        try {
            BaseLogger.info("save player " + u.getUUID());
            UUID uuid = u.getUUID();
            PreparedStatement st;
            if(!search(uuid).next()) {
                st = connection.prepareStatement("INSERT INTO `accounts` (`uuid`, `activePet`, `allowedPets`, `isConnect`) VALUES (?, ?, ?, ?)");
                st.setString(1, uuid.toString());
                st.setString(2, u.getActivePet().name());
                st.setString(3, u.getAllowedPets().stream().map(Enum::name).collect(Collectors.joining(",")));
                st.setBoolean(4, u.isConnect());
            } else {
                st = connection.prepareStatement("UPDATE `accounts` SET `activePet`= ?, `allowedPets`= ?, `isConnect`= ? WHERE uuid = ?");
                st.setString(1, u.getActivePet().name());
                st.setString(2, u.getAllowedPets().stream().map(Enum::name).collect(Collectors.joining(",")));
                st.setBoolean(3, u.isConnect());
                st.setString(4, uuid.toString());
            }
            st.executeUpdate();
        } catch (SQLException e) {
            BaseLogger.info("Ошибка записи данных игрока в базу");
            e.printStackTrace();
        }
    }

    public static PetAccount get(Player player) {
        return get(player.getUniqueId());
    }

    public static PetAccount get(UUID uuid){
        PetAccount user = getPetsAccounts().get(uuid);
        if(user != null) {
            return user;
        } else {
            return PetAccount.empty;
        }
    }

    public static void removePetAccount(UUID uuid) {
        PetAccount l = getPetsAccounts().get(uuid);
        getPetsAccounts().remove(uuid, l);
    }

    public static void insert(Player player) {
        insert(get(player));
    }
}