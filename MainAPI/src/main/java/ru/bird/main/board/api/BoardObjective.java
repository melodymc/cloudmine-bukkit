package ru.bird.main.board.api;

import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.google.common.base.Preconditions;
import org.bukkit.entity.Player;
import ru.bird.main.packets.WrapperPlayServerScoreboardDisplayObjective;
import ru.bird.main.packets.WrapperPlayServerScoreboardObjective;
import ru.bird.main.board.utils.Utils;

public class BoardObjective {

    private WrapperPlayServerScoreboardObjective packet = new WrapperPlayServerScoreboardObjective();
    private Board board;

    private boolean registered = false;

    public BoardObjective(Board board, String name, String displayName) {
        this.board = board;
        packet.setName(Utils.parse(name));
        setDisplayName(displayName);
    }

    public String getName() {
        return packet.getName();
    }

    public void setDisplayName(String displayName) {
        packet.setDisplayName(WrappedChatComponent.fromText(Utils.parse(displayName, 32)));
        if(isRegistered()) {
            update();
        }
    }

    public void register() {
        Preconditions.checkArgument(!isRegistered(), "Objective already registered");
        packet.setMode(WrapperPlayServerScoreboardObjective.Mode.ADD_OBJECTIVE);
        packet.broadcastPacket();

        registered = true;
    }

    public void send(Player player) {
        packet.setMode(WrapperPlayServerScoreboardObjective.Mode.ADD_OBJECTIVE);
        packet.sendPacket(player);
    }

    public void display(Player... players) {
        WrapperPlayServerScoreboardDisplayObjective displayPacket = new WrapperPlayServerScoreboardDisplayObjective();
        displayPacket.setScoreName(getName());
        displayPacket.setPosition(1);
        for(Player player: players) {
            displayPacket.sendPacket(player);
        }
    }

    public void update() {
        packet.setMode(WrapperPlayServerScoreboardObjective.Mode.UPDATE_VALUE);
        packet.broadcastPacket();
    }

    public void unregister() {
        Preconditions.checkArgument(isRegistered(), "Objective must be registered");

        board.lineWrappers().forEach(BoardLine::remove);

        packet.setMode(WrapperPlayServerScoreboardObjective.Mode.REMOVE_OBJECTIVE);
        packet.broadcastPacket();

        registered = false;
    }

    public boolean isRegistered() {
        return registered;
    }
}
