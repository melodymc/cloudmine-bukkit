package ru.bird.jobs.jobgame.miner.event;

import org.bukkit.inventory.Inventory;
import ru.bird.jobs.jobgame.event.JobEventUnified;
import ru.bird.jobs.jobgame.miner.JobGameMiner;

public class LevelGeneratedEvent extends JobEventUnified
{
    private Inventory levelData;
    private boolean isUnlucky;

    public LevelGeneratedEvent(JobGameMiner instance, Inventory levelData, boolean isUnlucky)
    {
        super(instance);

        this.levelData = levelData;
        this.isUnlucky = isUnlucky;
    }

    public Inventory getLevelData()
    {
        return levelData;
    }
    public boolean isUnlucky()
    {
        return isUnlucky;
    }
}
