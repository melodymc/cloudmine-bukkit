/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.java.JavaPlugin
 *  ru.wellfail.wapi.game.GameSettings
 *  ru.wellfail.wapi.utils.Utils
 */
package ru.bird.skywars;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.game.GameSettings;
import ru.bird.main.utils.GameUtils;
import ru.bird.skywars.game.GameFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Main
extends JavaPlugin {
    private static Main instance;
    private Player winner;

    public static Main getInstance() {
        return instance;
    }

    public void onLoad() {
        instance = this;
    }

    public void onEnable() {
        this.saveDefaultConfig();
        GameSettings.prefix = "§b§lSkyWars §8⋙§f ";
        GameSettings.wboardname = "skywars";
        GameSettings.damage = true;
        GameSettings.isSpectate = true;
        GameSettings.damage = true;
        GameSettings.pickup = true;
        GameSettings.shop = true;
        GameSettings.inventory = true;
        GameSettings.drop = true;
        GameSettings.fallDamage = true;
        GameSettings.physical = true;
        GameSettings.itemSpawn = true;
        GameSettings.chest = true;
        GameSettings.isFoodChange = true;
        GameSettings.itemdrop = true;
        GameSettings.explode = true;
//        ArrayList<String> list = new ArrayList<String>();
//        list.add("§e§lSKYWARS");
//        list.add("§e§lSKYWARS");
//        list.add("§e§lSKYWARS");
//        list.add("§e§lSKYWARS");
//        list.add("§e§lSKYWARS");
//        list.add("§6§lS§e§lKYWARS");
//        list.add("§f§lS§6§lK§e§lYWARS");
//        list.add("§f§lSK§6§lY§e§lWARS");
//        list.add("§f§lSKY§6§lW§e§lARS");
//        list.add("§f§lSKYW§6§lA§e§lRS");
//        list.add("§f§lSKYWA§6§lR§e§lS");
//        list.add("§e§lSKYWAR§6§lS");
//        list.add("§f§lSKYWARS");
//        list.add("§e§lSKYWARS");
//        list.add("§f§lSKYWARS");
//        list.add("§e§lSKYWARS");
//        list.add("§f§lSKYWARS");
//        list.add("§e§lSKYWARS");
//        list.add("§e§lSKYWARS");
//        list.add("§e§lSKYWARS");
//        list.add("§e§lSKYWARS");
//        GameSettings.animation = list;
        GameSettings.gameTime = -1;
        if (this.getConfig().getBoolean("Setup")) {
            GameSettings.setup = true;
        }
        GameSettings.slots = this.getConfig().getInt("Slots");
//        GameSettings.slots = 4;
        GameSettings.toStart = GameSettings.slots - 5;
        GameSettings.mapLocation = GameUtils.stringToLocation((String)this.getConfig().getString("Map"));
        new GameFactory();
//        FileConfiguration mainConfig = GameFactory.configMain;
//        List<String> items_tr = mainConfig.getStringList("Traped.items");
//        Map<String, Object> items_traped = new HashMap<>();
//        items_tr.forEach(st_item -> {
//            ItemStack item = getItemByCustomFormatFromString(st_item);
//            items_traped.put(item.getType().toString(), item.serialize());
//        });
//        Map<String, Object> lalal = new HashMap<>();
//        lalal.put("items", items_traped);
//        mainConfig.set("converted_item_traped", lalal);
//
//        List<String> items_sn = mainConfig.getStringList("Chest.items");
//        Map<String, Object> items_sn_tmp = new HashMap<>();
//        items_sn.forEach(st_item -> {
//            ItemStack item = getItemByCustomFormatFromString(st_item);
//            items_sn_tmp.put(item.getType().toString(), item.serialize());
//        });
//        Map<String, Object> lalal1 = new HashMap<>();
//        lalal1.put("items", items_sn_tmp);
//        mainConfig.set("converted_item_chest", lalal1);
//        try {
//            mainConfig.save(new File(API.getInstance().getDataFolder(), "main.yml"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    private ItemStack getItemByCustomFormatFromString(String st_item) {
        String id;
        ItemStack itemStack;
        String[] items = st_item.split(" ");
        String chance = items[0];
        String val = items[2];
        String[] meta = items[1].split(":", 2);
        if (meta.length == 2) {
            id = meta[0];
            String m = meta[1];
            itemStack = new ItemStack(Material.getMaterial(Integer.parseInt(id)), Integer.valueOf(val), Short.parseShort(m));
        } else {
            id = meta[0];
            itemStack = new ItemStack(Material.getMaterial(Integer.parseInt(id)));
            itemStack.setAmount(Integer.valueOf(val));
        }
        return itemStack;
    }
}