package ru.bird.jobs.jobgame.event;

import ru.bird.jobs.jobgame.JobGame;

public interface TargetListener
{
    Class<? extends  JobGame> getCorrectType();

    default boolean isCorrect(JobEventUnified event)
    {
        return event.getJobInstance().getClass() == getCorrectType();
    }
}
