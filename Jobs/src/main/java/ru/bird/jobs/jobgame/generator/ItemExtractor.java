package ru.bird.jobs.jobgame.generator;

import ru.bird.jobs.JobsPlugin;
import ru.bird.jobs.jobgame.miner.ItemData;

public interface ItemExtractor<T>
{
    void invoke(ItemData item);
    void invoke(GeneratorItem item);

    T extract();
}

class GeneratorItemExtractor implements ItemExtractor<GeneratorItem>
{
    private GeneratorItem result;

    @Override
    public void invoke(ItemData item)
    {
        result = item.aggregatedItem;
    }

    @Override
    public void invoke(GeneratorItem item)
    {
        result = item;
    }

    public GeneratorItem extract()
    {
        return result;
    }
}
