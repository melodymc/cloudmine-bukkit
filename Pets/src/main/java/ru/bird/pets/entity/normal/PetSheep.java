package ru.bird.pets.entity.normal;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Sheep;

import net.minecraft.server.v1_16_R2.DataWatcher;
import ru.bird.pets.entity.AbstractStandardPet;

public class PetSheep extends AbstractStandardPet {

	public PetSheep(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		Sheep entity = (Sheep) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		entity.setBaby();
		DataWatcher data = ((CraftEntity) entity).getHandle().getDataWatcher();
		entity.remove();
		return data;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.SHEEP;
	}

	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

}
