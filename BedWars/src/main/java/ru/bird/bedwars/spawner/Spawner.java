package ru.bird.bedwars.spawner;

import ru.bird.bedwars.Main;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;

public class Spawner
{
    private List<Location> brick;
    private List<Location> iron;
    private List<Location> gold;
    List<BukkitTask> tasks;

    public Spawner(final List<Location> brick, final List<Location> iron, final List<Location> gold) {
        this.brick = brick;
        this.iron = iron;
        this.gold = gold;
        this.start();
    }

    public void cancel() {
        this.tasks.forEach(BukkitTask::cancel);
    }

    private void start() {
        (this.tasks = new ArrayList<BukkitTask>()).add(new BukkitRunnable() {
            public void run() {
                Spawner.this.brick.forEach(loc -> loc.getWorld().dropItem(loc, new ItemStack(Material.CLAY_BRICK)));
            }
        }.runTaskTimer(Main.getInstance(), 0L, 60L));
        this.tasks.add(new BukkitRunnable() {
            public void run() {
                Spawner.this.iron.forEach(loc -> loc.getWorld().dropItem(loc, new ItemStack(Material.IRON_INGOT)));
            }
        }.runTaskTimer(Main.getInstance(), 0L, 270L));
        this.tasks.add(new BukkitRunnable() {
            public void run() {
                Spawner.this.gold.forEach(loc -> loc.getWorld().dropItem(loc, new ItemStack(Material.GOLD_INGOT)));
            }
        }.runTaskTimer(Main.getInstance(), 0L, 550L));
    }
}