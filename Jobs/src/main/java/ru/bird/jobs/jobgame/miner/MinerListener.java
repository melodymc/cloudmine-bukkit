package ru.bird.jobs.jobgame.miner;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import ru.bird.jobs.jobgame.JobGame;
import ru.bird.jobs.jobgame.event.FinishJobEvent;
import ru.bird.jobs.jobgame.event.InterruptedJobEvent;
import ru.bird.jobs.jobgame.event.StartJobEvent;
import ru.bird.jobs.jobgame.event.TargetListener;
import ru.bird.jobs.jobgame.miner.event.AppearExitEvent;
import ru.bird.jobs.jobgame.miner.event.CollectItemEvent;
import ru.bird.jobs.jobgame.miner.event.FinishLevelEvent;
import ru.bird.jobs.jobgame.miner.event.TntExplodedEvent;

public final class MinerListener implements Listener, TargetListener
{
    @Override
    public Class<? extends JobGame> getCorrectType()
    {
        return JobGameMiner.class;
    }

    @EventHandler
    public void onStartEvent(StartJobEvent event)
    {
        if ( !isCorrect(event) ) return;

        Player onlinePlayer = event.getWorker().getOnlinePlayer();

        if (onlinePlayer != null)
        {
            onlinePlayer.sendMessage("[Шахтёр] Игра началась. Уровнь 1.");
        }
    }

    @EventHandler
    public void onInterruptedJobEvent(InterruptedJobEvent event)
    {
        if ( !isCorrect(event) ) return;

        Player onlinePlayer = event.getWorker().getOnlinePlayer();

        if (onlinePlayer != null)
        {
            onlinePlayer.sendMessage("[Шахтёр] Игра была прервана.");
        }
    }

    @EventHandler
    public void onFinishEvent(FinishJobEvent event)
    {
        if ( !isCorrect(event) ) return;

        Player onlinePlayer = event.getWorker().getOnlinePlayer();

        if (onlinePlayer != null)
        {
            onlinePlayer.sendMessage("[Шахтёр] Поздравляем, вы прошли все уровни!");
        }
    }

    @EventHandler
    public void onFinishLevelEvent(FinishLevelEvent event)
    {
        if ( !isCorrect(event) ) return;

        Player onlinePlayer = event.getWorker().getOnlinePlayer();
        JobGameMiner jobInstance = (JobGameMiner) event.getJobInstance();

        if (onlinePlayer != null)
        {
            onlinePlayer.sendMessage("[Шахтёр] Вы успешно прошли " + jobInstance.getCurrentLevel() + " уровень. Ваш счет составляет " + event.getWorker().getPoints() + " очков.");
        }
    }

    @EventHandler
    public void onCollectItemEvent(CollectItemEvent event)
    {
        if ( !isCorrect(event) ) return;

        Player onlinePlayer = event.getWorker().getOnlinePlayer();

        if (onlinePlayer != null)
        {
            if (event.isBonus())
            {
                onlinePlayer.sendMessage("[Шахтёр] Получены бонусные очки!");
            }
        }
    }

    @EventHandler
    public void onAppearExitEvent(AppearExitEvent event)
    {
        if ( !isCorrect(event) ) return;

        Player onlinePlayer = event.getWorker().getOnlinePlayer();

        if (onlinePlayer != null)
        {
            if (event.isBonus())
            {
                onlinePlayer.sendMessage("[Шахтёр] Найден секретный выход!");
            }
        }
    }

    @EventHandler
    public void onTntExplodedEvent(TntExplodedEvent event)
    {
        if ( !isCorrect(event) ) return;

        Player onlinePlayer = event.getWorker().getOnlinePlayer();
        JobGameMiner gameInstance = (JobGameMiner) event.getJobInstance();

        if (onlinePlayer != null)
        {
            if (gameInstance.getMineDurability() <= 0)
            {
                onlinePlayer.sendMessage("[Шахтёр] Игра окончена, так как шахта была обрушена!");
            }
            else
            {
                onlinePlayer.sendMessage("[Шахтёр] Шахте нанесён урон в " + event.getDamage() + " ед.");
            }
        }
    }
}
