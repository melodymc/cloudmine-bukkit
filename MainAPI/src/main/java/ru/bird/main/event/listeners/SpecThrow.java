package ru.bird.main.event.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;
import ru.bird.main.game.Gamer;
import ru.bird.main.utils.GameUtils;

public class SpecThrow extends BasicListener {
    @EventHandler
    private void onMovie(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        Gamer gamer = Gamer.getGamer(player);
        if (gamer.isSpectator()) {
            for (Player pl : Bukkit.getOnlinePlayers()) {
                if (Gamer.getGamer(pl).isSpectator()) continue;
                if (pl == null) {
                    return;
                }
                if (pl.getName().equals(player.getName()) || !pl.getLocation().toVector().isInSphere(player.getLocation().toVector(), 3.0))
                    continue;

                player.setVelocity(GameUtils.PlayerThrow(player));
                return;
            }
        }
    }
}
