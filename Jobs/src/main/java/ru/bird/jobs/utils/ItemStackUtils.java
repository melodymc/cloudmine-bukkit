package ru.bird.jobs.utils;


import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemStackUtils {
    public static ItemStack createItem(Material material, int amount){
        return createItem(material, amount, (short) 0, null, new ArrayList<>());
    }
    public static ItemStack createItem(Material material, int amount, short durability){
        return createItem(material, amount, durability, null, new ArrayList<>());
    }

    public static ItemStack createItem(Material material, int amount, int durability, String diaplayname){
        return createItem(material, amount, (short) durability, diaplayname);
    }

    public static ItemStack createItem(Material material, int amount, short durability, String diaplayname){
        return createItem(material, amount, durability, diaplayname, new ArrayList<>());
    }

    public static ItemStack createItem(Material material, int amount, int durability, String diaplayname, List<String> lore) {
        return createItem(material, amount, (short) durability, diaplayname, lore);
    }

    public static ItemStack createItem(Material material, int amount, short durability, String diaplayname, List<String> lore){
        ItemStack item = new ItemStack(material);
        if(amount != 0) {
            item.setAmount(amount);
        }
        if(durability != 0) {
            item.setDurability(durability);
        }
        ItemMeta meta = item.getItemMeta();
        if(diaplayname != null){
            meta.setDisplayName(diaplayname);
        }
        if(!lore.isEmpty()){
            meta.setLore(lore);
        }
        item.setItemMeta(meta);
        return item;
    }

}
