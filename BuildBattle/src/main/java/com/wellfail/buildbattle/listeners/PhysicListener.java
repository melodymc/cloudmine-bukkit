/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.block.Block
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.block.BlockBurnEvent
 *  org.bukkit.event.block.BlockExplodeEvent
 *  org.bukkit.event.block.BlockFromToEvent
 *  org.bukkit.event.block.BlockPhysicsEvent
 *  org.bukkit.event.block.BlockPistonExtendEvent
 *  org.bukkit.event.block.BlockSpreadEvent
 *  org.bukkit.event.entity.EntityChangeBlockEvent
 *  org.bukkit.event.entity.EntitySpawnEvent
 *  ru.wellfail.wapi.listeners.PhysicalListener
 */
package com.wellfail.buildbattle.listeners;

import com.wellfail.buildbattle.BuildRegion;
import com.wellfail.buildbattle.GameFactory;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import ru.bird.main.event.listeners.PhysicalListener;

public class PhysicListener
extends PhysicalListener {
    public void onBlockBurn(BlockBurnEvent e) {
        for (BuildRegion region : GameFactory.getInstance().getRegions()) {
            if (!region.isInside(e.getBlock().getLocation())) continue;
            return;
        }
        super.onBlockBurn(e);
    }

    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockExplode(BlockExplodeEvent e) {
        e.setCancelled(true);
    }

    public void onPhysic(BlockPhysicsEvent e) {
        for (BuildRegion region : GameFactory.getInstance().getRegions()) {
            if (!region.isInside(e.getBlock().getLocation())) continue;
            return;
        }
        super.onPhysic(e);
    }

    public void onBlockFromTo(BlockFromToEvent e) {
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onBlockFromToEvent(BlockFromToEvent e) {
        for (BuildRegion region : GameFactory.getInstance().getRegions()) {
            if (!region.isInside(e.getToBlock().getLocation())) continue;
            return;
        }
        e.setCancelled(true);
    }

    @EventHandler
    public void onPistonExtend(BlockPistonExtendEvent event) {
        for (BuildRegion region : GameFactory.getInstance().getRegions()) {
            event.getBlocks().stream().filter(block -> !region.isInside(block.getLocation())).forEach(block -> {
                event.setCancelled(true);
            }
            );
        }
    }

    public void onBlockSpread(BlockSpreadEvent e) {
        for (BuildRegion region : GameFactory.getInstance().getRegions()) {
            if (!region.isInside(e.getBlock().getLocation())) continue;
            return;
        }
        super.onBlockSpread(e);
    }

    public void onEntityChangeBlock(EntityChangeBlockEvent e) {
        for (BuildRegion region : GameFactory.getInstance().getRegions()) {
            if (!region.isInside(e.getBlock().getLocation())) continue;
            return;
        }
        super.onEntityChangeBlock(e);
    }
}

