package ru.bird.murdermistery.game;

import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Role {
    private String displayname;
    private List<ItemStack> startinv;

    public Role(final String displayname, final List<ItemStack> startinv){
        this.displayname = displayname;
        this.startinv = startinv;
    }
    public String getDisplayname() {
        return this.displayname;
    }

    public List<ItemStack> getStartInv() {
        return this.startinv;
    }

}
