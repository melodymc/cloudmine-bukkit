package ru.bird.jobs.jobgame;

import java.util.UUID;

public abstract class JobGame
{
    public final static JobGame EMPTY = null;

    public final Worker worker;

    public JobGame(UUID playerUuid)
    {
        this.worker = new Worker(playerUuid);
    }
}
