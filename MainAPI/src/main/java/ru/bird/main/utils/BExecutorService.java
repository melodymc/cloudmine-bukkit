package ru.bird.main.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class BExecutorService
        implements ExecutorService {
    private static final List<Runnable> tasks = new ArrayList<Runnable>();

    public BExecutorService() {
        (new Thread(() -> {
            while(true) {
                Runnable task = getTask();
                if(task != null) {
                    task.run();
                }

                try {
                    Thread.sleep(10L);
                } catch (InterruptedException var2) {
                    var2.printStackTrace();
                }
            }
        })).start();
    }

    public static synchronized boolean hasTask() {
        if (tasks.size() == 0) {
            return false;
        }
        return true;
    }

    public static synchronized Runnable getTask() {
        if (tasks.size() == 0) {
            return null;
        }
        Runnable task = tasks.get(0);
        tasks.remove(task);
        return task;
    }

    @Override
    public void execute(Runnable command) {
        tasks.add(command);
    }

    @Override
    public void shutdown() {
    }

    @Override
    public List<Runnable> shutdownNow() {
        return null;
    }

    @Override
    public boolean isShutdown() {
        return false;
    }

    @Override
    public boolean isTerminated() {
        return false;
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        return null;
    }

    @Override
    public <T> Future<T> submit(Runnable task, T result) {
        return null;
    }

    @Override
    public Future<?> submit(Runnable task) {
        return null;
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        return null;
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
        return null;
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
        return null;
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return null;
    }
}