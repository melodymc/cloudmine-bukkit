package ru.bird.gameportal;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public final class Main extends JavaPlugin implements Listener {

    private static final String CONFIG_PATH = "/config.yml";

    @Override
    public void onEnable() {
        config();
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void portalEvent(PlayerPortalEvent event){
        event.setCancelled(true);
    }

    private void config() {
        File f = new File(this.getDataFolder().getAbsolutePath() + CONFIG_PATH);
        YamlConfiguration config = YamlConfiguration.loadConfiguration(f);
        config.addDefault("portal.current", "SW");
        try {
            config.options().copyDefaults(true);
            config.save(f);
        }
        catch (IOException e) {
            this.getLogger().warning("ERROR: can not save config to " + CONFIG_PATH + ". Please check it.");
        }
        new CurrentServerCommand(config.getString("portal.current"));
    }
}