package ru.bird.board.main;

import ru.bird.main.event.listeners.BasicListener;
import ru.bird.main.sidebar.Sidebar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.sidebar.SidebarManager;

public class JoinQuitListener extends BasicListener {

    public JoinQuitListener(JavaPlugin plugin) {
        super(plugin);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        Main.fillSidebar(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();
        Sidebar sidebar = SidebarManager.getSidebar(player);
        if (sidebar != null) {
            sidebar.getUpdateTask().cancel();
            sidebar.stopDoUpdate();
            SidebarManager.unregisterSidebar(sidebar);
        }
    }
}
