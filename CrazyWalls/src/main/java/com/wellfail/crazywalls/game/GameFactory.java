package com.wellfail.crazywalls.game;

import com.wellfail.crazywalls.Main;
import com.wellfail.crazywalls.sidebar.GameSidebar;
import com.wellfail.crazywalls.config.Config;
import com.wellfail.crazywalls.kits.KitLoader;
import com.wellfail.crazywalls.shop.Shop;
import com.wellfail.crazywalls.shop.ShopLoader;
import com.wellfail.crazywalls.timer.Timer;
import com.wellfail.crazywalls.utils.Barrier;
import com.wellfail.crazywalls.utils.CWUtils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.main.API;
import ru.bird.main.event.TimerTickEvent;
import ru.bird.main.event.listeners.DamageListener;
import ru.bird.main.event.listeners.StartListener;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.Gamer;
import ru.bird.main.logger.BaseLogger;
import ru.bird.main.utils.FIterator;
import ru.bird.main.utils.GameUtils;

public class GameFactory extends Game
{
    public static Map<Player, GPlayer> players = new HashMap<>();
    public static Timer timer;
    public static Map<String, Shop> shops;

    public GameFactory() {
        super(new GameListener());
        this.loadConfiguration();
        new ShopLoader();
        new KitLoader();
    }

    public static GPlayer getGplayer(final Player player) {
        return GameFactory.players.get(player);
    }

    private void loadConfiguration() {
        FileConfiguration config = Main.getInstance().getConfig();
        try {
            Config.wallHeight = config.getInt("WallHeight");
            Config.greenWalls = CWUtils.getWalls(config, "GreenWalls");
            Config.redWalls = CWUtils.getWalls(config, "RedWalls");
            Config.locations = GameUtils.stringListToLocations(config.getStringList("Locations"));
            Config.delayColb = config.getInt("DelayColb");
            Config.traped = config.getString("Traped");
            Config.chest = config.getString("Chest");
            Config.replace = new ItemStack(config.getInt("Replace")).getType();
            Config.shops = GameUtils.stringListToLocations(config.getStringList("Shop"));
            Config.center = GameUtils.stringToLocation(config.getString("Center"));
            Config.radius = config.getInt("Radius");
            Config.barrier = new Barrier(GameUtils.stringToLocation(config.getString("BarrierMin")), GameUtils.stringToLocation((String)config.getString("BarrierMax")));
            GameSettings.respawnLocation = GameUtils.stringToLocation(config.getString("Lobby"));
            GameSettings.spectrLocation = Config.center;
        }
        catch (Exception ex) {
            BaseLogger.error((String)(GameFactory.getPrefix() + "Ошибка при загрузке конфигурации!" + ex.getMessage()));
        }
    }

    protected void onStartGame() {
        timer = new Timer(840);
        GameSettings.isBreak = false;
        GameSettings.isPlace = false;
        FIterator<Location> it = new FIterator<>(Config.locations);
        Bukkit.getOnlinePlayers().forEach(player -> {
            players.put(player, new GPlayer(player));
            CWUtils.sendStartMessages(player);
            Location loc = it.getNext();
            CWUtils.removeCell(player);
            player.teleport(loc);
            GameUtils.buildCell(player);
            player.teleport(loc);
            CWUtils.giveStartItems(player);
            CWUtils.removeCell(player);
            new GameSidebar(player);
            shops.put(player.getName(), new Shop());
        });
        ShopLoader.holograms.forEach(hologram -> {
            API.getInstance().getHologramsManager().registetHologram(hologram);
        });
        final DamageListener dmg = new DamageListener();
        new BukkitRunnable(){

            public void run() {
                HandlerList.unregisterAll(dmg);
            }
        }.runTaskLater(API.getInstance(), (long)(Config.delayColb + 200));

//        new BukkitRunnable(){
//            public void run() {
//                timer.onTick(new TimerTickEvent(StartListener.getTime()));
//            }
//        }.runTaskTimer(API.getInstance(), 0, 20);

//        Board.spectator = new SpectatorBoard();
    }

    protected void onEndGame() {
        Collection<Gamer> gamers = Gamer.getGamers();
        Player lastAlive = GameUtils.getLastAlive();
        if (lastAlive == null) {
            return;
        }
        Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("                               §c§lCrazy Walls");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage(("                         §6Победил игрок - " + lastAlive.getDisplayName()));
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        Gamer.getGamer(lastAlive).addMoney(50);
        for (Gamer gamer : gamers) {
            GPlayer gPlayer;
            if (players == null
                    || (gPlayer = players.get(gamer.getPlayer())) == null ||
                    gamer.getPlayer() == null
                    ) continue;
            boolean win = false;
            if (lastAlive.getName().equals(gamer.getPlayer().getName())) {
                win = true;
            }

            Player player = gamer.getPlayer();
//            String sql = "INSERT INTO `stats` (`name`, " +
//                    "`cwfb`, `cwgames`, `cwkills`, `cwwins`, `cwdeaths`)" +
//                    " VALUES ('" + gamer.getPlayer().getName() + "', '" + players.get(gamer.getPlayer()).getFb() + "', '1'," + "'" + players.get(gamer.getPlayer()).getKills() + "'," + "'" + (win ? 1 : 0) + "'," + "'" + (!win ? 1 : 0) + "')" + "ON DUPLICATE KEY UPDATE ";
//            if (players.get(player).getKills() != 0) {
//                sql = sql + "`cwkills`=`cwkills`+'" + gamer.getKills() + "',";
//            }
//            if (!win) {
//                sql = sql + "`cwdeaths`=`cwdeaths`+'1',";
//            }
//            if (win) {
//                sql = sql + "`cwwins`=`cwwins`+'1',";
//            }
//            sql = sql + "`cwfb`=`cwfb`+'" + players.get(gamer.getPlayer()).getFb() + "',";
//            sql = sql + "`cwgames`=`cwgames`+'1'";
//            try {
//                BaseLogger.debug(sql);
//                GameFactory.saveStat(sql);
//            }
//            catch (SQLException e) {
//                e.printStackTrace();
//            }
            try {
                PreparedStatement st = API.getInstance().getSQLConnection().getConnection()
                        .prepareStatement("CALL save_stat_cw(?, ?, ?, ?);");
                st.setString(1, player.getUniqueId().toString());
                st.setInt(2, gPlayer.getFb());
                st.setInt(3, gamer.getKills());
                st.setBoolean(4, win);
                st.setInt(4, gamer.getDeaths());
                BaseLogger.debug(st.toString());
                st.executeUpdate();
            } catch (SQLException e) {
                BaseLogger.error(e.getMessage());
            }
        }
    }

    public void onRespawn(Player player) {
    }

    static {
        shops = new HashMap<String, Shop>();
    }

}

