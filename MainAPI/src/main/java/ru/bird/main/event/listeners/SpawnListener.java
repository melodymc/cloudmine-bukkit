/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.entity.CreatureSpawnEvent
 *  org.bukkit.event.entity.ItemSpawnEvent
 */
package ru.bird.main.event.listeners;

import org.bukkit.entity.ArmorStand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import ru.bird.main.event.listeners.FListener;

public class SpawnListener
extends BasicListener {
    @EventHandler
    public void onSpawn(CreatureSpawnEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onItemSpawn(ItemSpawnEvent e) {
        e.setCancelled(true);
    }
}

