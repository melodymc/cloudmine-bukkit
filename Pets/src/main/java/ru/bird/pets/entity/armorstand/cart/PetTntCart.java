package ru.bird.pets.entity.armorstand.cart;

import ru.bird.main.particles.ParticleEffect;

import java.util.UUID;

public class PetTntCart extends PetCart {

	public PetTntCart(UUID id, int uniqueID) {
		super(id, uniqueID, ParticleEffect.EXPLOSION_NORMAL, "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzRkN2ZjOGUzYTk1OWFkZTdkOWNmNjYzZjFlODJkYjc5NzU1NDNlMjg4YWI4ZDExZWIyNTQxODg4MjEzNTI2In19fQ==");
	}

}