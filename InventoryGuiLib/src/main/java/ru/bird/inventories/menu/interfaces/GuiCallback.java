package ru.bird.inventories.menu.interfaces;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public interface GuiCallback
{
    public void execute(Player player, Inventory inventory);
}
