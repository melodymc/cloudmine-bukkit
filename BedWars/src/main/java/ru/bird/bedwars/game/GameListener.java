/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.block.Block
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.Item
 *  org.bukkit.entity.Player
 *  org.bukkit.entity.Projectile
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.block.Action
 *  org.bukkit.event.block.BlockBreakEvent
 *  org.bukkit.event.entity.EntityDamageByEntityEvent
 *  org.bukkit.event.entity.ItemSpawnEvent
 *  org.bukkit.event.inventory.CraftItemEvent
 *  org.bukkit.event.player.AsyncPlayerChatEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.event.player.PlayerPickupItemEvent
 *  org.bukkit.event.player.PlayerQuitEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.potion.PotionEffect
 *  org.bukkit.potion.PotionEffectType
 *  org.bukkit.projectiles.ProjectileSource
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 *  ru.wellfail.groups.Group
 *  ru.wellfail.groups.Groups
 *  ru.wellfail.wapi.events.DeathEvent
 *  ru.wellfail.wapi.game.Game
 *  ru.wellfail.wapi.game.GameSettings
 *  ru.wellfail.wapi.game.GameState
 *  ru.wellfail.wapi.game.Gamer
 */
package ru.bird.bedwars.game;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.bedwars.Main;
import ru.bird.bedwars.Utils;
import ru.bird.bedwars.config.Config;
import ru.bird.bedwars.shop.Shop;
import ru.bird.bedwars.sidebar.GameSidebar;
import ru.bird.bedwars.team.Team;
import ru.bird.main.event.DeathEvent;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.GameState;
import ru.bird.main.game.Gamer;
import ru.bird.main.groups.Groups;

import static org.bukkit.Bukkit.getServer;

public class GameListener
implements Listener {

    @EventHandler
    public void onDeath(DeathEvent e) {
        Player player = e.getPlayer();
        GamePlayer gplayer = GameFactory.players.get(player);
//        GameSidebar sidebar = gplayer.getGameSidebar();
//        sidebar.setNewData();
        e.setPl(Utils.teamColorName(player));
        if (e.getKill() != null) {
            e.setkiller(Utils.teamColorName(e.getKill()));
        }
        e.setMessage(false);
        if (!gplayer.getTeam().isCanSpawn()) {
            gplayer.getTeam().removePlayer(e.getPlayer());
            e.getPlayer().sendMessage(GameSettings.prefix + "Ваша кровать сломана, вы умерли");
        } else {
            gplayer.setBalance(0);
            e.getPlayer().closeInventory();
            player.getInventory().clear();
            player.getInventory().setHelmet(null);
            player.getInventory().setChestplate(null);
            player.getInventory().setLeggings(null);
            player.getInventory().setBoots(null);
            e.setDeny(true);
            e.getPlayer().teleport(gplayer.getTeam().getSpawn());
            e.getPlayer().setFallDistance(0.0f);
            e.getPlayer().setHealth(20.0);
            for (PotionEffect effect : e.getPlayer().getActivePotionEffects()) {
                e.getPlayer().removePotionEffect(effect.getType());
            }
            e.getPlayer().sendMessage(GameSettings.prefix + "Вы возродились");
        }
        GameListener.checkEnd();
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (e.getBlock() == null) {
            return;
        }
        if (!Config.blocks.contains(e.getBlock().getTypeId())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onItemSpawn(ItemSpawnEvent e) {
        Material material = e.getEntity().getItemStack().getType();
        if (material == Material.BED || material == Material.OBSIDIAN || material == Material.STRING || material == Material.GLOWSTONE_DUST) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled = true)
    public void onBedBreak(BlockBreakEvent e) {
        Block block = e.getBlock();
        Player player = e.getPlayer();
        if (block == null) {
            return;
        }
        Material type = block.getType();
        if (type != Material.BED_BLOCK) {
            return;
        }
        e.setCancelled(true);
        Team.teams.forEach(team -> {
            if (team.getPlayers().contains(player)) {
                return;
            }
            if (team.breakBed(Utils.teamColorName(e.getPlayer()), e.getBlock().getLocation())) {
                ++GameFactory.players.get(e.getPlayer()).beds;
                for(Player current : getServer().getOnlinePlayers()){
                    current.playSound(current.getLocation(), Sound.ENTITY_WITHER_DEATH, 1, 1);
                }
            }
        }
        );
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onPickup(PlayerPickupItemEvent e) {
        Player player = e.getPlayer();
        Gamer ggamer = Gamer.getGamer(player);
        Material material = e.getItem().getItemStack().getType();
        if (!ggamer.isSpectator()) {
            ItemStack item = e.getItem().getItemStack();
            GamePlayer gamer = GameFactory.players.get(player);
            switch (material) {
                case BED:
                case OBSIDIAN:
                    e.getItem().remove();
                    e.setCancelled(true);
                    break;
                case LEATHER_CHESTPLATE:
                case LEATHER_HELMET:
                case LEATHER_LEGGINGS:
                case LEATHER_BOOTS:
                    Player p = e.getPlayer();
                    Team team = Team.select.get(p);
                    LeatherArmorMeta map = (LeatherArmorMeta) item.getItemMeta();
                    map.setColor(GameFactory.getColor(team.getColor()));
                    item.setItemMeta(map);
                    break;
                case CLAY_BRICK:
                case IRON_INGOT:
                case GOLD_INGOT:
                    e.getItem().remove();
                    e.setCancelled(true);
                    int price = Shop.cost_item(item);
                    if (price >= 0) {
                        gamer.addBalance(price);
                    }
                    player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP,1, 1);
                    break;
                default:
                    e.setCancelled(false);
            }
        }
    }

    @EventHandler(priority=EventPriority.LOWEST)
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Villager) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onCraft(CraftItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if (GameState.current != GameState.GAME || Gamer.getGamer(e.getPlayer()).isSpectator()) {
            return;
        }
        if (e.isCancelled()) {
            return;
        }
        String format = "*type*%player: %message";
        GamePlayer gamer = GameFactory.players.get(e.getPlayer());
        boolean global = false;
        if (e.getMessage().startsWith("!")) {
            e.setMessage(e.getMessage().replaceFirst("!", ""));
            global = true;
        }
        if (format.contains("*type*")) {
            format = format.replace("*type*", global ? "§8[" + gamer.getTeam().getColor() + "Всем§8] " : "§8[" + gamer.getTeam().getColor() + "Команде§8] ");
            format = format.replace("%1$s", e.getPlayer().getName());
        }

        format = format.replace("%message", "%2$s").replace("%player", Groups.getFullName(e.getPlayer())).replace("&", "§");
        e.setFormat(format);
        if (!global) {
            e.getRecipients().clear();
            Team team = gamer.getTeam();
            for (Player player : team.getPlayers()) {
                e.getRecipients().add(player);
            }
        }
    }

    @EventHandler
    public void EnderBank(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType() == Material.ENDER_CHEST) {
            e.setCancelled(true);
            if (Gamer.getGamer(player).isSpectator()) {
                return;
            }
            player.openInventory(GameFactory.players.get(player).getTeam().getChest());
        }
    }

    @EventHandler(priority=EventPriority.HIGHEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        ProjectileSource shooter;
        if (!(e.getEntity() instanceof Player)) {
            return;
        }
        GamePlayer damager = null;
        GamePlayer target = GameFactory.players.get(e.getEntity());
        if (e.getDamager() instanceof Player) {
            damager = GameFactory.players.get(e.getDamager());
        }
        if (e.getDamager() instanceof Projectile && (shooter = ((Projectile)e.getDamager()).getShooter()) != null && shooter instanceof Player) {
            damager = GameFactory.players.get(shooter);
        }
        if (damager == null || target == null) {
            return;
        }
        if (damager.getTeam() == target.getTeam()) {
            e.setCancelled(true);
        }
    }


    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        Gamer gamer = Gamer.getGamer(player);
        if(player.getLocation().getY() <= 0) {
            if(!gamer.isSpectator()) {
                DeathEvent event = new DeathEvent(player);
                Bukkit.getPluginManager().callEvent(event);
            } else {
                player.teleport(GameSettings.spectrLocation);
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        if (!Gamer.getGamer(e.getPlayer()).isSpectator() && GameState.current != GameState.END) {
            Team.teams.forEach(team -> {
                team.removePlayer(e.getPlayer());
            }
            );
        }
        if (GameState.current == GameState.GAME) {
            GameListener.checkEnd();
        }
    }

    public static synchronized void checkEnd() {
        Team.teams.forEach((t) -> {
            if(t.getSize() == 0) {
                if(t.isFirst()) { // костыль. изначально все команды win true и поэтому выводится один раз при проигрыше, после уже
                    t.setFirst(false);
                    Bukkit.broadcastMessage(GameSettings.prefix + "Команда " + t.getColor() + t.getName() + "§f проиграла");
                }
                t.setWin(false);
            } else {
                t.setWin(true);
            }
        });
        if (Team.teams.stream().filter(Team::isWin).count() == 1) {
            Team.teams.stream().filter(Team::isWin).forEach(t -> {
                try {
                    GameFactory.win = t;
                    t.win();
                    Game.getInstance().endGame();
                } catch (Exception var4) {
                    new BukkitRunnable() {
                        public void run() {
                            getServer().shutdown();
                        }
                    }.runTaskLater(Main.getInstance(), 40L);
                }
            });
        }
    }
}

