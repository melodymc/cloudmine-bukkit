/*
 * Decompiled with CFR 0_114.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.Player
 */
package ru.bird.bedwars.game;

import ru.bird.bedwars.sidebar.GameSidebar;
import ru.bird.bedwars.team.Team;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import ru.bird.main.actionbar.ActionBarApi;
import ru.bird.main.logger.BaseLogger;

public class GamePlayer {
    private Player player;
    private Team team;
    public int kills;
    public int beds;
    public int deaths;
    public boolean fb = false;
    public boolean win = false;
    public int balance = 0;
    private GameSidebar gameSidebar;

    public GamePlayer(Player player, Team team) {
        this.player = player;
        this.team = team;
    }

    public void addKill() {
        ++this.kills;
    }

    public String getName() {
        return this.player.getName();
    }

    public void setWin() {
        this.win = true;
    }

    public Team getTeam() {
        return this.team;
    }
    public Integer getBalance() {
        return this.balance;
    }
    public void addBalance(int plus) {
        int oldbal = this.balance;
        BaseLogger.debug("Изменение баланса\nold - "+ oldbal);
        this.balance = oldbal + plus;
        ActionBarApi.sendActionBar(player,  "§8▸ §fОчки: §b☄" + oldbal + "+" + plus, 41, false);
        BaseLogger.debug("new - "+ this.balance);
    }

    public boolean minusBalance(int minus) {
        int oldbal = this.balance;
        BaseLogger.debug("Изменение баланса\nold - " + oldbal);
        if(oldbal - minus >= 0) {
            this.balance = oldbal - minus;
            ActionBarApi.sendActionBar(player,  "§8▸ §fОчки: §b☄" + oldbal + "-" + minus, 41, false);
            BaseLogger.debug("new - "+ oldbal);

            return true;
        } else {
            BaseLogger.debug("Мало денег");
            return false;
        }
    }
    public void setBalance(int newb) {
        BaseLogger.debug("Изменение баланса\nnew - "+ newb);
        this.balance = newb;
    }

    public void setGameSidebar(GameSidebar gameSidebar) {
        this.gameSidebar = gameSidebar;
    }

    public GameSidebar getGameSidebar() {
        return gameSidebar;
    }
}