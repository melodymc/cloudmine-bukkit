/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.event.Listener
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.plugin.PluginManager
 */
package ru.bird.main.event.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import ru.bird.main.API;
import ru.bird.main.logger.BaseLogger;

public class FListener
implements Listener {
    protected FListener() {
        Bukkit.getServer().getPluginManager().registerEvents(this, API.getInstance());
        BaseLogger.info("Register listener - " + this.getClass().getSimpleName());
    }
}

