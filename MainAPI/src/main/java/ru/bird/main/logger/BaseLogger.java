package ru.bird.main.logger;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

public class BaseLogger {
    public static Logger logger;
    private static final String PREFIX = "MainAPI";
    private static boolean debug;

    public static void setLogger(Logger log) {
        logger = log;
    }

    public static void info(String message, Object ... objects) {
        info(String.format(message, objects));
    }

    public static void debug(String message, Object ... objects) {
        debug(String.format(message, objects));
    }

    public static void error(String message, Object ... objects) {
        error(String.format(message, objects));
    }

    public static void info(String message) {
        if (logger != null) {
            logger.log(Level.INFO, "[INFO]" + " " + message);
        }
    }

    public static void debug(String message) {
        if (debug && logger != null) {
            logger.log(Level.INFO, "[DEBUG]" + " " + message);
        }
    }

    public static void error(String message) {
        if (logger != null)
            logger.log(Level.INFO, "[ERROR]" + " " + message);
    }

    public static boolean getDebug() {
        return debug;
    }

    public static void setDebug(boolean debug) {
        BaseLogger.debug = debug;
    }
}