package pir.teraingenerator;

import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin
{
    public static String ADDITIONAL_SUPPLIER_WORLD_NAME = "world_supplier";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        switch (command.getName())
        {
            case "worlds":
            {
                sender.sendMessage(ChatColor.GRAY + "Available worlds: ");
                for (World world: Bukkit.getWorlds())
                {
                    sender.sendMessage(ChatColor.GRAY + " - " + world.getName());
                }

                return true;
            }
            case "initworld":
            {
                /* Checking inputs */
                String rawSeed = (args.length > 0) ? args[0] : "";
                long seed = WorldUtils.getSeed(rawSeed);

                /* Logic */
                if (Bukkit.getWorld(ADDITIONAL_SUPPLIER_WORLD_NAME) == null)
                {
                    Bukkit.createWorld(new WorldCreator(ADDITIONAL_SUPPLIER_WORLD_NAME).seed(seed).type(WorldType.NORMAL));

                    sender.sendMessage(ChatColor.GREEN + "Success.");
                }
                else
                {
                    sender.sendMessage(ChatColor.RED + "The world already exists.");

                    return false;
                }

                return true;
            }
            case "copychunk":
            {
                /* Checking inputs */
                if (args.length < 6) return false;

                // Unchecked args
                String supplierWorldName = args[0]; int sx, sz;
                String consumerWorldName = args[3]; int cx, cz;

                World supplierWorld = Bukkit.getWorld(supplierWorldName);
                World consumerWorld = Bukkit.getWorld(consumerWorldName);

                if (supplierWorld == null)
                {
                    sender.sendMessage(ChatColor.RED + "The supplier world not exists.");

                    return false;
                }

                if (consumerWorld == null)
                {
                    sender.sendMessage(ChatColor.RED + "The consumer world not exists.");

                    return false;
                }

                try
                {
                    sx = Integer.parseInt(args[1]); sz = Integer.parseInt(args[2]);
                    cx = Integer.parseInt(args[4]); cz = Integer.parseInt(args[5]);
                }
                catch (NumberFormatException exception)
                {
                    sender.sendMessage(ChatColor.RED + "Incorrect a chunk coords.");

                    return false;
                }

                if (args.length == 7 && args[6].isEmpty() == false)
                {
                    sender.sendMessage(ChatColor.GRAY + "Info: ");
                    sender.sendMessage(ChatColor.GRAY + " - Supplier world: " + supplierWorldName + ", x: " + sx + ", z: " + sz + ".");
                    sender.sendMessage(ChatColor.GRAY + " - Consumer world: " + consumerWorldName + ", x: " + cx + ", z: " + cz + ".");
                }

                /* Logic */
                WorldUtils.copyChunk(supplierWorld, sx, sz, consumerWorld, cx, cz);

                sender.sendMessage(ChatColor.GREEN + "Success.");

                return true;
            }
            case "toworld":
            {
                /* Checking inputs */
                if (args.length == 0) return false;

                // Unchecked args
                String destWorldName = args[0];
                String subjectName = (args.length < 2) ? sender.getName() : args[1];

                Player subject = Bukkit.getPlayer(subjectName);

                if (subject == null)
                {
                    sender.sendMessage(ChatColor.RED + "The player not exists.");

                    return false;
                }

                World source = subject.getWorld();
                World destination = Bukkit.getWorld(destWorldName);

                if (destination == null)
                {
                    sender.sendMessage(ChatColor.RED + "The destination world not exists.");

                    return false;
                }

                if ( source.equals(destination) )
                {
                    sender.sendMessage(ChatColor.RED + "You're already be within specified world.");

                    return false;
                }

                /* Logic */
                Location oldLocation = subject.getLocation();
                Location newLocation = new Location( destination, oldLocation.getX(), oldLocation.getY(), oldLocation.getZ() );

                subject.teleport( newLocation );

                return true;
            }
        }

        return false;
    }
}
