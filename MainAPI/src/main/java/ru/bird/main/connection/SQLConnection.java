package ru.bird.main.connection;

import org.bukkit.configuration.file.YamlConfiguration;
import ru.bird.main.API;
import ru.bird.main.logger.BaseLogger;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLConnection {
    private Connection connection;
    private String host;
    private String database;
    private String user;
    private String password;
    private YamlConfiguration config;

    public SQLConnection() {
        loadStatConfig();
    }

    public void loadStatConfig() {
        File f = new File(API.getInstance().getDataFolder().getAbsolutePath() + "/connection.yml");
        config = YamlConfiguration.loadConfiguration(f);
        config.addDefault("db.host", "localhost");
        config.addDefault("db.user", "root");
        config.addDefault("db.password", "changeme");
        config.addDefault("db.database", "database");
        try {
            config.options().copyDefaults(true);
            config.save(f);
            BaseLogger.info("connection.yml был успешно загружен");
        } catch (IOException e) {
            BaseLogger.error("ERROR: can not save config to connection.yml. Please check it.");
        }
        this.host = config.getString("db.host");
        this.user = config.getString("db.user");
        this.password = config.getString("db.password");
        this.database = config.getString("db.database");
    }

    public Connection getConnection() throws SQLException {
        if (this.connection == null || this.connection.isClosed()) {
            this.connect();
        }
        return this.connection;
    }

    public YamlConfiguration getConfig() {
        return config;
    }

    public void connect() throws SQLException {
        this.connection = DriverManager.getConnection("JDBC:mysql://" + this.host + ":3306/" + this.database + "?characterEncoding=utf8&useUnicode=true&interactiveClient=true&3DInnoDB&autoReconnect=true", this.user, this.password);
    }

    public void execute(String sql) throws SQLException {
        Statement statement = null;
        try {
            statement = this.getConnection().createStatement();
            statement.executeUpdate(sql);
        }
        finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

}