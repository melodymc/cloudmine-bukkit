package ru.bird.main.board.api;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import ru.bird.main.board.common.TaskManager;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Board {

    public static final Set<Board> REGISTERED_BOARDS = Sets.newHashSet();
    private static final AtomicInteger NEXT_ID = new AtomicInteger();

    private TaskManager taskManager = new TaskManager(this);
    private BoardObjective objective;
    private Map<Integer, BoardLine> lines = Maps.newHashMap();
    private int number;

    public Board(String displayName) {
        objective = new BoardObjective(this,NEXT_ID.getAndIncrement() + "Board", displayName);
    }

    public void setDisplay(String display) {
        objective.setDisplayName(display);
    }

    public BoardLine write(int index) {
        Preconditions.checkArgument(isWritable(), "Board don't writable");
        Preconditions.checkArgument(lines.size() < 16, "Cannot write more than 15 lines");
        return lines.put(index, check(new BoardLine(this, index, ChatColor.COLOR_CHAR + "1")));
    }

    public BoardLine write(int index, String text) {
        Preconditions.checkArgument(isWritable(), "Board don't writable");
        Preconditions.checkArgument(lines.size() < 16, "Cannot write more than 15 lines");
        return lines.put(index, check(new BoardLine(this, index, text)));
    }

    /**
     *
     * @param interval время в тиках (1/20 секунды), интервал обновления хедера
     * @param list список строк каждого кадра обновления
     */
    public void addUpdaterHeader(long interval, List<String> list) {
        addUpdater(interval, board -> {
            if(board.getNumber() == list.size()) {
                board.setNumber(0);
            }
            board.setDisplay(list.get(board.getNumber()));
            board.setNumber(board.getNumber() + 1);
        });
    }

    public void modifyLine(int index, String text) {
        Preconditions.checkArgument(!isWritable(), "Board must be created");
        Preconditions.checkArgument(lines.containsKey(index), "Board don't has line #" + index);
        lines.get(index).setText(text);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void addUpdater(long interval, BoardUpdater task) {
        taskManager.addUpdater(interval, task);
    }

    public boolean isWritable() {
        return !objective.isRegistered();
    }

    public void create() {
        Preconditions.checkArgument(isWritable(), "Board already created");
        objective.register();
        lines.values().forEach(BoardLine::update);
        if(taskManager.size() > 0) {
            taskManager.startUpdate();
        }
        REGISTERED_BOARDS.add(this);
    }

    public void remove() {
        Preconditions.checkArgument(!isWritable(), "Board not created yet");
        objective.unregister();
        taskManager.cancel();
        REGISTERED_BOARDS.remove(this);
    }

    public BoardObjective getObjective() {
        return objective;
    }

    private BoardLine check(BoardLine line) {
        String text = line.getText();
        while(getLines().contains(text)) text += "§r";
        line.setText(text);
        return line;
    }

    public List<String> getLines() {
        return lines.values().stream().map(BoardLine::getText).collect(Collectors.toList());
    }

    public Collection<BoardLine> lineWrappers() {
        return lines.values();
    }

    public void send() {
        Bukkit.getOnlinePlayers().forEach(this::send);
    }

    public void send(Player... player) {
        objective.display(player);
    }

    public static BoardBuilder getBuilder(Board board) {
        return new BoardBuilder(board);
    }


    public static BoardBuilder getBuilder(String title) {
        return getBuilder(new Board(title));
    }

    public static BoardBuilder getPersonalBuilder(String title, Player player) {
        return getBuilder(new PersonalBoard(title, player));
    }
}