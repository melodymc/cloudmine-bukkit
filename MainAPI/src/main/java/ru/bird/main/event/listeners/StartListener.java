/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.Sound
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.Event
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.player.PlayerCommandPreprocessEvent
 *  org.bukkit.event.player.PlayerJoinEvent
 *  org.bukkit.event.player.PlayerQuitEvent
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 */
package ru.bird.main.event.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.main.API;
import ru.bird.main.event.TimerTickEvent;
import ru.bird.main.groups.Groups;
import ru.bird.main.actionbar.ActionBarApi;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.GameState;
import ru.bird.main.utils.GameUtils;
import ru.bird.main.utils.Title;

import java.util.concurrent.atomic.AtomicInteger;

public class StartListener
extends BasicListener
implements Runnable {
    private boolean starting;
    private Thread thread;
    private static AtomicInteger time = new AtomicInteger();
    private static int i = GameSettings.gameTime;

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        if (GameState.current == GameState.GAME || GameState.current == GameState.END) {
            return;
        }
        int online = Bukkit.getOnlinePlayers().size();
        if (!this.starting) {
            if (GameSettings.toStart <= online) {
                this.start();
            }
        } else if (GameSettings.slots == online && time.get() > 15) {
            time.set(5);
        } else if (GameSettings.slots / 2 == online && time.get() > 15) {
            time.set(15);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        int online = Bukkit.getOnlinePlayers().size();
        Player player = e.getPlayer();
        if (GameUtils.isSpectator(player)) {
            return;
        }
        if (Game.spectators.contains(player)) {
            Game.spectators.remove(player);
        }
        Game.broadcast(Groups.getFullName(player) + "§f покинул игру");
        ActionBarApi.sendActionBar(player, "[§a" + (GameUtils.getAlivePlayers().size() - 1) + "§a/§a" + GameSettings.slots + "§f]", 21, false);
        if (GameState.current == GameState.STARTING) {
            if (online < GameSettings.toStart) {
                Game.broadcast("Недостаточно игроков для старта");
                this.starting = false;
                this.thread.stop();
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    pl.setExp(1.0f);
                    pl.setLevel(60);
                    pl.sendMessage("§fДостигнуто минимальное количество игроков для старта §a" + GameSettings.toStart + ". §a60 §fсекунд до начала игры.");
                }
                GameState.setCurrent(GameState.WAITING);
            }
        }
        if (GameState.current == GameState.GAME) {
            new BukkitRunnable(){
                public void run() {
                    int size = GameUtils.getAlivePlayers().size();
                    if (size < 1) {
                        Game.getInstance().endGame();
                    }
                }
            }.runTaskLater(API.getInstance(), 10);
        }
    }

    private void start() {
        time.set(60);
        this.thread = new Thread(this);
        this.thread.start();
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent e) {
        if (!e.getMessage().equals("/start")) {
            return;
        }
        Player player = e.getPlayer();
        if (!Groups.hasPermission(player, "mg.start")) {
            return;
        }
        this.start();
        time.set(5);
        e.setCancelled(true);
    }

    public static void setTime(int t) {
        i = t;
    }

    public static int getTime() {
        return time.get();
    }

    @Override
    public void run() {
        if (this.starting) {
            return;
        }
        this.starting = true;
        GameState.setCurrent(GameState.STARTING);
        try {
            while ((StartListener.i = time.get()) > 0) {
//                if (i % 30 == 0 || (i <= 15 && i >= 10)) {
//                    Game.broadcast("Игра начнется через §a" + i + "§f секунд");
//                }
                String title = null;
                Bukkit.getPluginManager().callEvent(new TimerTickEvent(i));
                if(i == 10) {
                    title = "§a➉";
                }
                if(i == 9) {
                    title = "§a➈";
                }
                if(i == 8) {
                    title = "§a➇";
                }
                if(i == 7) {
                    title = "§a➆";
                }
                if(i == 6) {
                    title = "§a➅";
                }
                if(i == 5) {
                    title = "§6➄";
                }
                if(i == 4) {
                    title = "§6➃";
                }
                if(i == 3) {
                    title = "§c➂";
                    GameUtils.getAlivePlayers().forEach(player -> {
                        player.playSound(player.getLocation(), Sound.BLOCK_PISTON_CONTRACT, 1.0f, 1.0f);
                    });
                }
                if(i == 2) {
                    title = "§c➁";
                    GameUtils.getAlivePlayers().forEach(player -> {
                        player.playSound(player.getLocation(), Sound.BLOCK_PISTON_EXTEND, 1.0f, 1.0f);
                    });
                }
                if(i == 1) {
                    GameUtils.getAlivePlayers().forEach(player -> {
                        player.playSound(player.getLocation(), Sound.BLOCK_PISTON_CONTRACT, 1.0f, 1.0f);
                    });
                    title = "§c➀";
                }
                if(title != null) {
                    Title object = new Title(title);
                    object.setStayTime(1);
                    object.broadcast();
                }
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.setExp((float)i / 120.0f);
                    player.setLevel(i);
                }
                Thread.sleep(1000);
                time.getAndDecrement();
            }
            new BukkitRunnable(){
                public void run() {
                    Game.getInstance().onGameStart();
                }
            }.runTask(API.getInstance());
            if (GameSettings.gameTime == -1) {
                return;
            }
            for (int i = GameSettings.gameTime; i > 0; --i) {
                time.set(i);
                Bukkit.getPluginManager().callEvent(new TimerTickEvent(i));
                if (i == 30 || i <= 10) {
                    Game.broadcast("Игра закончится через §a " + i + "§f секунд");
                }
                Thread.sleep(1000);
            }
//            while (time.get() > 0) {
//                Bukkit.getPluginManager().callEvent(new TimerTickEvent(i));
//                if (i == 30 || i <= 10) {
//                    Game.broadcast("Игра закончится через §a " + i + "§f секунд");
//                }
//                Thread.sleep(1000);
//                time.getAndDecrement();
//            }


            if (StartListener.i == -1) {
                return;
            }
            new BukkitRunnable(){
                public void run() {
                    Game.getInstance().endGame();
                }
            }.runTask(API.getInstance());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}

