package ru.bird.chat.utils;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

public class CommandsUtils {

    public static boolean senderIsNotConsole(CommandSender sender) {
        return !(sender instanceof ConsoleCommandSender);
    }
}
