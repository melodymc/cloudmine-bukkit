package ru.bird.portals;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.utils.GameUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

public final class Portals extends JavaPlugin implements Listener {

    private static final String CONFIG_PATH = "/config.yml";
    private YamlConfiguration config;
    private List<Portal> portals;
    private static Portals plugin;

    @Override
    public void onEnable() {
        plugin = this;
        config();
        setupPortals();
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onNativePortal(PlayerPortalEvent event){
        if(event.getCause() == PlayerTeleportEvent.TeleportCause.END_PORTAL) {
            event.setCancelled(true);
            event.getPlayer().setVelocity(GameUtils.PlayerThrow(event.getPlayer()));
        }
    }

    private void setupPortals() {
        ConfigurationSection srcPortals = config.getConfigurationSection("portals");
        Set<String> set = srcPortals.getKeys(false);
        portals = new ArrayList<Portal>();
        set.forEach(key -> {
            Map<String, Object> map = srcPortals.getConfigurationSection(key).getValues(true);
            List<Location> locations = deserializeListLocations(cast(map.get("locations")));
            Portal portal = new Portal(plugin, key, locations, (String) map.get("ru.bird.jobs.command"));
            portals.add(portal);
        });
    }

    private List<Location> deserializeListLocations(List<Map<String, Object>> locationsSRC) {
        List<Location> locations = new ArrayList<>();
        locationsSRC.forEach(locationsSRC_el -> {
            locations.add(Location.deserialize(locationsSRC_el));
        });
        return locations;
    }

    @SuppressWarnings("unchecked")
    public static <T extends List<?>> T cast(Object obj) {
        return (T) obj;
    }

    public void config() {
        File f = new File(this.getDataFolder().getAbsolutePath() + CONFIG_PATH);
        config = YamlConfiguration.loadConfiguration(f);
        Map<String, Object> defListportals = new HashMap<String, Object>();
        Map<String, Object> portal = new HashMap<>();
        List<Map<String, Object>> locations = new ArrayList<>();
        locations.add(new Location(Bukkit.getWorld("Lobby"), 0, 0, 0).serialize());
        locations.add(new Location(Bukkit.getWorld("Lobby"), 0, 0, 1).serialize());
        locations.add(new Location(Bukkit.getWorld("Lobby"), 0, 2, 1).serialize());
        locations.add(new Location(Bukkit.getWorld("Lobby"), 3, 2, 1).serialize());
        portal.put("locations", locations);
        portal.put("ru.bird.jobs.command", "swithservers SW");
        defListportals.put("testPortal", portal);
        this.config.addDefault("portals", defListportals);
        try {
            this.config.options().copyDefaults(true);
            this.config.save(f);
        }
        catch (IOException e) {
            this.getLogger().warning("ERROR: can not save config to " + CONFIG_PATH + ". Please check it.");
        }
    }

//    private String getGroupOnline(String s) {
//        Set<String> servers = SocketManager.getServers(s, (byte)1);
//        int online = 0;
//        for (String server : servers) {
//            online = API.getInstance().getSocketManager().getOnlineServer(server);
//        }
//        String str = String.valueOf(online);
//        System.out.println("servers " + s + " online " + online);
//        return str;
//    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public static Portals get() {
        return plugin;
    }

}