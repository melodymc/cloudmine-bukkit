package ru.bird.inventories;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class PageManager {
    private static final Map<JavaPlugin, Map<Player, Integer>> playersPage = new HashMap<>();

    public static Integer getPage(JavaPlugin plugin, Player player){
        return playersPage.get(plugin).get(player);
    }

    public static void setPage(JavaPlugin plugin, Player player, int i){
        playersPage.get(plugin).put(player, i);
    }
}