package ru.bird.jobs.jobgame.miner.event;

import org.bukkit.inventory.Inventory;
import ru.bird.jobs.jobgame.event.JobEventUnified;
import ru.bird.jobs.jobgame.miner.JobGameMiner;

public class AppearExitEvent extends JobEventUnified
{
    private int slot;
    private Inventory levelData;
    private boolean isBonus;

    public AppearExitEvent(JobGameMiner instance, int slot, Inventory levelData, boolean isBonus)
    {
        super(instance);

        this.slot = slot;
        this.levelData = levelData;
        this.isBonus = isBonus;
    }

    public int getSlot()
    {
        return slot;
    }

    public Inventory getLevelData()
    {
        return levelData;
    }

    public boolean isBonus()
    {
        return isBonus;
    }
}
