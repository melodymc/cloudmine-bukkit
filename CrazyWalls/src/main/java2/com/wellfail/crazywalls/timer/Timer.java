// 
// Decompiled by Procyon v0.5.30
// 

package com.wellfail.crazywalls.timer;

import com.wellfail.crazywalls.wall.Wall;
import org.bukkit.event.EventHandler;
import ru.wellfail.wapi.utils.IWorldBorder;
import com.wellfail.crazywalls.config.Config;
import ru.wellfail.wapi.game.Game;
import ru.wellfail.wapi.events.TimerTickEvent;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.listeners.FListener;

public class Timer extends FListener
{
    private int green;
    private int red;
    private int border;
    private boolean startetBoarder;
    private boolean destroyedGreen;
    private boolean destroyedRed;
    
    public Timer(final int time) {
        this.startetBoarder = false;
        this.destroyedGreen = false;
        this.destroyedRed = false;
        this.green = time - 210;
        this.red = time - 90;
        this.border = time;
        this.setTime(GameSettings.gameTime);
    }
    
    public int getGreen() {
        return this.green;
    }
    
    public int getRed() {
        return this.red;
    }
    
    public int getBorder() {
        return this.border;
    }
    
    public void setTime(final int time) {
        if (time - 570 >= 0) {
            this.green = time - 570;
        }
        if (time - 450 >= 0) {
            this.red = time - 450;
        }
        if (time - 360 >= 0) {
            this.border = time - 360;
        }
    }
    
    @EventHandler
    public void onTick(final TimerTickEvent event) {
        this.setTime(event.getTick());
        if (this.getGreen() == 60) {
            Game.broadcast("§a\u0417\u0435\u043b\u0435\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430 §f\u0443\u043f\u0430\u0434\u0435\u0442 \u0447\u0435\u0440\u0435\u0437 §a1 §f\u043c\u0438\u043d\u0443\u0442\u0443!");
        }
        if (this.getGreen() == 0 && !this.destroyedGreen) {
            Config.greenWalls.stream().forEach(wall -> wall.destroy());
            this.destroyedGreen = true;
        }
        if (this.getRed() == 60) {
            Game.broadcast("§c\u041a\u0440\u0430\u0441\u043d\u0430\u044f \u0441\u0442\u0435\u043d\u0430 §f\u0443\u043f\u0430\u0434\u0435\u0442 \u0447\u0435\u0440\u0435\u0437 §a1 §f\u043c\u0438\u043d\u0443\u0442\u0443!");
        }
        if (this.getRed() == 0 && !this.destroyedRed) {
            Config.redWalls.stream().forEach(wall -> wall.destroy());
            this.destroyedRed = true;
        }
        if (this.getBorder() == 60) {
            Game.broadcast("§f\u0414\u0435\u0437\u043c\u0430\u0442\u0447 \u043d\u0430\u0447\u043d\u0435\u0442\u0441\u044f \u0447\u0435\u0440\u0435\u0437 §a1 §f\u043c\u0438\u043d\u0443\u0442\u0443!");
        }
        if (this.getBorder() == 0 && !this.startetBoarder) {
            IWorldBorder.startBorder(Config.center, 720 / Config.radius, Config.radius);
            this.startetBoarder = true;
        }
    }
}
