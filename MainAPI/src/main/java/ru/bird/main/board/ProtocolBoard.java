package ru.bird.main.board;

import com.comphenix.protocol.ProtocolLibrary;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.API;
import ru.bird.main.APIInitialiser;
import ru.bird.main.board.api.Board;
import ru.bird.main.board.listeners.PacketListener;
import ru.bird.main.board.listeners.PlayerListener;

public class ProtocolBoard {
    
    private PacketListener packetListener;

    public void build(APIInitialiser api) {
        packetListener = new PacketListener();
        api.getProtocolManager().addPacketListener(packetListener);
        api.getServer().getPluginManager().registerEvents(packetListener, api);
        api.getServer().getPluginManager().registerEvents(new PlayerListener(), api);
    }

    public PacketListener getPacketListener() {
        return packetListener;
    }

}