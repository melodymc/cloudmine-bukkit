package ru.bird.board.main;

import ru.bird.main.sidebar.Sidebar;
import ru.bird.main.sidebar.SidebarManager;
import ru.bird.main.sidebar.SidebarString;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.main.socket.MessageManager;
import ru.bird.main.socket.message.Server;
import ru.bird.main.socket.message.StatusListener;
import ru.bird.main.API;
import ru.bird.main.socket.SocketManager;

import java.util.*;
import java.util.logging.Logger;

public final class Main extends JavaPlugin {

    private static Main plugin;
    public static Logger log;

    @Override
    public void onEnable() {
        plugin = this;
        log = getLogger();
        new StatusListener(plugin,"Survival");
        new StatusListener(plugin,"SW");
        new StatusListener(plugin,"BW");
        new JoinQuitListener(plugin);
    }

    public static Main getPlugin() {
        return plugin;
    }

    public static Map<String, Server> getServers() {
        return MessageManager.getServers();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public static synchronized void fillSidebar(Player player){
        if(!player.isOnline()) {
            return;
        }
        SidebarString[] entitys = new SidebarString[5];
        entitys[0] = new SidebarString(1, ChatColor.AQUA +"");
        entitys[1] = new SidebarString(1,"§fBedWars: §l§b"+ MessageManager.getGroupOnline("BW"));
        entitys[2] = new SidebarString(1,"§fSkyWars: §l§b" + MessageManager.getGroupOnline("SW"));
        entitys[3] = new SidebarString(1,"§fВыживание: §l§b" + MessageManager.getGroupOnline("Survival"));
        entitys[4] = new SidebarString(1, ChatColor.RED +"");
        Sidebar sidebar = new Sidebar("§e§lLATTYCRAFT", getPlugin(), 20, entitys);
        sidebar.showTo(player);
        sidebar.setUpdateDo(new Runnable() {
            @Override
            public void run() {
                startUpdateSidebar(player);
            }
        });
    }

    public static void startUpdateSidebar(Player player){
        Sidebar sidebar = SidebarManager.getSidebar(player);
        List<SidebarString> entitys = sidebar.getEntries();
        entitys.get(1).getVariations().set(0, "§fBedWars: §l§b" + MessageManager.getGroupOnline("BW"));
        entitys.get(2).getVariations().set(0, "§fSkyWars: §l§b" + MessageManager.getGroupOnline("SW"));
        entitys.get(3).getVariations().set(0, "§fВыживание: §l§b" + MessageManager.getGroupOnline("Survival"));

    }


//    private static String getOnlineGroup(String s) {
//        Set<String> servers = SocketManager.getServers(s, (byte)1);
//        int online = 0;
//        for (String server : servers) {
//            int tpm_online = API.getInstance().getSocketManager().getOnlineServer(server);
//            if(tpm_online != -1) {
//                online += tpm_online;
//            }
//        }
//        String str = String.valueOf(online);
//        System.out.println("servers " + s + " online " + online);
//        return str;
//    }
//
//    public static String getOnlineServer(String server){
//        int online = API.getInstance().getSocketManager().getOnlineServer(server);
//        return online != -1 ? online + "" : "0";
//    }
}
