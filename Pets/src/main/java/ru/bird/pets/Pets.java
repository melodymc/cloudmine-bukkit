package ru.bird.pets;

import org.bukkit.plugin.java.JavaPlugin;
import ru.bird.pets.commands.PetsCommands;

public final class Pets extends JavaPlugin {

    private static PetsManager petsManager;
    private static Pets plugin;

    @Override
    public void onEnable() {
        plugin = this;
        loadCommand();
        petsManager = new PetsManager();
    }

    @Override
    public void onDisable(){
        petsManager.purgeAll();
    }

    public static PetsManager getPetsManager() {
        return petsManager;
    }

    private void loadCommand() {
        new PetsCommands().register();
    }

    public static Pets getPlugin() {
        return plugin;
    }

}