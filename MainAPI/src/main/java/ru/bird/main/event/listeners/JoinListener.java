/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.player.PlayerJoinEvent
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.potion.PotionEffect
 *  org.bukkit.potion.PotionEffectType
 */
package ru.bird.main.event.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import ru.bird.main.API;
import ru.bird.main.actionbar.ActionBarApi;
import ru.bird.main.game.Game;
import ru.bird.main.game.GameSettings;
import ru.bird.main.game.GameState;
import ru.bird.main.game.Gamer;
import ru.bird.main.groups.Groups;
import ru.bird.main.utils.Title;

public class JoinListener
extends BasicListener {
    public static BukkitRunnable run;

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        if (GameState.current == GameState.GAME || GameState.current == GameState.END) {
            player.getInventory().clear();
            player.setHealth(20.0);
            player.setFoodLevel(20);
            player.getInventory().setHelmet(null);
            player.getInventory().setChestplate(null);
            player.getInventory().setLeggings(null);
            player.getInventory().setBoots(null);
            for (PotionEffect pt : player.getActivePotionEffects()) {
                player.removePotionEffect(pt.getType());
            }
            Gamer.getGamer(player).setSpectator(true);
            player.teleport(GameSettings.spectrLocation);
            Game.spectators.add(player);
            return;
        }
        Location spawn = GameSettings.respawnLocation;
        if (spawn != null) {
            player.teleport(GameSettings.respawnLocation);
        }
        Game.broadcast(Groups.getFullName(player) + "§f присоединился к игре.");
        new Title("&a+ &e" + player.getName(), "§8▸ §fНужно ещё §b§l[" + (GameSettings.slots - Bukkit.getOnlinePlayers().size()) + "] §fигроков", 1, 3, 2).broadcast();

        Game.waitingBoard.send(player);
        runUpdaterActionBar();
        player.getInventory().clear();
        player.setHealth(20.0);
        player.setFoodLevel(20);
        player.getInventory().setHelmet(null);
        player.getInventory().setChestplate(null);
        player.getInventory().setLeggings(null);
        player.getInventory().setBoots(null);
        for (PotionEffect pt : player.getActivePotionEffects()) {
            player.removePotionEffect(pt.getType());
        }
        if(!(GameSettings.shop && GameSettings.teamChanger)) {
            if (GameSettings.shop) {
                player.getInventory().setItem(0, Game.getInstance().shopItem.getItem());
            }
            if (GameSettings.teamChanger) {
                player.getInventory().setItem(0, Game.getInstance().teamChanger.getItem());
            }
        } else {
            player.getInventory().setItem(0, Game.getInstance().shopItem.getItem());
            player.getInventory().setItem(1, Game.getInstance().teamChanger.getItem());
        }
        player.getInventory().setItem(8, Game.getInstance().lobbyItem.getItem());
    }

    private void runUpdaterActionBar() {
        if(GameState.current != GameState.WAITING) {
            return;
        }
        if(run != null) {
            run.cancel();
        }
        run = new BukkitRunnable() {
            @Override
            public void run() {
                ActionBarApi.sendActionBarToAllPlayers("§8▸ §fВсего игроков §b§l[" + Bukkit.getOnlinePlayers().size() +"/" + GameSettings.slots + "]", 41, false);
            }
        };
        run.runTaskTimer(API.getInstance(), 1, 20);

    }
    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        runUpdaterActionBar();
    }
}

