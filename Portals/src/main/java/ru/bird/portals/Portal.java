package ru.bird.portals;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import ru.bird.main.utils.GameUtils;

import java.util.List;

public class Portal {

    private final Plugin plugin;
    private List<Location> locations;
    private String name;
    private String command;


    public Portal(Plugin plugin, String name, List<Location> locations, String command) {
        this.plugin = plugin;
        this.name = name;
        this.locations = locations;
        this.command = command;
        plugin.getLogger().info("Register new portal by plugin " + plugin.getName() + "\nName: " + name +"\nLocations: " + locations.toString() + "\nCommand: " + command);
        startCheckPortal();
    }

    private void startCheckPortal() {
        new BukkitRunnable() {
            @Override
            public void run() {
                locations.forEach(location -> {
                    location.getWorld().getEntities().forEach(entity -> {
                        Location loc1 = entity.getLocation();
                        if(entity instanceof Player) {
                            if(loc1.distance(location) <= 1) {
                                if(!Bukkit.getServer().dispatchCommand(entity, command)) {
//                                    Bukkit.dispatchCommand(entity, "spawn");
                                    entity.setVelocity(GameUtils.PlayerThrow((Player) entity));
                                }
                            }
                        }
                    });
                });
            }
        }.runTaskTimer(plugin, 0, 1);
    }

    public List<Location> getLocations() {
        return locations;
    }

    public String getCommand() {
        return command;
    }
}
