package ru.birb.modelengineutils;

import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.entity.ArmorStand;
import org.bukkit.plugin.java.JavaPlugin;

public class da
{
    public static JavaPlugin getPlugin() {
        return ImiPet.getMain();
    }

    public static do getServerInfo() {
    return ImiPet.getServerInfo();
}

    public static ArmorStand a(final ArmorStand armorStand, final dc dc) {
        setArmorStand(armorStand);
        armorStand.setMetadata("imipet.uuid", (MetadataValue)new FixedMetadataValue((Plugin)getPlugin(), (Object)dc.getPetUUID()));
        armorStand.setMetadata("imipet:playerUUID", (MetadataValue)new FixedMetadataValue((Plugin)getPlugin(), (Object)dc.getPlayer().getUniqueId()));
        armorStand.setMetadata("imiPet", (MetadataValue)new FixedMetadataValue((Plugin)getPlugin(), (Object)true));
        armorStand.setMetadata("imipet:modelId", (MetadataValue)new FixedMetadataValue((Plugin)getPlugin(), (Object)dc.getModelId()));
        armorStand.setCustomNameVisible(false);
        return armorStand;
    }

    private static void setArmorStand(final ArmorStand armorStand) {
        if (!getServerInfo().p()) {
            armorStand.setInvulnerable(true);
            armorStand.setSilent(true);
            armorStand.setAI(false);
        }
        armorStand.setArms(true);
        armorStand.setBasePlate(false);
        armorStand.setGravity(false);
        armorStand.setVisible(false);
        armorStand.setSmall(true);
    }

    public static ArmorStand a(final ArmorStand armorStand, final String s) {
        setArmorStand(armorStand);
        armorStand.setMetadata("imipet:modelId", (MetadataValue)new FixedMetadataValue((Plugin)getPlugin(), (Object)s));
        armorStand.setCustomNameVisible(false);
        return armorStand;
    }
}
