package ru.bird.main.board.api;

import com.comphenix.protocol.wrappers.EnumWrappers;
import org.bukkit.entity.Player;
import ru.bird.main.packets.WrapperPlayServerScoreboardScore;
import ru.bird.main.board.utils.Utils;

public class BoardLine {

    private Board board;
    private int index;
    private String text;

    public BoardLine(Board board, int index, String text) {
        this.board = board;
        this.index = index;
        setText(text);
    }

    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        boolean writable = board.isWritable();

        if(!writable) remove();
        this.text = Utils.parse(text, 48);
        if(!writable) update();
    }

    public void update(Player player) {
        preparePacket(EnumWrappers.ScoreboardAction.CHANGE).sendPacket(player);
    }

    public void update() {
        preparePacket(EnumWrappers.ScoreboardAction.CHANGE).broadcastPacket();
    }

    public void remove() {
        preparePacket(EnumWrappers.ScoreboardAction.REMOVE).broadcastPacket();
    }

    public WrapperPlayServerScoreboardScore preparePacket(EnumWrappers.ScoreboardAction action) {
        WrapperPlayServerScoreboardScore packet = new WrapperPlayServerScoreboardScore();
        packet.setObjectiveName(board.getObjective().getName());
        packet.setScoreName(text);
        packet.setValue(index);
        packet.setScoreboardAction(action);
        return packet;
    }
}
