package ru.bird.main.utils;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.*;
import org.bukkit.block.BlockFace;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.Vector;
import ru.bird.main.API;
import ru.bird.main.event.listeners.StartListener;
import ru.bird.main.game.Gamer;
import ru.bird.main.groups.Groups;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class GameUtils {

    private static String version;
    private static SimpleCommandMap commandMap;
    private static String currentVersion;
    private static Object propertyManager;
    private static Random rand;

//    public static Inventory cloneInv(Inventory inv) {
//        Inventory result = Bukkit.createInventory(null, (int)inv.getSize(), inv.getName());
//        for (int i = 0; i < inv.getSize(); ++i) {
//            if (inv.getItem(i) == null) continue;
//            result.setItem(i, inv.getItem(i));
//        }
//        return result;
//    }

//    public static void removeArrows(Player player) {
//        ((CraftPlayer)player).getHandle().getDataWatcher().set(new DataWatcherObject<>(10, DataWatcherRegistry.b),0);
//    }

    public static void buildCell(Player player) {
        Location[] locations = new Location[]{player.getLocation().add(0.0, -1.0, 0.0), player.getLocation().add(0.0, 3.0, 0.0), player.getLocation(), player.getLocation().add(0.0, 1.0, 0.0), player.getLocation().add(0.0, 2.0, 0.0)};
        ItemStack item = Groups.getItemValueByPermission(player);
        Material material = item.getType();
//        byte data = item.getDurability();
        BlockFace[] surrounding = new BlockFace[]{BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};
        if (!player.getLocation().getChunk().isLoaded()) {
            player.getLocation().getChunk().load();
        }
        for (Location loc : locations) {
            for (BlockFace bf : surrounding) {
                loc.getBlock().getRelative(bf, 1).setType(material);
//                if (item.getDurability() == 0) continue;
//                loc.getBlock().getRelative(bf, 1).setData(data);
            }
        }
        player.getLocation().add(0.0, -1.0, 0.0).getBlock().setType(material);
        player.getLocation().add(0.0, 3.0, 0.0).getBlock().setType(material);
//        if (item.getDurability() != 0) {
//            player.getLocation().add(0.0, -1.0, 0.0).getBlock().setData(data);
//            player.getLocation().add(0.0, -1.0, 0.0).getBlock().setData(data);
//        }
    }

    public static void removeBlocks(Location loc) {
        for (int x = -1; x <= 1; ++x) {
            for (int y = -3; y <= 3; ++y) {
                for (int z = -1; z <= 1; ++z) {
                    loc.getBlock().getRelative(x, y, z).setType(Material.AIR);
                }
            }
        }
    }

    public static void setupGhostLib() {
        try {
            commandMap = (SimpleCommandMap)Bukkit.getServer().getClass().getDeclaredMethod("getCommandMap", new Class[0]).invoke((Object)Bukkit.getServer(), new Object[0]);
            Object obj = Bukkit.getServer().getClass().getDeclaredMethod("getServer", new Class[0]).invoke((Object)Bukkit.getServer(), new Object[0]);
            propertyManager = obj.getClass().getDeclaredMethod("getPropertyManager", new Class[0]).invoke(obj, new Object[0]);
            currentVersion = propertyManager.getClass().getPackage().getName();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void setWidthHeight(Player p, float height, float width, float length) {
        if (p == null || !p.isOnline()) {
            return;
        }
        try {
            Method handle = p.getClass().getMethod("getHandle", new Class[0]);
            Class c = Class.forName(currentVersion + ".Entity");
            Field field1 = c.getDeclaredField("height");
            Field field2 = c.getDeclaredField("width");
            Field field3 = c.getDeclaredField("length");
            field1.setFloat(handle.invoke((Object)p, new Object[0]), height);
            field2.setFloat(handle.invoke((Object)p, new Object[0]), width);
            field3.setFloat(handle.invoke((Object)p, new Object[0]), length);
        }
        catch (Exception handle) {
            // empty catch block
        }
    }

    public static void redirectToGroup(Player player, String server) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        player.sendPluginMessage(API.instance, "CloudNet", out.toByteArray());
    }

    public String getVersion() {
        return version;
    }

    public static List<Location> stringListToLocations(List<String> lines) {
        ArrayList<Location> loc = new ArrayList<Location>();
        lines.forEach(line -> {
            loc.add(stringToLocation(line));
        }
        );
        return loc;
    }

    public static Location stringToLocation(String line) {
        if (line == null) {
            return null;
        }
        String[] loc = line.split(";");
        World world = Bukkit.getWorld((String)loc[0]);
        if (world == null) {
            world = Bukkit.createWorld((WorldCreator)new WorldCreator(loc[0]).generator("EmptyGenerator"));
        }
        Location location = new Location(world, Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3]));
        if (loc.length > 4) {
            location.setPitch(Float.parseFloat(loc[4]));
            location.setYaw(Float.parseFloat(loc[5]));
        }
        return location;
    }

    public static String locationToString(Location loc, boolean pitch) {
        StringBuffer line = new StringBuffer();
        line.append(loc.getWorld().getName());
        line.append(";" + loc.getX());
        line.append(";" + loc.getY());
        line.append(";" + loc.getZ());
        if (pitch) {
            line.append(";" + loc.getPitch());
            line.append(";" + loc.getYaw());
        }
        return line.toString();
    }

    public static boolean isSpectator(Entity entity) {
        if (entity == null) {
            return false;
        }
        if (!(entity instanceof Player)) {
            return false;
        }
        Gamer gamer = Gamer.getGamer((Player)entity);
        return gamer == null || gamer.isSpectator();
    }

    public static Set<Player> getAlivePlayers() {
        HashSet<Player> set = new HashSet<Player>();
        Bukkit.getOnlinePlayers().forEach(player -> {
            if (!isSpectator(player)) {
                set.add(player);
            }
        });
        return set;
    }

    public static Player getLastAlive() {
        Iterator<Player> i$ = getAlivePlayers().iterator();
        if (i$.hasNext()) {
            Player player = i$.next();
            return player;
        }
        return null;
    }

    public static String getTimeToEnd() {
        int time = StartListener.getTime();
        int minutes = time / 60;
        int seconds = time - minutes * 60;
        String m = String.valueOf(minutes);
        String s = String.valueOf(seconds);
        StringBuffer line = new StringBuffer();
        String color = "\u00a7a";
        if (time <= 60) {
            color = "\u00a7c";
        } else if (time <= 120) {
            color = "\u00a76";
        }
        line.append(color);
        if (m.length() == 1) {
            line.append("0" + m + ":");
        } else {
            line.append(m + ":");
        }
        if (s.length() == 1) {
            line.append("0" + s);
        } else {
            line.append(s);
        }
        return line.toString();
    }

    public static String getTimeToEnd(int t) {
        int time = t;
        int minutes = time / 60;
        int seconds = time - minutes * 60;
        String m = String.valueOf(minutes);
        String s = String.valueOf(seconds);
        StringBuffer line = new StringBuffer();
        String color = "\u00a7a";
        if (time <= 120) {
            color = "\u00a7c";
        } else if (time <= 300) {
            color = "\u00a76";
        }
        line.append(color);
        if (m.length() == 1) {
            line.append("0" + m + ":");
        } else {
            line.append(m + ":");
        }
        if (s.length() == 1) {
            line.append("0" + s);
        } else {
            line.append(s);
        }
        return line.toString();
    }

    public static GameProfile getProfile(String encodeUrl) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", encodeUrl));
        return profile;
    }

    public static void setSkullProfile(GameProfile profile, ItemStack skull) {
        if (skull.getType() != Material.PLAYER_HEAD) {
            throw new IllegalArgumentException("Block must be a skull.");
        }
        SkullMeta sm = (SkullMeta)skull.getItemMeta();
        Class skullClass = getCBClass("inventory.CraftMetaSkull");
        if (!skullClass.isInstance((Object)sm)) {
            throw new IllegalArgumentException("SkullItemMeta not an instance of CraftMetaSkull!");
        }
        try {
            Field f = skullClass.getDeclaredField("profile");
            f.setAccessible(true);
            f.set((Object)sm, (Object)profile);
            skull.setItemMeta((ItemMeta)sm);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Class<?> getCBClass(String ClassName) {
        String className = "org.bukkit.craftbukkit." + version + ClassName;
        Class c = null;
        try {
            c = Class.forName(className);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return c;
    }

    public static Inventory stringToItems(Inventory inv, String line, boolean rnd, boolean shop) {
        int added = 0;
        if (rand == null) {
            rand = new Random();
        }
        String[] itemData = line.split(";");
        for (int i = 0; i < itemData.length; ++i) {
            String name;
            ItemStack itemStack;
            String[] items = itemData[i].split(" ");
            String chance = items[0];
            String val = items[2];
            String[] meta = items[1].split(":", 2);
            if (meta.length == 2) {
                name = meta[0];
                String m = meta[1];
                itemStack = new ItemStack(Material.getMaterial(name), Integer.valueOf(val), Short.parseShort(m));
            } else {
                name = meta[0];
                itemStack = new ItemStack(Material.getMaterial(name));
                itemStack.setAmount(Integer.valueOf(val));
            }
            if (shop) {
                chance = "100";
            }
            if (rand.nextInt(100) > Integer.valueOf(chance)) continue;
            if (!rnd) {
                inv.addItem(itemStack);
            }
            if (rnd) {
                inv.setItem(rand.nextInt(inv.getSize()), itemStack);
            }
            if (added++ > inv.getSize()) break;
        }
        return inv;
    }

    public static void rollbackMap(String map) {
        Bukkit.unloadWorld((World)Bukkit.getWorld((String)map), (boolean)false);
        Bukkit.getServer().createWorld(new WorldCreator(map));
    }

    static {
        String name = Bukkit.getServer().getClass().getPackage().getName();
        String mcVersion = name.substring(name.lastIndexOf(46) + 1);
        version = mcVersion + ".";
    }

    public static Vector PlayerThrow(Player player) {
        double x = Math.sin(Math.toRadians(player.getLocation().getYaw())) * Math.cos(Math.toRadians(player.getLocation().getPitch()));
        double z = (-Math.cos(Math.toRadians(player.getLocation().getYaw()))) * Math.cos(Math.toRadians(player.getLocation().getPitch()));
        double y = Math.sin(Math.toRadians(player.getLocation().getPitch()));
        return new Vector(x, y, z);
    }

    public static String colorSet(String original){
        return original == null ? null : ChatColor.translateAlternateColorCodes('&', original);

    }

    public static ItemStack getHead(String url) {
        ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (short) SkullType.PLAYER.ordinal());
        GameUtils.setSkullProfile(GameUtils.getProfile(url), head);
        return head;
    }

}