package ru.bird.pets.entity.armorstand.mobs;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import ru.bird.main.particles.ParticleEffect;
import ru.bird.pets.entity.AbstractArmoredPet;
import ru.bird.main.utils.MathUtils;
import net.minecraft.server.v1_16_R2.DataWatcher;

public class PetGhost extends AbstractArmoredPet {

	public PetGhost(UUID id, int uniqueID) {
		super(id, uniqueID);
	}

	@Override
	protected DataWatcher build() {
		ArmorStand armor = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), getEntityType());
		armor.setSmall(true);
		armor.setBasePlate(false);
		armor.setArms(true);
		armor.setVisible(false);
		DataWatcher data = ((CraftEntity) armor).getHandle().getDataWatcher();
		armor.remove();
		return data;
	}



	@Override
	protected float getOffsetY() {
		return 1.2F;
	}

	@Override
	public ItemStack getMainHand() {
		return null;
	}

	@Override
	public ItemStack getOffHand() {
		return null;
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		return null;
	}

	@Override
	public ItemStack getChestplate() {
		return null;
	}

	@Override
	public ItemStack getHelmet() {
		ItemStack Head = new ItemStack(Material.PLAYER_HEAD, 1, (short) SkullType.PLAYER.ordinal());
		SkullMeta meta = (SkullMeta) Head.getItemMeta();
		meta.setOwner("Ghost");
		Head.setItemMeta(meta);
		return Head;
	}

	@Override
	public void onLivingUpdate() {
		this.setHeadPose(5 + 5 * MathUtils.sin(), 0, 0);
		this.sendWatcherUpdate();
		ParticleEffect.REDSTONE.sendColor(Bukkit.getOnlinePlayers(), this.getPetLocation().clone().add(MathUtils.random(-0.3F, 0.3F), .7F + MathUtils.random(0.6F), MathUtils.random(-0.3F, 0.3F)), Color.WHITE);
	}

}
