package ru.bird.pets.entity;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import ru.bird.main.API;
import ru.bird.main.packets.WrapperPlayServerEntityTeleport;
import java.util.UUID;

public abstract class AbstractStandardPet extends AbstractPet {

    public AbstractStandardPet(UUID ownerID, int uniqueID) {
        super(ownerID, uniqueID);
    }

    @Override
    public void move(Player player) {
        Location locTo = player.getLocation();
        double localAngle = locTo.getYaw() + getAngle();
        double newX = locTo.getX() - 1D * Math.sin(Math.toRadians(localAngle));
        double newZ = locTo.getZ() + 1D * Math.cos(Math.toRadians(localAngle));
        WrapperPlayServerEntityTeleport position = new WrapperPlayServerEntityTeleport();
        position.setEntityID(getEntityID());
        position.setX(newX);
        position.setY(locTo.getBlockY());
        position.setZ(newZ);
        position.setYaw(locTo.getYaw());
        position.setPitch(locTo.getPitch());
        position.broadcastPacket();
    }

    @Override
    public void onLivingUpdate() {
        sendWatcherUpdate();
    }
}
