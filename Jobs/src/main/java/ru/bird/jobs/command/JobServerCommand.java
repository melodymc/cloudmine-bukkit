package ru.bird.jobs.command;

import ru.bird.chat.AbstractCommand;
import ru.bird.inventories.menu.Menu;
import ru.bird.jobs.StorageJob;
import ru.bird.jobs.JobsPlugin;
import ru.bird.jobs.JobsUser;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import ru.bird.jobs.jobgame.enchanter.JobGameEnchanter;
import ru.bird.jobs.jobgame.miner.JobGameMiner;
import ru.bird.jobs.menu.MenuManager;

import java.util.Arrays;

public class JobServerCommand extends AbstractCommand {
    public JobServerCommand() {
        super("js");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if(args.length == 0){
            help(sender, alias);
            return false;
        }
        World world;
        String subcommand = "";
        if(args[0].equals("for") || (sender instanceof ConsoleCommandSender && args[0].equals("for"))) {
            if (args.length < 3)
                return false;
            world = JobsPlugin.get().getServer().getWorld(args[1]);
            if(world == null) {
                sender.sendMessage("Такого мира не существует");
                return false;
            }
            subcommand = args[2];
            args = args.length == 3 ? new String[0] : Arrays.copyOfRange(args, 3, args.length);
        } else {
            if(sender instanceof Player) {
                Player player = (Player) sender;
                world = player.getWorld();
                subcommand = args[0];
                args = args.length == 1 ? new String[0] : Arrays.copyOfRange(args, 1, args.length);
            } else {
                sender.sendMessage("Не указан мир /" + alias + " for [world] ...");
                return true;
            }
        }
        String permission = "js.ru.bird.jobs.command." + subcommand;
        if(sender.hasPermission(permission)) {
            switch (subcommand) {
                case ("start"):
                    return start(sender);
                case ("getnum"):
                    return getnum(sender);
                case ("menu"):
                    return menu(sender);
                default:
                    return help(sender, subcommand, alias);
            }
        } else {
            sender.sendMessage("Нет права " + permission);
        }
        return false;
    }

    private boolean getnum(CommandSender sender) {
        Player player = (Player) sender;
        JobsUser u = StorageJob.get(player.getUniqueId());
        player.sendMessage("Общий счётчик плота " + u.getSumm() + " (" + u.getTime() + ") за эту сессию: " + u.getNum());
        player.sendMessage("Общий счётчик работы " + u.getJobTimeSumm() + " (" + u.getJobTime() + ") за эту сессию: " + u.getJobTimeNum());

        return true;
    }

    private boolean help(CommandSender sender, String subcommand, String alias) {
        sender.sendMessage("Команды " + subcommand + " нет в списка сабкоманд.");
        help(sender, alias);
        return true;
    }

    private void help(CommandSender sender, String alias) {
        sender.sendMessage("/"+ alias + " start меню выбора профессии");
        sender.sendMessage("/"+ alias + " menu меню взаимодействия с плотом");
        sender.sendMessage("/"+ alias + " getnum сколько наиграно");
        sender.sendMessage("/"+ alias + " tp телепорт");
    }

    private boolean start(CommandSender sender) {
        Player player = (Player)sender;
        Menu menu = MenuManager.getStartMenu();
        menu.open(player);
        return true;
    }

    private boolean menu(CommandSender sender) {
        Player player = (Player)sender;

//        JobGameMiner jobgame = new JobGameMiner(player.getUniqueId());
//        MenuManager.minerPool.put(player.getUniqueId(), jobgame);

        JobGameEnchanter jobgame = new JobGameEnchanter(player.getUniqueId());
        MenuManager.enchanterPool.put(player.getUniqueId(), jobgame);

        jobgame.gui.open(player);

        return true;
    }
}